/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Location } from '@angular/common';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { RandomElectionEventId } from '@swiss-post/shared/testing';
import { AuthenticationGuard, ElectionEventIdGuard } from '@swiss-post/shared/ui';
import { MockProvider } from 'ng-mocks';
import { of } from 'rxjs';

import { routes } from './app-routing.module';

describe('AppRoutingModule', () => {
  let router: Router;
  let location: Location;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes),
      ],
      providers: [
        MockProvider(AuthenticationGuard, { canLoad: () => of(true) }),
        MockProvider(ElectionEventIdGuard, { canActivate: () => true }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    router = TestBed.inject(Router);
    location = TestBed.inject(Location);
    router.initialNavigation();
  });

  it('should redirect to the page not found if no route is set', () => {
    expect(location.path()).toBe('/page-not-found');
  });

  it('should redirect to the page not found when an invalid route is set', fakeAsync(() => {
    router.navigate(['/invalid-path']);

    tick();

    expect(location.path()).toBe('/page-not-found');
  }));

  it('should redirect to the page not found when trying to reach legal terms without an election event id', fakeAsync(() => {
    router.navigate(['/legal-terms']);

    tick();

    expect(location.path()).toBe('/page-not-found');
  }));

  it('should correctly navigate to the legal terms when an election event id is provided', fakeAsync(() => {
    const electionEventId = RandomElectionEventId();
    router.navigate(['/legal-terms', electionEventId]);

    tick();

    expect(location.path()).toBe(`/legal-terms/${electionEventId}`);
  }));
});
