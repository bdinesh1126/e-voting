/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'swp-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss'],
})
export class PageNotFoundComponent {
}
