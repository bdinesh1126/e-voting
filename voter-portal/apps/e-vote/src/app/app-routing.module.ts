/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard, DeactivationGuard, ElectionEventIdGuard } from '@swiss-post/shared/ui';
import { BackAction, RouteData } from '@swiss-post/types';
import { ChooseComponent } from '../../../../libs/choose/src/lib/choose/choose.component';
import { ConfirmComponent } from '../../../../libs/confirm/src/lib/confirm/confirm.component';
import { LegalTermsComponent } from '../../../../libs/legal-terms/src/lib/legal-terms/legal-terms.component';
import { ReviewComponent } from '../../../../libs/review-send/src/lib/review/review.component';
import { StartVotingComponent } from '../../../../libs/start-voting/src/lib/start-voting/start-voting.component';
import { VerifyComponent } from '../../../../libs/verify/src/lib/verify/verify.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

export const routes: Routes = [
  {
    path: 'legal-terms/:electionEventId',
    data: {
      stepKey: 'stepper.termsAndConditions',
      allowedBackPaths: [ 'start-voting' ],
    } as RouteData,
    canActivate: [ ElectionEventIdGuard ],
    canDeactivate: [ DeactivationGuard ],
    component: LegalTermsComponent,
  },
  {
    path: 'start-voting',
    data: {
      stepKey: 'stepper.startVoting',
      allowedBackPaths: [ 'legal-terms' ],
      backAction: BackAction.GoToLegalTermsPage,
    } as RouteData,
    canDeactivate: [ DeactivationGuard ],
    component: StartVotingComponent,
  },
  {
    path: 'choose',
    data: {
      stepKey: 'stepper.selectAnswers',
      allowedBackPaths: [ 'review' ],
      backAction: BackAction.ShowCancelVoteDialog,
    } as RouteData,
    canLoad: [ AuthenticationGuard ],
    canDeactivate: [ DeactivationGuard ],
    component: ChooseComponent,
  },
  {
    path: 'review',
    data: {
      stepKey: 'stepper.reviewAndSeal',
      allowedBackPaths: [ 'choose' ],
    } as RouteData,
    canLoad: [ AuthenticationGuard ],
    canDeactivate: [ DeactivationGuard ],
    component: ReviewComponent,
  },
  {
    path: 'verify',
    data: {
      stepKey: 'stepper.verifyAndCast',
      allowedBackPaths: [],
      backAction: BackAction.ShowLeaveProcessDialog,
    } as RouteData,
    canLoad: [ AuthenticationGuard ],
    canDeactivate: [ DeactivationGuard ],
    component: VerifyComponent,
  },
  {
    path: 'confirm',
    data: {
      stepKey: 'stepper.voteCast',
      allowedBackPaths: [],
      backAction: BackAction.GoToStartVotingPage,
    } as RouteData,
    canLoad: [ AuthenticationGuard ],
    canDeactivate: [ DeactivationGuard ],
    component: ConfirmComponent,
  },
  {
    path: 'page-not-found',
    component: PageNotFoundComponent
  },
  {path: '**', redirectTo: 'page-not-found'},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      useHash: true,
      anchorScrolling: 'enabled',
      onSameUrlNavigation: 'reload'
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
