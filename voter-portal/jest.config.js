/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
const {getJestProjects} = require('@nrwl/jest');

module.exports = {
  projects: getJestProjects(),
};
