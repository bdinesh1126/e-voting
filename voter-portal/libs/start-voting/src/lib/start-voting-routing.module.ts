/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {StartVotingComponent} from './start-voting/start-voting.component';

const routes: Routes = [
  {
    path: '',
    component: StartVotingComponent,
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StartVotingRoutingModule {
}
