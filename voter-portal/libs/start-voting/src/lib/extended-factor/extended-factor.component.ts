/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { AfterViewInit, Component, ElementRef, Input, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ConfigurationService } from '@swiss-post/shared/configuration';
import { SwpDateAdapter } from '@swiss-post/shared/ui';
import { ErrorStatus, ExtendedFactor } from '@swiss-post/types';
import { MaskPipe } from 'ngx-mask';

@Component({
  selector: 'swp-extended-factor',
  templateUrl: './extended-factor.component.html',
  styleUrls: [ './extended-factor.component.scss' ],
  providers: [ SwpDateAdapter, MaskPipe ],
})
export class ExtendedFactorComponent implements AfterViewInit {
  @ViewChild('dateOfBirthInput') dateOfBirthInput: ElementRef<HTMLInputElement> | undefined;
  @Input() voterForm!: FormGroup;
  @Input() formSubmitted = false;
  datepickerMinDate: NgbDateStruct;
  datepickerMaxDate: NgbDateStruct;
  datepickerDefaultDate: NgbDateStruct;
  dateOfBirthMaskExpression = '';

  readonly ExtendedFactor = ExtendedFactor;
  readonly ErrorMessage = ErrorStatus;

  get extendedFactor(): FormControl {
    return this.voterForm.get('extendedFactor') as FormControl;
  }

  get datePickerSelectedDate(): NgbDateStruct | null {
    return this.dateAdapter.fromModel(this.extendedFactor.value);
  }

  set datePickerSelectedDate(date: NgbDateStruct | null) {
    this.extendedFactor.setValue(this.dateAdapter.toModel(date));
  }

  constructor(
    public readonly configuration: ConfigurationService,
    private readonly dateAdapter: SwpDateAdapter,
    private readonly calendar: NgbCalendar,
  ) {
    const today = this.calendar.getToday();
    this.datepickerMinDate = this.calendar.getPrev(today, 'y', 120);
    this.datepickerMaxDate = this.calendar.getPrev(today, 'y', 16);
    this.datepickerDefaultDate = this.calendar.getPrev(today, 'y', 40);
  }

  public ngAfterViewInit(): void {
    if (this.configuration.identification === ExtendedFactor.YearOfBirth) {
      this.extendedFactor.addValidators(Validators.pattern(/^\d{4}$/));
    }

    if (this.configuration.identification === ExtendedFactor.DateOfBirth) {
      this.extendedFactor.addValidators(Validators.pattern(/^\d{8}$/));
    }
  }
}
