# start-voting

This library was generated with [Nx](https://nx.dev).

`StartVoting` `FeatureModule`: state is managed with `ngrx` by `SharedStateModule` which imports `AuthModule`
that `provides` `authService`;

`StartVotingComponent` inputs `startVotingKey` & `dateOfBirth`,

also need to provide `config` object behind scenes (details in libs/auth/README.md) to initiate Online Voting
API (`ovApi`)/ `authService`

that will authenticate user & place ballot in `ngrx` `store`

## Running unit tests

Run `nx test start-voting` to execute the unit tests.
