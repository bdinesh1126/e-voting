/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {ContestAndContestUserData} from '@swiss-post/types';

@Component({
  selector: 'swp-review-candidate-list',
  templateUrl: './review-candidate-list.component.html',
  styleUrls: ['./review-candidate-list.component.scss'],
})
export class ReviewCandidateListComponent {
  @Input() contestAndValues: ContestAndContestUserData | undefined;

  get showList() {
    return this.contestAndValues?.contest?.listQuestion && this.contestAndValues.contest.listQuestion.maxChoices > 0;
  }
}
