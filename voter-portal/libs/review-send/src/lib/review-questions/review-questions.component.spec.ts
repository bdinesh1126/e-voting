/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {DebugElement} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {
  MockContest,
  MockContestUserData,
  MockQuestions,
  QuestionsTestingComponent,
  RandomItem
} from '@swiss-post/shared/testing';
import {TranslateTestingModule} from 'ngx-translate-testing';

import {ReviewQuestionsComponent} from './review-questions.component';

describe('QuestionListReviewComponent', () => {
  let component: ReviewQuestionsComponent;
  let fixture: ComponentFixture<ReviewQuestionsComponent>;
  let questionsComponent: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReviewQuestionsComponent, QuestionsTestingComponent],
      imports: [TranslateTestingModule.withTranslations({})],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewQuestionsComponent);
    component = fixture.componentInstance;

    const questions = MockQuestions(['Reviewed Question A', 'Reviewed Question B']);

    component.contestAndValues = {
      contest: MockContest(questions),
      contestUserData: MockContestUserData(questions),
    }

    fixture.detectChanges();

    questionsComponent = fixture.debugElement.query(By.css('swp-questions'));
  });

  function getRandomQuestion() {
    return RandomItem(component.contestAndValues?.contest.questions);
  }

  function getContextWithSelectedAnswer() {
    const question = getRandomQuestion();
    return {question, answer: RandomItem(question.options).text};
  }

  function getContextWithBlankAnswer() {
    const question = getRandomQuestion();
    return {question, answer: question.blankOption.text};
  }

  it('should pass the provided contest to the questions component', () => {
    expect(questionsComponent.componentInstance.contest).toBe(component.contestAndValues?.contest);
  });

  it('should pass the provided contest user data to the questions component', () => {
    expect(questionsComponent.componentInstance.contestUserData).toBe(component.contestAndValues?.contestUserData);
  });

  it('should show answers within the questions component', () => {
    const testContext = questionsComponent.componentInstance.templateTestingContext = getContextWithSelectedAnswer();

    fixture.detectChanges();

    expect(questionsComponent.nativeElement.textContent).toContain(testContext.answer);
  });

  it('should have special information for screen readers when no option has been selected', () => {
    questionsComponent.componentInstance.templateTestingContext = getContextWithBlankAnswer();

    fixture.detectChanges();

    const screenReaderInformation = questionsComponent.query(By.css('.visually-hidden'));
    expect(screenReaderInformation).toBeTruthy();
  });

  it('should not have any special information for screen readers when an option has been selected', () => {
    questionsComponent.componentInstance.templateTestingContext = getContextWithSelectedAnswer();

    fixture.detectChanges();

    const screenReaderInformation = questionsComponent.query(By.css('.visually-hidden'));
    expect(screenReaderInformation).toBeFalsy();
  });
});
