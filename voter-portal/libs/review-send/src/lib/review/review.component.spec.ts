/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Location } from '@angular/common';
import { Component, DebugElement } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import '@angular/localize/init';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { IconComponent } from '@swiss-post/shared/icons';
import { ReviewActions, SHARED_FEATURE_KEY } from '@swiss-post/shared/state';
import { MockContest, MockContestUserData, MockQuestions, RandomArray } from '@swiss-post/shared/testing';
import {
  BackendErrorComponent,
  ConfirmationService,
  TranslationListDirective,
  ProcessCancellationService,
} from '@swiss-post/shared/ui';
import { Ballot, Contest, ContestUserData } from '@swiss-post/types';
import { MockComponent, MockDirective, MockProvider } from 'ng-mocks';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { of } from 'rxjs';
import { ReviewContestContainerComponent } from '../review-contest-container/review-contest-container.component';
import { ReviewComponent } from './review.component';

@Component({
  selector: 'swp-review',
  template: '',
})
class ChooseComponent {
}

class MockState {
  ballot: Ballot;
  ballotUserData: { contests: ContestUserData[] } | undefined;
  loading = false;

  constructor() {
    this.ballot = {
      contests: [],
      id: '1',
      correctnessIds: {},
    };
  }
}

describe('ReviewComponent', () => {
  let component: ReviewComponent;
  let fixture: ComponentFixture<ReviewComponent>;
  const initialState: MockState = Object.freeze(new MockState());
  let store: MockStore;
  let confirmationService: ConfirmationService;
  let processCancellationService: ProcessCancellationService;
  let location: Location;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ReviewComponent,
        MockComponent(ReviewContestContainerComponent),
        MockComponent(IconComponent),
        MockComponent(BackendErrorComponent),
        MockDirective(TranslationListDirective)
      ],
      imports: [
        RouterTestingModule.withRoutes([
          {path: 'choose', component: ChooseComponent},
        ]),
        TranslateTestingModule.withTranslations({}),
      ],
      providers: [
        provideMockStore({
          initialState: { [SHARED_FEATURE_KEY]: initialState },
        }),
        MockProvider(ConfirmationService, { confirm: () => of(true) }),
        MockProvider(ProcessCancellationService),
        MockProvider(NgbModal, { open: () => ({ shown: of(null) } as unknown as NgbModalRef) }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    store = TestBed.inject(MockStore);
    confirmationService = TestBed.inject(ConfirmationService);
    processCancellationService = TestBed.inject(ProcessCancellationService);
    location = TestBed.inject(Location);
    fixture = TestBed.createComponent(ReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('propagate data to child components', () => {
    let contestsUserData: ContestUserData[];
    let contestContainerComponents: DebugElement[];
    let contests: Contest[];
    let newState: MockState;

    function setContests(): void {
      contests = RandomArray(
        (contestIndex) => {
          const questionTexts = RandomArray(
            (questionIndex) => {
              return `Contest ${contestIndex} - Question ${questionIndex}`;
            },
            3,
            5
          );

          return MockContest(MockQuestions(questionTexts));
        },
        5,
        1
      );

      contestsUserData = contests.map((contest) =>
        MockContestUserData(contest.questions ?? [])
      );

      newState = {
        ...initialState,
        ballotUserData: { contests: contestsUserData },
      };
      newState.ballot.contests = contests;
      store.setState({ [SHARED_FEATURE_KEY]: newState });
      fixture.detectChanges();
      contestContainerComponents = fixture.debugElement.queryAll(
        By.css('swp-review-contest-container')
      );
    }

    it('should display as many contests as there are in the store', () => {
      setContests();
      expect(contestContainerComponents.length).toBe(contests.length);
    });

    it('should pass the proper contest to the contest container components', () => {
      setContests();
      contests.forEach((contest, i) => {
        const {
          contest: receivedContest,
          contestUserData: receivedContestUserData,
        } = contestContainerComponents[i].componentInstance.contestAndValues;

        expect(receivedContest).toBe(contest);
        expect(receivedContestUserData).toBe(contestsUserData[i]);
      });
    });
  });

  describe('sealing vote', () => {
    it('check call to dispatch store action', () => {
      jest.spyOn(store, 'dispatch');
      component.confirmSeal();
      expect(store.dispatch).toBeCalledWith(ReviewActions.sealVoteClicked());
    });

    it('should call confirmSeal on seal button click', () => {
      jest.spyOn(confirmationService, 'confirm');
      const buttonElement = fixture.debugElement.query(
        By.css('#btn_seal_vote')
      ).nativeElement;
      buttonElement.click();
      fixture.detectChanges();

      expect(confirmationService.confirm).toBeCalled();
    });
  });

  describe('buttons', () => {
    it('should show the sealing confirmation modal when the "Seal" button is clicked', () => {
      jest.spyOn(confirmationService, 'confirm');
      const buttonElement = fixture.debugElement.query(
        By.css('#btn_seal_vote')
      ).nativeElement;
      buttonElement.click();
      fixture.detectChanges();

      expect(confirmationService.confirm).toBeCalled();
    });

    it('should call confirmCancel on cancel-button click', () => {
      jest.spyOn(processCancellationService, 'cancelVote');
      const buttonElement = fixture.debugElement.query(
        By.css('#btn_cancel')
      ).nativeElement;
      buttonElement.click();
      fixture.detectChanges();

      expect(processCancellationService.cancelVote).toBeCalled();
    });

    it('should redirect to "/choose" when the "Back" button is clicked', fakeAsync(() => {
      const buttonElement = fixture.debugElement.query(
        By.css('#btn_back_to_choose')
      ).nativeElement;
      buttonElement.click();

      tick();

      expect(location.path()).toBe('/choose');
    }));
  });
});
