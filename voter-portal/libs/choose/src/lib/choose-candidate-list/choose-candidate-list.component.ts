/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormGroup} from '@angular/forms';
import {FAQService} from '@swiss-post/shared/ui';
import {ElectionContest, FAQSection, CandidateUserData, Contest} from '@swiss-post/types';
import {Subscription} from 'rxjs';
import { startWith } from 'rxjs/operators';

@Component({
  selector: 'swp-choose-candidates',
  templateUrl: './choose-candidate-list.component.html',
  styleUrls: ['./choose-candidate-list.component.scss'],
})
export class ChooseCandidateListComponent implements OnInit, OnDestroy {
  @Input() contestFormGroup!: FormGroup;
  @Input() showErrors = false;
  electionContest!: ElectionContest;
  cumulByCandidate!: Map<string, number>;
  unoccupiedSeat: FormGroup | undefined;
  private subscription!: Subscription;

  @Input() set contest(value: Contest | undefined) {
    if (value) {
      this.electionContest = new ElectionContest(value);
    }
  }

  get candidates(): FormArray {
    return this.contestFormGroup?.get('candidates') as FormArray;
  }

  get cumulationAllowed(): boolean {
    return !!this.electionContest.candidatesQuestion && this.electionContest.candidatesQuestion.cumul > 1;
  }

  constructor(
    private readonly fAQService: FAQService
  ) {}

  ngOnInit() {
    this.subscription = this.candidates.valueChanges
      .pipe(startWith(this.candidates.value as CandidateUserData[]))
      .subscribe((candidatesUserData: CandidateUserData[]) => {
        this.cumulByCandidate = this.getCumulByCandidate(candidatesUserData);
        this.unoccupiedSeat = this.getUnoccupiedSeat();
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  showWriteInsFAQ() {
    this.fAQService.showFAQ(FAQSection.HowToUseWriteIns);
  }

  private getUnoccupiedSeat(): FormGroup | undefined {
    return this.candidates.controls.find((control) => {
      return !control.value.candidateId
    }) as FormGroup;
  }

  private getCumulByCandidate(candidatesUserData: CandidateUserData[]) {
    return candidatesUserData.reduce((cumulByCandidate: Map<string, number>, {candidateId}: CandidateUserData) => {
      if (candidateId) {
        const alreadyCountedCumul = cumulByCandidate.get(candidateId) ?? 0;
        cumulByCandidate.set(candidateId, alreadyCountedCumul + 1);
      }
      return cumulByCandidate;
    }, new Map());
  }
}
