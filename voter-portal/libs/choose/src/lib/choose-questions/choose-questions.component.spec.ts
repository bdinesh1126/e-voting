/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {DebugElement} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormArray, FormControl, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {By} from '@angular/platform-browser';
import {IconComponent} from '@swiss-post/shared/icons';
import {
  MockContest,
  MockQuestions,
  QuestionsTestingComponent,
  RandomInt,
  RandomItem
} from '@swiss-post/shared/testing';
import {Option, Question} from '@swiss-post/types';
import {MockComponent} from 'ng-mocks';
import {TranslateTestingModule} from 'ngx-translate-testing';

import {ChooseQuestionsComponent} from './choose-questions.component';

describe('QuestionListComponent', () => {
  let component: ChooseQuestionsComponent;
  let fixture: ComponentFixture<ChooseQuestionsComponent>;
  let questionsComponent: DebugElement;
  let questions: Question[];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ChooseQuestionsComponent,
        QuestionsTestingComponent,
        MockComponent(IconComponent)
      ],
      imports: [
        ReactiveFormsModule,
        TranslateTestingModule.withTranslations({})
      ],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseQuestionsComponent);
    component = fixture.componentInstance;

    questions = MockQuestions(['Choose Question A', 'Choose Question B']);
    component.contest = MockContest(questions);
    component.contestFormGroup = getContestFormGroup();

    fixture.detectChanges();

    questionsComponent = fixture.debugElement.query(By.css('swp-questions'));
  });

  function getContestFormGroup(hasChosenOption = false): FormGroup {
    return new FormGroup({
      questions: new FormArray(questions.map(question => {
        const chosenOption = hasChosenOption ? RandomItem(question.options).id : null;
        return new FormGroup({
          id: new FormControl(question.id),
          chosenOption: new FormControl(chosenOption),
        });
      }))
    });
  }

  function getOptionInput(option: Option): DebugElement {
    return questionsComponent.query(By.css(`#option-${option.id}`));
  }

  function getClearButton(question: Question): HTMLButtonElement | undefined {
    return questionsComponent.query(By.css(`#btn_clear_${question.id}`))?.nativeElement;
  }

  it('should pass the provided contest to the questions component', () => {
    expect(questionsComponent.componentInstance.contest).toBe(component.contest);
  });

  describe('question with no option chosen', () => {
    let questionWithoutAnswer: Question;

    beforeEach(() => {
      const randomQuestionIndex = RandomInt(questions.length);
      questionWithoutAnswer = questions[randomQuestionIndex];
      questionsComponent.componentInstance.templateTestingContext = {
        questionIndex: randomQuestionIndex,
        question: questionWithoutAnswer,
      };

      component.contestFormGroup = getContestFormGroup(false);

      fixture.detectChanges();
    });

    it('should show all options as unchecked', () => {
      questionWithoutAnswer.options.forEach(option => {
        const optionInput = getOptionInput(option).nativeElement as HTMLInputElement;
        expect(optionInput.checked).toBe(false);
      });
    });

    it('should not show a clear button if the question has a minimum number of choices', () => {
      questionWithoutAnswer.optionsMinChoices = 1;

      fixture.detectChanges();

      const clearButton = getClearButton(questionWithoutAnswer);
      expect(clearButton).toBeFalsy();
    });

    it('should show a disabled clear button if the question has no minimum number of choices', () => {
      questionWithoutAnswer.optionsMinChoices = 0;

      fixture.detectChanges();

      const clearButton = getClearButton(questionWithoutAnswer);
      expect(clearButton).toBeTruthy();
      expect(clearButton?.disabled).toBeTruthy();
    });

    it('should enable the clear button after an option has been checked', () => {
      const randomOption = RandomItem(questionWithoutAnswer.options);
      const optionInput = getOptionInput(randomOption).nativeElement as HTMLInputElement;
      questionWithoutAnswer.optionsMinChoices = 0;

      optionInput.click();
      fixture.detectChanges();

      const clearButton = getClearButton(questionWithoutAnswer);
      expect(clearButton?.disabled).toBeFalsy();
    });
  });


  describe('question with option already chosen', () => {
    let questionWithAnswer: Question;
    let answer: string;

    beforeEach(() => {
      const randomQuestionIndex = RandomInt(questions.length);
      questionWithAnswer = questions[randomQuestionIndex];
      questionsComponent.componentInstance.templateTestingContext = {
        questionIndex: randomQuestionIndex,
        question: questionWithAnswer,
      };

      const contextFormGroup = getContestFormGroup(true);
      component.contestFormGroup = contextFormGroup;
      answer = contextFormGroup.value.questions[randomQuestionIndex].chosenOption;

      fixture.detectChanges();
    });

    it('should show the selected option as checked and the others as unchecked', () => {
      questionWithAnswer.options.forEach(option => {
        const optionInput = getOptionInput(option).nativeElement as HTMLInputElement;
        expect(optionInput.checked).toBe(option.id === answer);
      });
    });

    it('it should show an enabled clear button if the question has no minimum number of choices', () => {
      questionWithAnswer.optionsMinChoices = 0;

      fixture.detectChanges();

      const clearButton = getClearButton(questionWithAnswer);
      expect(clearButton).toBeTruthy();
      expect(clearButton?.disabled).toBeFalsy();
    });

    it('should not show a clear button if the question has a minimum number of choices', () => {
      questionWithAnswer.optionsMinChoices = 1;

      fixture.detectChanges();

      const clearButton = getClearButton(questionWithAnswer);
      expect(clearButton).toBeFalsy();
    });
  });
});
