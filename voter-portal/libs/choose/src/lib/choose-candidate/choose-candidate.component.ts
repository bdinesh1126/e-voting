/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Component, ElementRef, Input, ViewChild} from '@angular/core';
import {FormArray, FormGroup} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CandidateSelectionModalComponent} from '@swiss-post/candidate';
import {ElectionContest, Candidate} from '@swiss-post/types';

@Component({
  selector: 'swp-choose-candidate',
  templateUrl: './choose-candidate.component.html',
  styleUrls: ['./choose-candidate.component.scss'],
})
export class ChooseCandidateComponent {
  @ViewChild('mainButton') mainButton: ElementRef<HTMLButtonElement> | undefined;

  @Input() candidate: Candidate | undefined;
  @Input() candidateIndex!: number;
  @Input() electionContest!: ElectionContest;
  @Input() contestFormGroup!: FormGroup;
  @Input() candidates!: FormArray
  @Input() unoccupiedSeat: FormGroup | undefined;
  @Input() cumulByCandidate!: Map<string, number>;

  showSelectionMessage = false;
  showCumulationMessage = false;
  showDeletionMessage = false;

  constructor(
    private readonly modalService: NgbModal
  ) {
  }

  get candidateFormGroup(): FormGroup | undefined {
    return this.candidates?.controls[this.candidateIndex] as FormGroup;
  }

  get candidateMaxCumul(): number {
    return this.candidate?.allRepresentations?.length ?? 0;
  }

  get candidateCanCumulate(): boolean {
    if (!this.candidate) {
      return false;
    }

    const candidateCumul = this.cumulByCandidate.get(this.candidate.id) ?? 0;
    const candidateMaxCumul = this.candidate.allRepresentations?.length ?? 0;
    return candidateCumul < candidateMaxCumul;
  }

  openCandidateSelectionModal(): void {
    const modalOptions = {fullscreen: 'xl', size: 'xl'};
    const modalRef = this.modalService.open(CandidateSelectionModalComponent, modalOptions);

    Object.assign(modalRef.componentInstance, {
      contest: this.electionContest,
      contestUserData: this.contestFormGroup.value,
      candidateIndex: this.candidateIndex
    });

    modalRef.result
      .then((selectedCandidate: Candidate) => {
        this.showSelectionMessage = true;
        this.candidateFormGroup?.setValue({
          candidateId: selectedCandidate.id,
          writeIn: null,
        });
      })
      .catch(() => null);
  }

  cumulateCandidate() {
    if (this.unoccupiedSeat) {
      this.unoccupiedSeat.get('candidateId')?.setValue(this.candidate?.id);
      this.showCumulationMessage = true;
    }
  }

  clearCandidate(): void {
    this.candidateFormGroup?.setValue({
      candidateId: null,
      writeIn: null,
    });

    this.showDeletionMessage = true;
    setTimeout(() => this.mainButton?.nativeElement.focus());
  }

  removeMessages() {
    this.showSelectionMessage = false;
    this.showCumulationMessage = false;
    this.showDeletionMessage = false;
  }
}
