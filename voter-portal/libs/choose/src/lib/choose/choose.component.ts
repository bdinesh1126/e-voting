/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { ChooseActions, getContests, getContestsAndContestsUserData } from '@swiss-post/shared/state';
import { ProcessCancellationService } from '@swiss-post/shared/ui';
import { CandidatesQuestion, Contest, Question, TemplateType } from '@swiss-post/types';
import { take } from 'rxjs/operators';

@Component({
  selector: 'swp-choose',
  templateUrl: './choose.component.html',
  styleUrls: [ './choose.component.scss' ],
})
export class ChooseComponent implements OnInit {
  ballotFormGroup!: FormGroup;
  contests$ = this.store.select(getContests);

  constructor(
    private readonly store: Store,
    private readonly cancelProcessService: ProcessCancellationService,
    private readonly fb: FormBuilder,
  ) {
  }

  ngOnInit() {
    this.store
      .select(getContestsAndContestsUserData)
      .pipe(take(1))
      .subscribe(({contests, contestsUserData}) => {
        this.ballotFormGroup = this.fb.group({
          contests: this.getContestsFormArray(contests),
        });

        if (contestsUserData) {
          this.ballotFormGroup.patchValue({contests: contestsUserData});
        }
      });
  }

  confirmCancel() {
    this.cancelProcessService.cancelVote();
  }

  review() {
    if (this.ballotFormGroup.invalid) {
      const firstInvalid = document.querySelector<HTMLInputElement>('input.ng-invalid');
      if (firstInvalid) {
        firstInvalid.focus();
      }
      return;
    }

    this.store.dispatch(ChooseActions.reviewClicked({ ballotUserData: this.ballotFormGroup.value }));
  }

  getContestFormGroup(index: number): FormGroup {
    const contestFromArray = this.ballotFormGroup.get('contests') as FormArray;
    return contestFromArray.controls[index] as FormGroup;
  }

  private getContestsFormArray(contests: Contest[] | undefined): FormArray {
    const contestsFormGroups = contests?.map((contest) => {
      if (contest.template === TemplateType.Options) {
        return this.fb.group({
          questions: this.getOptionsFormArray(contest.questions),
        });
      }

      const listAndCandidatesFormGroup = this.fb.group({
        candidates: this.getCandidatesFormArray(contest.candidatesQuestion),
      });

      if (contest.listQuestion && contest.listQuestion.maxChoices > 0) {
        listAndCandidatesFormGroup.setControl('listId', this.fb.control(null));
      }

      return listAndCandidatesFormGroup;
    });

    return this.fb.array(contestsFormGroups || []);
  }

  private getCandidatesFormArray(
    candidateQuestions: CandidatesQuestion | undefined
  ): FormArray {
    const candidateFormGroups = Array.from(
      {length: candidateQuestions?.maxChoices || 0},
      () => {
        return this.fb.group({
          candidateId: this.fb.control(null),
          writeIn: this.fb.control(null),
        });
      }
    );

    return this.fb.array(candidateFormGroups);
  }

  private getOptionsFormArray(questions: Question[] | undefined): FormArray {
    const questionFormGroups = questions?.map((question) => {
      return this.fb.group({
        id: this.fb.control(question.id),
        chosenOption: this.fb.control(null),
      });
    });

    return this.fb.array(questionFormGroups ?? []);
  }
}
