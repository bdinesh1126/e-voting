/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormGroup} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {CandidateUserData, Contest, TemplateType} from '@swiss-post/types';
import {Observable, Subject, switchMap} from 'rxjs';
import {startWith} from 'rxjs/operators';

@Component({
  selector: 'swp-choose-contest-container',
  templateUrl: './choose-contest-container.component.html',
  styleUrls: ['./choose-contest-container.component.scss'],
})
export class ChooseContestContainerComponent implements OnInit, OnDestroy {
  @Input() contest!: Contest;
  @Input() contestFormGroup!: FormGroup;
  @Input() formSubmitted: boolean | undefined;
  readonly TemplateType = TemplateType;
  accordionSubtitle$: Observable<string> | undefined;
  destroy$ = new Subject<void>();

  constructor(private readonly translate: TranslateService) {}

  get candidates(): FormArray {
    return this.contestFormGroup.get('candidates') as FormArray;
  }

  ngOnInit() {
    if (this.contest.template === TemplateType.ListsAndCandidates) {
      this.accordionSubtitle$ = this.candidates.valueChanges.pipe(
        startWith(this.candidates.value as CandidateUserData[]),
        switchMap((candidatesUserData: CandidateUserData[]) => {
          const selectedCandidates = candidatesUserData.filter(
            ({ candidateId }) => !!candidateId
          );

          return this.translate.stream('choose.candidates.subtitle', {
            nbChoices: selectedCandidates.length,
            maxChoices: this.contest?.candidatesQuestion?.maxChoices,
          });
        })
      );
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
