/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {
  ElectionContest,
  Candidate,
  CandidateFilters,
  ContestUserData
} from '@swiss-post/types';
import {Subscription} from 'rxjs';

@Component({
  selector: 'swp-candidate-selection-modal',
  templateUrl: './candidate-selection-modal.component.html',
  styleUrls: ['./candidate-selection-modal.component.scss'],
})
export class CandidateSelectionModalComponent implements OnInit, OnDestroy {
  @Input() contest!: ElectionContest;
  @Input() contestUserData!: ContestUserData;
  @Input() candidateIndex!: number;
  candidatesByList!: ReadonlyMap<string, Candidate[]>;
  candidates!: Candidate[];
  filteredCandidates!: Candidate[];
  filters: FormGroup;
  filtersSubscription!: Subscription;

  get searchTerm(): FormGroup {
    return this.filters.get('searchTerm') as FormGroup;
  }

  get searchResultsArgs(): object {
    return {
      resultCount: this.filteredCandidates.length,
      candidateCount: this.candidates.length,
      searchTerm: this.searchTerm.value
    };
  }

  constructor(
    private readonly fb: FormBuilder,
    public readonly activeModal: NgbActiveModal
  ) {
    this.filters = fb.group({
      searchTerm: '',
      listId: null,
      selectedCandidatesOnly: false,
    });
  }

  ngOnInit() {
    this.candidatesByList = this.getCandidatesByList();
    this.filteredCandidates = this.candidates = this.getAllCandidates();
    this.filtersSubscription = this.filters.valueChanges.subscribe(filters => this.filterCandidates(filters));
  }

  ngOnDestroy() {
    this.filtersSubscription.unsubscribe();
  }

  private getCandidatesByList(): Map<string, Candidate[]> {
    return this.contest.realLists.reduce((candidatesByList, list) => {
      const listCandidates = list.candidates.filter(candidate => !candidate.isBlank && !candidate.isWriteIn);
      return candidatesByList.set(list.id, listCandidates);
    }, new Map());
  }

  private getAllCandidates(): Candidate[] {
    return Array.from(this.candidatesByList.values()).reduce((allCandidates, listCandidates) => {
      return [...allCandidates, ...listCandidates];
    }, []);
  }

  private filterCandidates(filters: CandidateFilters): void {
    const candidates = filters.listId && this.candidatesByList.has(filters.listId)
      ? this.candidatesByList.get(filters.listId) as Candidate[]
      : this.candidates;

    this.filteredCandidates = candidates.filter(candidate => {
      const candidateText = candidate.details.candidateType_attribute1 ?? '';
      const matchesSearchTerm = !filters.searchTerm || candidateText.toLowerCase().includes(filters.searchTerm.toLowerCase());
      const matchesSelectedOnly = !filters.selectedCandidatesOnly || this.isSelected(candidate);

      return matchesSearchTerm && matchesSelectedOnly;
    });
  }

  private isSelected(candidate: Candidate): boolean {
    return !!this.contestUserData.candidates?.find(({candidateId}) => candidateId === candidate.id);
  }
}
