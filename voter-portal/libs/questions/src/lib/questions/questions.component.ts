/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Component, Input, OnChanges, SimpleChanges, TemplateRef } from '@angular/core';
import { Contest, ContestUserData, Question } from '@swiss-post/types';

interface QuestionTemplateContext {
  question: Question;
  questionIndex: number;
  answer?: string;
}

@Component({
  selector: 'swp-questions',
  templateUrl: './questions.component.html',
  styleUrls: [ './questions.component.scss' ],
})
export class QuestionsComponent implements OnChanges {
  @Input() contest: Contest | undefined;
  @Input() contestUserData: ContestUserData | undefined;
  @Input() headingLevel: number = 3;
  @Input() leftColumnTemplate: TemplateRef<QuestionTemplateContext> | undefined;
  @Input() rightColumnTemplate: TemplateRef<QuestionTemplateContext> | undefined;

  variantBallots: ReadonlyMap<string, Question[]> | undefined;
  answers: ReadonlyMap<string, string | undefined> | undefined;

  get questions(): Question[] {
    return this.contest?.questions ?? [];
  }

  ngOnChanges({ contest, contestUserData }: SimpleChanges) {
    const newContest: Contest = contest?.currentValue;
    if (newContest) {
      this.variantBallots = this.getVariantBallots();
    }

    const newContestUserData: ContestUserData = contestUserData?.currentValue;
    if (newContest || newContestUserData) {
      this.answers = this.getAnswers();
    }
  }

  getVariantBallot(question: Question): Question[] {
    const variantBallot = question.ballotIdentification && this.variantBallots?.get(question.ballotIdentification);
    return variantBallot ? variantBallot : [];
  }

  isFirstInVariantBallot(question: Question): boolean {
    const [ firstQuestion ] = this.getVariantBallot(question);
    return firstQuestion.id === question.id;
  }

  getTemplateContext(question: Question, questionIndex: number): QuestionTemplateContext {
    return { question, questionIndex, answer: this.answers?.get(question.id) ?? '' };
  }

  private getVariantBallots(): ReadonlyMap<string, Question[]> {
    return this.questions
      .filter(question => question.variantBallot)
      .reduce((variantBallots, question) => {
        const variantBallotQuestions = variantBallots.get(question.ballotIdentification) ?? [];
        return variantBallots.set(question.ballotIdentification, [ ...variantBallotQuestions, question ]);
      }, new Map());
  }

  private getAnswers(): ReadonlyMap<string, string | undefined> | undefined {
    if (!this.contestUserData?.questions) {
      return;
    }

    const questionsById = this.questions.reduce((questionMap, question) => {
      return questionMap.set(question.id, question);
    }, new Map<string, Question>());

    return this.contestUserData?.questions?.reduce((answerMap, userData) => {
      const { id: questionId, chosenOption: chosenOptionId } = userData;
      const question = questionsById.get(questionId);

      const chosenOption = chosenOptionId
        ? question?.options?.find(option => option.id === chosenOptionId)
        : question?.blankOption;

      return !question || !chosenOption
        ? answerMap
        : answerMap.set(question.id, chosenOption?.text);
    }, new Map());
  }
}
