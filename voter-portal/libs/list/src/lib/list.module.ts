/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';
import {SharedIconsModule} from '@swiss-post/shared/icons';
import {SharedUiModule} from '@swiss-post/shared/ui';
import {ListSelectionModalComponent} from './list-selection-modal/list-selection-modal.component';
import {ListSelectorComponent} from './list-selector/list-selector.component';
import {ListComponent} from './list/list.component';
import {ListDetailsComponent} from './list-details/list-details.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgbCollapseModule,
    TranslateModule,
    SharedIconsModule,
    SharedUiModule,
  ],
  declarations: [
    ListSelectionModalComponent,
    ListSelectorComponent,
    ListComponent,
    ListDetailsComponent,
  ],
  exports: [
    ListSelectionModalComponent,
    ListComponent,
  ],
})
export class ListModule {
}
