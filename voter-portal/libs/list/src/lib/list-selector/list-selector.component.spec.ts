/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {NgbActiveModal, NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import {IconComponent} from '@swiss-post/shared/icons';
import {Candidate, CandidateList} from '@swiss-post/types';
import {MockComponent, MockModule, MockProvider} from 'ng-mocks';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {ListDetailsComponent} from '../list-details/list-details.component';

import {ListSelectorComponent} from './list-selector.component';

describe('ListSelectorComponent', () => {
  let component: ListSelectorComponent;
  let fixture: ComponentFixture<ListSelectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ListSelectorComponent,
        MockComponent(ListDetailsComponent),
        MockComponent(IconComponent),
      ],
      providers: [MockProvider(NgbActiveModal)],
      imports: [
        TranslateTestingModule.withTranslations({}),
        MockModule(NgbCollapseModule),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSelectorComponent);
    component = fixture.componentInstance;

    component.list = {
      details: {},
      candidates: [] as Candidate[]
    } as CandidateList;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
