/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {Store} from '@ngrx/store';
import {TranslateService} from '@ngx-translate/core';
import {ConfigurationService} from '@swiss-post/shared/configuration';
import {LegalTermsActions} from '@swiss-post/shared/state';
import {AdditionalLegalTerms, BackendConfig, LegalTerms} from '@swiss-post/types';
import {map, startWith, Subscription} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
	selector: 'swp-legal-terms',
	templateUrl: './legal-terms.component.html',
	styleUrls: ['./legal-terms.component.scss'],
})
export class LegalTermsComponent implements OnInit, OnDestroy {
	electionEventId!: string;
	additionalLegalTerms?: LegalTerms;
	additionalLegalTermsSubscription!: Subscription;
	agreementForm: FormGroup;
	private readonly libPath = 'crypto.ov-api.min.js';

	constructor(
		private readonly route: ActivatedRoute,
		private readonly store: Store,
		private readonly configuration: ConfigurationService,
		private readonly translate: TranslateService,
		private readonly fb: FormBuilder
	) {
		this.agreementForm = this.fb.group({
			defaultAgreement: [false, Validators.requiredTrue],
		});
	}

	ngOnInit(): void {
		this.electionEventId = this.route.snapshot.paramMap.get(
			'electionEventId',
		) as string;

		this.additionalLegalTermsSubscription = this.translate.onLangChange.pipe(
			startWith({lang: this.translate.currentLang}),
			map(({lang}) => {
				const {additionalLegalTerms} = this.configuration;
				return additionalLegalTerms && additionalLegalTerms[lang as keyof AdditionalLegalTerms];
			})
		).subscribe(
			additionalLegalTerms => {
				this.additionalLegalTerms = additionalLegalTerms;
				if (additionalLegalTerms) {
					this.agreementForm.addControl('additionalAgreement', this.fb.control(false, Validators.requiredTrue));
				} else {
					this.agreementForm.removeControl('additionalAgreement');
				}
			}
		);
	}

	ngOnDestroy() {
		this.additionalLegalTermsSubscription.unsubscribe();
	}

	agree(): void {
		const config: BackendConfig = {
			lib: this.libPath,
			lang: this.translate.currentLang,
			electionEventId: this.electionEventId,
		};

		this.store.dispatch(LegalTermsActions.agreeClicked({config}));
	}
}
