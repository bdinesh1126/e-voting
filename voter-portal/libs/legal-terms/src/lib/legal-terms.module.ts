/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiModule } from '@swiss-post/shared/ui';

import { LegalTermsRoutingModule } from './legal-terms-routing.module';
import { LegalTermsComponent } from './legal-terms/legal-terms.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
	imports: [
		CommonModule,
		LegalTermsRoutingModule,
		TranslateModule,
		SharedUiModule,
		FormsModule,
		ReactiveFormsModule,
	],
  declarations: [
    LegalTermsComponent,
  ],
  exports: [
    LegalTermsComponent,
  ],
})
export class LegalTermsModule {
}
