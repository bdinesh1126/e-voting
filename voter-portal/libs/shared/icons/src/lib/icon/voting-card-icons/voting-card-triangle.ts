/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
export const votingCardTriangle = "M0.46,22L12,2,23.54,22H0.46Z";
