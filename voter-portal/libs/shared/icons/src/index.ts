/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
export {SharedIconsModule} from './lib/shared-icons.module';
export * from './lib/icon/icon.component';
