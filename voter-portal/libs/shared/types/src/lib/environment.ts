/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
export interface Environment {
  production: boolean,
  voterPortalConfig: string,
  availableLanguages: Language[],
  progressOverlayCloseDelay: number | Date,
  progressOverlayNavigateDelay: number | Date,
}

export interface Language {
  id: string;
  label: string;
}
