/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

import { ExtendedFactor } from './extended-factor';

export interface VoterPortalConfig {
  identification: ExtendedFactor;
  contestsCapabilities: ContestsCapabilities;
  translatePlaceholders?: TranslatePlaceholders;
  header: HeaderConfig;
  additionalLegalTerms?: AdditionalLegalTerms;
  additionalFAQs?: AdditionalFAQs;
}

export interface ContestsCapabilities {
  writeIns: boolean;
}

export interface LegalTerms {
  stepper: string;
  mainTitle: string;
  additionalTitle: string;
  terms: string[];
  confirm: string;
}

export type AdditionalLegalTerms = ForAllLanguages<LegalTerms>;

export interface FAQSectionContent {
  title: string;
  content: string[];
}

export type AdditionalFAQs = ForAllLanguages<FAQSectionContent[]>;

export type TranslatePlaceholders = ForAllLanguages<Record<string, string>>;

export interface HeaderConfig {
  logoPath: ResponsiveConfig<string>;
  logoHeight: ResponsiveConfig<number>;
  reverse?: boolean;
  background?: string;
  bars?: HeaderBarConfig[];
}

interface HeaderBarConfig {
  height: ResponsiveConfig<number>;
  color: string;
}

interface ResponsiveConfig<T = unknown> {
  desktop: T;
  mobile: T;
}

type ForAllLanguages<T = any> = {
  [lang in 'de' | 'fr' | 'it' | 'rm' | 'en']: T
}
