/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
export type Nullable<T> = T | null | undefined;
