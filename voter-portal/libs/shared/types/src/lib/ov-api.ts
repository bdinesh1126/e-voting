/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {AuthenticateVoterResponse} from './authenticate-voter-response';
import {Ballot} from './ballot';

export interface OvApi {
  authenticateVoter(
    svk: string,
    extendedFactor: string,
    electionEventId: string,
    lang: string,
  ): Promise<AuthenticateVoterResponse>;

  sendVote(
    primes: string[],
    writeIns: string[],
  ): Promise<{ choiceReturnCodes: string[] }>;

  confirmVote(confirmationKey: string): Promise<{ voteCastReturnCode: string }>;

  translateBallot(ballot: Ballot, lang: string): Promise<Ballot>;
}
