/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgbAccordionModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { SharedConfigurationModule } from '@swiss-post/shared/configuration';
import { SharedIconsModule } from '@swiss-post/shared/icons';
import { BackendErrorComponent } from './components/backend-error/backend-error.component';
import { ClearableInputComponent } from './components/clearable-input/clearable-input.component';
import { ConfirmationModalComponent } from './components/confirmation-modal/confirmation-modal.component';
import { ContestAccordionComponent } from './components/contest-accordion/contest-accordion.component';
import { FAQModalComponent } from './components/faq-modal/faq-modal.component';
import {
  AccordionAccessibilityDirective,
} from './directives/accordion-accessibility/accordion-accessibility.directive';
import { ConditionallyDescribedByDirective } from './directives/conditionally-described-by.directive';
import { MaskDirective } from './directives/mask.directive';
import { MultilineInputDirective } from './directives/multiline-input.directive';
import { TranslationListDirective } from './directives/translation-list.directive';
import { MarkdownPipe } from './pipes/markdown/markdown.pipe';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    NgbModalModule,
    NgbAccordionModule,
    SharedIconsModule,
    SharedConfigurationModule,
  ],
  declarations: [
    FAQModalComponent,
    BackendErrorComponent,
    MarkdownPipe,
    ContestAccordionComponent,
    ClearableInputComponent,
    ConfirmationModalComponent,
    TranslationListDirective,
    AccordionAccessibilityDirective,
    ConditionallyDescribedByDirective,
    MaskDirective,
    MultilineInputDirective,
  ],
  exports: [
    FAQModalComponent,
    BackendErrorComponent,
    MarkdownPipe,
    ContestAccordionComponent,
    ClearableInputComponent,
    ConfirmationModalComponent,
    TranslationListDirective,
    AccordionAccessibilityDirective,
    ConditionallyDescribedByDirective,
    MaskDirective,
    MultilineInputDirective,
  ],
})
export class SharedUiModule {
}
