/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { TestBed } from '@angular/core/testing';
import { ConfigurationService } from '@swiss-post/shared/configuration';
import { RandomItem, RandomString } from '@swiss-post/shared/testing';
import { MockProvider } from 'ng-mocks';

import { SwpTranslateCompiler } from './translate-compiler.service';

describe('TranslateCompilerService', () => {
  let service: SwpTranslateCompiler;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SwpTranslateCompiler,
        MockProvider(ConfigurationService),
      ],
    });
    service = TestBed.inject(SwpTranslateCompiler);
  });

  describe('compile', () => {
    it('should return the value as-is', () => {
      const value = 'mockValue';
      expect(service.compile(value)).toBe(value);
    });
  });

  describe('compileTranslations', () => {
    let lang: string;
    let mockPlaceholder: string;
    let configurationService: ConfigurationService;

    beforeEach(() => {
      configurationService = TestBed.inject(ConfigurationService);

      lang = RandomItem(['de', 'fr', 'it', 'en']);
      mockPlaceholder = RandomString(10);
      configurationService.translatePlaceholders = { [lang]: { placeholder: mockPlaceholder } } as any;
    });

    it('should return the translations as-is if no placeholder is found for current language', () => {
      const translations = { mockTranslation: 'mockTranslation' };

      configurationService.translatePlaceholders = undefined;

      expect(service.compileTranslations(translations, lang)).toBe(translations);
    });

    it('should inject the placeholder from the configuration service into the translations', () => {
      const translations = { mockTranslation: 'I am a translation with a {{config:placeholder}}' };
      const expectedTranslations = { mockTranslation: `I am a translation with a ${mockPlaceholder}` };

      expect(service.compileTranslations(translations, lang)).toEqual(expectedTranslations);
    });

    it('should not replace the translation params', () => {
      const translations = { mockTranslation: 'I am a translation with a {{translationParam}}' };

      expect(service.compileTranslations(translations, lang)).toEqual(translations);
    });

    it('should work with nested translations', () => {
      const translations = {
        mockTranslationKey: {
          mockNestedTranslationKey: 'I am a nested translation with a {{config:placeholder}}'
        }
      };
      const expectedTranslations = {
        mockTranslationKey: {
          mockNestedTranslationKey: `I am a nested translation with a ${mockPlaceholder}`
        }
      };

      expect(service.compileTranslations(translations, lang)).toEqual(expectedTranslations);
    });
  });
});
