/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Injectable } from '@angular/core';
import { TranslateCompiler } from '@ngx-translate/core';
import { ConfigurationService } from '@swiss-post/shared/configuration';
import { TranslatePlaceholders } from '@swiss-post/types';

type Translations = {[key: string]: string | Translations};

function replacePlaceholders<T extends string | Translations>(translations: T, placeholders: Record<string, string>): T {
  if (typeof translations === 'string') {
    const replacer = (match: string, p: string) => p in placeholders ? placeholders[p] : match;
    return <T>translations.replace(/\{\{config:(\S+)}}/g, replacer);
  }

  return <T>Object
    .entries(translations)
    .reduce((compilesTranslations, [key, value]) => {
      return { ...compilesTranslations, [key]: replacePlaceholders(value, placeholders) };
    }, {});
}

/**
 * This service injects the placeholders defined by the portal configuration into the translations
 */
@Injectable()
export class SwpTranslateCompiler implements TranslateCompiler {
  constructor(
      private readonly configuration: ConfigurationService
  ) {}

  compile(value: string): string | Function {
    return value;
  }

  compileTranslations(translations: Translations, lang: string): Translations {
    const allPlaceholders = this.configuration.translatePlaceholders;
    const currentPlaceholders = allPlaceholders && allPlaceholders[lang as keyof TranslatePlaceholders];

    if (!currentPlaceholders) return translations;

    return replacePlaceholders(translations, currentPlaceholders);
  }
}
