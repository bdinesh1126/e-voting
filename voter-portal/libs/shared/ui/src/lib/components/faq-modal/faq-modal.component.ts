/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfigurationService } from '@swiss-post/shared/configuration';
import { AdditionalFAQs, ExtendedFactor, FAQSection, FAQSectionContent } from "@swiss-post/types";
import { TranslateService } from '@ngx-translate/core';
import { Observable, startWith } from "rxjs";
import { filter, map } from "rxjs/operators";

@Component({
  selector: 'swp-faq',
  templateUrl: './faq-modal.component.html',
  styleUrls: [ './faq-modal.component.scss' ],
})
export class FAQModalComponent implements AfterViewInit, OnInit {
  additionalFAQs$!: Observable<FAQSectionContent[]>;
  @Input() activeFAQSection: FAQSection | undefined;

  readonly FAQSection = FAQSection;
  readonly ExtendedFactor = ExtendedFactor;
  readonly allowedCharacters = 'ÀÁÂÃÄÅ àáâãäå ÈÉÊË èéêë ÌÍÎÏ ìíîï ÒÓÔÕÖ òóôõö ÙÚÛÜ ùúûü Ææ Çç Œœ Þþ Ññ Øø Šš Ýý Ÿÿ Žž ¢ () ð Ð ß \' , - . /';

  public constructor(
    public readonly configuration: ConfigurationService,
    public readonly activeModal: NgbActiveModal,
    private readonly translate: TranslateService
  ) {
  }

  ngOnInit(): void {

    this.additionalFAQs$ = this.translate.onLangChange.pipe(
      startWith({ lang: this.translate.currentLang }),
      map(({ lang }) => {
        const { additionalFAQs } = this.configuration;
        return additionalFAQs && additionalFAQs[lang as keyof AdditionalFAQs];
      }),
      filter((faqs): faqs is FAQSectionContent[] => !!faqs)
    );
  }

  ngAfterViewInit() {
    if (this.activeFAQSection) {
      const activeSectionButton = document.querySelector<HTMLButtonElement>(`button[aria-controls=${this.activeFAQSection}]`);
      activeSectionButton?.focus();
    }
  }
}
