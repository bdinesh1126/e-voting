/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Component, Input } from '@angular/core';

@Component({
  selector: 'swp-contest-accordion',
  templateUrl: './contest-accordion.component.html',
  styleUrls: ['./contest-accordion.component.scss'],
})
export class ContestAccordionComponent {
  @Input() id!: string;
  @Input() title: string | null | undefined;
  @Input() subtitle: string | null | undefined;
  @Input() cardClass = '';
}
