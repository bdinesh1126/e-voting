/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, convertToParamMap, UrlTree } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { RandomElectionEventId } from '@swiss-post/shared/testing';

import { ElectionEventIdGuard } from './election-event-id.guard';

describe('VotingProcessGuard', () => {
  let guard: ElectionEventIdGuard;
  let canActivateResult: boolean | UrlTree;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
    });

    guard = TestBed.inject(ElectionEventIdGuard);
  });

  function setElectionEventId(electionEventId?: string) {
    const route = {
      paramMap: convertToParamMap(electionEventId ? {electionEventId} : {}),
    };

    canActivateResult = guard.canActivate(route as ActivatedRouteSnapshot);
  }

  describe('canActivate', () => {
    it('should return a UrlTree to "Page not Found" if the election event id is missing', () => {
      setElectionEventId(undefined);

      expect(canActivateResult).toBeInstanceOf(UrlTree);
      expect(canActivateResult.toString()).toContain('page-not-found');
    });

    it('should return a UrlTree to "Page not Found" if the election event id has an incorrect format', () => {
      setElectionEventId('123');

      expect(canActivateResult).toBeInstanceOf(UrlTree);
      expect(canActivateResult.toString()).toContain('page-not-found');
    });

    it('should return true if the election event id is has a correct format', () => {
      setElectionEventId(RandomElectionEventId());

      expect(canActivateResult).toBeTruthy();
    });
  });
});
