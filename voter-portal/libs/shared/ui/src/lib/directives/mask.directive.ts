/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Directive, ElementRef, HostListener, Input, OnInit } from '@angular/core';
import { MaskService } from 'ngx-mask';

@Directive({
  selector: '.form-control[mask]',
})
export class MaskDirective implements OnInit {
  @Input() mask: string = '';

  constructor(
    private readonly el: ElementRef<HTMLInputElement | HTMLTextAreaElement>,
    private readonly maskService: MaskService,
  ) {
  }

  private get placeholder(): string {
    return this.el.nativeElement.getAttribute('placeholder') ?? '';
  }

  private set placeholder(value: string) {
    this.el.nativeElement.setAttribute('placeholder', value);
  }

  private get cursorPosition(): number {
    return this.el.nativeElement.selectionStart ?? 0;
  }

  private set cursorPosition(value: number) {
    this.el.nativeElement.setSelectionRange(value, value);
    this.el.nativeElement.focus();
  }

  public ngOnInit(): void {
    // generate a default placeholder from the mask
    if (!this.placeholder) {
      this.placeholder = this.mask.replace(/[\w\d]/g, '_');
    }
  }

  @HostListener('keydown.delete') onDeleteKeyDown() {
    // fix an issue with ngx-mask where the cursor is sometimes moved after deleting a character
    const cursorPositionBeforeKeydown = this.cursorPosition;
    setTimeout(() => {
      this.cursorPosition = cursorPositionBeforeKeydown;
    });
  }

  @HostListener('input', [ '$event.target.value' ]) onInput(value: string) {
    // show the real placeholder if there is no user input
    if (!value.length) {
      this.el.nativeElement.value = '';
      return;
    }

    const cursorPositionBeforeValueChanges = this.cursorPosition;

    // show the placeholder filled with the user input
    const maskedValue = this.maskService.applyMask(value, this.mask);
    this.el.nativeElement.value = maskedValue + this.placeholder.substring(maskedValue.length);

    // reset the position of the cursor that moved to the end of the value
    this.cursorPosition = cursorPositionBeforeValueChanges;
  }
}
