/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Component, ElementRef, Renderer2 } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';
import { MockProvider } from 'ng-mocks';
import { AccordionAccessibilityDirective } from './accordion-accessibility.directive';

@Component({
  template: `
    <ngb-accordion>
      <ngb-panel title="Panel Title">
        <ng-template ngbPanelContent>Panel Content</ng-template>
      </ngb-panel>
    </ngb-accordion>
  `,
})
class TestHostComponent {}

describe('AccordionDirective', () => {
  let testHost: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let accordionButton: HTMLElement;
  let accordionCollapse: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        TestHostComponent,
        AccordionAccessibilityDirective
      ],
      providers: [
          MockProvider(ElementRef),
          MockProvider(Renderer2),
      ],
      imports: [ NgbAccordionModule ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    testHost = fixture.componentInstance;

    fixture.detectChanges();

    const accordion = fixture.debugElement.query(By.directive(AccordionAccessibilityDirective));
    accordionButton = accordion.query(By.css(`.accordion-button`)).nativeElement;
    accordionCollapse = accordion.query(By.css(`.collapse`)).nativeElement;
  });

  it('should render the accordion button with an id', () => {
    expect(accordionButton.id).toBeTruthy();
  });

  it('should render the accordion button wrapped in an element with role heading', () => {
    expect(/^h[1-6]$/i.test(accordionButton.parentElement?.tagName || '')).toBeTruthy();
  });

  it('should render the accordion collapse with a role region', () => {
    expect(accordionCollapse.getAttribute('role')).toBe('region');
  });

  it('should render the accordion collapse to be aria labelled by the accordion button', () => {
    expect(accordionCollapse.getAttribute('aria-labelledby')).toBe(accordionButton.id);
  });
});
