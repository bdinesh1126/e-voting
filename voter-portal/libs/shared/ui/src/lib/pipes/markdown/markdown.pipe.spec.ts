/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { MarkdownPipe } from "./markdown.pipe";
import { TestBed } from "@angular/core/testing";
import { DomSanitizer } from "@angular/platform-browser";

describe("MarkdownPipe", () => {

  let pipe: MarkdownPipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MarkdownPipe],
      providers: [
        MarkdownPipe,
        {
          provide: DomSanitizer,
          useValue: {
            sanitize: (val: string) => val
          }
        }
      ]
    });
    pipe = TestBed.inject(MarkdownPipe);
  });

  it("create an instance", () => {
    expect(pipe).toBeTruthy();
  });
});
