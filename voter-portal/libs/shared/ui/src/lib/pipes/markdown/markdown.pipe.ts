/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Pipe, PipeTransform, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { marked, Renderer } from 'marked';

interface MarkdownOption {
  allowLinks: boolean;
  allowBlock: boolean;
}

@Pipe({
  name: 'markdown',
})
export class MarkdownPipe implements PipeTransform {
  constructor(
    private readonly sanitizer: DomSanitizer,
  ) {
  }

  transform(value: string | undefined, markdownOption: MarkdownOption = { allowLinks: true, allowBlock: false }): string {
    if (!value || !window) {
      return '';
    }

    marked.setOptions({ renderer: new RestrictedRenderer(markdownOption) });

    const parsedValue = markdownOption.allowBlock ? marked.parse(value) : marked.parseInline(value);

    return this.sanitizer.sanitize(SecurityContext.HTML, parsedValue) ?? '';
  }
}

class RestrictedRenderer extends Renderer {
  denyList = ['image', 'paragraph'];
  constructor(markdownOption: MarkdownOption) {
    super();

    if (!markdownOption.allowLinks) this.denyList.push('link');

    this.denyList.forEach((deniedToken) => {
      Object.assign(this, { [deniedToken]: (text: string) => super.text(text) });
    });
  }
}
