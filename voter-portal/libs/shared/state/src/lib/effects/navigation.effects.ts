/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import {
  ChooseActions,
  LegalTermsActions,
  ReviewActions,
  SharedActions,
  StartVotingActions,
  VerifyActions,
} from '../actions/shared-state.actions';

@Injectable()
export class NavigationEffects {
  navigateToStartVoting$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          SharedActions.loggedOut,
          LegalTermsActions.agreeClicked,
        ),
        tap(() => this.router.navigate([ '/start-voting' ])),
      ),
    { dispatch: false },
  );

  navigateToChoose$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          StartVotingActions.ballotLoaded,
          ReviewActions.sealVoteCanceled,
        ),
        tap(() => this.router.navigate([ '/choose' ])),
      ),
    { dispatch: false },
  );

  navigateToReview$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(ChooseActions.reviewClicked),
        tap(() => this.router.navigate([ '/review' ])),
      ),
    { dispatch: false },
  );

  navigateToVerify$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          ReviewActions.sealedVoteLoaded,
          StartVotingActions.choiceReturnCodesLoaded,
        ),
        tap(() => this.router.navigate([ '/verify' ])),
      ),
    { dispatch: false },
  );

  navigateToConfirm$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          VerifyActions.castVoteLoaded,
          StartVotingActions.voteCastReturnCodeLoaded,
        ),
        tap(() => this.router.navigate([ '/confirm' ])),
      ),
    { dispatch: false },
  );

  constructor(
    private readonly actions$: Actions,
    private readonly router: Router,
  ) {
  }
}
