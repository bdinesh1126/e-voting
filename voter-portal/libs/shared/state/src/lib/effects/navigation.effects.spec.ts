/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action } from '@ngrx/store';
import { Ballot } from '@swiss-post/types';
import { Observable, of } from 'rxjs';
import { ReviewActions, StartVotingActions } from '../actions/shared-state.actions';

import { NavigationEffects } from './navigation.effects';

describe('NavigationEffects', () => {
  let actions$: Observable<Action>;
  let effects: NavigationEffects;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      providers: [
        NavigationEffects,
        provideMockActions(() => actions$),
      ],
    });
    effects = TestBed.inject(NavigationEffects);
    router = TestBed.inject(Router);
    router.navigate = jest.fn();
  });

  it('should navigate to the start voting page after a logout', () => {
    actions$ = of(StartVotingActions.ballotLoaded({ ballot: {} as Ballot }));
    effects.navigateToChoose$.subscribe();
    expect(router.navigate).toHaveBeenNthCalledWith(1, [ '/choose' ]);
  });

  it('should navigate to the start voting page after the legal terms have been agreed', () => {
    actions$ = of(StartVotingActions.ballotLoaded({ ballot: {} as Ballot }));
    effects.navigateToChoose$.subscribe();
    expect(router.navigate).toHaveBeenNthCalledWith(1, [ '/choose' ]);
  });

  it('should navigate to the choose page after the ballot has been loaded', () => {
    actions$ = of(StartVotingActions.ballotLoaded({ ballot: {} as Ballot }));
    effects.navigateToChoose$.subscribe();
    expect(router.navigate).toHaveBeenNthCalledWith(1, [ '/choose' ]);
  });

  it('should navigate to the choose page after the vote seal has been canceled', () => {
    actions$ = of(ReviewActions.sealVoteCanceled());
    effects.navigateToChoose$.subscribe();
    expect(router.navigate).toHaveBeenNthCalledWith(1, [ '/choose' ]);
  });

  it('should navigate to the verify page after the choice return codes have been loaded', () => {
    actions$ = of(StartVotingActions.choiceReturnCodesLoaded({ ballot: {} as Ballot, choiceReturnCodes: [] }));
    effects.navigateToVerify$.subscribe();
    expect(router.navigate).toHaveBeenNthCalledWith(1, [ '/verify' ]);
  });

  it('should navigate to the confirm page after the vote cast return code has been loaded', () => {
    actions$ = of(StartVotingActions.voteCastReturnCodeLoaded({ voteCastReturnCode: '' }));
    effects.navigateToConfirm$.subscribe();
    expect(router.navigate).toHaveBeenNthCalledWith(1, [ '/confirm' ]);
  });
});
