/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { RandomInt } from '@swiss-post/shared/testing';
import {
  AuthenticateVoterResponse,
  BackendConfig,
  BackendError,
  Ballot,
  BallotUserData,
  ErrorStatus,
  Voter,
  VotingCardState,
} from '@swiss-post/types';
import { firstValueFrom, of } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { BackendService } from '../backend.service';
import mockBallot from './mock-ballot.json';

function generateId(length: number) {
  return Array.from({length}, () => RandomInt(10)).join('');
}

function throwBackendError(errorProperties: object) {
  const backendError = new BackendError();
  Object.assign(backendError, errorProperties);
  throw backendError;
}

function getMockTranslation(
  item: unknown,
  lang: string,
  translateStrings = true,
): unknown {
  if (
    typeof item === 'number' ||
    typeof item === 'boolean' ||
    (typeof item === 'string' && !translateStrings) ||
    !item
  ) {
    return item;
  }

  if (typeof item === 'string') {
    const stringToTranslate = item.replace(/^\*{3}(DE|FR|IT|RM|EN) /g, '');
    return `***${lang.toUpperCase()} ${stringToTranslate}`;
  }

  if (Array.isArray(item)) {
    return item.map((i) => getMockTranslation(i, lang));
  }

  if (item && typeof item === 'object') {
    return Object.keys(item).reduce((translatedObject, key) => {
      const translatableKeys = [
        'title',
        'description',
        'text',
        'questionType_text',
        'answerType_text',
      ];
      return {
        ...translatedObject,
        [key]: getMockTranslation(
          item[key as keyof typeof item],
          lang,
          translatableKeys.includes(key),
        ),
      };
    }, {});
  }

  return null;
}

class MockOvBackendService implements BackendService {
  get ballot(): Ballot {
    return { ...mockBallot } as Ballot;
  }

  translateBallot(
    ballot: Ballot,
    lang: string,
  ): Promise<Ballot> {
    const translatedBallot$ = of(getMockTranslation(ballot, lang) as Ballot);
    return firstValueFrom(translatedBallot$);
  }

  async authenticateVoter(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    voter: Voter, config: BackendConfig,
  ): Promise<AuthenticateVoterResponse> {
    if (voter.startVotingKey.substring(0, 1) === 's') {
      const verifyResponse$ = of({
        votingCardState: VotingCardState.Sent,
        ballot: this.ballot,
        choiceReturnCodes: this.getChoiceReturnCodes(),
      }).pipe(delay(500));

      return firstValueFrom(verifyResponse$);
    }

    if (voter.startVotingKey.substring(0, 1) === 'c') {
      const confirmResponse$ = of({
        votingCardState: VotingCardState.Confirmed,
        voteCastReturnCode: generateId(8),
      }).pipe(delay(500));

      return firstValueFrom(confirmResponse$);
    }

    const chooseResponse$ = of({
      votingCardState: VotingCardState.Initial,
      ballot: this.ballot,
    }).pipe(
      map((ballot) => {
        if (voter.startVotingKey.substring(2, 3) === 'r') {
          throwBackendError({
            numberOfRemainingAttempts: 3,
            errorStatus: ErrorStatus.ExtendedFactorInvalid,
          });
        } else if (voter.startVotingKey.substring(2, 3) === 'b') {
          throwBackendError({
            errorStatus: ErrorStatus.StartVotingKeyInvalid,
          });
        }

        return ballot;
      }),
      delay(500),
    );

    return firstValueFrom(chooseResponse$);
  }

  async sendVote(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    ballot: Ballot, userData: BallotUserData,
  ): Promise<string[]> {
    const choiceReturnCodes$ = of(this.getChoiceReturnCodes())
      .pipe(
        delay(3000),
        map((sendVoteResponse) => {
          return sendVoteResponse;
        }),
      );

    return firstValueFrom(choiceReturnCodes$);
  }

  confirmVote(confirmationKey: string): Promise<string> {
    const confirmationCode$ = of(generateId(8))
      .pipe(
        delay(500),
        map((voteCastReturnCode) => {
          if (confirmationKey.substring(0, 1) === '9') {
            throwBackendError({
              errorStatus: ErrorStatus.ConnectionError,
            });
          } else if (confirmationKey.substring(0, 1) === '8') {
            throwBackendError({
              numberOfRemainingAttempts: 3,
              errorStatus: ErrorStatus.ConfirmationAttemptsExceeded,
            });
          }
          return voteCastReturnCode;
        }),
      );

    return firstValueFrom(confirmationCode$);
  }

  private getChoiceReturnCodes(): string[] {
    return this.ballot.contests.reduce((choices, contest) => {
      let numChoiceCodeForContest = 0;
      numChoiceCodeForContest += contest.questions?.length ?? 0;
      numChoiceCodeForContest += contest.listQuestion?.maxChoices ?? 0;
      numChoiceCodeForContest += contest.candidatesQuestion?.maxChoices ?? 0;

      for (let i = 0; i < numChoiceCodeForContest; i++) {
        choices.push(generateId(4));
      }

      return choices;
    }, [] as string[]);
  }
}

export { MockOvBackendService as OvBackendService };
