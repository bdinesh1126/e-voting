/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {Component, ElementRef, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import {TranslateService} from '@ngx-translate/core';
import {getBackendError, getLoading, getSentButNotCast, getVerifyData, VerifyActions} from '@swiss-post/shared/state';
import {FAQService, ProcessCancellationService} from '@swiss-post/shared/ui';
import {Contest, ContestUserData, ContestVerifyData, ErrorStatus, FAQSection} from '@swiss-post/types';
import {Observable, take} from 'rxjs';
import {map} from 'rxjs/operators';

function getNumChoiceReturnCodesOfContest(contest: Contest): number {
  let numChoiceCodeForContest = 0;
  numChoiceCodeForContest += contest.questions?.length ?? 0;
  numChoiceCodeForContest += contest.listQuestion?.maxChoices ?? 0;
  numChoiceCodeForContest += contest.candidatesQuestion?.maxChoices ?? 0;
  return numChoiceCodeForContest;
}

function focusFirstInvalidControl() {
  setTimeout(() => {
    const firstInvalid = document.querySelector<HTMLInputElement>('.form-control.is-invalid');
    firstInvalid?.focus();
  });
}

@Component({
  selector: 'swp-verify',
  templateUrl: './verify.component.html',
  styleUrls: [ './verify.component.scss' ],
})
export class VerifyComponent {
  @ViewChild('confirmationKeyInput') confirmationKeyInput: ElementRef<HTMLInputElement> | null = null;

  readonly FAQSection = FAQSection;
  readonly ErrorMessage = ErrorStatus;

  isLoading$: Observable<boolean> = this.store.select(getLoading);
  sentButNotCast$ = this.store.select(getSentButNotCast);
  confirmationKeyForm: FormGroup;
  sendVoteResponseAndContests$ = this.store
    .select(getVerifyData)
    .pipe(map((data) => this.buildModel(data)));

  constructor(
    private readonly store: Store,
    private readonly fb: FormBuilder,
    private readonly cancelProcessService: ProcessCancellationService,
    private readonly faqService: FAQService,
    public readonly translate: TranslateService,
  ) {
    this.confirmationKeyForm = this.fb.group({
      confirmationKey: [ '', [ Validators.required, Validators.minLength(9) ] ],
    });
  }

  get confirmationKey(): FormControl {
    return this.confirmationKeyForm.get('confirmationKey') as FormControl;
  }

  private buildModel(data: {
    choiceReturnCodes: string[] | null | undefined;
    contests: Contest[] | undefined;
    contestsUserData: ContestUserData[] | undefined;
  }): ContestVerifyData[] {
    const { contests, contestsUserData, choiceReturnCodes } = data;

    if (!contests || !choiceReturnCodes) {
      return [];
    }

    let currentChoiceCodeIndex = 0;
    return contests?.map((contest, i) => {
      const numChoiceReturnCodesForContest = getNumChoiceReturnCodesOfContest(contest);
      const filteredChoiceReturnCodes =
        choiceReturnCodes.slice(
          currentChoiceCodeIndex,
          currentChoiceCodeIndex + numChoiceReturnCodesForContest,
        ) ?? [];
      currentChoiceCodeIndex += numChoiceReturnCodesForContest;

      return {
        contest: contest,
        choiceReturnCodes: filteredChoiceReturnCodes,
        contestUserData: contestsUserData ? contestsUserData[i] : undefined,
      };
    });
  }

  confirmCancel() {
    this.cancelProcessService.leaveProcess();
  }

  showFAQ(section: FAQSection) {
    this.faqService.showFAQ(section);
  }

  cast() {
    if (this.confirmationKeyForm.invalid) {
      this.confirmationKeyForm.reset();
      focusFirstInvalidControl();
      return;
    }

    this.store.dispatch(VerifyActions.castVoteClicked({ confirmationKey: this.confirmationKey.value }));
    this.store.select(getBackendError)
      .pipe(take(1))
      .subscribe(() => {
        focusFirstInvalidControl();
      });
  }
}
