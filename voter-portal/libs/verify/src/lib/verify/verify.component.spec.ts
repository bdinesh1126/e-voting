/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import '@angular/localize/init';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { IconComponent } from '@swiss-post/shared/icons';
import { SHARED_FEATURE_KEY, VerifyActions } from '@swiss-post/shared/state';
import {
  MockChoiceReturnCodes,
  MockContest,
  MockContestUserData,
  MockQuestions,
  RandomArray,
} from '@swiss-post/shared/testing';
import {
  BackendErrorComponent,
  ClearableInputComponent,
  FAQService,
  ProcessCancellationService,
} from '@swiss-post/shared/ui';
import { Ballot, Contest, ContestUserData, FAQSection } from '@swiss-post/types';
import { MockComponent, MockPipe, MockProvider } from 'ng-mocks';
import { NgxMaskModule } from 'ngx-mask';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { VerifyContestContainerComponent } from '../verify-contest-container/verify-contest-container.component';
import { VerifyComponent } from './verify.component';

class MockState {
  ballot: Ballot;
  ballotUserData: { contests: ContestUserData[] } | undefined;
  choiceReturnCodes: string[];
  sentButNotCast: boolean | undefined;
  loading = false;

  constructor() {
    this.ballot = {
      contests: [],
      id: '1',
      correctnessIds: {},
    };
    this.choiceReturnCodes = [];
  }
}

describe('VerifyComponent', () => {
  let fixture: ComponentFixture<VerifyComponent>;
  let store: MockStore;
  const initialState: MockState = Object.freeze(new MockState());
  let contests: Contest[];
  let faqService: FAQService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        RouterTestingModule,
        TranslateTestingModule.withTranslations({}),
        NgxMaskModule.forRoot(),
      ],
      declarations: [
        VerifyComponent,
        MockComponent(ClearableInputComponent),
        MockComponent(VerifyContestContainerComponent),
        MockComponent(BackendErrorComponent),
        MockComponent(IconComponent),
      ],
      providers: [
        provideMockStore({
          initialState: {[SHARED_FEATURE_KEY]: initialState},
        }),
        MockProvider(ProcessCancellationService),
        MockProvider(FAQService),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    store = TestBed.inject(MockStore);
    faqService = TestBed.inject(FAQService);
    fixture = TestBed.createComponent(VerifyComponent);
    fixture.detectChanges();
  });

  describe('propagate data to child components', () => {
    let contestsUserData: ContestUserData[];
    let contestContainerComponents: DebugElement[];
    let newState: MockState;

    function setContests(): void {
      contests = RandomArray(
        (contestIndex) => {
          const questionTexts = RandomArray(
            (questionIndex) => {
              return `Contest ${contestIndex} - Question ${questionIndex}`;
            },
            3,
            5
          );

          return MockContest(MockQuestions(questionTexts));
        },
        5,
        1
      );

      newState = {
        ...initialState,
      };
      newState.ballot.contests = contests;
      newState.choiceReturnCodes = MockChoiceReturnCodes(
        newState.ballot as Ballot,
      );
      store.setState({[SHARED_FEATURE_KEY]: newState});
    }

    describe('without user data', () => {
      beforeEach(() => {
        setContests();
        fixture.detectChanges();
        contestContainerComponents = fixture.debugElement.queryAll(
          By.css('swp-verify-contest-container')
        );
      });

      it('should display as many contests as there are in the store', () => {
        expect(contestContainerComponents.length).toBe(contests.length);
      });

      it('should pass the proper contest to the contest container components', () => {
        contests.forEach((contest, i) => {
          const {contest: receivedContest} =
            contestContainerComponents[i].componentInstance.contestVerifyData;
          expect(receivedContest).toBe(contest);
        });
      });

      it('should slice choice-codes properly for each contest', () => {
        let currentIndexInAllChoiceReturnCodes = 0;
        contests.forEach((contest, i) => {
          const { choiceReturnCodes: choiceReturnCodes } =
            contestContainerComponents[i].componentInstance.contestVerifyData;
          expect((choiceReturnCodes as string[]).length).toBe(
            contest.questions?.length,
          );

          const choiceReturnCodesSlice = initialState.choiceReturnCodes.slice(
            currentIndexInAllChoiceReturnCodes,
            contest.questions?.length,
          );
          expect(choiceReturnCodes).toEqual(expect.arrayContaining(choiceReturnCodesSlice));
          currentIndexInAllChoiceReturnCodes += contest.questions?.length ?? 0;
        });
      });
    });

    describe('with user data', () => {
      function setContestsUserData(): void {
        contestsUserData = contests.map((contest) =>
          MockContestUserData(contest.questions ?? [])
        );
        store.setState({
          [SHARED_FEATURE_KEY]: {
            ...initialState,
            ballotUserData: {contests: contestsUserData},
          },
        });
      }

      beforeEach(() => {
        setContests();
        setContestsUserData();

        fixture.detectChanges();
        contestContainerComponents = fixture.debugElement.queryAll(
          By.css('swp-verify-contest-container')
        );
      });

      it('should properly apply the contest user data to each contest form group', () => {
        contestsUserData.forEach((contestUserData, i) => {
          const {contestUserData: receivedContestUserData} =
            contestContainerComponents[i].componentInstance.contestVerifyData;
          expect(receivedContestUserData).toEqual(contestUserData);
        });
      });
    });
  });

  describe('"abandonned process in previous session" message', () => {
    let sentButNotCastDiv: DebugElement;

    const setFlagSentButNotCast = (flag: boolean) => {
      store.setState({
        [SHARED_FEATURE_KEY]: {...initialState, sentButNotCast: flag},
      });
      fixture.detectChanges();

      sentButNotCastDiv = fixture.debugElement.query(
        By.css('#warningSentButNotCast')
      );
    };

    it('show "abandonned process in previous session" message if flag is set', () => {
      setFlagSentButNotCast(true);
      expect(sentButNotCastDiv.nativeElement.textContent).toContain(
        'verify.warning.abandonedprocess'
      );
    });

    it('do not show "abandonned process in previous session" message if flag is not set', () => {
      setFlagSentButNotCast(false);
      expect(sentButNotCastDiv).toBeFalsy();
    });
  });

  describe('form validation', () => {
    beforeEach(() => {
      fixture.detectChanges();
    });

    describe('initialState', () => {
      it('should not show validation error without clicking submit button', () => {
        const validationError = fixture.debugElement.query(
          By.css('#confirmation-key-required'),
        );
        expect(validationError).toBeFalsy();
      });
    });

    describe('validators', () => {
      const setInputValue = (value: string) => {
        const confirmationKeyInputElement = fixture.debugElement.query(
          By.css('#confirmationKey'),
        ).nativeElement;
        confirmationKeyInputElement.value = value;
        confirmationKeyInputElement.dispatchEvent(new Event('input'));
        fixture.detectChanges();
      };

      it('should not validate with partial input', () => {
        setInputValue('1234');
        expect(fixture.debugElement.componentInstance.confirmationKey.invalid).toBeTruthy();
      });

      it('should validate with valid input', () => {
        setInputValue('123456789');
        expect(fixture.debugElement.componentInstance.confirmationKey.invalid).toBeFalsy();
      });
    });

    describe('validation error messages', () => {
      let validationErrorDiv: DebugElement;

      const setConfirmationKeyAndClickSubmit = (confirmationKey: string) => {
        fixture.debugElement.componentInstance.confirmationKey.setValue(confirmationKey);
        fixture.detectChanges();

        const confirmButtonElement = fixture.debugElement.query(
          By.css('#btn_confirm_vote'),
        ).nativeElement;

        confirmButtonElement.click();
        fixture.detectChanges();

        validationErrorDiv = fixture.debugElement.query(
          By.css('#confirmation-key-required'),
        );
      };

      it('should show validation error without input after submit button is clicked', () => {
        setConfirmationKeyAndClickSubmit('');
        expect(validationErrorDiv).toBeTruthy();
      });

      it('should not show validation error with correct input after submit button is clicked', () => {
        setConfirmationKeyAndClickSubmit('123456789');
        expect(validationErrorDiv).toBeFalsy();
      });
    });

    describe('check call to dispatch store action', () => {
      const castVoteWithKey = (value: string) => {
        fixture.debugElement.componentInstance.confirmationKey.setValue(value);
        fixture.detectChanges();
        fixture.debugElement.componentInstance.cast();
      };

      beforeEach(() => {
        jest.spyOn(store, 'dispatch');
      });

      it('should call dispatch action if input was valid', () => {
        const mockConfirmationKey = '123456789';
        castVoteWithKey(mockConfirmationKey);
        expect(store.dispatch).toHaveBeenCalledWith(
          VerifyActions.castVoteClicked({ confirmationKey: mockConfirmationKey }),
        );
      });

      it('should not call dispatch action if input was not valid', () => {
        castVoteWithKey('');
        expect(store.dispatch).not.toHaveBeenCalled();
      });
    });
  });
  describe('disable submit button while loading', () => {
    let confirmButton: DebugElement;
    const setLoadingState = (isLoading: boolean) => {
      store.setState({
        [SHARED_FEATURE_KEY]: {...initialState, loading: isLoading},
      });
      fixture.detectChanges();

      confirmButton = fixture.debugElement.query(By.css('#btn_confirm_vote'));
    };

    it('should not disable button while not loading data', () => {
      setLoadingState(false);
      expect(confirmButton.nativeElement.disabled).toBeFalsy();
    });

    it('should disable button while loading data', () => {
      setLoadingState(true);
      expect(confirmButton.nativeElement.disabled).toBeTruthy();
    });
  });

  describe('help buttons', () => {
    const clickHelpButton = (helpButtonId: string) => {
      jest.spyOn(faqService, 'showFAQ');
      const faqButton = fixture.debugElement.query(By.css(`#${helpButtonId}`));
      faqButton.nativeElement.click();
    };

    it('should call showFAQ on click on show-faq-choice-codes-link', () => {
      clickHelpButton('show-faq-choice-codes-link');
      expect(faqService.showFAQ).toBeCalledWith(FAQSection.WhatAreChoiceReturnCodes);
    });

    it('should call showFAQ on click on show-faq-codes-do-not-match-link', () => {
      clickHelpButton('show-faq-codes-do-not-match-link');
      expect(faqService.showFAQ).toBeCalledWith(
        FAQSection.WhatIfCodesDoNotMatch,
      );
    });

    it('should call showFAQ on click on show-faq-whatis-confirmationkey-link', () => {
      clickHelpButton('show-faq-whatis-confirmationkey-link');
      expect(faqService.showFAQ).toBeCalledWith(
        FAQSection.WhatIsConfirmationKey,
      );
    });
  });
});
