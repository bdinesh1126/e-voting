/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Component, Input } from '@angular/core';
import { ContestVerifyData } from '@swiss-post/types';

@Component({
  selector: 'swp-verify-candidate-list',
  templateUrl: './verify-candidate-list.component.html',
  styleUrls: [ './verify-candidate-list.component.scss' ],
})
export class VerifyCandidateListComponent {
  @Input() contestVerifyData: ContestVerifyData | undefined;

  get hasList(): boolean {
    const listMaxChoices = this.contestVerifyData?.contest.listQuestion?.maxChoices ?? 0;
    return listMaxChoices > 0;
  }

  getCandidateChoiceCode(candidateIndex: number): string {
    const choiceCodeIndex = this.hasList ? candidateIndex + 1 : candidateIndex;
    return this.contestVerifyData?.choiceReturnCodes[choiceCodeIndex] ?? '';
  }

  getListChoiceCode(): string {
    return this.contestVerifyData?.choiceReturnCodes[0] ?? '';
  }
}
