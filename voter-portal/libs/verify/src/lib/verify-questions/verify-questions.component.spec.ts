/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import {
  MockContest,
  MockContestUserData,
  MockQuestions,
  QuestionsTestingComponent,
  RandomInt,
} from '@swiss-post/shared/testing';
import { ContestVerifyData } from '@swiss-post/types';
import { TranslateTestingModule } from 'ngx-translate-testing';

import { VerifyQuestionsComponent } from './verify-questions.component';

describe('VerifyQuestionListComponent', () => {
  let component: VerifyQuestionsComponent;
  let fixture: ComponentFixture<VerifyQuestionsComponent>;
  let contestVerifyData: ContestVerifyData;
  let questionsComponent: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VerifyQuestionsComponent, QuestionsTestingComponent],
      imports: [TranslateTestingModule.withTranslations({})],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyQuestionsComponent);
    component = fixture.componentInstance;

    const questions = MockQuestions([
      'Verified Question A',
      'Verified Question B',
    ]);
    contestVerifyData = {
      contest: MockContest(questions),
      contestUserData: MockContestUserData(questions),
      choiceReturnCodes: questions.map((question) => `choice-code-${question.id}`),
    };

    component.contestVerifyData = contestVerifyData;

    questionsComponent = fixture.debugElement.query(By.css('swp-questions'));
    questionsComponent.componentInstance.templateTestingContext = {
      question: questions[0],
    };
    fixture.detectChanges();
  });

  it('should pass the provided contest to the questions component', () => {
    expect(questionsComponent.componentInstance.contest).toBe(
      component.contestVerifyData?.contest
    );
  });

  it('should pass the provided contest user data to the questions component', () => {
    expect(questionsComponent.componentInstance.contestUserData).toBe(
      component.contestVerifyData?.contestUserData
    );
  });

  it('should work properly without contest user data', () => {
    component.contestVerifyData = {
      ...contestVerifyData,
      contestUserData: undefined,
    };

    fixture.detectChanges();

    expect(component).toBeTruthy();
  });

  it('should show answers within the questions component', () => {
    const testContext =
      questionsComponent.componentInstance.templateTestingContext;
    testContext.answer = 'Verified answer';

    fixture.detectChanges();

    expect(questionsComponent.nativeElement.textContent).toContain(
      testContext.answer
    );
  });

  it('should show screenreader message "common.screenreader.nooptionselected" if no answer is selected', () => {
    const testContext =
      questionsComponent.componentInstance.templateTestingContext;
    testContext.question = {
      blankOption: {
        text: '',
      },
    };
    testContext.answer = '';
    fixture.detectChanges();
    expect(questionsComponent.nativeElement.textContent).toContain(
      'common.screenreader.nooptionselected'
    );
  });

  it('should show correct choice codes within the questions component', () => {
    const choiceReturnCodes = component.contestVerifyData?.choiceReturnCodes || [];
    const randomQuestionIndex = RandomInt(choiceReturnCodes.length);
    questionsComponent.componentInstance.templateTestingContext.questionIndex =
      randomQuestionIndex;

    fixture.detectChanges();

    expect(questionsComponent.nativeElement.textContent).toContain(
      choiceReturnCodes[randomQuestionIndex],
    );
  });
});
