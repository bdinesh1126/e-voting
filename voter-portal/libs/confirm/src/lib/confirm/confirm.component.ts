/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { getDefinedVoteCastReturnCode, getVoteCastedInPreviousSession } from '@swiss-post/shared/state';
import { FAQService, ProcessCancellationService } from '@swiss-post/shared/ui';
import { FAQSection } from '@swiss-post/types';
import { map } from 'rxjs/operators';

@Component({
  selector: 'swp-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: [ './confirm.component.scss' ],
})
export class ConfirmComponent {
  readonly FAQSection = FAQSection;

  voteCastReturnCode$ = this.store.pipe(
    getDefinedVoteCastReturnCode,
    map((voteCastReturnCode) => this.formatVoteCastCode(voteCastReturnCode)),
  );

  voteCastedInPreviousSession$ = this.store.select(getVoteCastedInPreviousSession);

  constructor(
    private readonly store: Store,
    private readonly faqService: FAQService,
    private readonly cancelProcessService: ProcessCancellationService,
  ) {
  }

  showFAQ(section: FAQSection): void {
    this.faqService.showFAQ(section);
  }

  formatVoteCastCode(voteCastReturnCode: string): string | null {
    return new RegExp(/^\d{8}$/).test(voteCastReturnCode)
      ? `${voteCastReturnCode.slice(0, 4)} ${voteCastReturnCode.slice(4)}`
      : null;
  }

  quit() {
    this.cancelProcessService.quit();
  }
}
