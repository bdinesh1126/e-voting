<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ (c) Copyright 2022 Swiss Post Ltd
  -->
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://maven.apache.org/POM/4.0.0" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>ch.post.it.evoting</groupId>
		<artifactId>evoting-dependencies</artifactId>
		<version>1.3.3.2</version>
		<relativePath>../evoting-dependencies/pom.xml</relativePath>
	</parent>

	<artifactId>voter-portal</artifactId>
	<version>1.3.3.2</version>
	<packaging>pom</packaging>
	<name>Voting Portal (Angular frontend)</name>

	<properties>
		<skipTests>false</skipTests>
		<sonar.sources>apps/e-vote,libs</sonar.sources>
		<sonar.exclusions>**/*.spec.ts,**/test-setup.ts,**/jest.config.js</sonar.exclusions>
		<sonar.tests>apps/e-vote,libs</sonar.tests>
		<sonar.test.inclusions>**/*.spec.ts</sonar.test.inclusions>
	</properties>

	<dependencies>
		<dependency>
			<groupId>ch.post.it.evoting</groupId>
			<artifactId>voting-client-js</artifactId>
			<type>zip</type>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<!-- Skip JaCoCo -->
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<executions>
					<execution>
						<id>prepare-agent</id>
						<phase>none</phase>
					</execution>
					<execution>
						<id>report</id>
						<phase>none</phase>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<!-- Skip integration tests -->
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-failsafe-plugin</artifactId>
				<executions>
					<execution>
						<id>integration-test</id>
						<phase>none</phase>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<!-- Cleaning directories -->
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-clean-plugin</artifactId>
				<configuration>
					<filesets>
						<fileset>
							<directory>dist</directory>
						</fileset>
						<fileset>
							<directory>vendor</directory>
						</fileset>
						<fileset>
							<directory>coverage</directory>
						</fileset>
					</filesets>
				</configuration>
			</plugin>

			<plugin>
				<!-- Unzip voting-client-js -->
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-dependency-plugin</artifactId>
				<executions>
					<execution>
						<id>unpack-voting-client-js</id>
						<goals>
							<goal>unpack-dependencies</goal>
						</goals>
						<phase>generate-sources</phase>
						<configuration>
							<outputDirectory>${project.basedir}/vendor/voting-client-js</outputDirectory>
							<includeGroupIds>ch.post.it.evoting</includeGroupIds>
							<includeArtifactIds>voting-client-js</includeArtifactIds>
							<excludeTransitive>true</excludeTransitive>
							<excludeTypes>pom</excludeTypes>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<!-- Copy built dist to target -->
				<artifactId>maven-resources-plugin</artifactId>
				<executions>
					<execution>
						<id>copy-resources</id>
						<goals>
							<goal>copy-resources</goal>
						</goals>
						<phase>package</phase>
						<configuration>
							<outputDirectory>${basedir}/target/dist</outputDirectory>
							<resources>
								<resource>
									<directory>dist</directory>
								</resource>
							</resources>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<!-- Create zip artifact -->
				<artifactId>maven-assembly-plugin</artifactId>
				<configuration>
					<descriptors>
						<descriptor>assembly/package.xml</descriptor>
					</descriptors>
					<appendAssemblyId>false</appendAssemblyId>
				</configuration>
				<executions>
					<execution>
						<id>create-archive</id>
						<goals>
							<goal>single</goal>
						</goals>
						<phase>package</phase>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<!-- Run npm commands -->
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>exec-maven-plugin</artifactId>
				<executions>
					<execution>
						<id>npm ci (initialize)</id>
						<goals>
							<goal>exec</goal>
						</goals>
						<phase>initialize</phase>
						<configuration>
							<environmentVariables>
								<CYPRESS_INSTALL_BINARY>0</CYPRESS_INSTALL_BINARY>
							</environmentVariables>
							<executable>npm</executable>
							<arguments>
								<argument>ci</argument>
								<argument>--omit=optional</argument>
								<argument>--fund=false</argument>
							</arguments>
						</configuration>
					</execution>
					<execution>
						<id>npm run compile (compile)</id>
						<goals>
							<goal>exec</goal>
						</goals>
						<phase>compile</phase>
						<configuration>
							<executable>npm</executable>
							<arguments>
								<argument>run</argument>
								<argument>build:without-cache</argument>
							</arguments>
						</configuration>
					</execution>
					<execution>
						<id>npm run test (test)</id>
						<goals>
							<goal>exec</goal>
						</goals>
						<phase>test</phase>
						<configuration>
							<executable>npm</executable>
							<arguments>
								<argument>run</argument>
								<argument>test:jest</argument>
							</arguments>
							<skip>${skipTests}</skip>
						</configuration>
					</execution>
				</executions>
			</plugin>

		</plugins>
	</build>

</project>
