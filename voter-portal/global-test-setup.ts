/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
import {randomBytes} from 'crypto';

Object.defineProperty(global.self, 'crypto', {
  value: {
    getRandomValues: (array: undefined[]) => randomBytes(array.length),
  },
});
