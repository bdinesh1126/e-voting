/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
const nxPreset = require('@nrwl/jest/preset');

module.exports = {
  ...nxPreset,
  setupFilesAfterEnv: ['./global-test-setup.ts'],
};
