# Voter Portal

The voter-portal provides a front-end for the voter and covers the entire voting process:

- confirming the legal conditions,
- entering the start voting key,
- selecting voting options,
- confirming the selection of voting options,
- displaying the Choice Return Codes for every selected voting option,
- entering the Ballot Casting Key,
- displaying the Vote Cast Return Code,
- providing a help menu to the voter.

Moreover, the voter-portal supports four languages (German, French, Italian, Rumantsch) and
authentication factors in addition to the Start Voting Key (date of birth, year of birth).

The voter-portal supports multiple electoral models:

- Referendums / Initiatives (allowing the voter to answer yes, no, or empty for specific questions)
- Elections with lists and candidates, with the possibility of accumulating candidates, list combinations,
  and candidates appearing on multiple lists
- Elections without lists

The voter-portal builds upon the voting-client-js, which implements the voting client's
cryptographic algorithms.

## Usage

The voter portal is packaged as an Angular application.

In our productive infrastructure, we deploy the voter portal behind a reverse proxy, which makes sure that the
following [HTTP Response Headers](https://owasp.org/www-project-secure-headers) are set:

| HTTP Response Header         | Value                                                                                                                                                                                                                                                              |
|------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Cache-Control                | no-cache; no-store; must-revalidate;max-age=0                                                                                                                                                                                                                      |
| Content-Disposition          | Inline                                                                                                                                                                                                                                                             |
| Content-Security-Policy      | default-src 'none'; script-src 'self' 'unsafe-inline' 'unsafe-eval'; style-src 'self' 'unsafe-inline'; img-src 'self' data:; connect-src 'self'; worker-src 'self'; frame-src 'none'; font-src 'self'; base-uri 'self'; frame-ancestors 'none'; form-action 'none' |
| Content-Type                 | Always add charset=utf-8 for HTML, JSON, Javascript and CSS resources                                                                                                                                                                                              |
| Cross-Origin-Embedder-Policy | require-corp                                                                                                                                                                                                                                                       |
| Cross-Origin-Opener-Policy   | same-origin                                                                                                                                                                                                                                                        |
| Cross-Origin-Resource-Policy | same-origin                                                                                                                                                                                                                                                        |
| Permission-policy            | vibrate=(), microphone=(), geolocation=(), camera=(), display-capture=()                                                                                                                                                                                           |
| Pragma                       | no-cache                                                                                                                                                                                                                                                           |
| Referrer-Policy              | no-referrer                                                                                                                                                                                                                                                        |
| Strict-Transport-Security    | max-age=63072000; includeSubDomains; preload                                                                                                                                                                                                                       |
| X-Content-Type-Options       | nosniff                                                                                                                                                                                                                                                            |
| X-Frame-Options              | DENY                                                                                                                                                                                                                                                               |

Our content security policy (CSP) differs from a more strict policy in three parameters: we allow unsafe-eval and
unsafe-inline for script-src, as
well as unsafe-inline for style-src.
While CSP is designed to disable certain features, we need to integrate a customizable voter portal and load WebAssembly
code, which depend on these
features.
It's important to note that for the Swiss Post Voting System, the CSP headers serve as a defense-in-depth mechanism.
However, verifiability and
privacy are not reliant on them.

Moreover, our productive infrastructure enforces best practices in server configuration such
as [DNSSEC](https://www.nic.ch/security/dnssec/)
, [OCSP](https://www.ietf.org/rfc/rfc2560.txt), and [CAA records](https://support.dnsimple.com/articles/caa-record/).

## Configuration

The Voter Portal needs some configuration keys to bootstrap properly. These configuration values are set
in `webapps/ROOT/assets/configuration/portalConfig.json`.

### Extended authentication factor

To render the corresponding extended authentication factor UI component, the `ìdentification` key must be configured
with `dob` or `yob`.

| Value | Description                                                      |
|-------|------------------------------------------------------------------|
| `dob` | "date of birth" rendered as a full date with `DD.MM.YYYY` format |
| `yob` | "year of birth" rendered as a date with `YYYY` format            |

```json5
{
  "identification": "dob"
}
```

### Contests capabilities

The contests capabilities are configured with the `contestsCapabilities` key.

| Contests capabilities node key | Data type | Description                                                             |
|--------------------------------|-----------|-------------------------------------------------------------------------|
| writeIns                       | `boolean` | `true` if the voter portal supports elections with write-in candidates. |

```json5
{
  "contestsCapabilities": {
    "writeIns": true
  }
}
```

### Translation Placeholders

The translations are customizable with the `translatePlaceholders` key.
All the values are mandatory, if a value is not defined for an election event please fill it an empty string `""`.

| Legal terms node key                                                     | Data type     | Description                                                                                                   |
|--------------------------------------------------------------------------|---------------|---------------------------------------------------------------------------------------------------------------|
| \<lang\>                                                                 | `Placeholder` | The language must be any of [`de`, `fr`, `it`, `rm`]. Contains a of `Placeholder`.                            |
| Placeholder.logo_title                                                   | `string`      | Title of the logo.                                                                                            |
| Placeholder.voting_card_1<br/>to Placeholder.voting_card_6               | `string`      | These placeholders define canton-specific variations of the term 'Voting Card'.                               |
| Placeholder.support                                                      | `string`      | The contact address for voter support.                                                                        |
| Placeholder.voting_card_remark_1<br/>to Placeholder.voting_card_remark_3 | `string`      | Placeholders designated for clarifications specific to each canton about the availability of voting material. |

```json5
{
  "translatePlaceholders": {
    "de": {
      "logo_title": "Die Post",
      "voting_card_1": "Stimmrechtsausweis",
      "voting_card_2": "dem Stimmrechtsausweis",
      "voting_card_3": "Dieser Stimmrechtsausweis",
      "voting_card_4": "der Stimmrechtsausweis",
      "voting_card_5": "Ihr Stimmrechtsausweis",
      "voting_card_6": "Karte 6",
      "support": "das [Supportteam](mailto:evoting@post.ch)",
      "voting_card_remark_1": "Dort erhalten Sie auch das Stimmmaterial für die Stimmabgabe per Brief oder an der Urne.",
      "voting_card_remark_2": "Das Stimmaterial für die Stimmabgabe per Brief oder an der Urne erhalten Sie bei <a href=\"https://www.evoting.ch\" target=\"_blank\">Ihrer Gemeinde</a>.",
      "voting_card_remark_3": "Solange Sie den Bestätigungscode noch nicht eingegeben haben, können Sie den Prozess der elektronischen Stimmabgabe abbrechen und stattdessen brieflich oder an der Urne abstimmen bzw. wählen. Das Stimmmaterial für die Stimmabgabe per Brief oder an der Urne erhalten Sie bei <a href=\"https://www.evoting.ch\" target=\"_blank\">Ihrer Gemeinde</a>."
    }
  }
}
```

To see where these keys are used you can have a look at the translation files `[LOCALE].json` located
in `webapps/ROOT/assets/i18n/`.

### Header Customization

The Voter Portal header can be customized to display for example a specific logo. Here are the customizable keys in
the `header` node.

| Header node key       | Data type                          | Description                                                                                                                 |
|-----------------------|------------------------------------|-----------------------------------------------------------------------------------------------------------------------------|
| logoPath              | `{desktop:string, mobile: string}` | Path to the logo displayed in the header. Each path is relative from `assets/configuration/img/`.                           |
| logoHeight            | `{desktop:number, mobile: number}` | Height of the logo in pixels.                                                                                               |
| reverse (optional)    | `boolean`                          | If `true` the header is reversed, the logo appears on the right. Default is `false`.                                        |
| background (optional) | `string`                           | CSS background definition (i.e. `linear-gradient(180deg,#f7f7f7 0,#ebebeb)`). Default is `none`.                            |
| bars (optional)       | `bar[]`                            | A `bar` represents a line displayed below the navigation bar. It has a `height` and a `color` properties. Default is empty. |
| bar.height            | `{desktop:number, mobile: number}` | Height of the bar in pixels.                                                                                                |
| bar.color             | `string`                           | Hexadecimal color value (i.e. `#000000`)                                                                                    |

```json5
{
  "header": {
    "logoPath": {
      "desktop": "post-logo.svg",
      "mobile": "post-logo.svg"
    },
    "logoHeight": {
      "desktop": 72,
      "mobile": 56
    },
    "reverse": false,
    "background": "#fff",
    "bars": [
      {
        "height": {
          "desktop": 2,
          "mobile": 2
        },
        "color": "#000000"
      }
    ]
  }
}
```

### Additional Legal Terms

By configuring an `additionalLegalTerms` node, you add additional legal terms to the Legal Terms page. These additional
legal terms are added on a
new section after the default legal terms.

Here are the customizable keys in the `additionalLegalTerms` node.

| Legal terms node key      | Data type   | Description                                                                                                                                                           |
|---------------------------|-------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| \<lang\>                  | `LegalTerm` | The language must be any of [`de`, `fr`, `it`, `rm`]. Contains a `LegalTerm`.                                                                                         |
| LegalTerm.stepper         | `string`    | Label of the legal terms step.                                                                                                                                        |
| LegalTerm.mainTitle       | `string`    | Title of the legal terms.                                                                                                                                             |
| LegalTerm.additionalTitle | `string`    | Title of additional the legal terms.                                                                                                                                  |
| LegalTerm.terms           | `string[]`  | Array of additional legal terms. Each term is displayed on a new line and can optionally be supplemented with the help of [Markdown](https://www.markdownguide.org/). |
| LegalTerm.confirm         | `string`    | Label of the additional legal terms confirmation checkbox.                                                                                                            |

```json5
{
  "additionalLegalTerms": {
    "de": {
      "stepper": "Label of the legal terms step",
      "mainTitle": "Title of the legal terms",
      "additionalTitle": "Title of the additional legal terms",
      "terms": [
        "First legal terms paragraph, you can write **bold** or *italic*.",
        "Second paragraph with a [link](https://www.your-website.com).",
        "Third paragraph with bullets \n - Bullet 1 \n - Bullet 2"
      ],
      "button": "Legal terms agreement confirmation checkbox label"
    },
    "fr": {
      ...
    },
    "it": {
      ...
    },
    "rm": {
      ...
    },
  }
}
```

### Additional FAQs

By configuring an `additionalFAQs` node, you add additional FAQ sections to the Help page. These additional FAQ sections
are added the top of the FAQ
sections list in the order defined in this configuration.

Here are the customizable keys in the `additionalFAQs` node.

| Legal terms node key      | Data type             | Description                                                                                                                                                                      |
|---------------------------|-----------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| \<lang\>                  | `FAQSectionContent[]` | The language must be any of [`de`, `fr`, `it`, `rm`]. Contains an array of `FAQSectionContent`.                                                                                  |
| FAQSectionContent.title   | `string`              | Title of the additional FAQ section.                                                                                                                                             |
| FAQSectionContent.content | `string[]`            | Array of additional FAQ section content. Each content is displayed on a new line and can optionally be supplemented with the help of [Markdown](https://www.markdownguide.org/). |

```json5
{
  "additionalFAQs": {
    "de": [
      {
        "title": "FAQ Title 1",
        "content": [
          "For example, you can write **bold** or *italic*.",
          "And add a second paragraph with a [link](https://www.your-website.com)",
          " - Also use bullet point. \n - With a second bullet point."
        ]
      },
      {
        "title": "FAQ Title 2",
        "content": [
          "For example, you can write **bold** or *italic*.",
          "And add a second paragraph with a [link](https://www.your-website.com)"
        ]
      },
      ...
    ],
    "fr": [
      ...
    ],
    "it": [
      ...
    ],
    "rm": [
      ...
    ],
  }
}
```

## Development

Check the build instructions in the readme of the repository 'evoting' for compiling the voter-portal.

The file `package.json` contains the script section for building and test the Voter Portal.
