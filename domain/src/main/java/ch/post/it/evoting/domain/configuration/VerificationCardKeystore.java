/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateBase64Encoded;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.util.List;

import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;

public record VerificationCardKeystore(String verificationCardId,
									   String verificationCardKeystore) implements HashableList {
	public VerificationCardKeystore {
		validateUUID(verificationCardId);
		validateBase64Encoded(verificationCardKeystore);
	}

	@Override
	public List<Hashable> toHashableForm() {
		return List.of(HashableString.from(verificationCardId), HashableString.from(verificationCardKeystore));
	}
}
