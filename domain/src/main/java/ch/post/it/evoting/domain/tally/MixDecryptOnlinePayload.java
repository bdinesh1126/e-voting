/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.tally;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.cryptoprimitives.utils.Validations.allEqual;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.stream.Collectors;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.evotinglibraries.domain.tally.ControlComponentBallotBoxPayload;

public record MixDecryptOnlinePayload(String electionEventId, String ballotBoxId,
									  List<ControlComponentBallotBoxPayload> controlComponentBallotBoxPayloads,
									  List<ControlComponentShufflePayload> controlComponentShufflePayloads) {

	public MixDecryptOnlinePayload {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);
		checkNotNull(controlComponentBallotBoxPayloads);
		checkNotNull(controlComponentShufflePayloads);

		checkArgument(NODE_IDS.size() == controlComponentBallotBoxPayloads.size(),
				"There must be exactly the expected number of control component ballot box payloads.");
		checkArgument(NODE_IDS.size() == controlComponentShufflePayloads.size(),
				"There must be exactly the expected number of control component shuffle payloads.");

		checkArgument(NODE_IDS.equals(controlComponentBallotBoxPayloads.stream()
				.map(ControlComponentBallotBoxPayload::getNodeId)
				.collect(Collectors.toSet())), "The node ids of the control component ballot box payloads must be part of the known node ids");
		checkArgument(NODE_IDS.equals(controlComponentShufflePayloads.stream()
				.map(ControlComponentShufflePayload::getNodeId)
				.collect(Collectors.toSet())), "The node ids of the control component shuffle payloads must be part of the known node ids");

		checkArgument(allEqual(controlComponentBallotBoxPayloads.stream(), ControlComponentBallotBoxPayload::getElectionEventId),
				"All control component ballot box payloads must have the same election event id.");
		checkArgument(allEqual(controlComponentBallotBoxPayloads.stream(), ControlComponentBallotBoxPayload::getBallotBoxId),
				"All control component ballot box payloads must have the same ballot box id.");
		checkArgument(allEqual(controlComponentBallotBoxPayloads.stream(), ControlComponentBallotBoxPayload::getEncryptionGroup),
				"All control component ballot box payloads must have the same encryption group.");
		checkArgument(allEqual(controlComponentBallotBoxPayloads.stream(), ControlComponentBallotBoxPayload::getConfirmedEncryptedVotes),
				"All control component ballot box payloads must have the same confirmed encrypted votes.");
		checkArgument(
				controlComponentBallotBoxPayloads.get(0).getEncryptionGroup().equals(controlComponentShufflePayloads.get(0).getEncryptionGroup()),
				"The encryption group of the control component ballot box payloads and the control component shuffle payloads must be the same.");

		checkArgument(allEqual(controlComponentShufflePayloads.stream(), ControlComponentShufflePayload::getElectionEventId),
				"All control component shuffle payloads must have the same election event id.");
		checkArgument(allEqual(controlComponentShufflePayloads.stream(), ControlComponentShufflePayload::getBallotBoxId),
				"All control component shuffle payloads must have the same ballot box id.");
		checkArgument(allEqual(controlComponentShufflePayloads.stream(), ControlComponentShufflePayload::getEncryptionGroup),
				"All control component shuffle payloads must have the same encryption group.");

		checkArgument(controlComponentBallotBoxPayloads.get(0).getElectionEventId().equals(electionEventId),
				"The election event id of the mix decrypt online payload and the control component ballot box payloads must be the same.");
		checkArgument(controlComponentShufflePayloads.get(0).getElectionEventId().equals(electionEventId),
				"The election event id of the mix decrypt online payload and the control component shuffle payloads must be the same.");

		checkArgument(controlComponentBallotBoxPayloads.get(0).getBallotBoxId().equals(ballotBoxId),
				"The ballot box id of the mix decrypt online payload and the control component ballot box payloads must be the same.");
		checkArgument(controlComponentShufflePayloads.get(0).getBallotBoxId().equals(ballotBoxId),
				"The  ballot box id of the mix decrypt online payload and the control component shuffle payloads must be the same.");
	}
}
