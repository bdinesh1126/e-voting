/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.signers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URL;
import java.nio.file.Path;
import java.security.SignatureException;
import java.util.function.BiConsumer;
import java.util.function.Function;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;

public abstract class XmlSigner<T> {

	private final SignatureKeystore<Alias> signatureKeystore;
	private final BiConsumer<T, byte[]> signatureSetter;
	private final Function<T, byte[]> signatureGetter;
	private final Function<T, Hashable> hashGetter;
	private final Function<T, Hashable> additionalContextDataHashGetter;
	private final Class<T> clazz;
	private final Alias verifyAlias;
	private final URL schema;

	protected XmlSigner(final SignatureKeystore<Alias> signatureKeystore,
			final BiConsumer<T, byte[]> signatureSetter,
			final Function<T, byte[]> signatureGetter,
			final Function<T, Hashable> hashGetter,
			final Function<T, Hashable> additionalContextDataHashGetter,
			final Alias verifyAlias,
			final Class<T> clazz,
			final URL schema) {
		this.signatureKeystore = signatureKeystore;
		this.signatureSetter = signatureSetter;
		this.signatureGetter = signatureGetter;
		this.hashGetter = hashGetter;
		this.additionalContextDataHashGetter = additionalContextDataHashGetter;
		this.clazz = clazz;
		this.verifyAlias = verifyAlias;
		this.schema = schema;
	}

	protected T load(final Path path) {
		try (final FileInputStream fis = new FileInputStream(path.toFile())) {
			final JAXBContext jaxbContext = JAXBContext.newInstance(this.clazz);
			final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			if (this.schema != null) {
				unmarshaller.setSchema(loadSchema(this.schema));
			}

			final SAXParserFactory spf = SAXParserFactory.newInstance();
			spf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			spf.setFeature("http://xml.org/sax/features/external-general-entities", false);
			spf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
			spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			spf.setNamespaceAware(true);
			final Source xmlSource = new SAXSource(spf.newSAXParser().getXMLReader(), new InputSource(fis));

			return unmarshaller.unmarshal(xmlSource, this.clazz).getValue();
		} catch (final FileNotFoundException | JAXBException | ParserConfigurationException | SAXException e) {
			throw new IllegalStateException(String.format("Unable to load the file. [filePath: %s]", path), e);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	private Schema loadSchema(final URL schemaUrl) {
		final SchemaFactory schemaFactory = SchemaFactory.newDefaultInstance();
		try {
			schemaFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			schemaFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "file");

			return schemaFactory.newSchema(schemaUrl);
		} catch (final SAXException e) {
			throw new IllegalStateException(String.format("Could not create new schema. [schemaUrl: %s]", schemaUrl), e);
		}

	}

	protected void save(final Path path, final T document) {
		try {
			final JAXBContext jaxbContext = JAXBContext.newInstance(this.clazz);
			jaxbContext.createMarshaller().marshal(document, path.toFile());
		} catch (final JAXBException e) {
			throw new IllegalStateException(String.format("Unable to save the file. [filePath: %s]", path), e);
		}
	}

	public void sign(final Path path) throws SignatureException {

		final T document = load(path);

		final Hashable hash = hashGetter.apply(document);
		final Hashable additionalHash = additionalContextDataHashGetter.apply(document);

		final byte[] signature = signatureKeystore.generateSignature(hash, additionalHash);

		signatureSetter.accept(document, signature);

		save(path, document);
	}

	public boolean verify(final Path path) throws SignatureException {
		final T document = load(path);

		final Hashable hash = hashGetter.apply(document);
		final Hashable additionalHash = additionalContextDataHashGetter.apply(document);
		final byte[] signature = signatureGetter.apply(document);

		return signatureKeystore.verifySignature(verifyAlias, hash, additionalHash, signature);
	}
}
