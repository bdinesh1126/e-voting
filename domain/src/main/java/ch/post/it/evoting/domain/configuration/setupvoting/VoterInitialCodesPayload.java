/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.hasNoDuplicates;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Objects;

public record VoterInitialCodesPayload(String electionEventId,
									   String verificationCardSetId,
									   List<VoterInitialCodes> voterInitialCodes) {

	public VoterInitialCodesPayload(
			final String electionEventId,
			final String verificationCardSetId,
			final List<VoterInitialCodes> voterInitialCodes) {

		this.electionEventId = validateUUID(electionEventId);
		this.verificationCardSetId = validateUUID(verificationCardSetId);
		this.voterInitialCodes = List.copyOf(checkNotNull(voterInitialCodes));

		checkArgument(!this.voterInitialCodes.isEmpty(), "The voter initial codes list must not be empty.");
		checkArgument(this.voterInitialCodes.stream().allMatch(Objects::nonNull),
				"The voter initial codes list must not contain null elements.");

		checkArgument(hasNoDuplicates(this.voterInitialCodes.stream().map(VoterInitialCodes::voterIdentification).toList()),
				"The list of voter initial codes must not contain any duplicate voter identifications.");
		checkArgument(hasNoDuplicates(this.voterInitialCodes.stream().map(VoterInitialCodes::votingCardId).toList()),
				"The list of voter initial codes must not contain any duplicate voting card ids.");
		checkArgument(hasNoDuplicates(this.voterInitialCodes.stream().map(VoterInitialCodes::verificationCardId).toList()),
				"The list of voter initial codes must not contain any duplicate verification card ids.");
	}

	@Override
	public List<VoterInitialCodes> voterInitialCodes() {
		return List.copyOf(voterInitialCodes);
	}

}
