/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

public record ControlComponentKeyGenerationRequestPayload(String electionEventId, GqGroup encryptionParameters) {

	public ControlComponentKeyGenerationRequestPayload {
		validateUUID(electionEventId);
		checkNotNull(encryptionParameters);
	}
}
