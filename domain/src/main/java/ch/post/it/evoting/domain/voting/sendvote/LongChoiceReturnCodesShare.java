/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.GqGroupVectorDeserializer;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableBigInteger;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;

public record LongChoiceReturnCodesShare(String electionEventId,
										 String verificationCardSetId,
										 String verificationCardId,
										 int nodeId,
										 @JsonDeserialize(using = GqGroupVectorDeserializer.class)
										 GroupVector<GqElement, GqGroup> longChoiceReturnCodeShare,
										 ExponentiationProof exponentiationProof) implements HashableList {

	public LongChoiceReturnCodesShare {
		checkArgument(NODE_IDS.contains(nodeId), "The node id must be part of the known node ids. [nodeId: %s]", nodeId);
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);
		checkNotNull(longChoiceReturnCodeShare);
		checkNotNull(exponentiationProof);
		checkArgument(longChoiceReturnCodeShare.getGroup().hasSameOrderAs(exponentiationProof.getGroup()),
				"The groups of the long choice return code share and the exponentiation proof must be of same order.");
	}

	@Override
	public List<Hashable> toHashableForm() {
		return List.of(
				HashableString.from(electionEventId),
				HashableString.from(verificationCardSetId),
				HashableString.from(verificationCardId),
				HashableBigInteger.from(BigInteger.valueOf(nodeId)),
				longChoiceReturnCodeShare,
				exponentiationProof);
	}

}
