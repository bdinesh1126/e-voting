/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */

package ch.post.it.evoting.domain.voting.confirmvote;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Locale;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;

class LongVoteCastReturnCodesShareTest {

	private static final Random random = RandomFactory.createRandom();
	private final GqGroup gqGroup = GroupTestData.getGqGroup();
	private final GqGroupGenerator gqGroupGenerator = new GqGroupGenerator(gqGroup);

	private final String electionEventId = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private final String verificationCardSetId = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private final String verificationCardId = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private final int nodeId = 1;

	private final GqElement longVoteCastReturnCodeShare = gqGroupGenerator.genMember();

	private final ZqElement randomZqElement = new ZqGroupGenerator(ZqGroup.sameOrderAs(gqGroup)).genRandomZqElementMember();
	private final ExponentiationProof exponentiationProof = new ExponentiationProof(randomZqElement, randomZqElement);

	@Nested
	@DisplayName("Check constructor validation")
	class CheckConstructor {
		@Test
		@DisplayName("Check null arguments")
		void nullArgs() {

			assertAll(
					() -> assertThrows(NullPointerException.class,
							() -> new LongVoteCastReturnCodesShare(null, verificationCardSetId, verificationCardId, nodeId,
									longVoteCastReturnCodeShare, exponentiationProof)),
					() -> assertThrows(NullPointerException.class,
							() -> new LongVoteCastReturnCodesShare(electionEventId, null, verificationCardId, nodeId,
									longVoteCastReturnCodeShare, exponentiationProof)),
					() -> assertThrows(NullPointerException.class,
							() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, null, nodeId,
									longVoteCastReturnCodeShare, exponentiationProof)),
					() -> assertThrows(NullPointerException.class,
							() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, nodeId,
									null, exponentiationProof)),
					() -> assertThrows(NullPointerException.class,
							() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, nodeId,
									longVoteCastReturnCodeShare, null)),

					() -> assertDoesNotThrow(
							() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, nodeId,
									longVoteCastReturnCodeShare, exponentiationProof))
			);
		}

		@Test
		@DisplayName("Check UUID format")
		void formatArgs() {

			final String invalidElectionEventId = "electionEventId";
			final String invalidVerificationCardSetId = "verificationCardSetId";
			final String invalidVerificationCardId = "verificationCardId";

			assertAll(
					() -> assertThrows(FailedValidationException.class,
							() -> new LongVoteCastReturnCodesShare(invalidElectionEventId, verificationCardSetId, verificationCardId, nodeId,
									longVoteCastReturnCodeShare, exponentiationProof)),
					() -> assertThrows(FailedValidationException.class,
							() -> new LongVoteCastReturnCodesShare(electionEventId, invalidVerificationCardSetId, verificationCardId, nodeId,
									longVoteCastReturnCodeShare, exponentiationProof)),
					() -> assertThrows(FailedValidationException.class,
							() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, invalidVerificationCardId, nodeId,
									longVoteCastReturnCodeShare, exponentiationProof))
			);
		}

		@Test
		@DisplayName("Check nodeIds in range 1 to 4")
		void nodeIsArgs() {

			assertAll(
					() -> assertThrows(IllegalArgumentException.class,
							() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 0,
									longVoteCastReturnCodeShare, exponentiationProof)),
					() -> assertDoesNotThrow(() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 1,
							longVoteCastReturnCodeShare, exponentiationProof)),
					() -> assertDoesNotThrow(() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 2,
							longVoteCastReturnCodeShare, exponentiationProof)),
					() -> assertDoesNotThrow(() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 3,
							longVoteCastReturnCodeShare, exponentiationProof)),
					() -> assertDoesNotThrow(() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 4,
							longVoteCastReturnCodeShare, exponentiationProof)),
					() -> assertThrows(IllegalArgumentException.class,
							() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 5,
									longVoteCastReturnCodeShare, exponentiationProof))
			);
		}

		@Test
		@DisplayName("Check all groups are of the same order")
		void checkGroupAreSameOrder() {
			final GqGroup differentGqGroup = GroupTestData.getDifferentGqGroup(gqGroup);
			final GqGroupGenerator gqDifferentGroupGenerator = new GqGroupGenerator(differentGqGroup);

			final GqElement longVoteCastReturnCodeShareDifferentGroup = gqDifferentGroupGenerator.genMember();

			final ZqElement randomZqElementDifferentGqGroup = new ZqGroupGenerator(ZqGroup.sameOrderAs(differentGqGroup)).genRandomZqElementMember();
			final ExponentiationProof exponentiationProofDifferentGroup = new ExponentiationProof(randomZqElementDifferentGqGroup,
					randomZqElementDifferentGqGroup);

			assertAll(
					() -> assertThrows(IllegalArgumentException.class,
							() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 0,
									longVoteCastReturnCodeShareDifferentGroup, exponentiationProof)),
					() -> assertThrows(IllegalArgumentException.class,
							() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 2,
									longVoteCastReturnCodeShare, exponentiationProofDifferentGroup))
			);
		}
	}

}
