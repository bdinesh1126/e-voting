/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.commandmessaging;

import static com.google.common.base.Preconditions.checkNotNull;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CommandService {

	private final CommandRepository commandRepository;

	public CommandService(final CommandRepository commandRepository) {
		this.commandRepository = commandRepository;
	}

	public void saveRequest(final CommandId commandId, final byte[] payload) {
		checkNotNull(commandId);
		checkNotNull(payload);

		final Command command = Command.builder()
				.commandId(commandId)
				.requestPayload(payload)
				.requestDateTime(LocalDateTime.now())
				.build();
		commandRepository.save(command);
	}

	public boolean isRequestForContextIdAndContextAlreadyPresent(final String contextId, final String context) {
		checkNotNull(contextId);
		checkNotNull(context);

		return commandRepository.existsByContextIdAndContext(contextId, context);
	}

	public boolean isRequestForContextIdAndContextAndNodeIdPresent(final String contextId, final String context, final Integer nodeId) {
		checkNotNull(contextId);
		checkNotNull(context);
		checkNotNull(nodeId);

		return commandRepository.existsByContextIdAndContextAndNodeId(contextId, context, nodeId);
	}

	public Command saveResponse(final CommandId commandId, final byte[] responsePayload) {
		checkNotNull(commandId);
		checkNotNull(responsePayload);

		final Command command = findCommand(commandId);
		command.setResponsePayload(responsePayload);
		command.setResponseDateTime(LocalDateTime.now());
		return commandRepository.save(command);
	}

	public Command save(final CommandId commandId, final byte[] requestPayload, final LocalDateTime requestDateTime, final byte[] responsePayload,
			final LocalDateTime responseDateTime) {
		checkNotNull(commandId);
		checkNotNull(requestPayload);
		checkNotNull(responsePayload);

		final Command command = Command.builder()
				.commandId(commandId)
				.requestPayload(requestPayload)
				.requestDateTime(requestDateTime)
				.responsePayload(responsePayload)
				.responseDateTime(responseDateTime)
				.build();
		return commandRepository.save(command);
	}

	private Command findCommand(final CommandId commandId) {
		checkNotNull(commandId);

		final Optional<Command> message = commandRepository.findById(commandId);
		return message.orElseThrow(() -> new IllegalStateException(String.format("Could not find a matching command. [CommandId: %s]", commandId)));
	}

	public List<Command> findAllCommandsWithCorrelationId(final String correlationId) {
		checkNotNull(correlationId);

		return commandRepository.findAllByCorrelationId(correlationId);
	}

	public List<Command> findAllCommandsWithResponsePayload(final String correlationId) {
		checkNotNull(correlationId);

		final List<Command> allByCorrelationIdAndResponsePayloadIsNotNull = commandRepository.findAllByCorrelationIdAndResponsePayloadIsNotNull(
				correlationId);
		return allByCorrelationIdAndResponsePayloadIsNotNull.stream()
				.sorted(Comparator.comparingInt(Command::getNodeId))
				.toList();
	}

	/**
	 * Count the number of commands with a response payload for this correlationId.
	 *
	 * @param correlationId the correlationId to aggregate on
	 * @return the count
	 */
	public Integer countAllCommandsWithResponsePayload(final String correlationId) {
		return commandRepository.countByCorrelationIdAndResponsePayloadIsNotNull(correlationId);
	}

	public Optional<Command> findIdenticalCommand(final CommandId commandId) {
		checkNotNull(commandId);

		return commandRepository.findById(commandId);
	}

	public List<Command> findSemanticallyIdenticalCommand(final CommandId commandId) {
		checkNotNull(commandId);

		final String contextId = commandId.getContextId();
		final String context = commandId.getContext();
		final Integer nodeId = commandId.getNodeId();

		return commandRepository.findAllByContextIdAndContextAndNodeId(contextId, context, nodeId);
	}

	public List<String> findAllDistinctContextIdsByContextAndContextIdLike(final Context context, final String contextId) {
		checkNotNull(context);
		checkNotNull(contextId);

		return commandRepository.findAllDistinctContextIdsByContextAndContextIdLike(context.toString(), contextId);
	}
}
