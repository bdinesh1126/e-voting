#!/bin/bash
#
# (c) Copyright 2022 Swiss Post Ltd.
#
#

while [[ $# -gt 0 ]]; do
  case $1 in
    -h|--help)
      echo "Usage: environment-checker.sh"
      echo "Checks that the needed tools in expected version for a successful build are installed."
      exit 0
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
  esac
done

check_version() {
  echo "$1" | egrep -q "$2" || die $3 "$4"
}

echo "Checking tools and versions. Please wait..."

check_version "$(java -version 2>&1)" 'version "17.0.8.1' 2 'java version'
check_version "$(mvn -v)" 'Apache Maven 3.9.1' 3 'maven version'
check_version "$(node -v)" 'v16.20.2' 3 'node version'
check_version "$(npm -v)" '8.19.4' 3 'npm version'

echo "All needed tools in expected versions are installed."
