FROM centos:7

USER root

RUN yum install glibc.i686 git bzip2 unzip -y

ARG JAVA_URL=https://github.com/adoptium/temurin17-binaries/releases/download/jdk-17.0.8.1%2B1/OpenJDK17U-jdk_x64_linux_hotspot_17.0.8.1_1.tar.gz
ARG MAVEN_URL=https://archive.apache.org/dist/maven/maven-3/3.9.1/binaries/apache-maven-3.9.1-bin.tar.gz
ARG NODE_URL=https://nodejs.org/dist/v16.20.2/node-v16.20.2-linux-x64.tar.xz
ARG CHROMIUM_URL=https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
ARG YUM_DOCKER_URL=https://download.docker.com/linux/centos/docker-ce.repo
ARG WINE_BINARIES_CENTOS7_URL=https://gitlab.com/swisspost-evoting/e-voting/e-voting/-/package_files/35844850/download
ARG USE_DOCKER_HOST

#JAVA
RUN mkdir -p /opt/customtools/java && curl -ksSL -o jdk.tar.gz $JAVA_URL && tar xvzf jdk.tar.gz -C /opt/customtools/java && rm jdk.tar.gz
ENV JAVA_HOME="/opt/customtools/java/jdk-17.0.8.1+1/"

#MAVEN
RUN mkdir -p /opt/customtools/maven && curl -ksSL -o maven.tar.gz $MAVEN_URL && tar xvzf maven.tar.gz -C /opt/customtools/maven && rm maven.tar.gz
ENV MAVEN_HOME="/opt/customtools/maven/apache-maven-3.9.1/"

#NODE
RUN mkdir -p /opt/customtools/node && curl -ksSL -o node.tar.xz $NODE_URL && tar -xf node.tar.xz -C /opt/customtools/node && rm node.tar.xz
ENV NODE_HOME="/opt/customtools/node/node-v16.20.2-linux-x64/"

#CHROMIUM
RUN curl -ksSL -o google-chrome.rpm $CHROMIUM_URL && yum install google-chrome.rpm -y && rm google-chrome.rpm

#WINE
RUN if [[ -n "$WINE_BINARIES_CENTOS7_URL" ]]; then cd /usr && curl -ksSL -o wine-binaries.tar.gz $WINE_BINARIES_CENTOS7_URL \
&& tar xvzf ./wine-binaries.tar.gz && rm wine-binaries.tar.gz; fi

ENV PATH="${JAVA_HOME}bin/:${MAVEN_HOME}bin/:${NODE_HOME}bin:${PATH}"

#OPENSSL
RUN yum install --setopt=sslverify=false openssl -y

RUN useradd baseuser

#DOCKER
RUN if [[ "$USE_DOCKER_HOST" == true ]]; then yum install -y yum-utils && yum-config-manager --setopt=sslverify=false --add-repo $YUM_DOCKER_URL \
&& yum install --setopt=sslverify=false -y docker-ce-cli && usermod -aG root baseuser; fi

USER baseuser

WORKDIR /home/baseuser

VOLUME [ "/home/baseuser/data" ]

RUN if [[ -n "$WINE_BINARIES_CENTOS7_URL" ]]; then wineboot; fi
RUN if [[ -n "$WINE_BINARIES_CENTOS7_URL" ]]; then wineserver -d; fi

COPY --chmod=755 build.sh .
COPY --chmod=755 environment-checker.sh .

ENTRYPOINT ["/bin/bash"]
