#!/usr/bin/env bash
#
# (c) Copyright 2022 Swiss Post Ltd.
#
#

set -o errexit

evoting_version="master"
fullexport="false"
repository_base_url="https://gitlab.com/swisspost-evoting/"
export_dir="/home/baseuser/data"

while [[ $# -gt 0 ]]; do
  case $1 in
    -v|--version)
      evoting_version="$2"
      shift
      shift
      ;;
    -fe|--full-export)
      fullexport="true"
      shift
      ;;
    -ms|--maven-settings)
      mvn_settings="-s $2"
      shift
      shift
      ;;
    -h|--help)
      echo "Usage: build.sh [OPTIONS]"
      echo "Runs the e-voting build process and hashes computation. Exports the result in an archive in ${export_dir}."
      echo "Note that this script performs the build omitting the execution of tests (unit and integration) such that it can be done faster."
      echo
      echo -e '-v, --version [VERSION] \t Provide a specific e-voting version to be cloned. Default is master branch as available on '${repository_base_url}e-voting/e-voting.
      echo -e '-fe, --full-export \t\t Activate full export in the final archive. Default content is e-voting, evoting-e2e-dev and hashes_256.txt.'
      echo -e '-ms, --maven-settings [PATH] \t Provide a custom maven settings file for the build process. Default is empty.'
      echo
      echo "WARN: This script is meant to be used in a Docker container running the evoting-build Docker image."
      exit 0
      ;;
    -*)
      echo "Unknown option $1"
      exit 1
      ;;
  esac
done

./environment-checker.sh

rm -rf e-voting
rm -rf evoting-e2e-dev
rm -rf crypto-primitives*
rm -rf e-voting-libraries

echo "Cloning e-voting $evoting_version branch..."

# Get into e-voting and get associated internal projects versions
if [ $evoting_version != "master" ]; then
  git clone -b e-voting-$evoting_version ${repository_base_url}e-voting/e-voting.git
else
  git clone -b $evoting_version ${repository_base_url}e-voting/e-voting.git
fi

crypto_primitives_version=$(grep -oP "(?<=<crypto-primitives.version\>).*(?=</crypto-primitives.version>)" e-voting/evoting-dependencies/pom.xml)
crypto_primitives_ts_version=$(grep -oP "(?<=<crypto-primitives-ts.version\>).*(?=</crypto-primitives-ts.version>)" e-voting/evoting-dependencies/pom.xml)
crypto_primitives_domain_version=$(grep -oP "(?<=<crypto-primitives-domain.version\>).*(?=</crypto-primitives-domain.version>)" e-voting/evoting-dependencies/pom.xml)
e_voting_libraries_version=$(grep -oP "(?<=<e-voting-libraries.version\>).*(?=</e-voting-libraries.version>)" e-voting/evoting-dependencies/pom.xml)
evoting_e2e_dev_version=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout -f e-voting/pom.xml)

evoting_e2e_dev_tagInfo="$(git ls-remote --tags ${repository_base_url}e-voting/evoting-e2e-dev.git | grep infrastructure-$evoting_e2e_dev_version)" || true

# Clone each associated crypto-primitives libraries in associated versions

# crypto-primitives
git clone -b crypto-primitives-$crypto_primitives_version ${repository_base_url}crypto-primitives/crypto-primitives.git

# crypto-primitives-ts
git clone -b crypto-primitives-ts-$crypto_primitives_ts_version ${repository_base_url}crypto-primitives/crypto-primitives-ts.git

# crypto-primitives-domain
git clone -b crypto-primitives-domain-$crypto_primitives_domain_version ${repository_base_url}crypto-primitives/crypto-primitives-domain.git

# check no mismatch between crypto-primitives version in evoting vs in crypto-primitives-domain. Stops otherwise.
crypto_primitives_version_from_domain=$(grep -oP "(?<=<crypto-primitives.version\>).*(?=</crypto-primitives.version>)" crypto-primitives-domain/pom.xml)
if [ "$crypto_primitives_version_from_domain" != "$crypto_primitives_version" ]; then
	echo "Mismatch of crypto-primitives versions!"
	exit 1
fi

# Clone e-voting-libraries
git clone -b e-voting-libraries-$e_voting_libraries_version ${repository_base_url}e-voting/e-voting-libraries.git

# Build
# Maven options to clean install while skipping tests execution and ensuring parallel build where possible.
# The command avoids also the transfer progress logging and makes maven outputs quiet.
maven_options="clean install -DskipTests -T 1.5C --no-transfer-progress -q $mvn_settings"

echo "Building crypto-primitives $crypto_primitives_version. Please wait..."
mvn $maven_options -f crypto-primitives
echo "Crypto-primitives successfully built!"

echo "Building crypto-primitives-ts $crypto_primitives_ts_version. Please wait..."
mvn $maven_options -f crypto-primitives-ts
echo "Crypto-primitives-ts successfully built!"

echo "Building crypto-primitives-domain $crypto_primitives_domain_version. Please wait..."
mvn $maven_options -f crypto-primitives-domain
echo "Crypto-primitives-domain successfully built!"

echo "Building e-voting-libraries $e_voting_libraries_version. Please wait..."
mvn $maven_options -f e-voting-libraries
echo "E-voting-libraries successfully built!"

echo "Building e-voting $evoting_version. Please wait..."
mvn $maven_options -f e-voting
echo "E-voting successfully built!"

hash_filename="hashes_256.txt"
echo "Creating $hash_filename file. Please wait..."
find_file_selection=" -not -path "*/.m2/*" -not -path "*/archive-tmp/*" -a
(
-name "control-component-runnable*.jar"
-o -name "voting-server-*-runnable.jar"
-o -name "secure-data-manager-package*.zip"
-o -name "voter-portal-*.zip"
-o -name "config-cryptographic-parameters-tool-*.jar"
-o -name "xml-signature-*.jar"
-o -wholename "*/voter-portal/dist/crypto.ov-api.js"
-o -wholename "*/voter-portal/dist/crypto.ov-worker.js"
-o -wholename "*/voter-portal/dist/main.js"
-o -wholename "*/voter-portal/dist/polyfills.js"
-o -wholename "*/voter-portal/dist/runtime.js"
)"
true > $hash_filename
find e-voting \( $find_file_selection \) -exec sha256sum {} >> $hash_filename \;

# Archive result of builds, computed hashes and evoting-e2e-dev into a build.tar.gz file.
echo "Creating the archive of builds and hashes into build.tar.gz. Please wait..."

rm -rf "e-voting/voter-portal/node_modules/.bin/tao"
rm -rf "e-voting/voter-portal/node_modules/.bin/resolve"
rm -rf "e-voting/voter-portal/node_modules/.bin/import-local-fixture"
rm -rf "e-voting/voter-portal/node_modules/.bin/json5"

export_default_folders="e-voting/ $hash_filename"

if [ -n "$evoting_e2e_dev_tagInfo" ]; then
  # Clone evoting-e2e-dev
  echo "A compatible version of the end-to-end is available."
  git clone -b infrastructure-$evoting_e2e_dev_version ${repository_base_url}e-voting/evoting-e2e-dev.git
  export_default_folders="${export_default_folders} evoting-e2e-dev/"
  mv evoting-e2e-dev/scripts/e2e.sh ${export_dir}
else
  echo "WARN: A compatible version of end-to-end is not available. Skipping the clone of evoting-e2e-dev."
fi

if [ $fullexport == "true" ]; then
  tar czfh build.tar.gz $export_default_folders crypto-primitives/ crypto-primitives-ts/ crypto-primitives-domain/ e-voting-libraries/
else
  tar czfh build.tar.gz $export_default_folders
fi

mv build.tar.gz ${export_dir}

echo "Creation of the archive complete! Please find it in ${export_dir}."
