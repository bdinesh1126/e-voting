/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.voting;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.commandmessaging.Command;
import ch.post.it.evoting.commandmessaging.CommandId;
import ch.post.it.evoting.commandmessaging.CommandService;
import ch.post.it.evoting.commandmessaging.Context;

/**
 * Provides reusability and transactionality for command service.
 */
@Service
public class CommandFacade {

	private static final Logger LOGGER = LoggerFactory.getLogger(CommandFacade.class);

	private final CommandService commandService;

	public CommandFacade(final CommandService commandService) {
		this.commandService = commandService;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void saveRequestInNewTransaction(final byte[] payload, final String correlationId, final String contextId, final Context context,
			final int nodeId) {
		saveRequest(payload, correlationId, contextId, context, nodeId);
	}

	@Transactional
	public void saveRequest(final byte[] payload, final String correlationId, final String contextId, final Context context,
			final int nodeId) {
		final CommandId commandId = CommandId.builder()
				.contextId(contextId)
				.context(context.toString())
				.correlationId(correlationId)
				.nodeId(nodeId)
				.build();
		commandService.saveRequest(commandId, payload);
		LOGGER.info("Saved request. [context: {}, contextId: {}, correlationId: {}, nodeId: {}]", context, contextId, correlationId, nodeId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void saveResponse(final byte[] encodedResponse, final String correlationId, final String contextId, final Context context,
			final int nodeId) {
		if (!commandService.isRequestForContextIdAndContextAndNodeIdPresent(contextId, context.toString(), nodeId)) {
			LOGGER.warn("No request corresponding to this response found. [contextId: {}, correlationId: {}, nodeId: {}]", contextId, correlationId,
					nodeId);
		}

		final CommandId commandId = CommandId.builder()
				.contextId(contextId)
				.context(context.toString())
				.correlationId(correlationId)
				.nodeId(nodeId)
				.build();
		commandService.saveResponse(commandId, encodedResponse);

		LOGGER.info("Saved response [contextId: {}, correlationId: {}, nodeId: {}]", contextId, correlationId, nodeId);
	}

	/**
	 * Get all node responses.
	 *
	 * @param correlationId the id for which to search for the responses.
	 * @return the list of commands-
	 * @throws IllegalStateException if the number of commands found is not the expected.
	 */
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public List<Command> getAllNodesResponses(final String correlationId) {
		final List<Command> allMessagesWithCorrelationId = commandService.findAllCommandsWithResponsePayload(correlationId);
		if (allMessagesWithCorrelationId.size() != NODE_IDS.size()) {
			throw new IllegalStateException(String.format("The number of node response is invalid. [expected: %s, actual: %s]",
					NODE_IDS.size(), allMessagesWithCorrelationId.size()));
		}
		return allMessagesWithCorrelationId;
	}

	@Transactional
	public List<String> getAllDistinctContextIdsByContextAndContextIdLike(final Context context, final String contextId) {
		checkNotNull(context);
		checkNotNull(contextId);

		return commandService.findAllDistinctContextIdsByContextAndContextIdLike(context, contextId);
	}
}
