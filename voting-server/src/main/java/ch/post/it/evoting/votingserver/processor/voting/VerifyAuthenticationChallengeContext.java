/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Regroups the context values needed by the VerifyAuthenticationChallenge algorithm.
 */
public record VerifyAuthenticationChallengeContext(String electionEventId, AuthenticationStep authenticationStep) {

	public VerifyAuthenticationChallengeContext {
		validateUUID(electionEventId);
		checkNotNull(authenticationStep);
	}

}
