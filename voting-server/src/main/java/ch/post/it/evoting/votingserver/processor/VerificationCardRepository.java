/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface VerificationCardRepository extends CrudRepository<VerificationCardEntity, String> {

	Optional<VerificationCardEntity> findByVotingCardId(final String votingCardId);

	@Transactional(isolation = Isolation.SERIALIZABLE)
	@Query("select new ch.post.it.evoting.votingserver.processor.UsedVotingCardDTO("
			+ "vc.verificationCardSetEntity.electionEventEntity.electionEventId, "
			+ "vc.verificationCardSetEntity.verificationCardSetId, "
			+ "vc.verificationCardId, "
			+ "vc.votingCardId, "
			+ "vcst.state, "
			+ "vcst.stateDate) "
			+ "from VerificationCardStateEntity vcst "
			+ "join VerificationCardEntity vc on vcst.verificationCardId = vc.verificationCardId "
			+ "where vc.verificationCardSetEntity.electionEventEntity.electionEventId like %?1% and vcst.state <> ch.post.it.evoting.votingserver.processor.VerificationCardState.INITIAL")
	List<UsedVotingCardDTO> findAllUsedByElectionEventId(final String electionEventId);

	Optional<VerificationCardEntity> findByCredentialId(final String credentialId);

	@Transactional(isolation = Isolation.SERIALIZABLE)
	@Query("select v from VerificationCardEntity v where v.votingCardId LIKE %?1%")
	List<VerificationCardEntity> findAllByContainingPartialId(final String partialVotingCardId);

	@Transactional(isolation = Isolation.SERIALIZABLE)
	@Query("select count(v) from VerificationCardEntity v where v.votingCardId LIKE %?1%")
	long countAllByPartialVotingCardId(final String partialVotingCardId);

}