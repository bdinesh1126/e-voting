/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Converter
public class SuccessfulAuthenticationAttemptsConverter implements AttributeConverter<SuccessfulAuthenticationAttempts, byte[]> {

	private final ObjectMapper objectMapper;

	public SuccessfulAuthenticationAttemptsConverter(final ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	@Override
	public byte[] convertToDatabaseColumn(final SuccessfulAuthenticationAttempts successfulAuthenticationAttempts) {
		checkNotNull(successfulAuthenticationAttempts);

		try {
			return objectMapper.writeValueAsBytes(successfulAuthenticationAttempts);
		} catch (JsonProcessingException e) {
			throw new UncheckedIOException(String.format("Failed to serialize authentication challenges: %s", successfulAuthenticationAttempts), e);
		}
	}

	@Override
	public SuccessfulAuthenticationAttempts convertToEntityAttribute(final byte[] bytes) {
		checkNotNull(bytes);

		try {
			return objectMapper.readValue(bytes, SuccessfulAuthenticationAttempts.class);
		} catch (IOException e) {
			throw new UncheckedIOException("Failed to deserialize authentication challenge.", e);
		}
	}
}
