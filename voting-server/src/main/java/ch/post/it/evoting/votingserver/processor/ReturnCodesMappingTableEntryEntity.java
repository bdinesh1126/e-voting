/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "RETURN_CODES_MAPPING_TABLE_ENTRY")
public class ReturnCodesMappingTableEntryEntity {

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "VERIFICATION_CARD_SET_ID", referencedColumnName = "VERIFICATION_CARD_SET_ID")
	private VerificationCardSetEntity verificationCardSetEntity;

	@Id
	private String hashedLongReturnCode;

	private String encryptedShortReturnCode;

	@Version
	private Integer changeControlId;

	public ReturnCodesMappingTableEntryEntity() {
	}

	public ReturnCodesMappingTableEntryEntity(final VerificationCardSetEntity verificationCardSetEntity, final String hashedLongReturnCode,
			final String encryptedShortReturnCode) {
		this.verificationCardSetEntity = checkNotNull(verificationCardSetEntity);
		this.hashedLongReturnCode = checkNotNull(hashedLongReturnCode);
		this.encryptedShortReturnCode = checkNotNull(encryptedShortReturnCode);
	}

}
