/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.voting.sendvote;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.util.Arrays;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentPartialDecryptPayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.votingserver.orchestrator.aggregator.AggregatorService;
import ch.post.it.evoting.votingserver.orchestrator.voting.CommandFacade;

@Component
public class ReturnCodesPartialDecryptContributionsConsumer {

	private final CommandFacade commandFacade;
	private final ObjectMapper objectMapper;
	private final AggregatorService aggregatorService;

	public ReturnCodesPartialDecryptContributionsConsumer(final CommandFacade commandFacade,
			final ObjectMapper objectMapper,
			final AggregatorService aggregatorService) {
		this.commandFacade = commandFacade;
		this.objectMapper = objectMapper;
		this.aggregatorService = aggregatorService;
	}

	@RabbitListener(queues = "#{queueNameResolver.get(\"PARTIAL_DECRYPT_PCC_RESPONSE_PATTERN\")}")
	public void consumer(final Message message) throws IOException {

		final String correlationId = message.getMessageProperties().getCorrelationId();
		checkNotNull(correlationId, "Correlation Id should not be null");

		final byte[] encodedResponse = message.getBody();
		final ControlComponentPartialDecryptPayload controlComponentPartialDecryptPayload = objectMapper.readValue(encodedResponse,
				ControlComponentPartialDecryptPayload.class);
		final ContextIds contextIds = controlComponentPartialDecryptPayload.getPartiallyDecryptedEncryptedPCC().contextIds();
		final String contextId = String.join("-",
				Arrays.asList(contextIds.electionEventId(), contextIds.verificationCardSetId(),
						contextIds.verificationCardId()));
		final int nodeId = controlComponentPartialDecryptPayload.getPartiallyDecryptedEncryptedPCC().nodeId();

		commandFacade.saveResponse(encodedResponse, correlationId, contextId, Context.VOTING_RETURN_CODES_PARTIAL_DECRYPT_PCC, nodeId);
		aggregatorService.notifyPartialResponseReceived(correlationId, contextId);
	}

}
