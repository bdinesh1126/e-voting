/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.domain.SharedQueue.PARTIAL_DECRYPT_PCC_REQUEST_PATTERN;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Comparator;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.VotingServerEncryptedVotePayload;
import ch.post.it.evoting.votingserver.orchestrator.voting.BroadcastCommand;
import ch.post.it.evoting.votingserver.orchestrator.voting.BroadcastCommandProducer;

@Service
public class ReturnCodesPartialDecryptContributionsService {

	private final ObjectMapper objectMapper;
	private final BroadcastCommandProducer broadcastCommandProducer;

	public ReturnCodesPartialDecryptContributionsService(
			final ObjectMapper objectMapper,
			final BroadcastCommandProducer broadcastCommandProducer) {
		this.objectMapper = objectMapper;
		this.broadcastCommandProducer = broadcastCommandProducer;
	}

	public List<ControlComponentPartialDecryptPayload> getChoiceReturnCodesPartialDecryptContributions(final String electionEventId,
			final String verificationCardSetId, final String verificationCardId,
			final VotingServerEncryptedVotePayload votingServerEncryptedVotePayload) {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);
		checkNotNull(votingServerEncryptedVotePayload);

		final String contextId = String.join("-", electionEventId, verificationCardSetId, verificationCardId);

		final BroadcastCommand<ControlComponentPartialDecryptPayload> broadcastCommand = new BroadcastCommand.Builder<ControlComponentPartialDecryptPayload>()
				.contextId(contextId)
				.context(Context.VOTING_RETURN_CODES_PARTIAL_DECRYPT_PCC)
				.payload(votingServerEncryptedVotePayload)
				.pattern(PARTIAL_DECRYPT_PCC_REQUEST_PATTERN)
				.deserialization(this::deserializePayload)
				.build();

		final List<ControlComponentPartialDecryptPayload> controlComponentPartialDecryptPayloads = broadcastCommandProducer.sendMessagesAwaitingNotification(
				broadcastCommand);

		return controlComponentPartialDecryptPayloads.stream()
				.sorted(Comparator.comparingInt(
						controlComponentPartialDecryptPayload -> controlComponentPartialDecryptPayload.getPartiallyDecryptedEncryptedPCC().nodeId()))
				.toList();
	}

	private ControlComponentPartialDecryptPayload deserializePayload(final byte[] payload) {
		try {
			return objectMapper.readValue(payload, ControlComponentPartialDecryptPayload.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

}
