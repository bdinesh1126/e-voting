/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.tally;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.domain.SharedQueue.MIX_DEC_ONLINE_REQUEST_PATTERN;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.domain.tally.MixDecryptOnlineRequestPayload;
import ch.post.it.evoting.votingserver.orchestrator.voting.CommandFacade;

@Service
public class MixDecryptOnlineProducer {

	private static final Logger LOGGER = LoggerFactory.getLogger(MixDecryptOnlineProducer.class);

	private static final String RABBITMQ_EXCHANGE = "evoting-exchange";

	private final RabbitTemplate rabbitTemplate;
	private final ObjectMapper objectMapper;
	private final CommandFacade commandFacade;

	public MixDecryptOnlineProducer(final ObjectMapper objectMapper, final RabbitTemplate rabbitTemplate,
			final CommandFacade commandFacade) {
		this.objectMapper = objectMapper;
		this.rabbitTemplate = rabbitTemplate;
		this.commandFacade = commandFacade;
	}

	@Transactional
	public void initialSend(final String electionEventId, final String ballotBoxId) {
		final String correlationId = UUID.randomUUID().toString();
		final int initialNodeId = 1;

		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		send(electionEventId, ballotBoxId, correlationId, initialNodeId, Collections.emptyList());
	}

	@Transactional
	public void send(final String electionEventId, final String ballotBoxId, final String correlationId, final int nodeId,
			final List<ControlComponentShufflePayload> shufflePayloads) {

		validateUUID(electionEventId);
		validateUUID(ballotBoxId);
		checkNotNull(correlationId);
		checkNotNull(shufflePayloads);
		checkArgument(NODE_IDS.contains(nodeId));

		final String contextId = String.join("-", Arrays.asList(electionEventId, ballotBoxId));

		final MixDecryptOnlineRequestPayload mixDecryptOnlineRequestPayload =
				new MixDecryptOnlineRequestPayload(electionEventId, ballotBoxId, nodeId, shufflePayloads);

		final byte[] payload;
		try {
			payload = objectMapper.writeValueAsBytes(mixDecryptOnlineRequestPayload);

			commandFacade.saveRequest(payload, correlationId, contextId, Context.MIXING_TALLY_MIX_DEC_ONLINE, nodeId);
		} catch (final IOException e) {
			throw new UncheckedIOException("Failed to process the mixing DTO", e);
		}

		final MessageProperties messageProperties = new MessageProperties();
		messageProperties.setCorrelationId(correlationId);
		final Message message = new Message(payload, messageProperties);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, MIX_DEC_ONLINE_REQUEST_PATTERN + nodeId, message);
		LOGGER.info("Sent request [node: {}, correlationId:{}]", nodeId, correlationId);
	}
}
