/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator;

import java.util.List;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponentlVCCSharePayload;
import ch.post.it.evoting.domain.voting.confirmvote.VotingServerConfirmPayload;
import ch.post.it.evoting.domain.voting.sendvote.CombinedControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentLCCSharePayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.VotingServerEncryptedVotePayload;
import ch.post.it.evoting.votingserver.orchestrator.configuration.setupvoting.ElectionEventContextService;
import ch.post.it.evoting.votingserver.orchestrator.configuration.setupvoting.SetupComponentPublicKeysService;
import ch.post.it.evoting.votingserver.orchestrator.voting.confirmvote.LongVoteCastReturnCodesSharesService;
import ch.post.it.evoting.votingserver.orchestrator.voting.sendvote.ReturnCodesLCCShareContributionsService;
import ch.post.it.evoting.votingserver.orchestrator.voting.sendvote.ReturnCodesPartialDecryptContributionsService;

@Service
public class OrchestratorFacade {

	private final ElectionEventContextService electionEventContextService;
	private final SetupComponentPublicKeysService setupComponentPublicKeysService;
	private final LongVoteCastReturnCodesSharesService longVoteCastReturnCodesSharesService;
	private final ReturnCodesLCCShareContributionsService returnCodesLCCShareContributionsService;
	private final ReturnCodesPartialDecryptContributionsService returnCodesPartialDecryptContributionsService;

	public OrchestratorFacade(
			final ElectionEventContextService electionEventContextService,
			final SetupComponentPublicKeysService setupComponentPublicKeysService,
			final LongVoteCastReturnCodesSharesService longVoteCastReturnCodesSharesService,
			final ReturnCodesLCCShareContributionsService returnCodesLCCShareContributionsService,
			final ReturnCodesPartialDecryptContributionsService returnCodesPartialDecryptContributionsService) {
		this.electionEventContextService = electionEventContextService;
		this.setupComponentPublicKeysService = setupComponentPublicKeysService;
		this.longVoteCastReturnCodesSharesService = longVoteCastReturnCodesSharesService;
		this.returnCodesLCCShareContributionsService = returnCodesLCCShareContributionsService;
		this.returnCodesPartialDecryptContributionsService = returnCodesPartialDecryptContributionsService;
	}

	public List<ControlComponentlVCCSharePayload> getLongVoteCastReturnCodesContributions(final String electionEventId,
			final String verificationCardSetId, final String verificationCardId, final VotingServerConfirmPayload votingServerConfirmPayload) {

		return longVoteCastReturnCodesSharesService.getLongVoteCastReturnCodesContributions(electionEventId, verificationCardSetId,
				verificationCardId, votingServerConfirmPayload);
	}

	public List<ControlComponentPartialDecryptPayload> getChoiceReturnCodesPartialDecryptContributions(final String electionEventId,
			final String verificationCardSetId, final String verificationCardId,
			final VotingServerEncryptedVotePayload votingServerEncryptedVotePayload) {

		return returnCodesPartialDecryptContributionsService.getChoiceReturnCodesPartialDecryptContributions(electionEventId,
				verificationCardSetId, verificationCardId, votingServerEncryptedVotePayload);
	}

	public List<ControlComponentLCCSharePayload> getLongChoiceReturnCodesContributions(final String electionEventId,
			final String verificationCardSetId, final String verificationCardId,
			final CombinedControlComponentPartialDecryptPayload combinedControlComponentPartialDecryptPayload) {

		return returnCodesLCCShareContributionsService.getLongChoiceReturnCodesContributions(electionEventId, verificationCardSetId,
				verificationCardId, combinedControlComponentPartialDecryptPayload);
	}

	public void uploadElectionEventContext(final String electionEventId, final ElectionEventContextPayload electionEventContextPayload) {
		electionEventContextService.uploadElectionEventContextPayload(electionEventId, electionEventContextPayload);
	}

	public void uploadSetupComponentPublicKeys(final String electionEventId, final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload) {
		setupComponentPublicKeysService.uploadSetupComponentPublicKeys(electionEventId, setupComponentPublicKeysPayload);
	}

}
