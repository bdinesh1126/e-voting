/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import java.util.List;

import ch.post.it.evoting.votingserver.processor.voting.AuthenticationStep;
import ch.post.it.evoting.votingserver.processor.voting.VerifyAuthenticationChallengeException;
import ch.post.it.evoting.votingserver.processor.voting.VerifyAuthenticationChallengeOutput;

public enum VerificationCardState {
	INITIAL,
	SENT,
	CONFIRMING,
	CONFIRMED,
	BLOCKED,
	AUTHENTICATION_ATTEMPTS_EXCEEDED,
	CONFIRMATION_ATTEMPTS_EXCEEDED;

	private static final List<VerificationCardState> ALLOWED_STATES = List.of(VerificationCardState.INITIAL, VerificationCardState.SENT,
			VerificationCardState.CONFIRMING, VerificationCardState.CONFIRMED);

	public boolean isInactive() {
		return BLOCKED.equals(this) || AUTHENTICATION_ATTEMPTS_EXCEEDED.equals(this) || CONFIRMATION_ATTEMPTS_EXCEEDED.equals(this);
	}

	public static void validateVerificationCardState(final AuthenticationStep authenticationStep, final VerificationCardState verificationCardState) {
		// If the verification card is in an inactive state, return blocked.
		if (verificationCardState.isInactive()) {
			throw new VerifyAuthenticationChallengeException(
					VerifyAuthenticationChallengeOutput.VerifyAuthenticationChallengeStatus.VOTING_CARD_BLOCKED);
		}

		// Otherwise check state is coherent with current step.
		switch (authenticationStep) {
		case AUTHENTICATE_VOTER -> {
			if (!ALLOWED_STATES.contains(verificationCardState)) {
				throw new VerifyAuthenticationChallengeException(
						VerifyAuthenticationChallengeOutput.VerifyAuthenticationChallengeStatus.VOTING_CARD_BLOCKED);
			}
		}
		case SEND_VOTE -> {
			if (!VerificationCardState.INITIAL.equals(verificationCardState)) {
				throw new VerifyAuthenticationChallengeException(
						VerifyAuthenticationChallengeOutput.VerifyAuthenticationChallengeStatus.AUTHENTICATION_CHALLENGE_ERROR);
			}
		}
		case CONFIRM_VOTE -> {
			if (!VerificationCardState.SENT.equals(verificationCardState) && !VerificationCardState.CONFIRMING.equals(verificationCardState)) {
				throw new VerifyAuthenticationChallengeException(
						VerifyAuthenticationChallengeOutput.VerifyAuthenticationChallengeStatus.AUTHENTICATION_CHALLENGE_ERROR);
			}
		}
		}
	}

}
