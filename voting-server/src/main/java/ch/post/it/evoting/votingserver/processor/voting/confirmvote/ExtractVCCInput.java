/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting.confirmvote;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.votingserver.processor.voting.confirmvote.ExtractVCCAlgorithm.NUMBER_OF_CONTROL_COMPONENTS;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.votingserver.processor.voting.ReturnCodesMappingTable;

/**
 * Regroups the inputs needed by the ExtractVCC algorithm.
 */
public record ExtractVCCInput(GroupVector<GqElement, GqGroup> longVoteCastReturnCodeShares, String verificationCardId,
							  ReturnCodesMappingTable returnCodesMappingTable) {

	/**
	 * @param longVoteCastReturnCodeShares (lCC<sub>1,id</sub>, lCC<sub>2,id</sub>, lCC<sub>3,id</sub>, lCC<sub>4,id</sub>) ∈
	 *                                     G<sub>q</sub><sup>4</sup>, CCR long Vote Cast Return Code shares.
	 * @param verificationCardId           vc<sub>id</sub>, the verification card id.
	 * @param returnCodesMappingTable      CMtable, the Return Codes Mapping Table.
	 * @throws NullPointerException      if any of the fields is null.
	 * @throws IllegalArgumentException  if the {@code longVoteCastReturnCodeShares} size is not
	 *                                   {@value ExtractVCCAlgorithm#NUMBER_OF_CONTROL_COMPONENTS}.
	 * @throws FailedValidationException if the {@code verificationCardId} do not comply the UUID format.
	 */
	public ExtractVCCInput {
		checkNotNull(longVoteCastReturnCodeShares);
		validateUUID(verificationCardId);
		checkNotNull(returnCodesMappingTable);

		final GroupVector<GqElement, GqGroup> longVoteCastReturnCodeSharesCopy = GroupVector.from(longVoteCastReturnCodeShares);

		checkArgument(longVoteCastReturnCodeSharesCopy.size() == NUMBER_OF_CONTROL_COMPONENTS,
				String.format("There must be long Vote Cast Return Code shares from %s control-components.", NUMBER_OF_CONTROL_COMPONENTS));

		longVoteCastReturnCodeShares = longVoteCastReturnCodeSharesCopy;
	}

	@Override
	public GroupVector<GqElement, GqGroup> longVoteCastReturnCodeShares() {
		return GroupVector.from(longVoteCastReturnCodeShares);
	}

	public GqGroup getGroup() {
		return this.longVoteCastReturnCodeShares.get(0).getGroup();
	}

}
