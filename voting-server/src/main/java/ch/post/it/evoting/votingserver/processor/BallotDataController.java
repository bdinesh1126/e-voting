/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.domain.configuration.BallotDataPayload;
import ch.post.it.evoting.votingserver.common.Constants;

/**
 * Web service for handling ballot data resource.
 */
@RestController
@RequestMapping("api/v1/processor/configuration/setupvoting/ballotdata")
public class BallotDataController {

	private static final Logger LOGGER = LoggerFactory.getLogger(BallotDataController.class);

	private final BallotDataService ballotDataService;

	public BallotDataController(final BallotDataService ballotDataService) {
		this.ballotDataService = ballotDataService;
	}

	/**
	 * Save data for a ballot given the tenant, the election event id and the ballot id.
	 *
	 * @param electionEventId   - the election event identifier.
	 * @param ballotId          - the ballot identifier.
	 * @param ballotDataPayload - the data for a ballot.
	 */
	@PostMapping("electionevent/{electionEventId}/ballot/{ballotId}")
	public void saveBallotData(
			@PathVariable(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@PathVariable(Constants.PARAMETER_VALUE_BALLOT_ID)
			final String ballotId,
			@RequestBody
			final BallotDataPayload ballotDataPayload) {

		validateUUID(electionEventId);
		validateUUID(ballotId);

		ballotDataService.saveBallotData(ballotDataPayload);
		LOGGER.info("Ballot data saved. [electionEventId: {}, ballotId: {}].", electionEventId, ballotId);
	}

}
