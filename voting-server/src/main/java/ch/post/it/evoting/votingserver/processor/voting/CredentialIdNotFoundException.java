/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting;

public class CredentialIdNotFoundException extends RuntimeException {

	public CredentialIdNotFoundException(final String message) {
		super(message);
	}

}
