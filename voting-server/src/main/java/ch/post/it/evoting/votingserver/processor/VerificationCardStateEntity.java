/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "VERIFICATION_CARD_STATE")
public class VerificationCardStateEntity {

	@Id
	private String verificationCardId;

	@Convert(converter = ListConverter.class)
	private List<String> shortChoiceReturnCodes;

	private String shortVoteCastReturnCode;

	private VerificationCardState state;

	private int authenticationAttempts;

	private long lastSuccessfulAuthenticationTimeStep;

	@Convert(converter = SuccessfulAuthenticationAttemptsConverter.class)
	private SuccessfulAuthenticationAttempts successfulAuthenticationAttempts;

	private int confirmationAttempts;

	private LocalDateTime stateDate;

	@Version
	private Integer changeControlId;

	public VerificationCardStateEntity() {
	}

	public VerificationCardStateEntity(final String verificationCardId) {
		this.verificationCardId = verificationCardId;
		this.state = VerificationCardState.INITIAL;
		this.authenticationAttempts = 0;
		this.lastSuccessfulAuthenticationTimeStep = 0;
		this.successfulAuthenticationAttempts = new SuccessfulAuthenticationAttempts(List.of());
		this.confirmationAttempts = 0;
		this.stateDate = LocalDateTime.now();
	}

	public String getVerificationCardId() {
		return verificationCardId;
	}

	public List<String> getShortChoiceReturnCodes() {
		return List.copyOf(shortChoiceReturnCodes);
	}

	public void setShortChoiceReturnCodes(final List<String> shortChoiceReturnCodes) {
		checkNotNull(shortChoiceReturnCodes);
		this.shortChoiceReturnCodes = List.copyOf(shortChoiceReturnCodes);
	}

	public String getShortVoteCastReturnCode() {
		return shortVoteCastReturnCode;
	}

	public void setShortVoteCastReturnCode(final String shortVoteCastReturnCode) {
		checkNotNull(shortVoteCastReturnCode);
		this.shortVoteCastReturnCode = shortVoteCastReturnCode;
	}

	public VerificationCardState getState() {
		return state;
	}

	public void setState(final VerificationCardState state) {
		checkNotNull(state);
		this.state = state;
	}

	public int getAuthenticationAttempts() {
		return authenticationAttempts;
	}

	public void setAuthenticationAttempts(final int authenticationAttempts) {
		checkArgument(authenticationAttempts > 0 && authenticationAttempts <= 5, "Authentication attempt must be in ]0,5].");
		this.authenticationAttempts = authenticationAttempts;
	}

	public long getLastSuccessfulAuthenticationTimeStep() {
		return lastSuccessfulAuthenticationTimeStep;
	}

	public void setLastSuccessfulAuthenticationTimeStep(final long lastTimeStep) {
		checkArgument(lastTimeStep >= 0, "Last time step must be positive.");
		this.lastSuccessfulAuthenticationTimeStep = lastTimeStep;
	}

	public SuccessfulAuthenticationAttempts getSuccessfulAuthenticationAttempts() {
		return successfulAuthenticationAttempts;
	}

	public void setSuccessfulAuthenticationAttempts(final SuccessfulAuthenticationAttempts lastSuccessfulAuthenticationChallenge) {
		this.successfulAuthenticationAttempts = lastSuccessfulAuthenticationChallenge;
	}

	public int getConfirmationAttempts() {
		return confirmationAttempts;
	}

	public void setConfirmationAttempts(final int confirmationAttempts) {
		checkArgument(confirmationAttempts > 0 && confirmationAttempts <= 5, "Confirmation attempt must be in ]0,5].");
		this.confirmationAttempts = confirmationAttempts;
	}

	public LocalDateTime getStateDate() {
		return stateDate;
	}

	public void setStateDate(final LocalDateTime stateDate) {
		this.stateDate = stateDate;
	}

}
