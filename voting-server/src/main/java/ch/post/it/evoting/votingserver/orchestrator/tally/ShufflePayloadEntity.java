/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.tally;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "SHUFFLE_PAYLOAD")
public class ShufflePayloadEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "shufflePayloadSeq")
	@SequenceGenerator(name = "shufflePayloadSeq", sequenceName = "SHUFFLE_PAYLOAD_SEQ", allocationSize = 1)
	private Long id;

	private String electionEventId;

	private String ballotBoxId;

	private int nodeId;

	@Column(name = "PAYLOAD")
	private byte[] shufflePayload;

	@Version
	private int changeControlId;

	protected ShufflePayloadEntity() {
		// No-argument constructor to keep JPA happy.
	}

	public ShufflePayloadEntity(final String electionEventId, final String ballotBoxId, final int nodeId, final byte[] shufflePayload) {
		this.electionEventId = validateUUID(electionEventId);
		this.ballotBoxId = validateUUID(ballotBoxId);
		this.nodeId = nodeId;
		this.shufflePayload = checkNotNull(shufflePayload);
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public String getBallotBoxId() {
		return ballotBoxId;
	}

	public int getNodeId() {
		return nodeId;
	}

	public byte[] getShufflePayload() {
		return shufflePayload;
	}

}
