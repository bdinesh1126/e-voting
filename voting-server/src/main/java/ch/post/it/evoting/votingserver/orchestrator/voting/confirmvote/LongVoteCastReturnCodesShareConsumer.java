/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.voting.confirmvote;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableBigInteger;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponentlVCCSharePayload;
import ch.post.it.evoting.votingserver.orchestrator.aggregator.AggregatorService;
import ch.post.it.evoting.votingserver.orchestrator.voting.CommandFacade;

@Component
public class LongVoteCastReturnCodesShareConsumer {
	private static final Logger LOGGER = LoggerFactory.getLogger(LongVoteCastReturnCodesShareConsumer.class);
	private final CommandFacade commandFacade;
	private final ObjectMapper objectMapper;
	private final AggregatorService aggregatorService;
	private final Hash hash;

	public LongVoteCastReturnCodesShareConsumer(CommandFacade commandFacade, ObjectMapper objectMapper, AggregatorService aggregatorService,
			Hash hash) {
		this.commandFacade = commandFacade;
		this.objectMapper = objectMapper;
		this.aggregatorService = aggregatorService;
		this.hash = hash;
	}

	@RabbitListener(queues = "#{queueNameResolver.get(\"VERIFY_LVCC_SHARE_HASH_RESPONSE_PATTERN\")}")
	public void consumer(final Message message) throws IOException {

		LOGGER.debug("Started processing LVCC share response.");

		final String correlationId = message.getMessageProperties().getCorrelationId();
		checkNotNull(correlationId, "Correlation Id should not be null.");

		final byte[] encodedResponse = message.getBody();
		final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload =
				objectMapper.readValue(encodedResponse, ControlComponentlVCCSharePayload.class);

		final String electionEventId = controlComponentlVCCSharePayload.getElectionEventId();
		final String verificationCardSetId = controlComponentlVCCSharePayload.getVerificationCardSetId();
		final String verificationCardId = controlComponentlVCCSharePayload.getVerificationCardId();

		final BigInteger confirmationKeyValue = controlComponentlVCCSharePayload.getConfirmationKey().element().getValue();
		final String confirmationKeyAsString = Base64.getEncoder().encodeToString(hash.recursiveHash(HashableBigInteger.from(confirmationKeyValue)));

		final String contextId = String.join("-", Arrays.asList(electionEventId, verificationCardSetId, verificationCardId, confirmationKeyAsString));
		final int nodeId = controlComponentlVCCSharePayload.getNodeId();

		LOGGER.info("Received LVCC share response for [electionEventId : {}, verificationCardSetId : {}, verificationCardId : {}, nodeId : {}].",
				electionEventId, verificationCardSetId, verificationCardId, nodeId);

		commandFacade.saveResponse(encodedResponse, correlationId, contextId, Context.VOTING_RETURN_CODES_VERIFY_LVCC_SHARE_HASH, nodeId);

		aggregatorService.notifyPartialResponseReceived(correlationId, contextId);
	}
}
