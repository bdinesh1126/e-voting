/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.util.List;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.votingserver.orchestrator.voting.CommandFacade;

@Service
public class QueuedComputeChunkIdsService {

	private final CommandFacade commandFacade;

	public QueuedComputeChunkIdsService(final CommandFacade commandFacade) {
		this.commandFacade = commandFacade;
	}

	public List<Integer> getQueuedComputeChunksIds(final String electionEventId, final String verificationCardSetId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		final String contextIdPrefix = String.join("-", electionEventId, verificationCardSetId);

		return commandFacade.getAllDistinctContextIdsByContextAndContextIdLike(Context.CONFIGURATION_RETURN_CODES_GEN_ENC_LONG_CODE_SHARES,
						contextIdPrefix).stream()
				.map(contextId -> contextId.replace(contextIdPrefix + "-", ""))
				.map(Integer::valueOf)
				.toList();
	}

}