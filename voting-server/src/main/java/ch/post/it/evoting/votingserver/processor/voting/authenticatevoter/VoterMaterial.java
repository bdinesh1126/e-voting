/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting.authenticatevoter;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;

/**
 * Holds the information needed by the voting client during authentication. The presence of fields depends on if it is (and when) a re-login.
 * <p>
 * <ul>
 *     <li>first authentication: only {@code ballot} and {@code ballotTexts} are present.</li>
 *     <li>authentication after vote has been sent, before confirmed: {@code ballot}, {@code ballotTexts} and {@code shortChoiceReturnCodes} are present.</li>
 *     <li>authentication after vote has been confirmed: only {@code shortVoteCastReturnCode} is present.</li>
 * </ul>
 *
 * @param ballot                  the ballot. Non-null during first authentication.
 * @param ballotTexts             the ballot texts as a json string. Non-null during first authentication.
 * @param shortChoiceReturnCodes  the short Choice Return Codes. Non-null after vote has been sent but before cast. Must be non-empty.
 * @param shortVoteCastReturnCode the short Vote Cast Return Code. Non-null after vote has been cast.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
record VoterMaterial(Ballot ballot, String ballotTexts, List<String> shortChoiceReturnCodes, String shortVoteCastReturnCode) {

	/**
	 * @throws IllegalArgumentException if {@code shortChoiceReturnCodes} is non-null but empty.
	 */
	VoterMaterial {
		if (shortChoiceReturnCodes != null) {
			checkArgument(!shortChoiceReturnCodes.isEmpty(), "The list of short Choice Return Codes can not be empty.");
			shortChoiceReturnCodes = List.copyOf(shortChoiceReturnCodes);
		}
	}

	/**
	 * First authentication case.
	 *
	 * @throws NullPointerException if any parameter is null.
	 */
	VoterMaterial(final Ballot ballot, final String ballotTexts) {
		this(checkNotNull(ballot), checkNotNull(ballotTexts), null, null);
	}

	/**
	 * Authentication after vote has been sent.
	 *
	 * @throws NullPointerException if any parameter is null.
	 */
	VoterMaterial(final Ballot ballot, final String ballotTexts, final List<String> shortChoiceReturnCodes) {
		this(checkNotNull(ballot), checkNotNull(ballotTexts), checkNotNull(shortChoiceReturnCodes), null);
	}

	/**
	 * Authentication after vote has been confirmed.
	 *
	 * @throws NullPointerException if {@code shortVoteCastReturnCode} is null.
	 */
	VoterMaterial(final String shortVoteCastReturnCode) {
		this(null, null, null, checkNotNull(shortVoteCastReturnCode));
	}

}
