/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting.sendvote;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import com.google.common.base.Preconditions;

/**
 * Response to the voting-client containing the short Choice Return Codes.
 *
 * @param shortChoiceReturnCodes the short Choice Return Codes. Must be non-null and non-empty.
 */
record SendVoteResponsePayload(List<String> shortChoiceReturnCodes) {

	private static final int SHORT_CHOICE_RETURN_CODE_LENGTH = 4;

	/**
	 * @throws NullPointerException     if {@code shortChoiceReturnCodes} is null or contains any null.
	 * @throws IllegalArgumentException if
	 *                                  <ul>
	 *                                      <li>{@code shortChoiceReturnCodes} is empty.</li>
	 *                                      <li>the short Choice Return Codes are not digits of length {@value SHORT_CHOICE_RETURN_CODE_LENGTH}.</li>
	 *                                  </ul>
	 */
	SendVoteResponsePayload(final List<String> shortChoiceReturnCodes) {
		checkNotNull(shortChoiceReturnCodes).forEach(Preconditions::checkNotNull);
		this.shortChoiceReturnCodes = List.copyOf(shortChoiceReturnCodes);

		checkArgument(!this.shortChoiceReturnCodes.isEmpty(), "There must be at least one short Choice Return Codes.");
		checkArgument(this.shortChoiceReturnCodes.stream().parallel().allMatch(cc -> cc.matches("^[0-9]{" + SHORT_CHOICE_RETURN_CODE_LENGTH + "}$")),
				"The short Choice Return Codes must be only digits and have a length of " + SHORT_CHOICE_RETURN_CODE_LENGTH);
	}

	public List<String> shortChoiceReturnCodes() {
		return List.copyOf(shortChoiceReturnCodes);
	}

}
