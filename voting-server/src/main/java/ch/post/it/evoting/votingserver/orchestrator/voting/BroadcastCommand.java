/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.voting;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.function.Function;

import ch.post.it.evoting.commandmessaging.Context;

public class BroadcastCommand<R> {
	private final String contextId;
	private final Context context;
	private final Object payload;
	private final String pattern;

	private final Function<byte[], R> deserialization;

	private BroadcastCommand(final Builder<R> builder) {
		this.contextId = builder.contextId;
		this.context = builder.context;
		this.payload = builder.payload;
		this.pattern = builder.pattern;
		this.deserialization = builder.deserialization;
	}

	public String getContextId() {
		return contextId;
	}

	public Context getContext() {
		return context;
	}

	public Object getPayload() {
		return payload;
	}

	public String getPattern() {
		return pattern;
	}

	public Function<byte[], R> getDeserialization() {
		return deserialization;
	}

	public static class Builder<R> {
		private String contextId;
		private Context context;
		private Object payload;
		private String pattern;

		private Function<byte[], R> deserialization;

		/**
		 * Sets the context id which uniquely identifies the resource being operated on.
		 *
		 * @param contextId The context id to be used in the BroadcastCommand.
		 * @return this Builder with the contextId set.
		 */
		public Builder<R> contextId(final String contextId) {
			this.contextId = contextId;
			return this;
		}

		/**
		 * Sets the context which identifies the business operation being executed.
		 *
		 * @param context The context to be used in the BroadcastCommand.
		 * @return this Builder with the context set.
		 */
		public Builder<R> context(final Context context) {
			this.context = context;
			return this;
		}

		/**
		 * Sets the request content which is the input to the process.
		 *
		 * @param payload The payload to be used in the BroadcastCommand.
		 * @return this Builder with the context set.
		 */
		public Builder<R> payload(final Object payload) {
			this.payload = payload;
			return this;
		}

		/**
		 * Sets the pattern of the queues to which the request has to be sent. Must be non-null.
		 *
		 * @param pattern The pattern of the queues to be used in the BroadcastCommand.
		 * @return this Builder with the context set.
		 */
		public Builder<R> pattern(final String pattern) {
			this.pattern = pattern;
			return this;
		}

		/**
		 * Sets the deserialization that is to be used by the process. Must not be null.
		 *
		 * @param deserialization The deserialization to be used in the BroadcastCommand.
		 * @return the Builder with the deserialization set.
		 */
		public Builder<R> deserialization(final Function<byte[], R> deserialization) {
			this.deserialization = deserialization;
			return this;
		}

		public BroadcastCommand<R> build() {

			checkNotNull(contextId, "ContextId Id must not be null");
			checkNotNull(context, "Context must not be null");
			checkNotNull(payload, "Payload must not be null");
			checkNotNull(pattern, "Queue pattern must not be null");

			return new BroadcastCommand<>(this);
		}
	}
}
