/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "VERIFICATION_CARD_SET")
@SuppressWarnings("java:S1068")
public class VerificationCardSetEntity {

	@Id
	@Column(name = "VERIFICATION_CARD_SET_ID")
	private String verificationCardSetId;

	@ManyToOne
	@JoinColumn(name = "ELECTION_EVENT_FK_ID", referencedColumnName = "ELECTION_EVENT_ID")
	private ElectionEventEntity electionEventEntity;

	private String ballotBoxId;

	private int gracePeriod;

	private int numberOfVotingCards;

	private int numberOfWriteInFields;

	private byte[] primesMappingTable;

	private LocalDateTime ballotBoxStartTime;

	private LocalDateTime ballotBoxFinishTime;

	@Convert(converter = BooleanConverter.class)
	private boolean testBallotBox;

	@Version
	private Integer changeControlId;

	public VerificationCardSetEntity() {
	}

	public VerificationCardSetEntity(final String verificationCardSetId, final ElectionEventEntity electionEventEntity,
			final String ballotBoxId, final int gracePeriod, final int numberOfVotingCards, final int numberOfWriteInFields,
			final byte[] primesMappingTable, final LocalDateTime ballotBoxStartTime, final LocalDateTime ballotBoxFinishTime,
			final boolean testBallotBox) {
		this.verificationCardSetId = validateUUID(verificationCardSetId);
		this.electionEventEntity = checkNotNull(electionEventEntity);
		this.ballotBoxId = validateUUID(ballotBoxId);

		checkArgument(gracePeriod >= 0, "The grace period must be positive.");
		this.gracePeriod = gracePeriod;

		checkArgument(numberOfVotingCards > 0, "The number of voting cards must be strictly positive.");
		this.numberOfVotingCards = numberOfVotingCards;

		checkArgument(numberOfWriteInFields >= 0, "The number of write-in field must be positive.");
		this.numberOfWriteInFields = numberOfWriteInFields;

		checkArgument(ballotBoxStartTime.isBefore(ballotBoxFinishTime) || ballotBoxStartTime.equals(ballotBoxFinishTime),
				"The ballot box start time must not be after the ballot box finish time.");
		this.ballotBoxStartTime = ballotBoxStartTime;
		this.ballotBoxFinishTime = ballotBoxFinishTime;

		this.primesMappingTable = checkNotNull(primesMappingTable);
		this.testBallotBox = testBallotBox;
	}

	public String getVerificationCardSetId() {
		return verificationCardSetId;
	}

	public ElectionEventEntity getElectionEventEntity() {
		return electionEventEntity;
	}

	public String getBallotBoxId() {
		return ballotBoxId;
	}

	public int getGracePeriod() {
		return gracePeriod;
	}

	public LocalDateTime getBallotBoxStartTime() {
		return ballotBoxStartTime;
	}

	public LocalDateTime getBallotBoxFinishTime() {
		return ballotBoxFinishTime;
	}

}
