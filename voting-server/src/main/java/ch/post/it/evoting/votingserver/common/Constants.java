/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.common;

import java.math.BigInteger;

/**
 * Class for holding constants used in the rest web services.
 */
public final class Constants {

	public static final String PARAMETER_VALUE_VERSION = "version";

	public static final String PARAMETER_VALUE_TENANT_ID = "tenantId";

	public static final String PARAMETER_VALUE_ELECTION_EVENT_ID = "electionEventId";

	public static final String PARAMETER_VALUE_BALLOT_ID = "ballotId";

	public static final String PARAMETER_X_FORWARDED_FOR = "X-Forwarded-For";

	public static final String PARAMETER_VALUE_BALLOT_BOX_ID = "ballotBoxId";

	public static final String PARAMETER_VALUE_VERIFICATION_CARD_SET_ID = "verificationCardSetId";

	public static final String PARAMETER_VALUE_VERIFICATION_CARD_ID = "verificationCardId";

	public static final String PARAMETER_VALUE_CREDENTIAL_ID = "credentialId";

	public static final String VERIFICATION_CARD_ID = "verificationCardId";

	public static final String PARTIAL_VOTING_CARD_ID = "partialVotingCardId";

	public static final String PARAMETER_VALUE_CHUNK_ID = "chunkId";

	public static final String PARAMETER_VALUE_CHUNK_COUNT = "chunkCount";

	public static final String VOTING_CARD_ID = "votingCardId";

	public static final int MAX_AUTHENTICATION_ATTEMPTS = 5;

	public static final int MAX_CONFIRMATION_ATTEMPTS = 5;

	public static final BigInteger TWO_POW_256 = BigInteger.ONE.shiftLeft(256);

	// Avoid instantiation.
	private Constants() {
	}
}
