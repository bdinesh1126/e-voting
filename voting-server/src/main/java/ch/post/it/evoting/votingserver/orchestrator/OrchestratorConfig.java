/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FullyQualifiedAnnotationBeanNameGenerator;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

@Configuration
@ComponentScan(value = { "ch.post.it.evoting.votingserver.orchestrator",
		"ch.post.it.evoting.commandmessaging" }, nameGenerator = FullyQualifiedAnnotationBeanNameGenerator.class)
@EntityScan(basePackages = { "ch.post.it.evoting.votingserver.orchestrator", "ch.post.it.evoting.commandmessaging" })
@EnableJpaRepositories(basePackages = { "ch.post.it.evoting.votingserver.orchestrator", "ch.post.it.evoting.commandmessaging" })
public class OrchestratorConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(OrchestratorConfig.class);

	@Bean
	public Cache<String, CompletableFuture<String>> getInFlightRequestCache(
			@Value("${orchestrator.request.cache.timeout.seconds}")
			int timeOut) {

		return Caffeine.newBuilder()
				.expireAfterWrite(timeOut, TimeUnit.SECONDS)
				.removalListener((key, value, cause) -> {
					if (cause.wasEvicted()) {
						LOGGER.debug("In flight request evicted from cache. [correlationId : {}]", key);
					}
				}).build();
	}

}
