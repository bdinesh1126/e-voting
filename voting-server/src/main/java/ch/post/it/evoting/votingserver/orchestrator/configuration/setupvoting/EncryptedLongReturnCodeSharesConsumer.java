/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.configuration.setupvoting;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;

@Component
public class EncryptedLongReturnCodeSharesConsumer {

	private static final Logger LOGGER = LoggerFactory.getLogger(EncryptedLongReturnCodeSharesConsumer.class);

	private final ObjectMapper objectMapper;

	private final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService;

	public EncryptedLongReturnCodeSharesConsumer(final ObjectMapper objectMapper,
			EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService) {
		this.objectMapper = objectMapper;
		this.encryptedLongReturnCodeSharesService = encryptedLongReturnCodeSharesService;
	}

	@RabbitListener(queues = "#{queueNameResolver.get(\"GEN_ENC_LONG_CODE_SHARES_RESPONSE_PATTERN\")}")
	public void consumer(final Message message) throws IOException {

		final String correlationId = message.getMessageProperties().getCorrelationId();
		checkNotNull(correlationId, "Correlation Id should not be null");

		final byte[] encodedResponse = message.getBody();
		final ControlComponentCodeSharesPayload controlComponentCodeSharesPayload = objectMapper.readValue(encodedResponse,
				ControlComponentCodeSharesPayload.class);

		final int nodeId = controlComponentCodeSharesPayload.getNodeId();
		String electionEventId = controlComponentCodeSharesPayload.getElectionEventId();
		String verificationCardSetId = controlComponentCodeSharesPayload.getVerificationCardSetId();
		int chunkId = controlComponentCodeSharesPayload.getChunkId();

		final String contextId = String.join("-", Arrays.asList(electionEventId, verificationCardSetId, String.valueOf(chunkId)));

		LOGGER.info("Received EncLongCodeShares calculation response [contextId: {}, correlationId: {}, nodeId: {}]", contextId, correlationId,
				nodeId);

		encryptedLongReturnCodeSharesService.saveControlComponentCodeSharesPayload(controlComponentCodeSharesPayload);
	}
}
