/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.votingserver.common.Constants;

/**
 * Web service for handling ballot resource.
 */
@RestController
@RequestMapping("api/v1/processor/voting/authenticatevoter/ballots")
public class BallotResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(BallotResource.class);

	private final BallotDataService ballotDataService;

	public BallotResource(final BallotDataService ballotDataService) {
		this.ballotDataService = ballotDataService;
	}

	/**
	 * Return a ballot given the election event and the ballot identifiers.
	 *
	 * @param electionEventId - the election event identifier.
	 * @param ballotId        - the ballot identifier.
	 * @return Returns the corresponding ballot for the electionEventId and ballotId.
	 */
	@GetMapping("electionevent/{electionEventId}/ballot/{ballotId}")
	public Ballot getBallot(
			@PathVariable(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@PathVariable(Constants.PARAMETER_VALUE_BALLOT_ID)
			final String ballotId) {

		validateUUID(electionEventId);
		validateUUID(ballotId);

		final Ballot ballot = ballotDataService.findBallotByElectionEventIdAndBallotId(electionEventId, ballotId);
		LOGGER.info("Ballot found. [electionEventId: {}, ballotId: {}]", electionEventId, ballotId);

		return ballot;
	}

}
