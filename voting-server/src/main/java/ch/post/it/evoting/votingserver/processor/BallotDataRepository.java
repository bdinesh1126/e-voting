/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface BallotDataRepository extends CrudRepository<BallotDataEntity, String> {

	@Query("select e from BallotDataEntity e where e.electionEventEntity.electionEventId = ?1 and e.ballotId = ?2")
	Optional<BallotDataEntity> findByElectionEventIdAndBallotId(final String electionEventId, final String ballotId);

}
