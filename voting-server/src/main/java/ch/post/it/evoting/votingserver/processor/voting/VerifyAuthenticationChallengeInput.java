/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateBase64Encoded;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.votingserver.common.Constants.TWO_POW_256;
import static com.google.common.base.Preconditions.checkArgument;

import java.math.BigInteger;

/**
 * Regroups the input values needed by the VerifyAuthenticationChallenge algorithm.
 */
public record VerifyAuthenticationChallengeInput(String credentialId, String derivedAuthenticationChallenge, String baseAuthenticationChallenge,
												 BigInteger authenticationNonce) {

	public VerifyAuthenticationChallengeInput {
		validateUUID(credentialId);
		validateBase64Encoded(derivedAuthenticationChallenge);
		validateBase64Encoded(baseAuthenticationChallenge);
		checkArgument(authenticationNonce.compareTo(BigInteger.ZERO) >= 0);
		checkArgument(authenticationNonce.compareTo(TWO_POW_256) < 0);
	}
}
