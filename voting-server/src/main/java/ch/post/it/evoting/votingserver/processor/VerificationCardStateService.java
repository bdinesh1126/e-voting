/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class VerificationCardStateService {

	private final VerificationCardStateRepository verificationCardStateRepository;

	public VerificationCardStateService(final VerificationCardStateRepository verificationCardStateRepository) {
		this.verificationCardStateRepository = verificationCardStateRepository;
	}

	@Transactional
	public void saveVerificationCardStates(final List<VerificationCardStateEntity> verificationCardStateEntities) {
		checkNotNull(verificationCardStateEntities);

		verificationCardStateRepository.saveAll(verificationCardStateEntities);
	}

	@Transactional
	public void saveVerificationCardState(final VerificationCardStateEntity verificationCardStateEntity) {
		checkNotNull(verificationCardStateEntity);

		verificationCardStateRepository.save(verificationCardStateEntity);
	}

	private VerificationCardStateEntity getVerificationCardStateEntity(final String verificationCardId) {
		return verificationCardStateRepository.findById(verificationCardId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Verification card state not found. [verificationCardId: %s]", verificationCardId)));
	}

	@Transactional
	public int incrementAuthenticationAttempts(final String verificationCardId) {
		validateUUID(verificationCardId);

		final VerificationCardStateEntity verificationCardStateEntity = getVerificationCardStateEntity(verificationCardId);
		final VerificationCardState verificationCardState = verificationCardStateEntity.getState();

		checkState(!verificationCardState.isInactive(),
				"The current state does not allow to increment authentication attempts. [verificationCardId: %s, verificationCardState: %s]",
				verificationCardId, verificationCardState);

		int authenticationAttempts = verificationCardStateEntity.getAuthenticationAttempts();
		verificationCardStateEntity.setAuthenticationAttempts(++authenticationAttempts);

		verificationCardStateRepository.save(verificationCardStateEntity);
		return verificationCardStateEntity.getAuthenticationAttempts();
	}

	/**
	 * Adds the authentication challenge to the list of successful authentication challenges for this verification card ID. To prevent bloating the
	 * list, we reinitialize the list if the current time step is larger than the time step of the last successful authentication challenge.
	 *
	 * @param verificationCardId      the verification card ID for which to add the authentication challenge. Must be a valid UUID.
	 * @param currentTimeStep         the time step associated with the authentication challenge.
	 * @param authenticationChallenge the authentication challenge to be added to the list. Must be non-null.
	 * @throws ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException if the verification card ID is not a valid UUID.
	 * @throws NullPointerException                                                             if the authentication challenge is null.
	 */
	@Transactional
	public void addToListOfSuccessfulAuthenticationChallenges(final String verificationCardId, final long currentTimeStep,
			final String authenticationChallenge) {
		validateUUID(verificationCardId);
		checkNotNull(authenticationChallenge);

		final VerificationCardStateEntity verificationCardStateEntity = getVerificationCardStateEntity(verificationCardId);
		final long previousTimeStep = currentTimeStep - 1;
		final long lastSuccessfulAuthenticationTimeStep = verificationCardStateEntity.getLastSuccessfulAuthenticationTimeStep();
		if (lastSuccessfulAuthenticationTimeStep + 1 < previousTimeStep) { // Reinitialize the list
			verificationCardStateEntity.setLastSuccessfulAuthenticationTimeStep(currentTimeStep);
			verificationCardStateEntity.setSuccessfulAuthenticationAttempts(new SuccessfulAuthenticationAttempts(List.of(authenticationChallenge)));
		} else { // Otherwise add challenge to existing list
			final SuccessfulAuthenticationAttempts successfulAuthenticationAttempts = verificationCardStateEntity.getSuccessfulAuthenticationAttempts();
			final List<String> successfulChallenges = new ArrayList<>(successfulAuthenticationAttempts.successfulChallenges());
			successfulChallenges.add(authenticationChallenge);
			verificationCardStateEntity.setSuccessfulAuthenticationAttempts(new SuccessfulAuthenticationAttempts(successfulChallenges));
		}

		verificationCardStateRepository.save(verificationCardStateEntity);
	}

	@Transactional
	public int incrementConfirmationAttempts(final String verificationCardId) {
		validateUUID(verificationCardId);

		final VerificationCardStateEntity verificationCardStateEntity = getVerificationCardStateEntity(verificationCardId);
		final VerificationCardState verificationCardState = verificationCardStateEntity.getState();

		checkState(VerificationCardState.CONFIRMING.equals(verificationCardState),
				"The current state does not allow to increment confirmation attempts. [verificationCardId: %s, verificationCardState: %s]",
				verificationCardId, verificationCardState);

		int confirmationAttempts = verificationCardStateEntity.getConfirmationAttempts();
		verificationCardStateEntity.setConfirmationAttempts(++confirmationAttempts);

		verificationCardStateRepository.save(verificationCardStateEntity);
		return verificationCardStateEntity.getConfirmationAttempts();
	}

}
