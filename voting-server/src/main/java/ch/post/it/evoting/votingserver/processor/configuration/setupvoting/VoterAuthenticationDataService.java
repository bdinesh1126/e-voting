/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.domain.configuration.SetupComponentVoterAuthenticationData;
import ch.post.it.evoting.domain.configuration.SetupComponentVoterAuthenticationDataPayload;
import ch.post.it.evoting.votingserver.processor.ElectionEventEntity;
import ch.post.it.evoting.votingserver.processor.VerificationCardEntity;
import ch.post.it.evoting.votingserver.processor.VerificationCardService;
import ch.post.it.evoting.votingserver.processor.VerificationCardSetEntity;
import ch.post.it.evoting.votingserver.processor.VerificationCardSetService;
import ch.post.it.evoting.votingserver.processor.VerificationCardStateEntity;

@Service
public class VoterAuthenticationDataService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VoterAuthenticationDataService.class);

	private final VerificationCardService verificationCardService;
	private final VerificationCardSetService verificationCardSetService;

	public VoterAuthenticationDataService(
			final VerificationCardService verificationCardService,
			final VerificationCardSetService verificationCardSetService) {
		this.verificationCardService = verificationCardService;
		this.verificationCardSetService = verificationCardSetService;
	}

	@Transactional
	public void saveVoterAuthenticationData(final SetupComponentVoterAuthenticationDataPayload setupComponentVoterAuthenticationDataPayload) {
		checkNotNull(setupComponentVoterAuthenticationDataPayload);

		final String electionEventId = setupComponentVoterAuthenticationDataPayload.getElectionEventId();
		final String verificationCardSetId = setupComponentVoterAuthenticationDataPayload.getVerificationCardSetId();
		final VerificationCardSetEntity verificationCardSetEntity = verificationCardSetService.getVerificationCardSetEntity(verificationCardSetId);

		final List<VerificationCardEntity> verificationCardEntities = setupComponentVoterAuthenticationDataPayload.getSetupComponentVoterAuthenticationData()
				.stream()
				.map(setupComponentVoterAuthenticationData -> {
					final String verificationCardId = setupComponentVoterAuthenticationData.verificationCardId();
					final String votingCardId = setupComponentVoterAuthenticationData.votingCardId();
					final String credentialId = setupComponentVoterAuthenticationData.credentialId();

					final VerificationCardStateEntity verificationCardStateEntity = new VerificationCardStateEntity(verificationCardId);
					return new VerificationCardEntity(verificationCardId, verificationCardSetEntity, credentialId, votingCardId,
							setupComponentVoterAuthenticationData, verificationCardStateEntity);
				})
				.toList();

		verificationCardService.saveVerificationCards(verificationCardEntities);
		LOGGER.info("Successfully saved all verification cards. [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);
	}

	public SetupComponentVoterAuthenticationData getVoterAuthenticationData(final String electionEventId, final String credentialId) {
		validateUUID(electionEventId);
		validateUUID(credentialId);

		final VerificationCardEntity verificationCardEntity = verificationCardService.getVerificationCardEntityByCredentialId(
				credentialId);

		final ElectionEventEntity electionEventEntity = verificationCardEntity.getVerificationCardSetEntity().getElectionEventEntity();
		checkState(electionEventId.equals(electionEventEntity.getElectionEventId()),
				"The request election event id does not match the election event id associated to the credential id. [electionEventId: %s, credentialId: %s]",
				electionEventId, credentialId);

		LOGGER.info("Retrieved voter authentication data. [electionEventId: {}, credentialId: {}]", electionEventId, credentialId);

		return verificationCardEntity.getVoterAuthenticationData();
	}

}
