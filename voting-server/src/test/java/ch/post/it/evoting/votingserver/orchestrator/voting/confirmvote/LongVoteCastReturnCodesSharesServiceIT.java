/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.voting.confirmvote;

import static ch.post.it.evoting.domain.SharedQueue.CREATE_LVCC_SHARE_HASH_REQUEST_PATTERN;
import static ch.post.it.evoting.domain.SharedQueue.CREATE_LVCC_SHARE_HASH_RESPONSE_PATTERN;
import static ch.post.it.evoting.domain.SharedQueue.VERIFY_LVCC_SHARE_HASH_REQUEST_PATTERN;
import static ch.post.it.evoting.domain.SharedQueue.VERIFY_LVCC_SHARE_HASH_RESPONSE_PATTERN;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableBigInteger;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.domain.voting.confirmvote.ConfirmationKey;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponenthlVCCPayload;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponentlVCCSharePayload;
import ch.post.it.evoting.domain.voting.confirmvote.LongVoteCastReturnCodesShare;
import ch.post.it.evoting.domain.voting.confirmvote.VotingServerConfirmPayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.votingserver.orchestrator.BroadcastIntegrationTestService;
import ch.post.it.evoting.votingserver.orchestrator.IntegrationTestSupport;

class LongVoteCastReturnCodesSharesServiceIT extends IntegrationTestSupport {

	private static GqGroup encryptionGroup;
	private static ConfirmationKey confirmationKey;

	@Autowired
	private LongVoteCastReturnCodesSharesService longVoteCastReturnCodesSharesService;

	@Autowired
	private BroadcastIntegrationTestService broadcastIntegrationTestService;

	@Autowired
	private Hash hash;

	@AfterEach
	void cleanUp() {
		broadcastIntegrationTestService.cleanUpOrchestrator();
	}

	@BeforeAll
	static void setUpAll() {
		encryptionGroup = GroupTestData.getGqGroup();
		final GqElement randomGqElement = new GqGroupGenerator(encryptionGroup).genMember();
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		confirmationKey = new ConfirmationKey(contextIds, randomGqElement);
	}

	@Test
	@DisplayName("Process create LVCC Share Contributions")
	void happyPath() throws InterruptedException {

		final Context firstContext = Context.VOTING_RETURN_CODES_CREATE_LVCC_SHARE_HASH;
		final Context secondContext = Context.VOTING_RETURN_CODES_VERIFY_LVCC_SHARE_HASH;

		final CountDownLatch serviceCountDownLatch = new CountDownLatch(1);

		final VotingServerConfirmPayload votingServerConfirmPayload = genRequestPayload();

		final BigInteger confirmationKeyValue = votingServerConfirmPayload.getConfirmationKey().element().getValue();
		final String confirmationKeyAsString = Base64.getEncoder().encodeToString(hash.recursiveHash(HashableBigInteger.from(confirmationKeyValue)));

		final String contextId = String.join("-", Arrays.asList(electionEventId, verificationCardSetId, verificationCardId, confirmationKeyAsString));

		final ExecutorService executorService = Executors.newFixedThreadPool(1, new CustomizableThreadFactory("service-pool-"));

		// Call the service in a separate thread and wait for the results.
		executorService.execute(() -> {
			longVoteCastReturnCodesSharesService.getLongVoteCastReturnCodesContributions(electionEventId, verificationCardSetId, verificationCardId,
					votingServerConfirmPayload);

			serviceCountDownLatch.countDown();
		});

		broadcastIntegrationTestService.awaitBroadcastRequestsSaved(firstContext, contextId, 30, SECONDS);

		broadcastIntegrationTestService.respondWith(CREATE_LVCC_SHARE_HASH_REQUEST_PATTERN, CREATE_LVCC_SHARE_HASH_RESPONSE_PATTERN,
				this::getLongVoteCastReturnCodesShareHashResponsePayload);

		broadcastIntegrationTestService.awaitBroadcastRequestsSaved(secondContext, contextId, 30, SECONDS);

		broadcastIntegrationTestService.respondWith(VERIFY_LVCC_SHARE_HASH_REQUEST_PATTERN, VERIFY_LVCC_SHARE_HASH_RESPONSE_PATTERN,
				this::generateControlComponentlVCCSharePayload);

		assertTrue(serviceCountDownLatch.await(30, SECONDS));
	}

	private ControlComponenthlVCCPayload getLongVoteCastReturnCodesShareHashResponsePayload(final Integer nodeId) {
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature("randomSignatureContents".getBytes(StandardCharsets.UTF_8));
		return new ControlComponenthlVCCPayload(encryptionGroup, nodeId, "E1A6F972679C8BD8A7C25C400A842EE7", confirmationKey, signature);
	}

	private VotingServerConfirmPayload genRequestPayload() {
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature("".getBytes(StandardCharsets.UTF_8));
		return new VotingServerConfirmPayload(encryptionGroup, confirmationKey, signature);
	}

	private ControlComponentlVCCSharePayload generateControlComponentlVCCSharePayload(final Integer nodeId) {
		final GqElement randomGqElement = new GqGroupGenerator(encryptionGroup).genMember();
		final ZqElement randomZqElement = new ZqGroupGenerator(ZqGroup.sameOrderAs(encryptionGroup)).genRandomZqElementMember();
		final ExponentiationProof exponentiationProof = new ExponentiationProof(randomZqElement, randomZqElement);
		final LongVoteCastReturnCodesShare longVoteCastReturnCodesShare = new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId,
				verificationCardId, nodeId, randomGqElement, exponentiationProof);
		final ControlComponentlVCCSharePayload longReturnCodesSharePayload = new ControlComponentlVCCSharePayload(electionEventId,
				verificationCardSetId, verificationCardId, nodeId, encryptionGroup, longVoteCastReturnCodesShare, confirmationKey, true);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(new byte[] {});
		longReturnCodesSharePayload.setSignature(signature);
		return longReturnCodesSharePayload;
	}
}
