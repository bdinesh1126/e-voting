/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting.sendvote;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigInteger;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.PlaintextEqualityProof;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.common.EncryptedVerifiableVote;
import ch.post.it.evoting.votingserver.common.ConfirmationKeyInvalidException;
import ch.post.it.evoting.votingserver.processor.IdentifierValidationService;
import ch.post.it.evoting.votingserver.processor.voting.AuthenticationChallenge;
import ch.post.it.evoting.votingserver.processor.voting.AuthenticationStep;
import ch.post.it.evoting.votingserver.processor.voting.CredentialIdNotFoundException;
import ch.post.it.evoting.votingserver.processor.voting.VerifyAuthenticationChallengeException;
import ch.post.it.evoting.votingserver.processor.voting.VerifyAuthenticationChallengeOutput.VerifyAuthenticationChallengeStatus;
import ch.post.it.evoting.votingserver.processor.voting.VerifyAuthenticationChallengeService;

@WebMvcTest(SendVoteController.class)
class SendVoteControllerIT extends TestGroupSetup {

	private static final Random random = RandomFactory.createRandom();
	private static final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
	private static final String TWO_POW_256 = "115792089237316195423570985008687907853269984665640564039457584007913129639936";

	private final List<String> shortChoiceReturnCodes = List.of("1111", "2222", "3333", "4444");

	private String electionEventId;
	private String verificationCardSetId;
	private String credentialId;
	private String verificationCardId;
	private ContextIds contextIds;
	private SendVotePayload sendVotePayload;

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private IdentifierValidationService identifierValidationService;

	@MockBean
	private VerifyAuthenticationChallengeService verifyAuthenticationChallengeService;

	@MockBean
	private ChoiceReturnCodesService choiceReturnCodesService;

	@BeforeEach
	void setup() {
		reset(identifierValidationService, verifyAuthenticationChallengeService, choiceReturnCodesService);

		electionEventId = random.genRandomBase16String(32);
		verificationCardSetId = random.genRandomBase16String(32);
		credentialId = random.genRandomBase16String(32);
		verificationCardId = random.genRandomBase16String(32);
		contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);

		sendVotePayload = createSendVotePayload(verificationCardSetId, verificationCardId);
	}

	@Test
	@DisplayName("returns 200 with correct response body")
	void happyPath() throws Exception {
		doNothing().when(identifierValidationService).validateContextIds(contextIds, credentialId);

		doNothing()
				.when(verifyAuthenticationChallengeService)
				.verifyAuthenticationChallenge(electionEventId, AuthenticationStep.CONFIRM_VOTE, sendVotePayload.authenticationChallenge());

		when(choiceReturnCodesService.retrieveShortChoiceReturnCodes(contextIds, credentialId, sendVotePayload.encryptedVerifiableVote()))
				.thenReturn(shortChoiceReturnCodes);

		final String targetUrl = String.format(
				"/api/v1/processor/voting/sendvote/electionevent/%s/verificationcardset/%s/credentialId/%s/verificationcard/%s",
				electionEventId, verificationCardSetId, credentialId, verificationCardId);

		// Request payload.
		final byte[] sendVotePayloadBytes = objectMapper.writeValueAsBytes(sendVotePayload);

		// Expected response payload.
		final SendVoteResponsePayload sendVoteResponsePayload = new SendVoteResponsePayload(shortChoiceReturnCodes);

		mockMvc.perform(MockMvcRequestBuilders.post(targetUrl)
						.contentType(MediaType.APPLICATION_JSON)
						.content(sendVotePayloadBytes))
				.andExpect(status().isOk())
				.andExpect(content().bytes(objectMapper.writeValueAsBytes(sendVoteResponsePayload)));
	}

	@Test
	@DisplayName("returns 401 when verification of authentication challenge fails")
	void verifyAuthenticationChallengeFails() throws Exception {
		doNothing().when(identifierValidationService).validateContextIds(contextIds, credentialId);

		doThrow(new VerifyAuthenticationChallengeException(VerifyAuthenticationChallengeStatus.EXTENDED_FACTOR_INVALID, 3))
				.when(verifyAuthenticationChallengeService)
				.verifyAuthenticationChallenge(electionEventId, AuthenticationStep.SEND_VOTE, sendVotePayload.authenticationChallenge());

		final String targetUrl = String.format(
				"/api/v1/processor/voting/sendvote/electionevent/%s/verificationcardset/%s/credentialId/%s/verificationcard/%s",
				electionEventId, verificationCardSetId, credentialId, verificationCardId);

		// Request payload.
		final byte[] sendVotePayloadBytes = objectMapper.writeValueAsBytes(sendVotePayload);

		// Expected response payload.
		final int attemptsLeft = 3;

        final String responseBody = mockMvc.perform(MockMvcRequestBuilders.post(targetUrl)
						.contentType(MediaType.APPLICATION_JSON)
						.content(sendVotePayloadBytes))
				.andExpect(status().isUnauthorized())
				.andReturn()
                .getResponse()
                .getContentAsString();

		// Assert the response
		final JsonNode jsonNode = objectMapper.readTree(responseBody);
		assertThat(jsonNode.get("errorStatus").asText()).matches(VerifyAuthenticationChallengeStatus.EXTENDED_FACTOR_INVALID.name());
		assertThat(jsonNode.get("numberOfRemainingAttempts").asInt()).isEqualTo(attemptsLeft);
		assertThat(jsonNode.get("timestamp").asText()).matches("^\\d{10}$");
	}

	@Test
	@DisplayName("returns 401 when verification corresponding to credential id is not found")
	void credentialIdNotFound() throws Exception {
		doNothing().when(identifierValidationService).validateContextIds(contextIds, credentialId);

		doThrow(new CredentialIdNotFoundException("Not found"))
				.when(verifyAuthenticationChallengeService)
				.verifyAuthenticationChallenge(electionEventId, AuthenticationStep.SEND_VOTE, sendVotePayload.authenticationChallenge());

		final String targetUrl = String.format(
				"/api/v1/processor/voting/sendvote/electionevent/%s/verificationcardset/%s/credentialId/%s/verificationcard/%s",
				electionEventId, verificationCardSetId, credentialId, verificationCardId);

		// Request payload.
		final byte[] sendVotePayloadBytes = objectMapper.writeValueAsBytes(sendVotePayload);

		// Expected response payload.
		final ObjectNode errorMessageNode = objectMapper.createObjectNode();
		errorMessageNode.put("errorStatus", "START_VOTING_KEY_INVALID");

		mockMvc.perform(MockMvcRequestBuilders.post(targetUrl)
						.contentType(MediaType.APPLICATION_JSON)
						.content(sendVotePayloadBytes))
				.andExpect(status().isUnauthorized())
				.andExpect(content().string(errorMessageNode.toString()));
	}

	@Test
	@DisplayName("returns 401 when confirmation key is invalid")
	void invalidConfirmationKey() throws Exception {
		doNothing().when(identifierValidationService).validateContextIds(contextIds, credentialId);

		doThrow(new ConfirmationKeyInvalidException("Invalid confirmation key.", 4))
				.when(verifyAuthenticationChallengeService)
				.verifyAuthenticationChallenge(electionEventId, AuthenticationStep.SEND_VOTE, sendVotePayload.authenticationChallenge());

		final String targetUrl = String.format(
				"/api/v1/processor/voting/sendvote/electionevent/%s/verificationcardset/%s/credentialId/%s/verificationcard/%s",
				electionEventId, verificationCardSetId, credentialId, verificationCardId);

		// Request payload.
		final byte[] sendVotePayloadBytes = objectMapper.writeValueAsBytes(sendVotePayload);

		// Expected response payload.
		final ObjectNode errorMessageNode = objectMapper.createObjectNode();
		errorMessageNode.put("errorStatus", "CONFIRMATION_KEY_INVALID");
		errorMessageNode.put("numberOfRemainingAttempts", 4);

		mockMvc.perform(MockMvcRequestBuilders.post(targetUrl)
						.contentType(MediaType.APPLICATION_JSON)
						.content(sendVotePayloadBytes))
				.andExpect(status().isUnauthorized())
				.andExpect(content().string(errorMessageNode.toString()));
	}

	private SendVotePayload createSendVotePayload(final String verificationCardSetId, final String verificationCardId) {
		final String derivedAuthenticationChallenge = random.genRandomBase64String(44);
		final BigInteger authenticationNonce = random.genRandomInteger(new BigInteger(TWO_POW_256));

		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		final EncryptedVerifiableVote encryptedVerifiableVote = genEncryptedVerifiableVote(elGamalGenerator, contextIds);
		final AuthenticationChallenge authenticationChallenge = new AuthenticationChallenge(credentialId, derivedAuthenticationChallenge,
				authenticationNonce);

		return new SendVotePayload(contextIds, gqGroup, encryptedVerifiableVote, authenticationChallenge);
	}

	private EncryptedVerifiableVote genEncryptedVerifiableVote(final ElGamalGenerator elGamalGenerator, final ContextIds contextIds) {
		final int numberOfWriteInsPlusOne = 1;
		final ElGamalMultiRecipientCiphertext encryptedVote = elGamalGenerator.genRandomCiphertext(numberOfWriteInsPlusOne);
		final GqGroup encryptionGroup = encryptedVote.getGroup();
		final ZqGroup zqGroup = ZqGroup.sameOrderAs(encryptionGroup);
		final BigInteger exponentValue = RandomFactory.createRandom().genRandomInteger(encryptionGroup.getQ());
		final ZqElement exponent = ZqElement.create(exponentValue, zqGroup);
		final ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote = encryptedVote.getCiphertextExponentiation(exponent);
		final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(zqGroup);
		final ExponentiationProof exponentiationProof = new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementMember());
		final PlaintextEqualityProof plaintextEqualityProof = new PlaintextEqualityProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementVector(2));
		final ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes = elGamalGenerator.genRandomCiphertext(numberOfWriteInsPlusOne);

		return new EncryptedVerifiableVote(contextIds, encryptedVote, encryptedPartialChoiceReturnCodes, exponentiatedEncryptedVote,
				exponentiationProof, plaintextEqualityProof);
	}

}
