/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.processor.voting.sendvote;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.configuration.SetupComponentVoterAuthenticationData;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentLCCSharePayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentPartialDecryptPayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.common.EncryptedVerifiableVote;
import ch.post.it.evoting.votingserver.orchestrator.OrchestratorFacade;
import ch.post.it.evoting.votingserver.processor.ElectionEventService;
import ch.post.it.evoting.votingserver.processor.ReturnCodesMappingTableService;
import ch.post.it.evoting.votingserver.processor.IdentifierValidationService;
import ch.post.it.evoting.votingserver.processor.VerificationCardService;
import ch.post.it.evoting.votingserver.processor.configuration.setupvoting.VoterAuthenticationDataService;
import ch.post.it.evoting.votingserver.processor.voting.AuthenticationChallenge;
import ch.post.it.evoting.votingserver.processor.voting.AuthenticationStep;
import ch.post.it.evoting.votingserver.processor.voting.ReturnCodesMappingTableSupplier;
import ch.post.it.evoting.votingserver.processor.voting.VerifyAuthenticationChallengeService;

@ExtendWith(MockitoExtension.class)
@DisplayName("ChoiceReturnCodesService")
class ChoiceReturnCodesServiceTest {

	private static final Random random = RandomFactory.createRandom();
	private static final String TWO_POW_256 = "115792089237316195423570985008687907853269984665640564039457584007913129639936";

	private static GqGroup gqGroup;

	private final String electionEventId = "1882e6ff0c6e4557808272f8841b6af0";
	private final String verificationCardId = "e4a26a470347c9f54324866a185893d2";
	private final String verificationCardSetId = "22484e401f6b4bb188ea9e2ef24e7b3f";
	private final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();

	private ExtractCRCAlgorithm mockExtractCRCAlgorithm;
	private ChoiceReturnCodesService choiceReturnCodesService;
	private SignatureKeystore<Alias> mockSignatureKeystoreService;
	private OrchestratorFacade mockOrchestratorFacade;
	private ReturnCodesMappingTableService mockReturnCodesMappingTableService;
	private VoterAuthenticationDataService mockVoterAuthenticationDataService;
	private VerificationCardService mockVerificationCardService;
	private ElectionEventService mockElectionEventService;
	private ReturnCodesMappingTableSupplier mockReturnCodesMappingTableSupplier;
	private EncryptedVerifiableVote encryptedVerifiableVote;
	private String ballotId;
	private String credentialId;
	private String derivedAuthenticationChallenge;
	private BigInteger authenticationNonce;

	private static InputStream getResourceAsStream(final String name) {
		return ChoiceReturnCodesServiceTest.class.getResourceAsStream("/processor/choiceReturnCodesServiceTest/" + name);
	}

	@BeforeAll
	static void setUpAll() {
		gqGroup = new GqGroup(
				new BigInteger(
						"4688924687101842747043789622943639451238379583421515083490992727662966887592566806385937818968022125389969548661587837554751555551125316712096348517237427873704786293289327916804886782297937842786976257115026543950184245775805780806466220397371589271121288423507399259602000829340247207828163695625078614543895796426520749021726851028889703185286047412971103954221566262244551464311742715148749253272752397456639673970809661134301137187709504653404337846916552289737947354931132013578668381751783880929044628310827581093820299384525520498865891279620245835238216578248104372858793870818677470303875907983918128169426975079981824932085456362035949171853839641915630629131160070262536188292899390408699336228786000874475423989873636678599713537438189405718684830889918695758274995475275472436678812224043372657209577592652124268309300980776740510688847875393810499063675510899395946989871928027318991345508642195522561212328039"),
				new BigInteger(
						"2344462343550921373521894811471819725619189791710757541745496363831483443796283403192968909484011062694984774330793918777375777775562658356048174258618713936852393146644663958402443391148968921393488128557513271975092122887902890403233110198685794635560644211753699629801000414670123603914081847812539307271947898213260374510863425514444851592643023706485551977110783131122275732155871357574374626636376198728319836985404830567150568593854752326702168923458276144868973677465566006789334190875891940464522314155413790546910149692262760249432945639810122917619108289124052186429396935409338735151937953991959064084713487539990912466042728181017974585926919820957815314565580035131268094146449695204349668114393000437237711994936818339299856768719094702859342415444959347879137497737637736218339406112021686328604788796326062134154650490388370255344423937696905249531837755449697973494935964013659495672754321097761280606164019"),
				BigInteger.valueOf(2));
	}

	@BeforeEach
	void setUp() throws SignatureException, IOException {
		mockExtractCRCAlgorithm = mock(ExtractCRCAlgorithm.class);
		mockSignatureKeystoreService = mock(SignatureKeystore.class);
		mockOrchestratorFacade = mock(OrchestratorFacade.class);
		mockVoterAuthenticationDataService = mock(VoterAuthenticationDataService.class);
		mockVerificationCardService = mock(VerificationCardService.class);
		mockElectionEventService = mock(ElectionEventService.class);
		mockReturnCodesMappingTableSupplier = mock(ReturnCodesMappingTableSupplier.class);

		choiceReturnCodesService = new ChoiceReturnCodesService(
				mockExtractCRCAlgorithm,
				mockSignatureKeystoreService,
				mockOrchestratorFacade,
				mockElectionEventService,
				mockVerificationCardService,
				mockVoterAuthenticationDataService,
				mockReturnCodesMappingTableSupplier
		);

		final String dummySignature = "dummySignature";
		when(mockSignatureKeystoreService.generateSignature(any(), any())).thenReturn(dummySignature.getBytes(StandardCharsets.UTF_8));

		when(mockElectionEventService.getEncryptionGroup(electionEventId)).thenReturn(gqGroup);

		ballotId = random.genRandomBase16String(32);
		credentialId = random.genRandomBase16String(32);
		derivedAuthenticationChallenge = random.genRandomBase64String(44);
		authenticationNonce = random.genRandomInteger(new BigInteger(TWO_POW_256));

		final JsonNode voteWithBallotIdNode = objectMapper.readTree(getResourceAsStream("voteWithBallotIdNode.json"));
		final String encryptedVerifiableVoteJson = voteWithBallotIdNode.get("encryptedVerifiableVote").toString();
		encryptedVerifiableVote = deserializeEncryptedVerifiableVote(encryptedVerifiableVoteJson);
	}

	@Test
	@DisplayName("retrieveShortChoiceReturnCodes with valid parameters and happy path")
	void retrieveShortChoiceReturnCodesHappyPath() throws IOException, SignatureException {
		// given
		setupMockMessageBrokerOrchestratorClient("partiallyDecryptedEncryptedPCCPayloads/happyPath.json");

		final List<ControlComponentLCCSharePayload> longReturnCodesSharePayload = Arrays.asList(
				objectMapper.readValue(getResourceAsStream("longReturnCodesSharePayloads.json"), ControlComponentLCCSharePayload[].class));
		final List<GroupVector<GqElement, GqGroup>> lccShares = choiceReturnCodesService.getLCCShares(longReturnCodesSharePayload);
		when(mockExtractCRCAlgorithm.extractCRC(any(), any())).thenReturn(getExtractCRCOutput(lccShares.get(0).size()));
		when(mockOrchestratorFacade.getLongChoiceReturnCodesContributions(eq(electionEventId), eq(verificationCardSetId),
				eq(verificationCardId), any())).thenReturn(longReturnCodesSharePayload);

		when(mockSignatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(true);

		final SetupComponentVoterAuthenticationData setupComponentVoterAuthenticationData = new SetupComponentVoterAuthenticationData(
				electionEventId,
				verificationCardSetId,
				verificationCardSetId,
				verificationCardSetId,
				ballotId,
				verificationCardId,
				verificationCardId,
				credentialId,
				derivedAuthenticationChallenge
		);
		when(mockVoterAuthenticationDataService.getVoterAuthenticationData(anyString(), anyString())).thenReturn(
				setupComponentVoterAuthenticationData);
		when(mockReturnCodesMappingTableSupplier.get(verificationCardSetId)).thenReturn(hashedLongReturnCode -> Optional.empty());
		doNothing().when(mockVerificationCardService).saveSentState(anyString(), any());

		// when
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		final List<String> shortChoiceReturnCodes = choiceReturnCodesService.retrieveShortChoiceReturnCodes(contextIds, credentialId,
				encryptedVerifiableVote);

		// then
		verify(mockSignatureKeystoreService, atLeast(1)).verifySignature(any(), any(), any(), any());
		verify(mockSignatureKeystoreService).generateSignature(any(), any());

		assertNotNull(shortChoiceReturnCodes);
		assertFalse(shortChoiceReturnCodes.isEmpty());
	}

	@Test
	@DisplayName("retrieveShortChoiceReturnCodes with different gqGroups")
	void allGqGroupsAreNotTheSame() throws IOException {
		// given
		setupMockMessageBrokerOrchestratorClient("partiallyDecryptedEncryptedPCCPayloads/gqGroupDifferent.json");

		// when / then
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		assertThrows(IllegalStateException.class,
				() -> choiceReturnCodesService.retrieveShortChoiceReturnCodes(contextIds, credentialId, encryptedVerifiableVote),
				"GqGroup is not identical for all the payloads.");
	}

	@Test
	@DisplayName("retrieveShortChoiceReturnCodes with different gqGroups")
	void allContextIdsAreNotTheSame() throws IOException {
		// given
		setupMockMessageBrokerOrchestratorClient("partiallyDecryptedEncryptedPCCPayloads/contextIdsDifferent.json");

		// when / then
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		assertThrows(IllegalStateException.class,
				() -> choiceReturnCodesService.retrieveShortChoiceReturnCodes(contextIds, credentialId, encryptedVerifiableVote),
				"ContextIds are not identical for all the payloads.");
	}

	private void setupMockMessageBrokerOrchestratorClient(final String path) throws IOException {

		final InputStream is = getResourceAsStream(path);
		final List<ControlComponentPartialDecryptPayload> controlComponentPartialDecryptPayloads = Arrays.asList(
				objectMapper.readValue(is, ControlComponentPartialDecryptPayload[].class));
		when(mockOrchestratorFacade.getChoiceReturnCodesPartialDecryptContributions(eq(electionEventId), eq(verificationCardSetId),
				eq(verificationCardId), any())).thenReturn(controlComponentPartialDecryptPayloads);
	}

	private ExtractCRCOutput getExtractCRCOutput(final int psi) {
		final List<String> shortChoiceReturnCodes = new ArrayList<>();
		for (int i = 0; i < psi; i++) {
			shortChoiceReturnCodes.add("1234");
		}
		return new ExtractCRCOutput(shortChoiceReturnCodes);
	}

	private EncryptedVerifiableVote deserializeEncryptedVerifiableVote(final String encryptedVerifiableVoteJson) {
		try {
			return objectMapper.reader()
					.withAttribute("group", gqGroup)
					.readValue(encryptedVerifiableVoteJson, EncryptedVerifiableVote.class);
		} catch (final IOException e) {
			throw new UncheckedIOException("Failed to deserialize encrypted verifiable vote.", e);
		}
	}

}
