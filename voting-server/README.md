# Voting Server
The voting server is an integral component of the Swiss Post Voting System, facilitating secure and dependable communication between the voting client, online Secure Data Manager, and control components. 
The voting server communicates with the voting client and online Secure Data Manager via HTTPS, and with the control components using AMQP. HTTPS (Hypertext Transfer Protocol Secure) is a secure version of HTTP, the primary protocol used for data communication on the World Wide Web, which encrypts data exchange between a client and a server using SSL/TLS encryption to ensure privacy and data integrity. AMQP (Advanced Message Queuing Protocol) is an open-standard application layer protocol for message-oriented middleware, designed to enable interoperable and reliable messaging between distributed systems.
This hybrid communication approach guarantees reliable and efficient message exchange within the Swiss Post Voting System.

In addition, the voting server performs authentication of the voting client in the VerifyAuthenticationChallenge algorithm during the execution of the voting phase, making certain that only eligible voters can interact with the voting server.

Furthermore, the voting server executes the ExtractCRC and ExtractVCC algorithms of the cryptographic protocol.

The voting server's codebase is organized into the following distinct elements:

| Name             | Description                                                                                                                                                     | 
|------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **processor**    | Contains the voting server's business logic that implements voter authentication and the algorithms of the cryptographic protocol.                              |
| **orchestrator** | Manages the communication between the voting client / online SDM and the voting server (using HTTPS) and the voting server and the control components (using AMQP).  |
| **common**       | Contains shared code utilized by both the processor and the orchestrator.                                                                                       |

## Usage

The voting server is packaged as a .jar file and deployed in a Spring Boot instance.

## Development

```
mvn clean install
```
