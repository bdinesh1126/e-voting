/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
/* jshint ignore: start */
/* global require */

const { validateUUID } = require("../../services/validation-service");
const createVoteAlgorithm = require("./create-vote-algorithm");
const sessionService = require("../../services/session-service");
const endpointService = require("../../services/endpoint-service");
const { VotingServerResponseError } = require("../../services/voting-server-response-error");
const writeInToQuadraticResidueAlgorithm = require("../../write-ins/write-in-to-quadratic-residue-algorithm");
const getAuthenticationChallengeAlgorithm = require("../authenticate-voter/get-authentication-challenge-algorithm");
const {
  serializeElGamalCiphertext,
  serializeExponentiationProof,
  serializePlaintextEqualityProof,
  serializeGqGroup
} = require("../../services/primitives-serializer");
const { GqElement } = require("crypto-primitives-ts/lib/cjs/math/gq_element");
const { GroupVector } = require("crypto-primitives-ts/lib/cjs/group_vector");
const { PrimeGqElement } = require("crypto-primitives-ts/lib/cjs/math/prime_gq_element");
const { ImmutableBigInteger } = require("crypto-primitives-ts/lib/cjs/immutable_big_integer");

module.exports = (function() {
  "use strict";

  /**
   * Implements the sendVote phase of the protocol.
   *
   * @param {string[]} selectedVotingOptions, the voter selected voting options.
   * @param {string[]} voterWriteIns, the voter write ins
   * @returns {Promise<SendVoteResponse>}, the sendVote response expected by the Voter-Portal.
   */
  async function sendVotePhase(selectedVotingOptions, voterWriteIns) {
    const primitivesParams = sessionService.primitivesParams();
    const voterAuthenticationData = sessionService.voterAuthenticationData();

    // Call CreateVote algorithm
    const createVoteOutput = createVoteAlgorithm.createVote(
      {
        encryptionGroup: primitivesParams.encryptionGroup,
        electionEventId: validateUUID(voterAuthenticationData.electionEventId),
        numberOfSelectableVotingOptions: primitivesParams.correctnessInformationSelections.length,
        encodedVotingOptions: primitivesParams.encodedVotingOptions,
        actualVotingOptions: primitivesParams.actualVotingOptions,
        semanticInformation: primitivesParams.semanticInformation
      },
      validateUUID(voterAuthenticationData.verificationCardId),
      convertSelectedVotingOptionsToPrimeGqElements(selectedVotingOptions, primitivesParams.encryptionGroup),
      computeWriteInsToQuadraticResidues(voterWriteIns, primitivesParams.encryptionGroup, primitivesParams.totalNumberOfWriteIns),
      primitivesParams.electionPublicKey,
      primitivesParams.choiceReturnCodesEncryptionPublicKey,
      sessionService.verificationCardSecretKey()
    );

    // Call GetAuthenticationChallenge algorithm
    const authenticationChallenge = await getAuthenticationChallengeAlgorithm.getAuthenticationChallenge(
      {
        electionEventId: voterAuthenticationData.electionEventId,
        authenticationStep: "sendVote"
      },
      sessionService.startVotingKey(),
      sessionService.extendedAuthenticationFactor()
    );

    // Prepare and post payload to the Voting Server
    const sendVoteResponsePayload = await sendVoteToVotingServer(createVoteOutput, authenticationChallenge);

    /**
     * @typedef {object} SendVoteResponse.
     * @property {string[]} choiceReturnCodes, the short choice return codes.
     */
    return {
      choiceReturnCodes: sendVoteResponsePayload.shortChoiceReturnCodes
    };
  }

  /**
   * Converts the selected voting options to a list of primes.
   *
   * @param {string[]} selectedVotingOptions, the voter selected voting options.
   * @param {GqGroup} encryptionGroup, the encryption group.
   * @returns {GroupVector<PrimeGqElement>}, the selected voting options as a list of primes.
   */
  function convertSelectedVotingOptionsToPrimeGqElements(selectedVotingOptions, encryptionGroup) {
    const selectedEncodedVotingOptionList = selectedVotingOptions.map(function(o) {
      // Avoid type coercion in the voter's selections
      const votersSelection = parseInt(o, 10);
      return PrimeGqElement.fromValue(votersSelection, encryptionGroup);
    });
    return GroupVector.from(selectedEncodedVotingOptionList);
  }

  /**
   * Converts the selected voting options to a list of primes.
   *
   * @param {string[]} voterWriteIns, the voter writeIns.
   * @param {GqGroup} encryptionGroup, the encryption group.
   * @param {number} totalNumberOfWriteIns, the total number of allowed writeIns.
   * @returns {GroupVector<GqElement>}, the voter writeIns as a list of integers.
   */
  function computeWriteInsToQuadraticResidues(voterWriteIns, encryptionGroup, totalNumberOfWriteIns) {
    const writeInsQuadraticResidues = voterWriteIns.map(
      (w) => writeInToQuadraticResidueAlgorithm.writeInToQuadraticResidue({ encryptionGroup: encryptionGroup }, w)
    );
    // Add dummy writeIns if needed.
    const writeInsDummies = [];
    const dummyValue = GqElement.fromValue(ImmutableBigInteger.ONE, encryptionGroup);
    for (let i = 0; i < totalNumberOfWriteIns - writeInsQuadraticResidues.length; i++) {
      writeInsDummies[i] = dummyValue;
    }
    return GroupVector.of(...writeInsQuadraticResidues, ...writeInsDummies);
  }

  /**
   * Sends the vote to the Voting-Server.
   *
   * @param {Vote} createVoteOutput, the output of the createVote algorithm.
   * @param {AuthenticationChallenge} authenticationChallenge, the authentication challenge.
   * @returns {SendVoteResponsePayload}, the sendVote response payload produced by the Voting-Server.
   */
  async function sendVoteToVotingServer(createVoteOutput, authenticationChallenge) {
    const voterAuthenticationData = sessionService.voterAuthenticationData();

    // Prepare the sendVotePayload
    const sendVotePayload = prepareSendVotePayload(createVoteOutput, authenticationChallenge);

    // Prepare endpoint
    const sendVoteEndpoint = endpointService.sendVote(
      voterAuthenticationData.electionEventId,
      voterAuthenticationData.verificationCardSetId,
      voterAuthenticationData.credentialId,
      voterAuthenticationData.verificationCardId
    );

    // Post the payload
    const response = await fetch(sendVoteEndpoint,
      {
        method: "POST",
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json;charset=UTF-8"
        },
        body: JSON.stringify(sendVotePayload)
      }
    );

    // Check if errors
    if (!response.ok) {
      const errorJson = await response.json();
      throw new VotingServerResponseError(response.status, errorJson);
    }

    /**
     * @typedef {object} SendVoteResponsePayload.
     * @property {string[]} shortChoiceReturnCodes, the short choice return codes
     */
    return response.json();
  }

  /**
   * Prepares the sendVote payload expected by the Voting-Server.
   *
   * @param {Vote} createVoteOutput, the output of the createVote algorithm.
   * @param {AuthenticationChallenge} authenticationChallenge, the authentication challenge.
   * @returns {SendVotePayload}, the sendVote payload expected by the Voting-Server.
   */
  function prepareSendVotePayload(createVoteOutput, authenticationChallenge) {
    // Serialize vote elements for payload
    const serializedEncryptedVote = serializeElGamalCiphertext(createVoteOutput.encryptedVote);
    const serializedExponentiatedEncryptedVote = serializeElGamalCiphertext(createVoteOutput.exponentiatedEncryptedVote);
    const serializedEncryptedPartialChoiceReturnCodes = serializeElGamalCiphertext(createVoteOutput.encryptedPartialChoiceReturnCodes);
    const serializedExponentiationProof = serializeExponentiationProof(createVoteOutput.exponentiationProof);
    const serializedPlaintextEqualityProof = serializePlaintextEqualityProof(createVoteOutput.plaintextEqualityProof);

    const primitivesParams = sessionService.primitivesParams();
    const serializedEncryptionGroup = serializeGqGroup(primitivesParams.encryptionGroup);
    const voterAuthenticationData = sessionService.voterAuthenticationData();

    /**
     * @typedef {object} EncryptedVerifiableVote.
     * @property {ContextIds} contextIds, the context identifiers.
     * @property {string} encryptedVote, the serialized encrypted vote.
     * @property {string} exponentiatedEncryptedVote, the serialized exponentiated encrypted vote.
     * @property {string} encryptedPartialChoiceReturnCodes, the serialized encrypted partial choice return codes.
     * @property {string} exponentiationProof, the serialized exponentiation proof.
     * @property {string} plaintextEqualityProof, the serialized plaintext equality proof.
     */

    /**
     * @typedef {object} SendVotePayload.
     * @property {ContextIds} contextIds, the context identifiers.
     * @property {string} encryptionGroup, the serialized encryption group.
     * @property {EncryptedVerifiableVote} encryptedVerifiableVote, the encrypted verifiable vote.
     * @property {AuthenticationChallenge} authenticationChallenge, the authentication challenge.
     */
    return {
      contextIds: {
        electionEventId: voterAuthenticationData.electionEventId,
        verificationCardSetId: voterAuthenticationData.verificationCardSetId,
        verificationCardId: voterAuthenticationData.verificationCardId
      },
      encryptionGroup: JSON.parse(serializedEncryptionGroup),
      encryptedVerifiableVote: {
        contextIds: {
          electionEventId: voterAuthenticationData.electionEventId,
          verificationCardSetId: voterAuthenticationData.verificationCardSetId,
          verificationCardId: voterAuthenticationData.verificationCardId
        },
        encryptedVote: JSON.parse(serializedEncryptedVote),
        exponentiatedEncryptedVote: JSON.parse(serializedExponentiatedEncryptedVote),
        encryptedPartialChoiceReturnCodes: JSON.parse(serializedEncryptedPartialChoiceReturnCodes),
        exponentiationProof: JSON.parse(serializedExponentiationProof),
        plaintextEqualityProof: JSON.parse(serializedPlaintextEqualityProof)
      },
      authenticationChallenge: authenticationChallenge
    };
  }

  return {
    sendVotePhase: sendVotePhase
  };
})();
