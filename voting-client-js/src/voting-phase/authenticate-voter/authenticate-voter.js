/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
/* global require */

const {parseBallot} = require("../../model/ballot-parser");
const {validateUUID, validateSVK} = require("../../services/validation-service");
const sessionService = require("../../services/session-service");
const getKeyAlgorithm = require("./get-key-algorithm");
const endpointService = require("../../services/endpoint-service.js");
const primitivesParamsParser = require("../../services/primitives-params-service");
const {VotingServerResponseError} = require("../../services/voting-server-response-error");
const getAuthenticationChallengeAlgorithm = require("./get-authentication-challenge-algorithm");
const {CHARACTER_LENGTH_OF_START_VOTING_KEY, START_VOTING_KEY_INVALID_ERROR_CODE} = require("../../voter-authentication/constants");
const {VoterPortalInputError} = require("../../services/voter-portal-input-error");

module.exports = (function () {
	"use strict";

	/**
	 * Implements the authenticateVoter phase of the protocol.
	 *
	 * @param {string} startVotingKey, the voter start voting key.
	 * @param {string} extendedAuthenticationFactor, the voter extended authentication factor (i.e. birthdate).
	 * @param {string} electionEventId, the election event id.
	 * @param {string} lang, the supported language code.
	 * @returns {Promise<AuthenticateVoterResponse>}, the authenticateVoter response expected by the Voter-Portal.
	 */
	async function authenticateVoterPhase(startVotingKey, extendedAuthenticationFactor, electionEventId, lang) {

		validateStartVotingKeyInput(startVotingKey);

		// Call GetAuthenticationChallenge algorithm.
		const authenticationChallenge = await getAuthenticationChallengeAlgorithm.getAuthenticationChallenge(
			{
				electionEventId: electionEventId,
				authenticationStep: "authenticateVoter"
			},
			startVotingKey,
			extendedAuthenticationFactor
		);

		// Store authentication challenge inputs in session.
		sessionService.startVotingKey(startVotingKey);
		sessionService.extendedAuthenticationFactor(extendedAuthenticationFactor);

		// Prepare and post payload to the Voting Server.
		const authenticateVoterResponsePayload = await authenticateVoterToVotingServer(electionEventId, authenticationChallenge);

		// If the verification card is already CONFIRMED (happens in a re-login), there is nothing left to calculate.
		const verificationCardState = authenticateVoterResponsePayload.verificationCardState;

		/**
		 * @typedef {object} AuthenticateVoterResponse.
		 * @property {string} votingCardState, the voting card state.
		 * @property {object} [ballot], the parsed ballot.
		 * @property {string[]} [choiceReturnCodes], the short choice return codes.
		 * @property {string} [voteCastReturnCode], the short vote cast return code.
		 */
		const authenticateVoterResponse = {
			votingCardState: verificationCardState
		};

		if (verificationCardState === "INITIAL" || verificationCardState === "SENT") {
			// The verificationCard in the payload is escaped as string, we must convert it before returning its value.
			authenticateVoterResponsePayload.verificationCard = JSON.parse(authenticateVoterResponsePayload.verificationCard);

			// Store ids in session.
			sessionService.voterAuthenticationData(authenticateVoterResponsePayload.voterAuthenticationData);

			// Parse the primitives params.
			const primitivesParams = primitivesParamsParser.parsePrimitivesParams(authenticateVoterResponsePayload);
			sessionService.primitivesParams(primitivesParams);

			// Call GetKey algorithm.
			// NOSONAR
			const verificationCardSecretKey = await getKeyAlgorithm.getKey(
				{
					encryptionGroup: primitivesParams.encryptionGroup,
					electionEventId: validateUUID(authenticateVoterResponsePayload.voterAuthenticationData.electionEventId),
					verificationCardSetId: authenticateVoterResponsePayload.voterAuthenticationData.verificationCardSetId,
					electionPublicKey: primitivesParams.electionPublicKey,
					choiceReturnCodesEncryptionPublicKey: primitivesParams.choiceReturnCodesEncryptionPublicKey,
					encodedVotingOptions: primitivesParams.encodedVotingOptions,
					actualVotingOptions: primitivesParams.actualVotingOptions,
					semanticInformation: primitivesParams.semanticInformation,
					ciSelections: primitivesParams.correctnessInformationSelections,
					ciVotingOptions: primitivesParams.correctnessInformationVotingOptions,
					characterLengthOfTheStartVotingKey: CHARACTER_LENGTH_OF_START_VOTING_KEY
				},
				startVotingKey,
				authenticateVoterResponsePayload.verificationCard.verificationCardKeystore,
				authenticateVoterResponsePayload.verificationCard.id
			);

			// Store the verification card secret key in session.
			sessionService.verificationCardSecretKey(verificationCardSecretKey);

			// The ballotTexts in the payload is escaped as string, we must convert it before returning its value.
			authenticateVoterResponsePayload.voterMaterial.ballotTexts = JSON.parse(authenticateVoterResponsePayload.voterMaterial.ballotTexts);
			// The ballot must be parsed to integrate the ballotTexts and prepare a specific ballot for the Voter-Portal.
			authenticateVoterResponse.ballot = await parseBallot(authenticateVoterResponsePayload.voterMaterial.ballot,
				authenticateVoterResponsePayload.voterMaterial.ballotTexts, lang);

			// In the SENT state, we also have the Choice Return Codes to return.
			if (verificationCardState === "SENT") {
				authenticateVoterResponse.choiceReturnCodes = authenticateVoterResponsePayload.voterMaterial.shortChoiceReturnCodes;
			}
		}

		// In the CONFIRMED state, we do not need to perform getKey algorithm. Only the Cast Return Code is needed.
		if (verificationCardState === "CONFIRMED") {
			authenticateVoterResponse.voteCastReturnCode = authenticateVoterResponsePayload.voterMaterial.shortVoteCastReturnCode;
		}

		return authenticateVoterResponse;
	}

	/**
	 * Throws a Voter Portal input error if the start voting key is not valid.
	 * @param {string} startVotingKey, the voter start voting key.
	 */
	function validateStartVotingKeyInput(startVotingKey) {
		try {
			validateSVK(startVotingKey);
		} catch (_error) {
			throw new VoterPortalInputError(START_VOTING_KEY_INVALID_ERROR_CODE);
		}
	}

	/**
	 * Authenticates the voter to the Voting-Server.
	 *
	 * @param {string} electionEventId, the election event id.
	 * @param {AuthenticationChallenge} authenticationChallenge, the authentication challenge.
	 * @returns {AuthenticateVoterResponsePayload}, the authenticateVoter response payload produced by the Voting-Server.
	 */
	async function authenticateVoterToVotingServer(electionEventId, authenticationChallenge) {

		/**
		 * @typedef {object} AuthenticateVoterPayload.
		 * @property {string} electionEventId, the election event id.
		 * @property {AuthenticationChallenge} authenticationChallenge, the authentication challenge.
		 */
		const authenticateVoterPayload = {
			electionEventId: electionEventId,
			authenticationChallenge: authenticationChallenge
		};

		// Prepare endpoint
		const authenticateVoterEndpoint = endpointService.authenticateVoter(
			electionEventId,
			authenticationChallenge.derivedVoterIdentifier
		);

		// Post the payload
		const response = await fetch(authenticateVoterEndpoint,
			{
				method: "POST",
				headers: {
					"Accept": "application/json",
					"Content-Type": "application/json;charset=UTF-8"
				},
				body: JSON.stringify(authenticateVoterPayload)
			}
		);

		// Check if errors
		if (!response.ok) {
			const errorJson = await response.json();
			throw new VotingServerResponseError(response.status, errorJson);
		}

		/**
		 * @typedef {object} VoterAuthenticationData.
		 * @property {string} electionEventId, the election event id.
		 * @property {string} verificationCardSetId, the verification card set id.
		 * @property {string} votingCardSetId, the svoting card set id.
		 * @property {string} ballotBoxId, the ballot box id.
		 * @property {string} ballotId, the ballot id.
		 * @property {string} verificationCardId, the verification card id.
		 * @property {string} ballotBoxId, the voting card id.
		 * @property {string} credentialId, the credential id.
		 */
		/**
		 * @typedef {object} VotingClientPublicKeys.
		 * @property {string} encryptionParameters, the serialized encryption parameters.
		 * @property {string[]} electionPublicKey, the serialized election public key.
		 * @property {string[]} choiceReturnCodesEncryptionPublicKey, the serialized choice return codes encryption public key.
		 */
		/**
		 * @typedef {object} VoterMaterial.
		 * @property {object} [ballot], the ballot of the voter.
		 * @property {object} [ballotTexts], the ballot translations.
		 * @property {string[]} [shortChoiceReturnCodes], the short choice return codes.
		 * @property {string} [shortVoteCastReturnCode], the short vote cast return code.
		 */
		/**
		 * @typedef {object} AuthenticateVoterResponsePayload.
		 * @property {string} verificationCardState, the verification card state.
		 * @property {VoterAuthenticationData} voterAuthenticationData, the voter context ids.
		 * @property {object} verificationCard, all the verification card information.
		 * @property {VotingClientPublicKeys} votingClientPublicKeys, contains the public keys.
		 * @property {VoterMaterial} voterMaterial, the voter material.
		 */
		return response.json();
	}

	return {
		authenticateVoterPhase: authenticateVoterPhase
	};
})();
