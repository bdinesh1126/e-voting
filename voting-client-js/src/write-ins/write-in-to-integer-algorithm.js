/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

const {WRITE_IN_ALPHABET} = require("./write-in-alphabet");
const {ZqGroup} = require("crypto-primitives-ts/lib/cjs/math/zq_group");
const {ZqElement} = require("crypto-primitives-ts/lib/cjs/math/zq_element");
const {ImmutableBigInteger} = require("crypto-primitives-ts/lib/cjs/immutable_big_integer");
const {checkNotNull, checkArgument} = require("crypto-primitives-ts/lib/cjs/validation/preconditions");

module.exports = (function () {
  'use strict';

  /**
   * @typedef {object} WriteInToIntegerContext
   * @property {GqGroup} encryptionGroup, the encryption parameters p, q and g.
   */

  /**
   * Implements the WriteInToInteger algorithm described in the cryptographic protocol.
   * Maps a character string to an integer value.
   *
   * @param {WriteInToIntegerContext} context, the WriteInToInteger context.
   * @param {string} characterString, s the character string to map.
   * @returns {ZqElement}, x the mapped value.
   */
  function writeInToInteger(
    context,
    characterString
  ) {
    checkNotNull(context);
    const encryptionGroup = checkNotNull(context.encryptionGroup);
    const s = checkNotNull(characterString);

    const a = ImmutableBigInteger.fromNumber(WRITE_IN_ALPHABET.length);
    const s_length = ImmutableBigInteger.fromNumber(s.length);


    // Require.
    checkArgument(checkExpLength(a, s_length, encryptionGroup), 'The exponential form of a to s_length must be smaller than q.');
    checkArgument(s_length > 0, 'The character string length must be greater than 0.');
    checkArgument(s.charAt(0) !== WRITE_IN_ALPHABET[0], 'The character string must not start with rank 0 character.');

    // Operation.
    let x = ImmutableBigInteger.fromNumber(0);
    for (let i = 0; i < s.length; i++) {
      const c = s.charAt(i);
      const b = ImmutableBigInteger.fromNumber(WRITE_IN_ALPHABET.indexOf(c));

      x = x.multiply(a).add(b);
    }

    return ZqElement.create(x, ZqGroup.sameOrderAs(encryptionGroup));
  }

  /**
   * Provides a check on the exponential form of a to s_length compared to q.
   * We use logarithm operation on lengths instead of exponentiation on big numbers, because exponentiation can be expensive.
   * Corresponds to a ** s_length < q.
   *
   * @param {ImmutableBigInteger} a, the write-in alphabet length.
   * @param {ImmutableBigInteger} s_length, the string length.
   * @param {GqGroup} encryptionGroup, the encryption group.
   * @returns {boolean}, true if the exponential form of a to s_length is smaller than q.
   */
  function checkExpLength(a, s_length, encryptionGroup) {
    checkNotNull(a);
    checkNotNull(s_length);
    checkNotNull(encryptionGroup);
    const a_int = a.intValue();
    const s_length_int = s_length.intValue();
    const q_bitLength = encryptionGroup.q.bitLength();
    const p = encryptionGroup.p;

    const exp = Math.ceil(Math.log2(a_int) * s_length_int);

    if (exp < q_bitLength) {
      return true;
    }
    if (exp > q_bitLength) {
      return false;
    }
    return a.modPow(s_length, p) < q_bitLength;
  }

  return {
    writeInToInteger: writeInToInteger,
    checkExpLength: checkExpLength
  }
})();
