/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });

/**
 * List of symbols (with their UTF-8 codepoint) included in the write-in alphabet.
 * @type {string[]} WRITE_IN_ALPHABET
 */
exports.WRITE_IN_ALPHABET = [
  String.fromCharCode(parseInt('0023', 16)), // # (U+0023) -> 000
  String.fromCharCode(parseInt('0020', 16)), //   (U+0020) -> 001
  String.fromCharCode(parseInt('0027', 16)), // ' (U+0027) -> 002
  String.fromCharCode(parseInt('0028', 16)), // ( (U+0028) -> 003
  String.fromCharCode(parseInt('0029', 16)), // ) (U+0029) -> 004
  String.fromCharCode(parseInt('002C', 16)), // , (U+002C) -> 005
  String.fromCharCode(parseInt('002D', 16)), // - (U+002D) -> 006
  String.fromCharCode(parseInt('002E', 16)), // . (U+002E) -> 007
  String.fromCharCode(parseInt('002F', 16)), // / (U+002F) -> 008
  String.fromCharCode(parseInt('0030', 16)), // 0 (U+0030) -> 009

  String.fromCharCode(parseInt('0031', 16)), // 1 (U+0031) -> 010
  String.fromCharCode(parseInt('0032', 16)), // 2 (U+0032) -> 011
  String.fromCharCode(parseInt('0033', 16)), // 3 (U+0033) -> 012
  String.fromCharCode(parseInt('0034', 16)), // 4 (U+0034) -> 013
  String.fromCharCode(parseInt('0035', 16)), // 5 (U+0035) -> 014
  String.fromCharCode(parseInt('0036', 16)), // 6 (U+0036) -> 015
  String.fromCharCode(parseInt('0037', 16)), // 7 (U+0037) -> 016
  String.fromCharCode(parseInt('0038', 16)), // 8 (U+0038) -> 017
  String.fromCharCode(parseInt('0039', 16)), // 9 (U+0039) -> 018
  String.fromCharCode(parseInt('0041', 16)), // A (U+0041) -> 019

  String.fromCharCode(parseInt('0042', 16)), // B (U+0042) -> 020
  String.fromCharCode(parseInt('0043', 16)), // C (U+0043) -> 021
  String.fromCharCode(parseInt('0044', 16)), // D (U+0044) -> 022
  String.fromCharCode(parseInt('0045', 16)), // E (U+0045) -> 023
  String.fromCharCode(parseInt('0046', 16)), // F (U+0046) -> 024
  String.fromCharCode(parseInt('0047', 16)), // G (U+0047) -> 025
  String.fromCharCode(parseInt('0048', 16)), // H (U+0048) -> 026
  String.fromCharCode(parseInt('0049', 16)), // I (U+0049) -> 027
  String.fromCharCode(parseInt('004A', 16)), // J (U+004A) -> 028
  String.fromCharCode(parseInt('004B', 16)), // K (U+004B) -> 029

  String.fromCharCode(parseInt('004C', 16)), // L (U+004C) -> 030
  String.fromCharCode(parseInt('004D', 16)), // M (U+004D) -> 031
  String.fromCharCode(parseInt('004E', 16)), // N (U+004E) -> 032
  String.fromCharCode(parseInt('004F', 16)), // O (U+004F) -> 033
  String.fromCharCode(parseInt('0050', 16)), // P (U+0050) -> 034
  String.fromCharCode(parseInt('0051', 16)), // Q (U+0051) -> 035
  String.fromCharCode(parseInt('0052', 16)), // R (U+0052) -> 036
  String.fromCharCode(parseInt('0053', 16)), // S (U+0053) -> 037
  String.fromCharCode(parseInt('0054', 16)), // T (U+0054) -> 038
  String.fromCharCode(parseInt('0055', 16)), // U (U+0055) -> 039

  String.fromCharCode(parseInt('0056', 16)), // V (U+0056) -> 040
  String.fromCharCode(parseInt('0057', 16)), // W (U+0057) -> 041
  String.fromCharCode(parseInt('0058', 16)), // X (U+0058) -> 042
  String.fromCharCode(parseInt('0059', 16)), // Y (U+0059) -> 043
  String.fromCharCode(parseInt('005A', 16)), // Z (U+005A) -> 044
  String.fromCharCode(parseInt('0061', 16)), // a (U+0061) -> 045
  String.fromCharCode(parseInt('0062', 16)), // b (U+0062) -> 046
  String.fromCharCode(parseInt('0063', 16)), // c (U+0063) -> 047
  String.fromCharCode(parseInt('0064', 16)), // d (U+0064) -> 048
  String.fromCharCode(parseInt('0065', 16)), // e (U+0065) -> 049

  String.fromCharCode(parseInt('0066', 16)), // f (U+0066) -> 050
  String.fromCharCode(parseInt('0067', 16)), // g (U+0067) -> 051
  String.fromCharCode(parseInt('0068', 16)), // h (U+0068) -> 052
  String.fromCharCode(parseInt('0069', 16)), // i (U+0069) -> 053
  String.fromCharCode(parseInt('006A', 16)), // j (U+006A) -> 054
  String.fromCharCode(parseInt('006B', 16)), // k (U+006B) -> 055
  String.fromCharCode(parseInt('006C', 16)), // l (U+006C) -> 056
  String.fromCharCode(parseInt('006D', 16)), // m (U+006D) -> 057
  String.fromCharCode(parseInt('006E', 16)), // n (U+006E) -> 058
  String.fromCharCode(parseInt('006F', 16)), // o (U+006F) -> 059

  String.fromCharCode(parseInt('0070', 16)), // p (U+0070) -> 060
  String.fromCharCode(parseInt('0071', 16)), // q (U+0071) -> 061
  String.fromCharCode(parseInt('0072', 16)), // r (U+0072) -> 062
  String.fromCharCode(parseInt('0073', 16)), // s (U+0073) -> 063
  String.fromCharCode(parseInt('0074', 16)), // t (U+0074) -> 064
  String.fromCharCode(parseInt('0075', 16)), // u (U+0075) -> 065
  String.fromCharCode(parseInt('0076', 16)), // v (U+0076) -> 066
  String.fromCharCode(parseInt('0077', 16)), // w (U+0077) -> 067
  String.fromCharCode(parseInt('0078', 16)), // x (U+0078) -> 068
  String.fromCharCode(parseInt('0079', 16)), // y (U+0079) -> 069

  String.fromCharCode(parseInt('007A', 16)), // z (U+007A) -> 070
  String.fromCharCode(parseInt('00A0', 16)), //   (U+00A0) -> 071
  String.fromCharCode(parseInt('00A2', 16)), // ¢ (U+00A2) -> 072
  String.fromCharCode(parseInt('0160', 16)), // Š (U+0160) -> 073
  String.fromCharCode(parseInt('0161', 16)), // š (U+0161) -> 074
  String.fromCharCode(parseInt('017D', 16)), // Ž (U+017D) -> 075
  String.fromCharCode(parseInt('017E', 16)), // ž (U+017E) -> 076
  String.fromCharCode(parseInt('0152', 16)), // Œ (U+0152) -> 077
  String.fromCharCode(parseInt('0153', 16)), // œ (U+0153) -> 078
  String.fromCharCode(parseInt('0178', 16)), // Ÿ (U+0178) -> 079

  String.fromCharCode(parseInt('00C0', 16)), // À (U+00C0) -> 080
  String.fromCharCode(parseInt('00C1', 16)), // Á (U+00C1) -> 081
  String.fromCharCode(parseInt('00C2', 16)), // Â (U+00C2) -> 082
  String.fromCharCode(parseInt('00C3', 16)), // Ã (U+00C3) -> 083
  String.fromCharCode(parseInt('00C4', 16)), // Ä (U+00C4) -> 084
  String.fromCharCode(parseInt('00C5', 16)), // Å (U+00C5) -> 085
  String.fromCharCode(parseInt('00C6', 16)), // Æ (U+00C6) -> 086
  String.fromCharCode(parseInt('00C7', 16)), // Ç (U+00C7) -> 087
  String.fromCharCode(parseInt('00C8', 16)), // È (U+00C8) -> 088
  String.fromCharCode(parseInt('00C9', 16)), // É (U+00C9) -> 089

  String.fromCharCode(parseInt('00CA', 16)), // Ê (U+00CA) -> 090
  String.fromCharCode(parseInt('00CB', 16)), // Ë (U+00CB) -> 091
  String.fromCharCode(parseInt('00CC', 16)), // Ì (U+00CC) -> 092
  String.fromCharCode(parseInt('00CD', 16)), // Í (U+00CD) -> 093
  String.fromCharCode(parseInt('00CE', 16)), // Î (U+00CE) -> 094
  String.fromCharCode(parseInt('00CF', 16)), // Ï (U+00CF) -> 095
  String.fromCharCode(parseInt('00D0', 16)), // Ð (U+00D0) -> 096
  String.fromCharCode(parseInt('00D1', 16)), // Ñ (U+00D1) -> 097
  String.fromCharCode(parseInt('00D2', 16)), // Ò (U+00D2) -> 098
  String.fromCharCode(parseInt('00D3', 16)), // Ó (U+00D3) -> 099

  String.fromCharCode(parseInt('00D4', 16)), // Ô (U+00D4) -> 100
  String.fromCharCode(parseInt('00D5', 16)), // Õ (U+00D5) -> 101
  String.fromCharCode(parseInt('00D6', 16)), // Ö (U+00D6) -> 102
  String.fromCharCode(parseInt('00D8', 16)), // Ø (U+00D8) -> 103
  String.fromCharCode(parseInt('00D9', 16)), // Ù (U+00D9) -> 104
  String.fromCharCode(parseInt('00DA', 16)), // Ú (U+00DA) -> 105
  String.fromCharCode(parseInt('00DB', 16)), // Û (U+00DB) -> 106
  String.fromCharCode(parseInt('00DC', 16)), // Ü (U+00DC) -> 107
  String.fromCharCode(parseInt('00DD', 16)), // Ý (U+00DD) -> 108
  String.fromCharCode(parseInt('00DE', 16)), // Þ (U+00DE) -> 109

  String.fromCharCode(parseInt('00DF', 16)), // ß (U+00DF) -> 110
  String.fromCharCode(parseInt('00E0', 16)), // à (U+00E0) -> 111
  String.fromCharCode(parseInt('00E1', 16)), // á (U+00E1) -> 112
  String.fromCharCode(parseInt('00E2', 16)), // â (U+00E2) -> 113
  String.fromCharCode(parseInt('00E3', 16)), // ã (U+00E3) -> 114
  String.fromCharCode(parseInt('00E4', 16)), // ä (U+00E4) -> 115
  String.fromCharCode(parseInt('00E5', 16)), // å (U+00E5) -> 116
  String.fromCharCode(parseInt('00E6', 16)), // æ (U+00E6) -> 117
  String.fromCharCode(parseInt('00E7', 16)), // ç (U+00E7) -> 118
  String.fromCharCode(parseInt('00E8', 16)), // è (U+00E8) -> 119

  String.fromCharCode(parseInt('00E9', 16)), // é (U+00E9) -> 120
  String.fromCharCode(parseInt('00EA', 16)), // ê (U+00EA) -> 121
  String.fromCharCode(parseInt('00EB', 16)), // ë (U+00EB) -> 122
  String.fromCharCode(parseInt('00EC', 16)), // ì (U+00EC) -> 123
  String.fromCharCode(parseInt('00ED', 16)), // í (U+00ED) -> 124
  String.fromCharCode(parseInt('00EE', 16)), // î (U+00EE) -> 125
  String.fromCharCode(parseInt('00EF', 16)), // ï (U+00EF) -> 126
  String.fromCharCode(parseInt('00F0', 16)), // ð (U+00F0) -> 127
  String.fromCharCode(parseInt('00F1', 16)), // ñ (U+00F1) -> 128
  String.fromCharCode(parseInt('00F2', 16)), // ò (U+00F2) -> 129

  String.fromCharCode(parseInt('00F3', 16)), // ó (U+00F3) -> 130
  String.fromCharCode(parseInt('00F4', 16)), // ô (U+00F4) -> 131
  String.fromCharCode(parseInt('00F5', 16)), // õ (U+00F5) -> 132
  String.fromCharCode(parseInt('00F6', 16)), // ö (U+00F6) -> 133
  String.fromCharCode(parseInt('00F8', 16)), // ø (U+00F8) -> 134
  String.fromCharCode(parseInt('00F9', 16)), // ù (U+00F9) -> 135
  String.fromCharCode(parseInt('00FA', 16)), // ú (U+00FA) -> 136
  String.fromCharCode(parseInt('00FB', 16)), // û (U+00FB) -> 137
  String.fromCharCode(parseInt('00FC', 16)), // ü (U+00FC) -> 138
  String.fromCharCode(parseInt('00FD', 16)), // ý (U+00FD) -> 139

  String.fromCharCode(parseInt('00FE', 16)), // þ (U+00FE) -> 140
  String.fromCharCode(parseInt('00FF', 16)), // ÿ (U+00FF) -> 141
];

