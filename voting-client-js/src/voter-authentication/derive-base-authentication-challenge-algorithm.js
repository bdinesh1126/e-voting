/*
 * (c) Copyright 2023 Swiss Post Ltd
 */

const {validateSVK, validateUUID, validateExtendedAuthenticationFactor} = require("../services/validation-service");
const {HashService} = require("crypto-primitives-ts/lib/cjs/hashing/hash_service");
const {Base64Service} = require("crypto-primitives-ts/lib/cjs/math/base64_service");
const {Argon2Profile} = require("crypto-primitives-ts/lib/cjs/hashing/argon2_profile");
const {Argon2Service} = require("crypto-primitives-ts/lib/cjs/hashing/argon2_service");
const {cutToBitLength} = require("crypto-primitives-ts/lib/cjs/arrays");
const {stringToByteArray} = require("crypto-primitives-ts/lib/cjs/conversions");
const {ImmutableUint8Array} = require("crypto-primitives-ts/lib/cjs/immutable_uint8Array");

module.exports = (function () {
	'use strict';

	/**
	 * Derives the base authentication challenge for the given election event from the given start voting key and extended authentication factor
	 *
	 * @param {string} electionEventId ee, the identifier of the election event. Must be a valid UUID.
	 * @param {string} startVotingKey SVK<sub>id</sub>, a start voting key. Must be a valid Base32 string without padding of length l<sub>SVK</sub>.
	 * @param extendedAuthenticationFactor EA<sub>id</sub>, an extended authentication factor. Must be a valid Base10 string of length l<sub>EA</sub>.
	 * @return {Promise<string>} the base authentication challenge as a string.
	 * @throws NullPointerError if any of the inputs is null.
	 * @throws FailedValidationError if
	 * <ul>
	 *     <li>the election event id is not a valid UUID</li>
	 *     <li>the start voting key is not a valid Base32 string</li>
	 * </ul>
	 * @throws IllegalArgumentError if
	 * <ul>
	 *     <li>the start voting key is not of size l<sub>SVK</sub></li>
	 *     <li>the extended authentication factor is not a valid Base10 string</li>
	 * </ul>
	 */
	async function deriveBaseAuthenticationChallenge(
		electionEventId,
		startVotingKey,
		extendedAuthenticationFactor
	) {
		const ee = validateUUID(electionEventId);
		const SVK_id = validateSVK(startVotingKey);
		const EA_id = validateExtendedAuthenticationFactor(extendedAuthenticationFactor);

		const hashService = new HashService();
		const argon2Service = new Argon2Service(Argon2Profile.LESS_MEMORY);
		const base64Service = new Base64Service();

		const salt_auth = cutToBitLength(hashService.recursiveHash(ee, 'hAuth'), 128);
		const k = [...stringToByteArray(EA_id).value(), ...stringToByteArray(SVK_id).value()];
		const bhAuth_id = await argon2Service.getArgon2id(ImmutableUint8Array.from(k), salt_auth);
		return base64Service.base64Encode(bhAuth_id.value());
	}

	return {
		deriveBaseAuthenticationChallenge: deriveBaseAuthenticationChallenge
	}
})();
