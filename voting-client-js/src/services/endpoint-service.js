/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

module.exports = (function () {
  'use strict';

  const urls = {
    host: '/vs-ws-rest/api/v1/processor/voting',
    authenticateVoter: 'authenticatevoter/electionevent/{electionEventId}/credentialId/{credentialId}/authenticate',
    sendVote: 'sendvote/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/credentialId/{credentialId}/verificationcard/{verificationCardId}',
    confirmVote: 'confirmvote/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/credentialId/{credentialId}/verificationcard/{verificationCardId}'
  };

  /**
   * Provides the 'authenticateVoter' rest url.
   * @param {string} electionEventId, the election event id.
   * @param {string} credentialId, the credential id.
   * @returns {string} the 'authenticateVoter' url.
   */
  function authenticateVoterUrl(electionEventId, credentialId) {
    const url = urls.authenticateVoter
      .replace('{electionEventId}', electionEventId)
      .replace('{credentialId}', credentialId)
    return `${urls.host}/${url}`;
  }

  /**
   * Provides the 'sendVote' rest url.
   * @param {string} electionEventId, the election event id.
   * @param {string} verificationCardSetId, the verification card set id.
   * @param {string} credentialId, the credential id.
   * @param {string} verificationCardId, the verification card id.
   * @returns {string} the 'sendVote' url.
   */
  function sendVoteUrl(electionEventId, verificationCardSetId, credentialId, verificationCardId) {
    const url = urls.sendVote
      .replace('{electionEventId}', electionEventId)
      .replace('{verificationCardSetId}', verificationCardSetId)
      .replace('{credentialId}', credentialId)
      .replace('{verificationCardId}', verificationCardId);
    return `${urls.host}/${url}`;
  }

  /**
   * Provides the 'confirmVote' rest url.
   * @param {string} electionEventId, the election event id.
   * @param {string} verificationCardSetId, the verification card set id.
   * @param {string} credentialId, the credential id.
   * @param {string} verificationCardId, the verification card id.
   * @returns {string} the 'confirmVote' url.
   */
  function confirmVoteUrl(electionEventId, verificationCardSetId, credentialId, verificationCardId) {
    const url = urls.confirmVote
      .replace('{electionEventId}', electionEventId)
      .replace('{verificationCardSetId}', verificationCardSetId)
      .replace('{credentialId}', credentialId)
      .replace('{verificationCardId}', verificationCardId);
    return `${urls.host}/${url}`;
  }

  return {
    authenticateVoter: authenticateVoterUrl,
    sendVote: sendVoteUrl,
    confirmVote: confirmVoteUrl
  };
})();
