/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VotingServerResponseError = void 0;

class VotingServerResponseError extends Error {
	constructor(httpStatus, errorJson) {
		super("Voting Server response error.");

		errorJson.status = httpStatus;

		if (errorJson.errorStatus === 'EXTENDED_FACTOR_INVALID') {
			const serverTimestamp = errorJson.timestamp;
			const clientTimestamp = Math.floor(Date.now() / 1000);

			const timestampDifference = clientTimestamp - serverTimestamp;
			if (Math.abs(timestampDifference) > 300) {
				errorJson.errorStatus = 'TIMESTAMP_MISALIGNMENT';
			}
		}

		this.errorResponse = errorJson;
		// Set the prototype explicitly.
		Object.setPrototypeOf(this, VotingServerResponseError.prototype);
	}
}
exports.VotingServerResponseError = VotingServerResponseError;
//# sourceMappingURL=illegal_argument_error.js.map
