/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FailedValidationError = void 0;

class FailedValidationError extends Error {
	constructor(m) {
		super(m);
		// Set the prototype explicitly.
		Object.setPrototypeOf(this, FailedValidationError.prototype);
	}
}
exports.FailedValidationError = FailedValidationError;
//# sourceMappingURL=illegal_argument_error.js.map