/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
/* global require */
/* jshint maxlen: 666  */

const _ = require("lodash");
const { getText } = require("./util.js");
const { WRITE_IN_ALPHABET } = require("../write-ins/write-in-alphabet");

const { Ballot } = require("./ballot");

const parsers = {
  listsAndCandidates: require("./lists-and-candidates-parser"),
  options: require("./options-parser")
};

const getParser = function(contestType) {
  const parser = parsers[contestType];
  if (parser) {
    return parser;
  } else {
    console.error("Undefined contest type: " + contestType);
    throw new Error("Undefined contest type: " + contestType);
  }
};

module.exports = (function() {
  "use strict";

  let i18nTexts = null;

  const parseContest = function(contestJson) {
    const parser = getParser(contestJson.template);
    return parser.parse(contestJson);
  };

  const parseCorrectnessIds = function(correctnessIds, option, attributes) {
    const attrMap = _.reduce(
      attributes,
      function(acc, attr) {
        acc[attr.id] = attr;
        return acc;
      },
      {}
    );

    const prime = option.representation;
    const attribute = attrMap[option.attribute];

    if (attribute) {
      if (!correctnessIds[prime]) {
        correctnessIds[prime] = [];
      }

      const related = [attribute.id].concat(attribute.related);

      _.each(related, function(rel) {
        if (
          attrMap[rel] &&
          attrMap[rel].correctness &&
          ((attrMap[rel].correctness === true) ||
            (attrMap[rel].correctness === "true"))
        ) {
          correctnessIds[prime].push(rel);
        }
      });
    }
  };

  const removeAllPropertiesNamedParent = function(object) {
    if (object && typeof object === "object") {
      delete object["parent"];

      Object.keys(object).forEach((key) => {
        removeAllPropertiesNamedParent(object[key]);
      });
    }
  };

  const parseBallotJson = function(ballotJson) {
    const ballot = new Ballot(ballotJson.id);

    _.each(ballotJson.contests, function(c) {
      ballot.addContest(parseContest(c));
    });

    const correctnessIds = {};
    _.each(ballotJson.contests, function(contest) {
      _.each(contest.options, function(option) {
        if (option.representation) {
          parseCorrectnessIds(correctnessIds, option, contest.attributes);
        }
      });
    });

    ballot.correctnessIds = correctnessIds;

    // Add write-ins alphabet
    ballot.writeInAlphabet = WRITE_IN_ALPHABET.join("");

    removeAllPropertiesNamedParent(ballot);
    return ballot;
  };

  const getLocaleText = function(locale) {
    let i18n = _.find(i18nTexts, {
      locale: locale
    });
    if (!i18n) {
      i18n = _.find(i18nTexts, {
        locale: locale.replace("-", "_")
      });
    }

    return i18n ? i18n.texts : {};
  };

  const deepClone = function(ballot) {
    return JSON.parse(JSON.stringify(ballot));
  }

  /**
   * Provides a translated ballot.
   * @param ballotToTranslate
   * @param locale
   * @returns {Ballot} translatedBallot
   */
  const translateBallot = async function(ballotToTranslate, locale) {
    const ballot = deepClone(ballotToTranslate);
    const txt = getLocaleText(locale);

    _.each(ballot.contests, function(contest) {
      ballot.title = getText(txt, ballot.id, "title", null, "ballot");
      ballot.description = getText(
        txt,
        ballot.id,
        "description",
        null,
        "ballot"
      );

      const parser = getParser(contest.template);
      parser.setLocale(contest, txt);
    });
    return ballot;
  };

  /**
   * Provides a parsed and translated ballot.
   * @param ballotJson
   * @param ballotTextsJson
   * @param lang
   * @returns {Ballot} parsedBallot
   */
  const parseBallot = async function(ballotJson, ballotTextsJson, lang) {
    const parsedBallot = parseBallotJson(ballotJson);

    i18nTexts = ballotTextsJson;
    return translateBallot(parsedBallot, lang);
  };

  return {
    translateBallot: translateBallot,
    parseBallot: parseBallot
  };
})();
