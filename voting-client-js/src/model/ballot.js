/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

(function() {
    const _hasProp = {}.hasOwnProperty;

    const _extends = function(child, parent) {
        for (const key in parent) {
            if (_hasProp.call(parent, key)) {
                child[key] = parent[key];
            }
        }

        function Ctor() {
            this.constructor = child;
        }

        Ctor.prototype = parent.prototype;
        child.prototype = new Ctor();
        child.__super__ = parent.prototype;

        return child;
    };

    const log = function(msg) {
        // Empty function
    };

    const BallotItem = (function() {
        function newBallotItem(_atId) {
            this.id = _atId;
            this.title = '';
            this.description = '';
        }

        newBallotItem.prototype.getQualifiedId = function() {
            if (this.parent) {
                return `${this.parent.getQualifiedId()}_${this.id}`;
            } else {
                return this.id;
            }
        };

        return newBallotItem;

    })();

    const Option = (function(_super) {
        _extends(newOption, _super);

        function newOption(rawData, isBlank) {
            this.id = rawData.id;
            this.attribute = rawData.attribute;
            this.chosen = false;
            this.isBlank = isBlank || false;
            this.ordinal = 0;
            this.prime = rawData.representation;
            newOption.__super__.constructor.call(this, rawData.id);
        }

        return newOption;

    })(BallotItem);

    const Question = (function(_super) {
        _extends(newQuestion, _super);

        function newQuestion(rawData) {
            this.options = [];
            this.id = rawData.id;
            this.attribute = rawData.attribute;
            this.optionsMinChoices = rawData.min;
            this.optionsMaxChoices = rawData.max;
            this.questionNumber = rawData.questionNumber;
            this.variantBallot = rawData.variantBallot;
            this.ballotIdentification = rawData.ballotIdentification;
            this.blankOption = null;
            this.ordinal = 0;
            newQuestion.__super__.constructor.call(this, rawData.id);
        }

        newQuestion.prototype.addOption = function(option) {
            log('adding option ' + option.representation);
            if (!(option instanceof Option)) {
                throw new Error('Bad argument type, need an Option');
            }
            if (option.isBlank) {
                if (this.blankOption) {
                    throw new Error('Question already has a blank option');
                }
                this.blankOption = option;
            }
            if (!option.isBlank) {
                this.options.push(option);
                option.ordinal = this.options.length;
            }
            option.parent = this;
            return this;
        };

        return newQuestion;

    })(BallotItem);

    const Candidate = (function(_super) {
        _extends(newCandidate, _super);

        newCandidate.IS_WRITEIN = 0x1;

        newCandidate.IS_BLANK = 0x2;

        function newCandidate(rawData, settings) {
            if (settings == null) {
                settings = 0;
            }
            this.id = rawData.id;
            this.allIds = rawData.allIds;
            this.attribute = rawData.attribute;
            this.prime = rawData.representation;
            this.allRepresentations = rawData.allRepresentations;
            this.isBlank = ((settings & this.constructor.IS_BLANK) === this.constructor.IS_BLANK) || false;
            this.isWriteIn = ((settings & this.constructor.IS_WRITEIN) === this.constructor.IS_WRITEIN) || false;
            this.idNumber = '';
            this.name = '';
            this.chosen = 0;
            this.ordinal = 0;
            this.alias = rawData.alias;
            newCandidate.__super__.constructor.call(this, rawData.id);
        }

        return newCandidate;

    })(BallotItem);

    const List = (function(_super) {
        _extends(newList, _super);

        newList.IS_WRITEIN = 0x1;

        newList.IS_BLANK = 0x2;

        function newList(rawList, attribute, settings) {
            if (settings == null) {
                settings = 0;
            }
            this.candidates = [];
            this.id = rawList.id;
            this.attribute = attribute;
            this.idNumber = '';
            this.name = '';
            this.description = '';
            this.chosen = false;
            this.isBlank = ((settings & this.constructor.IS_BLANK) === this.constructor.IS_BLANK) || false;
            this.isWriteIn = ((settings & this.constructor.IS_WRITEIN) === this.constructor.IS_WRITEIN) || false;
            this.blankId = null;
            this.ordinal = 0;
            this.prime = rawList.representation;
            newList.__super__.constructor.call(this, rawList.id);
        }

        newList.prototype.addCandidate = function(candidate) {
            log('adding candidate ' + candidate.prime);
            if (!(candidate instanceof Candidate)) {
                throw new Error('Bad argument type, need a Candidate');
            }
            this.candidates.push(candidate);
            candidate.ordinal = this.candidates.length;
            candidate.parent = this;
            return this;
        };

        newList.prototype.setBlank = function(blankId) {
            if (this.blankId) {
                throw new Error('List already has a blank attribute');
            }
            this.isBlank = true;
            this.blankId = blankId;
            return this;
        };

        return newList;

    })(BallotItem);

    const Contest = (function(_super) {
        _extends(newContest, _super);

        function newContest(rawData) {
            this.template = rawData.template;
            newContest.__super__.constructor.call(this, rawData.id);
        }

        return newContest;

    })(BallotItem);

    const Options = (function(_super) {
        _extends(newOptions, _super);

        function newOptions(rawData) {
            this.questions = [];
            newOptions.__super__.constructor.call(this, rawData);
        }

        newOptions.prototype.addQuestion = function(question) {
            log('adding question ' + question.prime);
            if (!(question instanceof Question)) {
                throw new Error('Bad argument type, need a Question');
            }
            this.questions.push(question);
            question.ordinal = this.questions.length;
            question.parent = this;
            return this;
        };

        return newOptions;

    })(Contest);

    const ListsAndCandidates = (function(_super) {
        _extends(newListsAndCandidates, _super);

        function newListsAndCandidates(rawData) {
            this.allowFullBlank = rawData.fullBlank === 'true';
            this.lists = [];
            this.listQuestion = {
                minChoices: 0,
                maxChoices: 0,
                cumul: 1
            };
            this.candidatesQuestion = {
                minChoices: 0,
                maxChoices: 0,
                hasWriteIns: false,
                fusions: [],
                cumul: 1
            };
            newListsAndCandidates.__super__.constructor.call(this, rawData);
        }

        newListsAndCandidates.prototype.addList = function(list) {
            log('adding list ' + list.prime);
            if (!(list instanceof List)) {
                throw new Error('Bad argument type, need a List');
            }
            this.lists.push(list);
            list.ordinal = this.lists.length;
            list.parent = this;
            return this;
        };

        return newListsAndCandidates;

    })(Contest);

    const Ballot = (function(_super) {
        _extends(newBallot, _super);

        function newBallot(id) {
            this.contests = [];
            newBallot.__super__.constructor.apply(this, arguments);
        }

        newBallot.prototype.addContest = function(contest) {
            if (!(contest instanceof Contest)) {
                throw new Error('Bad argument type, need a Contest');
            }
            this.contests.push(contest);
            contest.parent = this;
            return this;
        };

        return newBallot;

    })(BallotItem);

    module.exports = {
        Option: Option,
        Question: Question,
        Candidate: Candidate,
        List: List,
        Options: Options,
        ListsAndCandidates: ListsAndCandidates,
        Ballot: Ballot
    };

}).call(this);
