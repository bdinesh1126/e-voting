/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

/**
 * Wraps the worker calls and responses with a promise.
 */

class WorkerWrapper {
  worker;

  constructor() {
    // Create the worker with its fixed bundle name (see Voter-Portal build configuration)
    this.worker = new Worker("./crypto.ov-worker.js");
    this.worker.onmessage = handleResponseFromWorker;
  }

  /**
   * Provides a fresh new instance of WorkerWrapper.
   * @returns {WorkerWrapper}
   */
  static getInstance() {
    if (!WorkerWrapper.instance) {
      WorkerWrapper.instance = new WorkerWrapper();
    }
    return WorkerWrapper.instance;
  }

  invoke(operation) {
    return sendRequestToWorker(this.worker, {
      operation: operation,
      args: [].slice.call(arguments, 1)
    });
  }
}

const callbacks = {
  resolves: {},
  rejects: {}
};

/**
 * @typedef {object} WorkerRequestPayload.
 * @property {string} operation, the operation to invoke.
 * @property {Array} args, arguments for the invoked operation.
 */

/**
 * Sends a request to the worker api.
 * @param {Worker} worker, the worker object who expect request.
 * @param {WorkerRequestPayload} payload, the payload to send.
 * @returns {Promise<unknown>|void}
 */
function sendRequestToWorker(worker, payload) {
  const { operation } = payload;
  return new Promise(function(resolve, reject) {
    // Save promise callbacks for worker operation response.
    callbacks.resolves[operation] = resolve;
    callbacks.rejects[operation] = reject;
    // Send request to the worker.
    worker.postMessage(payload);
  });
}

/**
 * @typedef {object} WorkerResponsePayload.
 * @property {string} operation, the responding worker operation.
 * @property {unknown} [result], the operation result.
 * @property {unknown} [error], the operation error.
 */

/**
 * @typedef {object} MessageEvent.
 * @property {WorkerResponsePayload} data, the response of the worker.
 */

/**
 * Handles a worker response.
 * @param {MessageEvent} workerMessage, the message received from the worker.
 * @returns {Promise<unknown>|unknown}
 */
function handleResponseFromWorker(workerMessage) {
  const { operation, result, error } = workerMessage.data;
  if (result) {
    const resolve = callbacks.resolves[operation];
    if (resolve) {
      resolve(result);
    } else {
      throw new Error(`Unhandled worker response [operation: ${operation}]`);
    }
  } else {
    const reject = callbacks.rejects[operation];
    if (reject && error) {
      reject(error);
    } else {
      throw new Error(`Unhandled worker error [operation: ${operation}]`);
    }
  }

  // Delete callbacks
  delete callbacks.resolves[operation];
  delete callbacks.rejects[operation];
}

module.exports.WorkerWrapper = WorkerWrapper;
