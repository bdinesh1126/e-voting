/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

const { WorkerWrapper } = require("./worker-wrapper");

/**
 * Provides an api to the Voter-Portal to communicate with the Voting-Server for all the voting phases described in the protocol.
 */
window.OvApi = function() {

  const workerWrapper = WorkerWrapper.getInstance();
  return {
    /**
     * Authenticates the voter to the Voting-Server.
     *
     * @param {string} startVotingKey, the start voting key of a voter.
     * @param {string} extendedAuthenticationFactor, the extended authentication factor, such as a year of birth OR date of birth.
     * @param {string} electionEventId, the related election event identifier.
     * @param {string} lang, the supported language code.
     * @returns {Promise<AuthenticateVoterResponse>} the authenticateVoter response.
     */
    authenticateVoter: function(startVotingKey, extendedAuthenticationFactor, electionEventId, lang) {
      return workerWrapper.invoke("authenticateVoter", startVotingKey, extendedAuthenticationFactor, electionEventId, lang);
    },

    /**
     * Sends the vote to the Voting-Server.
     *
     * @param {string[]} selectedVotingOptions, the voter selected voting options.
     * @param {string[]} voterWriteIns, the voter write ins.
     * @returns {Promise<SendVoteResponse>} the sendVote response.
     */
    sendVote: function(selectedVotingOptions, voterWriteIns) {
      return workerWrapper.invoke("sendVote", selectedVotingOptions, voterWriteIns);
    },

    /**
     * Confirms the vote to the Voting-Server.
     *
     * @param {string} ballotCastingKey The 'ballot casting key'.
     * @returns {Promise<ConfirmVoteResponse>} The confirmVote response.
     */
    confirmVote: function(ballotCastingKey) {
      return workerWrapper.invoke("confirmVote", ballotCastingKey);
    },

    /**
     * Selects the i18n texts associated to the locale on the supplied ballot.
     * @param {Ballot} ballot, the ballot object
     * @param {string} lang, the supported language code.
     * @returns {Promise<Ballot>} the translated ballot.
     */
    translateBallot: function(ballot, lang) {
      return workerWrapper.invoke("translateBallot", ballot, lang);
    }
  };
};