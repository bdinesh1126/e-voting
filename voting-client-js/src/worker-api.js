/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

const { sendVotePhase } = require('./voting-phase/send-vote/send-vote');
const { confirmVotePhase } = require('./voting-phase/confirm-vote/confirm-vote');
const { authenticateVoterPhase } = require('./voting-phase/authenticate-voter/authenticate-voter');
const { translateBallot } = require('./model/ballot-parser');

/**
 * Provides an api for the worker calls.
 */

const workerApi = {
  authenticateVoter: function(startVotingKey, extendedAuthenticationFactor, electionEventId, lang) {
    return authenticateVoterPhase(startVotingKey, extendedAuthenticationFactor, electionEventId, lang);
  },
  sendVote: function(selectedVotingOptions, voterWriteIns) {
    return sendVotePhase(selectedVotingOptions, voterWriteIns);
  },
  confirmVote: function(ballotCastingKey) {
    return confirmVotePhase(ballotCastingKey);
  },
  translateBallot: function(ballot, lang) {
    return translateBallot(ballot, lang);
  }
};

/**
 * @typedef {object} WorkerRequestPayload.
 * @property {string} operation, the operation to invoke.
 * @property {Array} args, arguments for the invoked operation.
 */

/**
 * @typedef {object} MessageEvent.
 * @property {WorkerRequestPayload} data, the request from the api.
 */

/**
 * Handles a request from the api.
 * @param {MessageEvent} workerMessage, the message received from the worker.
 * @returns {Promise<unknown>} operation response.
 */
self.onmessage = function handleRequestFromApi(workerMessage) {
  const { operation, args } = workerMessage.data;
  let response;
  try {
    response = workerApi[operation].apply(workerApi, Array.isArray(args) ? args : [args]);
  } catch (error) {
    throw new Error(`Error calling the Worker API. [operation: "${operation}, error:${error}]`);
  }

  // Handling only the result and error (no progress or pending).
  response.then(
    (promiseResult) => {
      self.postMessage({
        operation: operation,
        result: promiseResult
      });
    },
    (promiseError) => {
      self.postMessage({
        operation: operation,
        error: promiseError.errorResponse
      });
    }
  );
};
