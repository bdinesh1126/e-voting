/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

/* jshint maxlen: 6666 */

const primitivesParamsParser = require("../../../src/services/primitives-params-service")
const createConfirmMessageAlgorithm = require("../../../src/voting-phase/confirm-vote/create-confirm-message-algorithm");
const authResponse = require('../../tools/data/authResponse.json');
const verificationCardSecretKeyBytes = require("../../tools/data/verificationCardSecretKeyBytes.json");
const {byteArrayToInteger} = require("crypto-primitives-ts/lib/cjs/conversions");
const {ZqElement} = require("crypto-primitives-ts/lib/cjs/math/zq_element");
const {ZqGroup} = require("crypto-primitives-ts/lib/cjs/math/zq_group");
const {ImmutableUint8Array} = require("crypto-primitives-ts/lib/cjs/immutable_uint8Array");

describe('Create confirm message algorithm', function () {

	const primitivesParams = primitivesParamsParser.parsePrimitivesParams(authResponse);
	const k_id = byteArrayToInteger(ImmutableUint8Array.from(verificationCardSecretKeyBytes));
	const verificationCardSecretKey = ZqElement.create(k_id, ZqGroup.sameOrderAs(primitivesParams.encryptionGroup));

	it('should generate a confirmation key', function () {

		const confirmationKey = createConfirmMessageAlgorithm.createConfirmMessage(
			{
				encryptionGroup: primitivesParams.encryptionGroup,
				electionEventId: authResponse.authenticationToken.voterAuthenticationData.electionEventId,
				characterLengthOfTheBallotCastingKey: 9
			},
			'712586677',
			verificationCardSecretKey
		);

		expect('250126448112984756661011464708680137061057744877981402968346446675902445949016496580744187263129724956633065518017926590353010278547138828749768655127688870708390770151777003087906'+
		'7597857139130108075585186959846579716555069344963325586527083956476515984502600199960620804678008277911289401018271680550434917475318379893004173227985704198626343625541363973121455292523801'+
		'6593049314471286361497113113635624157034119880747307901690929602930686480840210725691238424719483584355435650944358086107544126716712706429048898791500241571457310870378907346605549300414144'+
		'1730179857655508186208839929478640894063881865066795145442922558636263897722779136231142813850431992683181759090409117517127158389574783048389482310035571394226452719587093898629710155394799'+
		'1830091235224633530509811692186455795855158163768840124598004810718659397223751679229516530105955786999859316068046221159268487268282108389679386895538668752257458279631014888')
			.toBe(confirmationKey.value.toString());
	});

	describe('should fail with', function () {

		const bck_length_error = "The ballot casting key length must be 9";
		const bck_numeric_error = "The ballot casting key must be a numeric value";

		const parameters = [
			{
				description: "a shorter ballot casting key",
				bck: "12345678",
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup,
					electionEventId: authResponse.authenticationToken.voterAuthenticationData.electionEventId,
					characterLengthOfTheBallotCastingKey: 9
				},
				error: new Error(bck_length_error)
			},
			{
				description: "a longer ballot casting key",
				bck: "1234567890",
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup,
					electionEventId: authResponse.authenticationToken.voterAuthenticationData.electionEventId,
					characterLengthOfTheBallotCastingKey: 9
				},
				error: new Error(bck_length_error)
			},
			{
				description: "a zero ballot casting key",
				bck: "000000000",
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup,
					electionEventId: authResponse.authenticationToken.voterAuthenticationData.electionEventId,
					characterLengthOfTheBallotCastingKey: 9
				},
				error: new Error("The ballot casting key must contain one non-zero element")
			},
			{
				description: "a non-numeric ballot casting key",
				bck: "1A3456789",
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup,
					electionEventId: authResponse.authenticationToken.voterAuthenticationData.electionEventId,
					characterLengthOfTheBallotCastingKey: 9
				},
				error: new Error(bck_numeric_error)
			},
			{
				description: "a space starting ballot casting key",
				bck: " 23456789",
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup,
					electionEventId: authResponse.authenticationToken.voterAuthenticationData.electionEventId,
					characterLengthOfTheBallotCastingKey: 9
				},
				error: new Error(bck_numeric_error)
			},
			{
				description: "a space ending ballot casting key",
				bck: "12345678 ",
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup,
					electionEventId: authResponse.authenticationToken.voterAuthenticationData.electionEventId,
					characterLengthOfTheBallotCastingKey: 9
				},
				error: new Error(bck_numeric_error)
			},
			{
				description: "a null ballot casting key",
				bck: null,
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup,
					electionEventId: authResponse.authenticationToken.voterAuthenticationData.electionEventId,
					characterLengthOfTheBallotCastingKey: 9
				},
				error: new Error()
			},
			{
				description: "a null verification card secret key",
				bck: '123456789',
				vcsk: null,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup,
					electionEventId: authResponse.authenticationToken.voterAuthenticationData.electionEventId,
					characterLengthOfTheBallotCastingKey: 9
				},
				error: new Error()
			},
			{
				description: "a null encryption params",
				bck: 123456789,
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: null,
					electionEventId: authResponse.authenticationToken.voterAuthenticationData.electionEventId,
					characterLengthOfTheBallotCastingKey: 9
				},
				error: new Error()
			}
		];

		parameters.forEach((parameter) => {
			it(parameter.description, function () {
				expect(function () {
					createConfirmMessageAlgorithm.createConfirmMessage(
						parameter.context,
						parameter.bck,
						parameter.vcsk
					)
				}).toThrow(parameter.error);
			});
		});
	});

});


