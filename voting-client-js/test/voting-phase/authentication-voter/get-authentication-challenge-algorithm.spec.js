/*
 * (c) Copyright 2023 Swiss Post Ltd
 */

const getAuthenticationChallengeAlgorithm = require("../../../src/voting-phase/authenticate-voter/get-authentication-challenge-algorithm");
const {NullPointerError} = require("crypto-primitives-ts/lib/cjs/error/null_pointer_error");
const {IllegalArgumentError} = require("crypto-primitives-ts/lib/cjs/error/illegal_argument_error");
const {FailedValidationError} = require("../../../src/services/failed_validation_error");

describe('Get authentication challenge', function () {

	const electionEventId = '34caee78ed3d4cf981ca06b659f558eb';
	const authenticationStep = 'authenticateVoter'
	const startVotingKey = '4d65ej2adb4ia6ghhzb52kg6';
	const extendedAuthenticationFactor = '01061944';
	const credentialId = '9660D63A4AB22ECCEF143D213BAF3EF2';

	describe('should return expected credential id when given valid input', function () {
		it('long extended authentication factor', async function () {
			getAuthenticationChallengeAlgorithm.getAuthenticationChallenge(
				{
					electionEventId: electionEventId,
					authenticationStep: authenticationStep
				},
				startVotingKey,
				extendedAuthenticationFactor
			).then(authenticationChallenge => expect(credentialId).toBe(authenticationChallenge.derivedVoterIdentifier)
			);
		}, 30000);

		it('short extended authentication factor', async function () {
			getAuthenticationChallengeAlgorithm.getAuthenticationChallenge(
				{
					electionEventId: electionEventId,
					authenticationStep: authenticationStep
				},
				startVotingKey,
				extendedAuthenticationFactor.substring(4)
			).then(authenticationChallenge => expect(credentialId).toBe(authenticationChallenge.derivedVoterIdentifier)
			);
		}, 30000);
	});

	describe('should throw an Error when given null arguments', function () {
		it('election event id', async function () {
			await expectAsync(getAuthenticationChallengeAlgorithm.getAuthenticationChallenge(
				{
					electionEventId: null,
					authenticationStep: authenticationStep
				},
				startVotingKey,
				extendedAuthenticationFactor)).toBeRejectedWith(new NullPointerError())
		}, 30000);

		it('authentication step', async function () {
			await expectAsync(getAuthenticationChallengeAlgorithm.getAuthenticationChallenge(
				{
					electionEventId: electionEventId,
					authenticationStep: null
				},
				startVotingKey,
				extendedAuthenticationFactor)).toBeRejectedWith(new NullPointerError())
		}, 30000);

		it('start voting key', async function () {
			await expectAsync(getAuthenticationChallengeAlgorithm.getAuthenticationChallenge(
				{
					electionEventId: electionEventId,
					authenticationStep: authenticationStep
				},
				null,
				extendedAuthenticationFactor)).toBeRejectedWith(new NullPointerError())
		}, 30000);

		it('extended authentication factor', async function () {
			await expectAsync(getAuthenticationChallengeAlgorithm.getAuthenticationChallenge(
				{
					electionEventId: electionEventId,
					authenticationStep: authenticationStep
				},
				startVotingKey,
				null)).toBeRejectedWith(new NullPointerError())
		}, 30000);
	});

	it('should throw an Error when given invalid authentication step ', async function () {
		await expectAsync(getAuthenticationChallengeAlgorithm.getAuthenticationChallenge(
			{
				electionEventId: electionEventId,
				authenticationStep: 'invalidAuthStep'
			},
			startVotingKey,
			extendedAuthenticationFactor
		)).toBeRejectedWith(new IllegalArgumentError("The authentication step must be one of the valid values."))
	}, 30000);

	it('should throw an Error when given non UUID election event id', async function () {
		const nonUuidElectionEventId = '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!';
		const expectedErrorMessage = `The given string does not comply with the required format. [string: ${nonUuidElectionEventId}, format: ^[a-fA-F0-9]{32}$].`;

		await expectAsync(getAuthenticationChallengeAlgorithm.getAuthenticationChallenge(
			{
				electionEventId: nonUuidElectionEventId,
				authenticationStep: authenticationStep
			},
			startVotingKey,
			extendedAuthenticationFactor)).toBeRejectedWith(new FailedValidationError(expectedErrorMessage))
	}, 30000);

	it('should throw an Error when given non valid start voting key', async function () {
		const nonValidStartVotingKey = '111111111111111111111111';
		const expectedErrorMessage = `The given string does not comply with the required format. [string: ${nonValidStartVotingKey}, format: ^[a-km-np-z2-9]{24}$].`;

		await expectAsync(getAuthenticationChallengeAlgorithm.getAuthenticationChallenge(
			{
				electionEventId: electionEventId,
				authenticationStep: authenticationStep
			},
			nonValidStartVotingKey,
			extendedAuthenticationFactor)).toBeRejectedWith(new FailedValidationError(expectedErrorMessage))
	}, 30000);

	describe('should throw an Error when given non compliant extended authentication factor', function () {
		it('non digit', async function () {
			const nonCompliantExtendedAuthenticationFactor = 'a106';
			const expectedErrorMessage = 'The given string does not comply with the required format. [string: a106, format: ^(\\d{4})(\\d{4})?$].';

			await expectAsync(getAuthenticationChallengeAlgorithm.getAuthenticationChallenge(
				{
					electionEventId: electionEventId,
					authenticationStep: authenticationStep
				},
				startVotingKey,
				nonCompliantExtendedAuthenticationFactor)).toBeRejectedWith(new FailedValidationError(expectedErrorMessage))
		}, 30000);

		it('incorrect size', async function () {
			const nonCompliantExtendedAuthenticationFactor = '010644';
			const expectedErrorMessage = 'The given string does not comply with the required format. [string: 010644, format: ^(\\d{4})(\\d{4})?$].';

			await expectAsync(getAuthenticationChallengeAlgorithm.getAuthenticationChallenge(
				{
					electionEventId: electionEventId,
					authenticationStep: authenticationStep
				},
				startVotingKey,
				nonCompliantExtendedAuthenticationFactor)).toBeRejectedWith(new FailedValidationError(expectedErrorMessage))
		}, 30000);
	});
})
