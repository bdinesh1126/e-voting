/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

/* jshint maxlen: 6666 */

const {ImmutableBigInteger} = require("crypto-primitives-ts/lib/cjs/immutable_big_integer");
const realValuesJson = require('../tools/data/write-in-to-quadratic-residue.json');
const {GqGroup} = require("crypto-primitives-ts/lib/cjs/math/gq_group");
const {GqElement} = require("crypto-primitives-ts/lib/cjs/math/gq_element");
const {IllegalArgumentError} = require("crypto-primitives-ts/lib/cjs/error/illegal_argument_error");
const writeInToQuadraticResidueAlgorithm = require("../../src/write-ins/write-in-to-quadratic-residue-algorithm");

describe('WriteIn to quadratic residue algorithm', () => {

  const args = [];
  const parametersList = JSON.parse(JSON.stringify(realValuesJson));
  parametersList.forEach(testParameters => {

    // Context.
    const p = readValue(testParameters.context.p);
    const q = readValue(testParameters.context.q);
    const g = readValue(testParameters.context.g);

    const gqGroup = new GqGroup(p, q, g);

    // Parse input parameter.
    const s = testParameters.input.s;

    // Parse output parameters.
    let output, error;
    if (testParameters.output.output) {
      output = GqElement.fromValue(readValue(testParameters.output.output), gqGroup);
    } else {
      error = testParameters.output.error;
    }

    args.push({
      encryptionGroup: gqGroup,
      s: s,
      output: output,
      error: error,
      description: testParameters.description
    });
  });

  args.forEach((arg) => {
    it(arg.description, () => {
      if (arg.output) {
        const actual = writeInToQuadraticResidueAlgorithm.writeInToQuadraticResidue({encryptionGroup: arg.encryptionGroup}, arg.s);
        expect(arg.output.equals(actual)).toBe(true);
      } else {
        expect(() => writeInToQuadraticResidueAlgorithm.writeInToQuadraticResidue({encryptionGroup: arg.encryptionGroup}, arg.s))
          .toThrow(new IllegalArgumentError(arg.error));
      }
    });
  });
});

function readValue(value) {
  return ImmutableBigInteger.fromString(value.substring(2), 16);
}

