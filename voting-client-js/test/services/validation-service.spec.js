/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */

/* jshint maxlen: 6666 */

const authResponse = require("../tools/data/authResponse.json");
const {
  validateUUID,
  validateSVK,
  validateBase64String,
  validateActualVotingOptions
} = require("../../src/services/validation-service");

describe('Validations methods', function () {

  it('validateUUID should validate and return a UUID', async function () {
    expect(() => validateUUID(authResponse.authenticationToken.voterAuthenticationData.electionEventId)).not.toThrow();
  });

  it('validateSVK should validate and return SVK alphabet string', async function () {
    expect(() => validateSVK('jwwjecjkixjk7r6mdfxcim7x')).not.toThrow();
  });

  it('validateBase64String should validate and return a base64 string', async function () {
    expect(() => validateBase64String(authResponse.verificationCard.verificationCardKeystore)).not.toThrow();
  });


  it('should fail with invalid XML xs:token actual voting option', function () {
    const invalidXMLTokens = ['aposidvnbq13458zœ¶@¼←“þ“ ¢@]œ“→”@µ€ĸ@{þ', ' spacestart', 'space between', 'spaceend ', 'pk23]', 'asdacà!32', ' a p2o3m', '<xyz>'];


    invalidXMLTokens.forEach(invalidXMLToken => {
      expect(() => validateActualVotingOptions([invalidXMLToken]))
        .toThrow(new Error("The actual voting options must match a valid xml xs:token."));
    });
  });

  it('should accept actual voting option when valid XML xs:token', function () {
    const validXMLTokens = ['123-456', 'abc123', '789xyz', 'xyz', 'a_token', 'another-token', 'another|token'];

    validXMLTokens.forEach(validXMLToken => {
      expect(() => validateActualVotingOptions([validXMLToken]))
        .not.toThrow();
    });
  });

});


