/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
const path = require('path');

module.exports = {
  mode: 'production',
  entry: {
    api: './src/api.js',
    worker: './src/worker-api.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'ov-[name].js',
  },
  resolve: {
    fallback: {crypto: false},
  },
  performance: {
    maxAssetSize: 500000,
    maxEntrypointSize: 500000,
  },
  optimization: {
    minimize: false,
    usedExports: true,
  }
};