/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.controlcomponent.voting.SetupComponentPublicKeysEntity;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface SetupComponentPublicKeysRepository extends CrudRepository<SetupComponentPublicKeysEntity, Long> {

	@Query("select e from SetupComponentPublicKeysEntity e where e.electionEventEntity.electionEventId = ?1")
	Optional<SetupComponentPublicKeysEntity> findByElectionEventId(final String electionEventId);

}
