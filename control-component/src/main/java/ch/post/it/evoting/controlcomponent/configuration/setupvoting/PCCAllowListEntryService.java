/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.controlcomponent.PCCAllowListEntryEntity;
import ch.post.it.evoting.controlcomponent.PCCAllowListEntryRepository;

@Service
public class PCCAllowListEntryService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PCCAllowListEntryService.class);

	private final PCCAllowListEntryRepository pccAllowListEntryRepository;
	private final int batchSize;

	@PersistenceContext
	private EntityManager entityManager;

	public PCCAllowListEntryService(
			final PCCAllowListEntryRepository pccAllowListEntryRepository,
			@Value("${spring.jpa.properties.hibernate.jdbc.batch_size}")
			final int batchSize) {
		this.pccAllowListEntryRepository = pccAllowListEntryRepository;
		this.batchSize = batchSize;
	}

	@Transactional
	public void saveAll(final List<PCCAllowListEntryEntity> pccAllowListEntryEntities) {
		checkNotNull(pccAllowListEntryEntities);
		checkArgument(!pccAllowListEntryEntities.isEmpty());

		final Iterator<PCCAllowListEntryEntity> iterator = pccAllowListEntryEntities.iterator();

		// Save the list of cast return codes in batches
		List<PCCAllowListEntryEntity> entities = new ArrayList<>(batchSize);
		while (iterator.hasNext()) {
			final PCCAllowListEntryEntity partialChoiceReturnCode = iterator.next();
			entities.add(partialChoiceReturnCode);
			if (entities.size() == batchSize || !iterator.hasNext()) {
				pccAllowListEntryRepository.saveAll(entities);
				entityManager.flush();
				entityManager.clear();
				entities = new ArrayList<>(batchSize);
			}
		}

		final int chunkId = pccAllowListEntryEntities.get(0).getChunkId();
		LOGGER.debug("Saved pcc allow list chunk entity. [chunkId: {}]", chunkId);
	}

	/**
	 * Gets the pcc allow lists for the given verification card set id.
	 *
	 * @param verificationCardSetId the verification card set id. Must be a valid UUID.
	 * @return the pcc allow lists.
	 */
	@Transactional
	public List<PCCAllowListEntryEntity> getPCCAllowList(final String verificationCardSetId) {
		validateUUID(verificationCardSetId);

		final List<PCCAllowListEntryEntity> pccAllowList = pccAllowListEntryRepository.findAllByVerificationCardSetId(verificationCardSetId);
		checkState(!pccAllowList.isEmpty(), "No pcc allow list found. [verificationCardSetId: %s]", verificationCardSetId);
		LOGGER.debug("Loaded pcc allow list. [verificationCardSetId: {}, size: {}]", verificationCardSetId, pccAllowList.size());

		return pccAllowList;
	}

	@Transactional
	public boolean exists(final String verificationCardSetId, final String longVoteCastReturnCode) {
		return pccAllowListEntryRepository.existsByPartialChoiceReturnCode(verificationCardSetId, longVoteCastReturnCode);
	}
}
