/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.tally.mixonline;

import static ch.post.it.evoting.controlcomponent.ControlComponentsApplicationBootstrap.RABBITMQ_EXCHANGE;
import static ch.post.it.evoting.controlcomponent.tally.mixonline.MixDecryptService.MixDecryptServiceOutput;
import static ch.post.it.evoting.domain.SharedQueue.MIX_DEC_ONLINE_REQUEST_PATTERN;
import static ch.post.it.evoting.domain.SharedQueue.MIX_DEC_ONLINE_RESPONSE_PATTERN;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.SignatureException;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.retry.RetryContext;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.retry.support.RetrySynchronizationManager;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.controlcomponent.ExactlyOnceCommand;
import ch.post.it.evoting.controlcomponent.ExactlyOnceCommandExecutor;
import ch.post.it.evoting.controlcomponent.Messages;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.signature.SignedPayload;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.tally.MixDecryptOnlineRequestPayload;
import ch.post.it.evoting.domain.tally.MixDecryptOnlineResponsePayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.tally.ControlComponentBallotBoxPayload;

@Service
class MixDecryptProcessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(MixDecryptProcessor.class);

	private final ObjectMapper objectMapper;
	private final RabbitTemplate rabbitTemplate;
	private final MixDecryptService mixDecryptService;
	private final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor;
	private final SignatureKeystore<Alias> signatureKeystoreService;

	private String responseQueue;

	@Value("${nodeID}")
	private int nodeId;

	MixDecryptProcessor(
			final ObjectMapper objectMapper,
			final RabbitTemplate rabbitTemplate,
			final MixDecryptService mixDecryptService,
			final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor,
			final SignatureKeystore<Alias> signatureKeystoreService) {
		this.objectMapper = objectMapper;
		this.rabbitTemplate = rabbitTemplate;
		this.mixDecryptService = mixDecryptService;
		this.exactlyOnceCommandExecutor = exactlyOnceCommandExecutor;
		this.signatureKeystoreService = signatureKeystoreService;
	}

	@PostConstruct
	public void initQueue() {
		responseQueue = String.format("%s%s", MIX_DEC_ONLINE_RESPONSE_PATTERN, nodeId);
	}

	@Retryable(backoff = @Backoff(delay = 30000))
	@RabbitListener(queues = MIX_DEC_ONLINE_REQUEST_PATTERN + "${nodeID}", autoStartup = "false", concurrency = "1")
	public void onMessage(final Message message) throws IOException {
		final String correlationId = message.getMessageProperties().getCorrelationId();
		checkNotNull(correlationId);

		final byte[] messageBytes = message.getBody();

		final MixDecryptOnlineRequestPayload mixDecryptOnlineRequestPayload = objectMapper.readValue(messageBytes,
				MixDecryptOnlineRequestPayload.class);

		checkArgument(mixDecryptOnlineRequestPayload.nodeId() == nodeId,
				"The mix decrypt online request payload is not addressed to the current control component. [nodeId: %s, payloadNodeId: %s]", nodeId,
				mixDecryptOnlineRequestPayload.nodeId());

		mixDecryptOnlineRequestPayload.controlComponentShufflePayloads().forEach(this::verifySignature);

		final String electionEventId = mixDecryptOnlineRequestPayload.electionEventId();
		final String ballotBoxId = mixDecryptOnlineRequestPayload.ballotBoxId();

		final String contextId = String.join("-", electionEventId, ballotBoxId);
		LOGGER.info("Received mix decrypt request. [contextId: {}, correlationId: {}]", contextId, correlationId);

		final RetryContext retryContext = RetrySynchronizationManager.getContext();
		final int retryNumber = retryContext.getRetryCount();
		if (retryNumber != 0) {
			LOGGER.warn("An exception previously occurred during mixing. Retrying (max 3 times). [retryNumber: {}]", retryNumber);
		}

		final ExactlyOnceCommand exactlyOnceTask = new ExactlyOnceCommand.Builder()
				.setCorrelationId(correlationId)
				.setContextId(contextId)
				.setContext(Context.MIXING_TALLY_MIX_DEC_ONLINE.toString())
				.setTask(() -> generateMixDecryptOnlineResponsePayload(mixDecryptOnlineRequestPayload))
				.setRequestContent(messageBytes)
				.build();
		final byte[] payloadBytes = exactlyOnceCommandExecutor.process(exactlyOnceTask);
		LOGGER.debug("Mixing task successfully processed. [contextId: {}]", contextId);

		final Message responseMessage = Messages.createMessage(correlationId, payloadBytes);

		rabbitTemplate.send(RABBITMQ_EXCHANGE, responseQueue, responseMessage);
		LOGGER.info("Mix decrypt response sent. [contextId: {}]", contextId);
	}

	private byte[] generateMixDecryptOnlineResponsePayload(final MixDecryptOnlineRequestPayload mixDecryptOnlineRequestPayload) {
		final String electionEventId = mixDecryptOnlineRequestPayload.electionEventId();
		final String ballotBoxId = mixDecryptOnlineRequestPayload.ballotBoxId();
		final String contextId = String.join("-", Arrays.asList(electionEventId, ballotBoxId));
		final List<ControlComponentShufflePayload> controlComponentShufflePayloads = mixDecryptOnlineRequestPayload.controlComponentShufflePayloads();

		final MixDecryptServiceOutput mixDecryptServiceOutput = mixDecryptService.performMixDecrypt(electionEventId, ballotBoxId,
				controlComponentShufflePayloads);

		final ControlComponentBallotBoxPayload controlComponentBallotBoxPayload = mixDecryptServiceOutput.controlComponentBallotBoxPayload();
		final ControlComponentShufflePayload shufflePayload = mixDecryptServiceOutput.controlComponentShufflePayload();

		controlComponentBallotBoxPayload.setSignature(getPayloadSignature(controlComponentBallotBoxPayload,
				ChannelSecurityContextData.controlComponentBallotBox(nodeId, electionEventId, ballotBoxId)));

		shufflePayload.setSignature(
				getPayloadSignature(shufflePayload,
						ChannelSecurityContextData.controlComponentShuffle(nodeId, electionEventId, ballotBoxId)));

		final MixDecryptOnlineResponsePayload mixDecryptOnlineResponsePayload = new MixDecryptOnlineResponsePayload(controlComponentBallotBoxPayload,
				shufflePayload);

		try {
			return objectMapper.writeValueAsBytes(mixDecryptOnlineResponsePayload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(String.format("Could not to serialize mix decrypt online response payload. [contextId: %s]", contextId),
					e);
		}
	}

	private CryptoPrimitivesSignature getPayloadSignature(final SignedPayload payload, final Hashable additionalContextData) {

		try {
			final byte[] signature = signatureKeystoreService.generateSignature(payload, additionalContextData);

			return new CryptoPrimitivesSignature(signature);

		} catch (final SignatureException se) {
			throw new IllegalStateException(
					String.format("Failed to generate payload signature [%s, %s]", payload.getClass().getSimpleName(), additionalContextData), se);
		}
	}

	private void verifySignature(final ControlComponentShufflePayload payload) {
		final int payloadNodeId = payload.getNodeId();
		final String electionEventId = payload.getElectionEventId();
		final String ballotBoxId = payload.getBallotBoxId();
		final String contextMessage = String.format("[nodeId: %s, electionEventId: %s, ballotBoxId: %s]", payloadNodeId, electionEventId,
				ballotBoxId);

		final CryptoPrimitivesSignature signature = payload.getSignature();

		checkState(signature != null, "The signature of the control component shuffle payload is null. %s", contextMessage);

		final Hashable additionalContextData = ChannelSecurityContextData.controlComponentShuffle(payloadNodeId, electionEventId, ballotBoxId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.getControlComponentByNodeId(payloadNodeId), payload,
					additionalContextData, signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not verify the signature of %s. %s", payload.getClass().getSimpleName(), contextMessage));
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(ControlComponentShufflePayload.class, contextMessage);
		}
	}
}
