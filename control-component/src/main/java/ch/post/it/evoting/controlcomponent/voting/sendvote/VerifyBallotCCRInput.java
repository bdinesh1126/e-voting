/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_WRITE_IN_OPTIONS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.cryptoprimitives.utils.Validations.allEqual;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.PlaintextEqualityProof;

/**
 * Regroups the input values needed by the VerifyBallotCCR<sub>j</sub> algorithm.
 *
 * <ul>
 * <li>vc<sub>id</sub>, the verification card id. Not null and a valid UUID.</li>
 * <li>E1, the encrypted vote. Not null.</li>
 * <li>E1_tilde, the exponentiated encrypted vote. Not null.</li>
 * <li>E2, the encrypted partial Choice Return Codes. Not null.</li>
 * <li>K<sub>id</sub>, the verification card public key. Not null.</li>
 * <li>EL<sub>pk</sub>, the election public key. Not null.</li>
 * <li>pk<sub>CCR</sub>, the Choice Return Codes encryption public key. Not null.</li>
 * <li>pi<sub>Exp</sub>, the exponentiation proof. Not null.</li>
 * <li>pi<sub>EqEnc</sub>, the plaintext equality proof. Not null.</li>
 * </ul>
 */
public class VerifyBallotCCRInput {

	private final String verificationCardId;
	private final ElGamalMultiRecipientCiphertext encryptedVote;
	private final ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote;
	private final ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes;
	private final GqElement verificationCardPublicKey;
	private final ElGamalMultiRecipientPublicKey electionPublicKey;
	private final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey;
	private final ExponentiationProof exponentiationProof;
	private final PlaintextEqualityProof plaintextEqualityProof;

	private VerifyBallotCCRInput(final String verificationCardId,
			final ElGamalMultiRecipientCiphertext encryptedVote,
			final ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote,
			final ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes,
			final GqElement verificationCardPublicKey,
			final ElGamalMultiRecipientPublicKey electionPublicKey,
			final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey,
			final ExponentiationProof exponentiationProof,
			final PlaintextEqualityProof plaintextEqualityProof) {
		this.verificationCardId = verificationCardId;
		this.encryptedVote = encryptedVote;
		this.exponentiatedEncryptedVote = exponentiatedEncryptedVote;
		this.encryptedPartialChoiceReturnCodes = encryptedPartialChoiceReturnCodes;
		this.verificationCardPublicKey = verificationCardPublicKey;
		this.electionPublicKey = electionPublicKey;
		this.choiceReturnCodesEncryptionPublicKey = choiceReturnCodesEncryptionPublicKey;
		this.exponentiationProof = exponentiationProof;
		this.plaintextEqualityProof = plaintextEqualityProof;
	}

	String getVerificationCardId() {
		return verificationCardId;
	}

	ElGamalMultiRecipientCiphertext getEncryptedVote() {
		return encryptedVote;
	}

	ElGamalMultiRecipientCiphertext getExponentiatedEncryptedVote() {
		return exponentiatedEncryptedVote;
	}

	ElGamalMultiRecipientCiphertext getEncryptedPartialChoiceReturnCodes() {
		return encryptedPartialChoiceReturnCodes;
	}

	GqElement getVerificationCardPublicKey() {
		return verificationCardPublicKey;
	}

	ElGamalMultiRecipientPublicKey getElectionPublicKey() {
		return electionPublicKey;
	}

	ElGamalMultiRecipientPublicKey getChoiceReturnCodesEncryptionPublicKey() {
		return choiceReturnCodesEncryptionPublicKey;
	}

	ExponentiationProof getExponentiationProof() {
		return exponentiationProof;
	}

	PlaintextEqualityProof getPlaintextEqualityProof() {
		return plaintextEqualityProof;
	}

	/**
	 * Builder performing input validations and cross-validations before constructing a {@link VerifyBallotCCRInput}.
	 */
	public static class Builder {

		private String verificationCardId;
		private ElGamalMultiRecipientCiphertext encryptedVote;
		private ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote;
		private ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes;
		private GqElement verificationCardPublicKey;
		private ElGamalMultiRecipientPublicKey electionPublicKey;
		private ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey;
		private ExponentiationProof exponentiationProof;
		private PlaintextEqualityProof plaintextEqualityProof;

		public Builder setVerificationCardId(final String verificationCardId) {
			this.verificationCardId = verificationCardId;
			return this;
		}

		public Builder setEncryptedVote(final ElGamalMultiRecipientCiphertext encryptedVote) {
			this.encryptedVote = encryptedVote;
			return this;
		}

		public Builder setExponentiatedEncryptedVote(final ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote) {
			this.exponentiatedEncryptedVote = exponentiatedEncryptedVote;
			return this;
		}

		public Builder setEncryptedPartialChoiceReturnCodes(
				final ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes) {
			this.encryptedPartialChoiceReturnCodes = encryptedPartialChoiceReturnCodes;
			return this;
		}

		public Builder setVerificationCardPublicKey(final GqElement verificationCardPublicKey) {
			this.verificationCardPublicKey = verificationCardPublicKey;
			return this;
		}

		public Builder setElectionPublicKey(final ElGamalMultiRecipientPublicKey electionPublicKey) {
			this.electionPublicKey = electionPublicKey;
			return this;
		}

		public Builder setChoiceReturnCodesEncryptionPublicKey(
				final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey) {
			this.choiceReturnCodesEncryptionPublicKey = choiceReturnCodesEncryptionPublicKey;
			return this;
		}

		public Builder setExponentiationProof(final ExponentiationProof exponentiationProof) {
			this.exponentiationProof = exponentiationProof;
			return this;
		}

		public Builder setPlaintextEqualityProof(final PlaintextEqualityProof plaintextEqualityProof) {
			this.plaintextEqualityProof = plaintextEqualityProof;
			return this;
		}

		/**
		 * Creates the VerifyBallotCCRInput. All fields must have been set and be non-null.
		 *
		 * @return a new VerifyBallotCCRInput.
		 * @throws NullPointerException      if any of the fields is null.
		 * @throws FailedValidationException if the verification card id is not a valid UUID.
		 * @throws IllegalArgumentException  if
		 *                                   <ul>
		 *                                       <li>The encrypted vote and the exponentiated encrypted vote do not have the same size.</li>
		 *                                       <li>The choice return codes encryption public key is not of size phi.</li>
		 *                                       <li>Not all inputs have the same Gq group.</li>
		 *                                       <li>The exponentiation proof does not have the same group order as the other inputs.</li>
		 *                                       <li>The plaintext equality proof does not have the same group order as the other inputs.</li>
		 *                                   </ul>
		 */
		public VerifyBallotCCRInput build() {
			validateUUID(verificationCardId);
			checkNotNull(encryptedVote);
			checkNotNull(exponentiatedEncryptedVote);
			checkNotNull(encryptedPartialChoiceReturnCodes);
			checkNotNull(verificationCardPublicKey);
			checkNotNull(electionPublicKey);
			checkNotNull(choiceReturnCodesEncryptionPublicKey);
			checkNotNull(exponentiationProof);
			checkNotNull(plaintextEqualityProof);

			// Size checks.
			final int delta_hat = encryptedVote.size();
			checkArgument(delta_hat > 0, "The size of the encrypted vote must be strictly positive. [numberOfAllowedWriteInsPlusOne: %s]", delta_hat);
			checkArgument(delta_hat <= MAXIMUM_NUMBER_OF_WRITE_IN_OPTIONS + 1,
					"The size of the encrypted vote must be smaller or equal to the maximum number of writing options + 1. [numberOfAllowedWriteInsPlusOne: %s]",
					delta_hat);
			checkArgument(exponentiatedEncryptedVote.size() == 1,
					"The size of the exponentiated encrypted vote must be equal to 1.");
			final int psi = encryptedPartialChoiceReturnCodes.size();
			checkArgument(psi <= MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS,
					"The size of the encrypted partial Choice Return Codes must be smaller or equal to the maximum number of selectable voting options. [numberOfSelectableVotingOptions: %s]",
					psi);

			// Cross group checks.
			final List<GqGroup> gqGroups = Arrays.asList(encryptedVote.getGroup(), exponentiatedEncryptedVote.getGroup(),
					encryptedPartialChoiceReturnCodes.getGroup(), verificationCardPublicKey.getGroup(), electionPublicKey.getGroup(),
					choiceReturnCodesEncryptionPublicKey.getGroup());
			checkArgument(allEqual(gqGroups.stream(), Function.identity()), "All input GqGroups must be the same.");

			checkArgument(gqGroups.get(0).hasSameOrderAs(exponentiationProof.getGroup()),
					"The exponentiation proof must have the same group order than the other inputs.");
			checkArgument(gqGroups.get(0).hasSameOrderAs(plaintextEqualityProof.getGroup()),
					"The plaintext equality proof must have the same group order than the other inputs.");

			// Require.
			final int phi = choiceReturnCodesEncryptionPublicKey.size();
			checkArgument(psi <= phi,
					"numberOfSelectableVotingOptions must be smaller or equal to phi. [numberOfSelectableVotingOptions: %s, phi: %s]", psi, phi);

			final int delta = electionPublicKey.size();
			checkArgument(delta_hat <= delta,
					"numberOfAllowedWriteInsPlusOne must be smaller than or equal to delta. [numberOfAllowedWriteInsPlusOne: %s, delta: %s]",
					delta_hat, delta);

			return new VerifyBallotCCRInput(verificationCardId, encryptedVote, exponentiatedEncryptedVote,
					encryptedPartialChoiceReturnCodes, verificationCardPublicKey, electionPublicKey, choiceReturnCodesEncryptionPublicKey,
					exponentiationProof, plaintextEqualityProof);
		}
	}
}
