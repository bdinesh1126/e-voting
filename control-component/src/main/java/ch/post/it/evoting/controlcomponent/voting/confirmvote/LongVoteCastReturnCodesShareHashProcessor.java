/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.confirmvote;

import static ch.post.it.evoting.controlcomponent.ControlComponentsApplicationBootstrap.RABBITMQ_EXCHANGE;
import static ch.post.it.evoting.domain.SharedQueue.CREATE_LVCC_SHARE_HASH_REQUEST_PATTERN;
import static ch.post.it.evoting.domain.SharedQueue.CREATE_LVCC_SHARE_HASH_RESPONSE_PATTERN;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.SignatureException;
import java.time.LocalDateTime;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.controlcomponent.CcrjReturnCodesKeysService;
import ch.post.it.evoting.controlcomponent.ElectionEventService;
import ch.post.it.evoting.controlcomponent.ExactlyOnceCommand;
import ch.post.it.evoting.controlcomponent.ExactlyOnceCommandExecutor;
import ch.post.it.evoting.controlcomponent.IdentifierValidationService;
import ch.post.it.evoting.controlcomponent.Messages;
import ch.post.it.evoting.controlcomponent.VerificationCardStateService;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.utils.Conversions;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.voting.confirmvote.ConfirmationKey;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponenthlVCCPayload;
import ch.post.it.evoting.domain.voting.confirmvote.VotingServerConfirmPayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;

@Service
public class LongVoteCastReturnCodesShareHashProcessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(LongVoteCastReturnCodesShareHashProcessor.class);
	private final ObjectMapper objectMapper;
	private final RabbitTemplate rabbitTemplate;
	private final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor;
	private final IdentifierValidationService identifierValidationService;
	private final ElectionEventService electionEventService;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final CcrjReturnCodesKeysService ccrjReturnCodesKeysService;
	private final CreateLVCCShareAlgorithm createLVCCShareAlgorithm;
	private final LongVoteCastReturnCodesShareService longVoteCastReturnCodesShareService;
	private final VerificationCardStateService verificationCardStateService;

	private String responseQueue;
	@Value("${nodeID}")
	private int nodeId;

	LongVoteCastReturnCodesShareHashProcessor(
			final ObjectMapper objectMapper,
			final RabbitTemplate rabbitTemplate,
			final IdentifierValidationService identifierValidationService,
			final ElectionEventService electionEventService,
			final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor,
			final SignatureKeystore<Alias> signatureKeystoreService,
			final CcrjReturnCodesKeysService ccrjReturnCodesKeysService,
			final CreateLVCCShareAlgorithm createLVCCShareAlgorithm,
			final LongVoteCastReturnCodesShareService longVoteCastReturnCodesShareService,
			final VerificationCardStateService verificationCardStateService) {
		this.objectMapper = objectMapper;
		this.rabbitTemplate = rabbitTemplate;
		this.exactlyOnceCommandExecutor = exactlyOnceCommandExecutor;
		this.electionEventService = electionEventService;
		this.identifierValidationService = identifierValidationService;
		this.signatureKeystoreService = signatureKeystoreService;
		this.ccrjReturnCodesKeysService = ccrjReturnCodesKeysService;
		this.createLVCCShareAlgorithm = createLVCCShareAlgorithm;
		this.longVoteCastReturnCodesShareService = longVoteCastReturnCodesShareService;
		this.verificationCardStateService = verificationCardStateService;
	}

	@PostConstruct
	public void initQueue() {
		responseQueue = String.format("%s%s", CREATE_LVCC_SHARE_HASH_RESPONSE_PATTERN, nodeId);
	}

	@RabbitListener(queues = CREATE_LVCC_SHARE_HASH_REQUEST_PATTERN + "${nodeID}", autoStartup = "false", concurrency = "4")
	public void onMessage(final Message message) throws IOException {
		final String correlationId = message.getMessageProperties().getCorrelationId();
		checkNotNull(correlationId, "Correlation Id must not be null");

		final byte[] messageBytes = message.getBody();

		final VotingServerConfirmPayload votingServerConfirmPayload = objectMapper.readValue(messageBytes, VotingServerConfirmPayload.class);

		verifyPayloadSignature(votingServerConfirmPayload);

		final VotingServerConfirmPayload unsignedVotingServerConfirmPayload = new VotingServerConfirmPayload(
				votingServerConfirmPayload.getEncryptionGroup(), votingServerConfirmPayload.getConfirmationKey());
		final byte[] messageContent = Conversions.stringToByteArray(objectMapper.writeValueAsString(unsignedVotingServerConfirmPayload));

		final ContextIds contextIds = votingServerConfirmPayload.getConfirmationKey().contextIds();

		identifierValidationService.validateContextIds(contextIds);
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		longVoteCastReturnCodesShareService.validateConfirmationIsAllowed(electionEventId, verificationCardId, LocalDateTime::now);

		// The variable confirmationAttempts is initialized to 0 and incremented for each confirmation attempt.
		final int contextConfirmationAttempts = verificationCardStateService.getConfirmationAttempts(verificationCardId) + 1;
		final String contextId = String.join("-", electionEventId, verificationCardSetId, verificationCardId,
				String.valueOf(contextConfirmationAttempts));
		LOGGER.info("Received create LVCC share hash request. [contextId: {}, correlationId: {}]", contextId, correlationId);

		// Compute the Long Vote Cast Return Code shares.
		final ExactlyOnceCommand exactlyOnceCommand = new ExactlyOnceCommand.Builder()
				.setCorrelationId(correlationId)
				.setContextId(contextId)
				.setContext(Context.VOTING_RETURN_CODES_CREATE_LVCC_SHARE_HASH.toString())
				.setTask(() -> generateControlComponenthlVCCPayload(unsignedVotingServerConfirmPayload))
				.setRequestContent(messageContent)
				.build();
		final byte[] payloadBytes = exactlyOnceCommandExecutor.process(exactlyOnceCommand);

		final Message responseMessage = Messages.createMessage(correlationId, payloadBytes);

		rabbitTemplate.send(RABBITMQ_EXCHANGE, responseQueue, responseMessage);
		LOGGER.info("Create LVCC share hash response sent. [contextId: {}]", contextIds);
	}

	private void verifyPayloadSignature(final VotingServerConfirmPayload votingServerConfirmPayload) {
		final CryptoPrimitivesSignature signature = votingServerConfirmPayload.getSignature();
		final ContextIds contextIds = votingServerConfirmPayload.getConfirmationKey().contextIds();

		checkState(signature != null, "The signature of voting server confirm payload is null. [contextIds: %s]",
				contextIds);

		final Hashable additionalContextData = ChannelSecurityContextData.votingServerConfirm(contextIds.electionEventId(),
				contextIds.verificationCardSetId(), contextIds.verificationCardId());
		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.VOTING_SERVER, votingServerConfirmPayload,
					additionalContextData, signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(String.format("Unable to verify signature [contextIds: %s]", contextIds), e);
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(VotingServerConfirmPayload.class,
					String.format("The signature of Long Vote Cast Return Codes Share hash response payload is invalid. [contextIds: %s]",
							contextIds));
		}
	}

	private byte[] generateControlComponenthlVCCPayload(final VotingServerConfirmPayload unsignedVotingServerConfirmPayload) {
		final ConfirmationKey confirmationKey = unsignedVotingServerConfirmPayload.getConfirmationKey();
		final ContextIds contextIds = confirmationKey.contextIds();
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();
		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);

		// Compute the LVCC share hash.
		final ZqElement secretKey = ccrjReturnCodesKeysService.getCcrjReturnCodesKeys(electionEventId).ccrjReturnCodesGenerationSecretKey();

		// Long Vote Cast Return Code share computation.
		final LVCCContext context = new LVCCContext(encryptionGroup, nodeId, electionEventId, verificationCardSetId);
		final CreateLVCCShareInput input = new CreateLVCCShareInput(confirmationKey.element(), secretKey, verificationCardId);
		final CreateLVCCShareOutput createLVCCShareOutput = createLVCCShareAlgorithm.createLVCCShare(context, input);

		LOGGER.info("Successfully generated the Long Vote Cast Return Codes Share. [contextIds: {}]", contextIds);

		longVoteCastReturnCodesShareService.saveLVCCShare(confirmationKey, createLVCCShareOutput);

		final ControlComponenthlVCCPayload payload = new ControlComponenthlVCCPayload(encryptionGroup, nodeId,
				createLVCCShareOutput.getHashedLongVoteCastReturnCodeShare(), confirmationKey);

		payload.setSignature(getPayloadSignature(payload));
		LOGGER.info("Successfully signed Control Component hlVCC payload. [contextIds: {}]", contextIds);
		return serialize(payload);
	}

	private byte[] serialize(final ControlComponenthlVCCPayload payload) {
		try {
			return objectMapper.writeValueAsBytes(payload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException("Could not serialize long return codes share payload", e);
		}
	}

	private CryptoPrimitivesSignature getPayloadSignature(final ControlComponenthlVCCPayload payload) {
		final ContextIds contextIds = payload.getConfirmationKey().contextIds();
		final Hashable additionalContextData = ChannelSecurityContextData.controlComponenthlVCC(nodeId, contextIds.electionEventId(),
				contextIds.verificationCardSetId(), contextIds.verificationCardId());

		final byte[] signature;
		try {
			signature = signatureKeystoreService.generateSignature(payload, additionalContextData);
		} catch (final SignatureException e) {
			throw new IllegalStateException(String.format("Failed to generate payload signature [contextIds: %s, nodeId: %s]", contextIds, nodeId),
					e);
		}
		return new CryptoPrimitivesSignature(signature);
	}

}
