/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.configuration;

import static ch.post.it.evoting.controlcomponent.ControlComponentsApplicationBootstrap.RABBITMQ_EXCHANGE;
import static ch.post.it.evoting.domain.SharedQueue.GEN_KEYS_CCR_REQUEST_PATTERN;
import static ch.post.it.evoting.domain.SharedQueue.GEN_KEYS_CCR_RESPONSE_PATTERN;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.SignatureException;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.controlcomponent.ElectionEventService;
import ch.post.it.evoting.controlcomponent.ExactlyOnceCommand;
import ch.post.it.evoting.controlcomponent.ExactlyOnceCommandExecutor;
import ch.post.it.evoting.controlcomponent.Messages;
import ch.post.it.evoting.cryptoprimitives.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.configuration.ControlComponentKeyGenerationRequestPayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.configuration.ControlComponentPublicKeysPayload;

@Service
public class KeyGenerationProcessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(KeyGenerationProcessor.class);

	private final ObjectMapper objectMapper;
	private final RabbitTemplate rabbitTemplate;
	private final KeyGenerationService keyGenerationService;
	private final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor;
	private final ElectionEventService electionEventService;
	private final SignatureKeystore<Alias> signatureKeystoreService;

	@Value("${nodeID}")
	private int nodeId;

	private String responseQueue;

	KeyGenerationProcessor(
			final ObjectMapper objectMapper,
			final RabbitTemplate rabbitTemplate,
			final KeyGenerationService keyGenerationService,
			final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor,
			final ElectionEventService electionEventService,
			final SignatureKeystore<Alias> signatureKeystoreService) {
		this.objectMapper = objectMapper;
		this.rabbitTemplate = rabbitTemplate;
		this.keyGenerationService = keyGenerationService;
		this.exactlyOnceCommandExecutor = exactlyOnceCommandExecutor;
		this.electionEventService = electionEventService;
		this.signatureKeystoreService = signatureKeystoreService;
	}

	@PostConstruct
	public void initQueue() {
		responseQueue = String.format("%s%s", GEN_KEYS_CCR_RESPONSE_PATTERN, nodeId);
	}

	@RabbitListener(queues = GEN_KEYS_CCR_REQUEST_PATTERN + "${nodeID}", autoStartup = "false")
	public void onMessage(final Message message) throws IOException {

		final String correlationId = message.getMessageProperties().getCorrelationId();
		checkNotNull(correlationId, "Correlation Id must not be null");

		final byte[] messageBytes = message.getBody();
		final ControlComponentKeyGenerationRequestPayload controlComponentKeyGenerationRequestPayload = objectMapper.readValue(messageBytes,
				ControlComponentKeyGenerationRequestPayload.class);

		final String contextId = controlComponentKeyGenerationRequestPayload.electionEventId();
		LOGGER.info("Received key generation request. [contextId: {}, correlationId: {}]", contextId, correlationId);

		// Process key generation payload.
		final ExactlyOnceCommand exactlyOnceCommand = new ExactlyOnceCommand.Builder()
				.setCorrelationId(correlationId)
				.setContextId(contextId)
				.setContext(Context.CONFIGURATION_RETURN_CODES_GEN_KEYS_CCR.toString())
				.setTask(() -> generateKeysPayload(controlComponentKeyGenerationRequestPayload))
				.setRequestContent(messageBytes)
				.build();
		final byte[] payloadBytes = exactlyOnceCommandExecutor.process(exactlyOnceCommand);

		final Message responseMessage = Messages.createMessage(correlationId, payloadBytes);

		rabbitTemplate.send(RABBITMQ_EXCHANGE, responseQueue, responseMessage);
		LOGGER.info("CCR generation key response sent. [contextId: {}]", contextId);
	}

	private byte[] generateKeysPayload(final ControlComponentKeyGenerationRequestPayload controlComponentKeyGenerationRequestPayload) {
		final String electionEventId = controlComponentKeyGenerationRequestPayload.electionEventId();
		final GqGroup encryptionParameters = controlComponentKeyGenerationRequestPayload.encryptionParameters();

		// Save the encryption parameters.
		electionEventService.save(electionEventId, controlComponentKeyGenerationRequestPayload.encryptionParameters());
		LOGGER.info("Saved encryption parameters. [electionEventId: {}]", electionEventId);

		// Generate ccrj and ccmj keys.
		final ControlComponentPublicKeys controlComponentPublicKeys = keyGenerationService.generateCCKeys(electionEventId, encryptionParameters);

		// Create and sign payload.
		final ControlComponentPublicKeysPayload controlComponentPublicKeysPayload = new ControlComponentPublicKeysPayload(encryptionParameters,
				electionEventId, controlComponentPublicKeys);
		controlComponentPublicKeysPayload.setSignature(getPayloadSignature(controlComponentPublicKeysPayload));

		LOGGER.info("Successfully signed control component public keys payload. [electionEventId: {}]", electionEventId);
		try {
			return objectMapper.writeValueAsBytes(controlComponentPublicKeysPayload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(
					String.format("Could not serialize control component public keys payload. [electionEventId: %s]", electionEventId), e);
		}
	}

	private CryptoPrimitivesSignature getPayloadSignature(final ControlComponentPublicKeysPayload payload) {
		final String electionEventId = payload.getElectionEventId();

		final Hashable additionalContextData = ChannelSecurityContextData.controlComponentPublicKeys(nodeId, electionEventId);

		try {
			final byte[] signature = signatureKeystoreService.generateSignature(payload, additionalContextData);

			return new CryptoPrimitivesSignature(signature);
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not generate control component public keys payload signature. [contextIds: %s]", electionEventId));
		}
	}

}
