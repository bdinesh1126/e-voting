/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.confirmvote;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Regroups the context values needed by the CreateLVCCShare<sub>j</sub> and VerifyLVCCHash<sub>j</sub> algorithms.
 *
 * <ul>
 * <li>encryptionGroup, the {@code GqGroup} with modulus p, cardinality q and generator g. Not null.</li>
 * <li>j, the CCR's index.</li>
 * <li>ee, the election event id. Not null and a valid UUID.</li>
 * <li>vcs, the verification card set id. Not null and a valid UUID.</li>
 * <li>l_HB64, the character length of the Base64 encoded hash output. Equal to 44 for a 256-bit hash function.</li>
 * </ul>
 */
public record LVCCContext(GqGroup encryptionGroup, int nodeId, String electionEventId, String verificationCardSetId) {

	@SuppressWarnings("java:S115")
	private static final int l_HB64 = 44;

	public LVCCContext {
		checkNotNull(encryptionGroup);
		checkArgument(NODE_IDS.contains(nodeId), "The node id must be part of the known node ids. [nodeId: %s]", nodeId);
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
	}

	int getlHB64() {
		return l_HB64;
	}

}
