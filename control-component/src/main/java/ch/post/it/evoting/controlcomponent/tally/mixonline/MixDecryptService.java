/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.tally.mixonline;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.annotations.VisibleForTesting;

import ch.post.it.evoting.controlcomponent.BallotBoxEntity;
import ch.post.it.evoting.controlcomponent.BallotBoxService;
import ch.post.it.evoting.controlcomponent.CcmjElectionKeysService;
import ch.post.it.evoting.controlcomponent.ElectionContextEntity;
import ch.post.it.evoting.controlcomponent.ElectionContextService;
import ch.post.it.evoting.controlcomponent.ElectionEventService;
import ch.post.it.evoting.controlcomponent.EncryptedVerifiableVoteService;
import ch.post.it.evoting.controlcomponent.SetupComponentPublicKeysService;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.mixnet.VerifiableShuffle;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.VerifiableDecryptions;
import ch.post.it.evoting.evotinglibraries.domain.common.EncryptedVerifiableVote;
import ch.post.it.evoting.evotinglibraries.domain.tally.ControlComponentBallotBoxPayload;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixoffline.VerifyMixDecInput;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixonline.GetMixnetInitialCiphertextsAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixonline.GetMixnetInitialCiphertextsContext;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixonline.GetMixnetInitialCiphertextsInput;

@Service
class MixDecryptService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MixDecryptService.class);

	private final ElectionEventService electionEventService;
	private final BallotBoxService ballotBoxService;
	private final ElectionContextService electionContextService;
	private final SetupComponentPublicKeysService setupComponentPublicKeysService;
	private final MixDecOnlineAlgorithm mixDecOnlineAlgorithm;
	private final CcmjElectionKeysService ccmjElectionKeysService;
	private final VerifyMixDecOnlineAlgorithm verifyMixDecOnlineAlgorithm;
	private final GetMixnetInitialCiphertextsAlgorithm getMixnetInitialCiphertextsAlgorithm;
	private final EncryptedVerifiableVoteService encryptedVerifiableVoteService;

	@Value("${nodeID}")
	private int nodeId;

	MixDecryptService(
			final ElectionEventService electionEventService,
			final BallotBoxService ballotBoxService,
			final ElectionContextService electionContextService,
			final SetupComponentPublicKeysService setupComponentPublicKeysService,
			final MixDecOnlineAlgorithm mixDecOnlineAlgorithm,
			final CcmjElectionKeysService ccmjElectionKeysService,
			final VerifyMixDecOnlineAlgorithm verifyMixDecOnlineAlgorithm,
			final GetMixnetInitialCiphertextsAlgorithm getMixnetInitialCiphertextsAlgorithm,
			final EncryptedVerifiableVoteService encryptedVerifiableVoteService) {
		this.electionEventService = electionEventService;
		this.ballotBoxService = ballotBoxService;
		this.electionContextService = electionContextService;
		this.setupComponentPublicKeysService = setupComponentPublicKeysService;
		this.mixDecOnlineAlgorithm = mixDecOnlineAlgorithm;
		this.ccmjElectionKeysService = ccmjElectionKeysService;
		this.verifyMixDecOnlineAlgorithm = verifyMixDecOnlineAlgorithm;
		this.getMixnetInitialCiphertextsAlgorithm = getMixnetInitialCiphertextsAlgorithm;
		this.encryptedVerifiableVoteService = encryptedVerifiableVoteService;
	}

	@Transactional
	public MixDecryptServiceOutput performMixDecrypt(final String electionEventId, final String ballotBoxId,
			final List<ControlComponentShufflePayload> controlComponentShufflePayloads) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);
		validateIdsCorrectness(electionEventId, ballotBoxId);
		final List<ControlComponentShufflePayload> controlComponentShufflePayloadsCopy = List.copyOf(checkNotNull(controlComponentShufflePayloads));
		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);

		validateConsistency(encryptionGroup, electionEventId, ballotBoxId, controlComponentShufflePayloadsCopy);

		final BallotBoxEntity ballotBoxEntity = ballotBoxService.getBallotBox(ballotBoxId);
		final int numberOfWriteInsPlusOne = ballotBoxEntity.getNumberOfWriteInsPlusOne();
		final String verificationCardSetId = ballotBoxEntity.getVerificationCardSetEntity().getVerificationCardSetId();
		final List<EncryptedVerifiableVote> confirmedVotes = encryptedVerifiableVoteService.getConfirmedVotes(verificationCardSetId);
		final Map<String, ElGamalMultiRecipientCiphertext> confirmedEncryptedVotes = confirmedVotes.stream()
				.collect(Collectors.toMap(encryptedVerifiableVote -> encryptedVerifiableVote.contextIds().verificationCardId(),
						EncryptedVerifiableVote::encryptedVote));
		final ElGamalMultiRecipientPublicKey electionPublicKey = setupComponentPublicKeysService.getElectionPublicKey(electionEventId);

		final GetMixnetInitialCiphertextsContext getMixnetInitialCiphertextsContext = new GetMixnetInitialCiphertextsContext(encryptionGroup,
				electionEventId, ballotBoxId);
		final GetMixnetInitialCiphertextsInput getMixnetInitialCiphertextsInput = new GetMixnetInitialCiphertextsInput(numberOfWriteInsPlusOne,
				confirmedEncryptedVotes, electionPublicKey);

		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> initialCiphertexts = getMixnetInitialCiphertextsAlgorithm.getMixnetInitialCiphertexts(
				getMixnetInitialCiphertextsContext, getMixnetInitialCiphertextsInput);
		LOGGER.info("Mixnet initial ciphertexts retrieved. [electionEventId: {}, ballotBoxId: {}]", electionEventId, ballotBoxId);

		final ElGamalMultiRecipientPublicKey electoralBoardPublicKey = setupComponentPublicKeysService.getElectoralBoardPublicKey(electionEventId);
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys = setupComponentPublicKeysService.getCcmElectionPublicKeys(
				electionEventId);
		final List<VerifiableDecryptions> precedingVerifiableDecryptions = controlComponentShufflePayloadsCopy.stream()
				.map(ControlComponentShufflePayload::getVerifiableDecryptions)
				.toList();

		// The first node has nothing to verify.
		if (nodeId != 1) {
			final VerifyMixDecOnlineContext verifyMixDecOnlineContext = new VerifyMixDecOnlineContext(encryptionGroup, nodeId, electionEventId,
					ballotBoxId, numberOfWriteInsPlusOne);

			final List<VerifiableShuffle> precedingVerifiableShuffledVotes = controlComponentShufflePayloadsCopy.stream()
					.map(ControlComponentShufflePayload::getVerifiableShuffle)
					.toList();
			final VerifyMixDecInput verifyMixDecInput = new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes,
					precedingVerifiableDecryptions, electionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey);

			if (!verifyMixDecOnlineAlgorithm.verifyMixDecOnline(verifyMixDecOnlineContext, verifyMixDecInput)) {
				throw new IllegalStateException(String.format(
						"The preceding control-component's mixing and decryption proofs are invalid. [electionEventId: %s, ballotBoxId: %s]",
						electionEventId, ballotBoxId));
			}
			LOGGER.info("The preceding control-component's mixing and decryption proofs are valid. [electionEventId: {}, ballotBoxId: {}]",
					electionEventId, ballotBoxId);
		}

		final MixDecOnlineContext mixDecOnlineContext = new MixDecOnlineContext.Builder()
				.setEncryptionGroup(encryptionGroup)
				.setElectionEventId(electionEventId)
				.setBallotBoxId(ballotBoxId)
				.setNumberOfAllowedWriteInsPlusOne(numberOfWriteInsPlusOne)
				.build();
		final ElGamalMultiRecipientPrivateKey ccmjElectionSecretKey = ccmjElectionKeysService.getCcmjElectionKeyPair(electionEventId).getPrivateKey();
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> partiallyDecryptedVotes;
		if (nodeId == 1) {
			partiallyDecryptedVotes = initialCiphertexts;
		} else {
			partiallyDecryptedVotes = precedingVerifiableDecryptions.get(precedingVerifiableDecryptions.size() - 1).getCiphertexts();
		}
		final MixDecOnlineInput mixDecOnlineInput = new MixDecOnlineInput.Builder()
				.setPartiallyDecryptedVotes(partiallyDecryptedVotes)
				.setCcmElectionPublicKeys(ccmElectionPublicKeys)
				.setElectoralBoardPublicKey(electoralBoardPublicKey)
				.setCcmjElectionSecretKey(ccmjElectionSecretKey)
				.build();

		final MixDecOnlineOutput mixDecOnlineOutput = mixDecOnlineAlgorithm.mixDecOnline(mixDecOnlineContext, mixDecOnlineInput);
		LOGGER.info("Ballot box successfully mixed. [electionEventId: {}, ballotBoxId: {}]", electionEventId, ballotBoxId);

		final VerifiableDecryptions verifiableDecryptions = mixDecOnlineOutput.verifiableDecryptions();
		final VerifiableShuffle verifiableShuffle = mixDecOnlineOutput.verifiableShuffle();
		final ControlComponentShufflePayload controlComponentShufflePayload = new ControlComponentShufflePayload(
				encryptionGroup, electionEventId,
				ballotBoxId, nodeId, verifiableDecryptions, verifiableShuffle);
		LOGGER.info("Control component shuffle payload retrieved. [electionEventId: {}, ballotBoxId: {}]", electionEventId, ballotBoxId);

		final ControlComponentBallotBoxPayload controlComponentBallotBoxPayload = new ControlComponentBallotBoxPayload(encryptionGroup,
				electionEventId, ballotBoxId, nodeId, confirmedVotes);

		return new MixDecryptServiceOutput(controlComponentBallotBoxPayload, controlComponentShufflePayload);
	}

	private void validateConsistency(final GqGroup encryptionGroup, final String electionEventId, final String ballotBoxId,
			final List<ControlComponentShufflePayload> shufflePayloads) {
		validateShufflePayload(encryptionGroup, electionEventId, ballotBoxId, shufflePayloads);
		validateMixIsAllowed(electionEventId, ballotBoxId, LocalDateTime::now);
	}

	private void validateIdsCorrectness(final String electionEventId, final String ballotBoxId) {
		checkArgument(electionEventService.exists(electionEventId), "The given election event ID does not exist. [electionEventId: %s]",
				electionEventId);
		checkArgument(ballotBoxService.existsForElectionEventId(ballotBoxId, electionEventId),
				"The given ballot box ID does not exist for the given election event ID. [ballotBoxId: %s, electionEventId: %s]", ballotBoxId,
				electionEventId);
	}

	@VisibleForTesting
	void validateShufflePayload(final GqGroup encryptionGroup, final String electionEventId, final String ballotBoxId,
			final List<ControlComponentShufflePayload> shufflePayloads) {
		checkArgument(shufflePayloads.size() == nodeId - 1,
				"There must be exactly the expected number of shuffle payloads. [expected: %s, actual: %s]", nodeId - 1, shufflePayloads.size());

		shufflePayloads.forEach(payload -> {
			checkState(electionEventId.equals(payload.getElectionEventId()),
					"Election event ID must be identical in shuffle payload. [expected: %s, actual: %s]", electionEventId,
					payload.getElectionEventId());
			checkState(ballotBoxId.equals(payload.getBallotBoxId()),
					"Ballot box ID must be identical in shuffle payload. [expected: %s, actual: %s]", ballotBoxId,
					payload.getBallotBoxId());
			checkState(encryptionGroup.equals(payload.getEncryptionGroup()),
					"Gq groups must be identical in shuffle payload. [expected: %s, actual: %s]", encryptionGroup,
					payload.getEncryptionGroup());
		});

		final Set<Integer> actualPayloadNodeIds = shufflePayloads.stream()
				.map(ControlComponentShufflePayload::getNodeId)
				.collect(Collectors.toSet());
		final Set<Integer> expectedPayloadNodeIds = IntStream.range(1, nodeId).boxed().collect(Collectors.toSet());

		checkState(actualPayloadNodeIds.containsAll(expectedPayloadNodeIds),
				"Payloads must come from expected nodes. [expected: %s, actual: %s]", expectedPayloadNodeIds, actualPayloadNodeIds);
	}

	@VisibleForTesting
	void validateMixIsAllowed(final String electionEventId, final String ballotBoxId, final Supplier<LocalDateTime> now) {
		final ElectionContextEntity electionContextEntity = electionContextService.getElectionContextEntity(electionEventId);
		final BallotBoxEntity ballotBoxEntity = ballotBoxService.getBallotBox(ballotBoxId);

		final LocalDateTime electionEndTime = electionContextEntity.getFinishTime();
		final LocalDateTime currentTime = now.get();

		final boolean afterEndTime = currentTime.isAfter(electionEndTime.plusSeconds(ballotBoxEntity.getGracePeriod()));

		// Test ballot boxes can be mixed and decrypted at any time. Real ballot boxes can be mixed and decrypted only after the election event period ended.
		checkState(ballotBoxEntity.isTestBallotBox() || afterEndTime,
				"The ballot box can not be mixed. [isTestBallotBox: %s, finishTime: %s, electionEventId: %s, ballotBoxId: %s]",
				ballotBoxEntity.isTestBallotBox(), electionEndTime, electionEventId, ballotBoxId);
	}

	record MixDecryptServiceOutput(ControlComponentBallotBoxPayload controlComponentBallotBoxPayload,
								   ControlComponentShufflePayload controlComponentShufflePayload) {

		MixDecryptServiceOutput {
			checkNotNull(controlComponentBallotBoxPayload);
			checkNotNull(controlComponentShufflePayload);
		}

	}

}
