/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "BALLOT_BOX")
public class BallotBoxEntity {

	@Id
	private Long id;

	private String ballotBoxId;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	@JoinColumn(name = "VERIFICATION_CARD_SET_FK_ID")
	private VerificationCardSetEntity verificationCardSetEntity;

	@Convert(converter = BooleanConverter.class)
	private boolean mixed;

	@Convert(converter = BooleanConverter.class)
	private boolean testBallotBox;

	private int numberOfWriteInsPlusOne;

	private int numberOfVotingCards;

	// The grace period in seconds.
	private int gracePeriod;

	private byte[] primesMappingTable;

	@Version
	private Integer changeControlId;

	public BallotBoxEntity() {
		this.mixed = false;
		this.testBallotBox = false;
	}

	public BallotBoxEntity(final String ballotBoxId, final VerificationCardSetEntity verificationCardSetEntity, final boolean testBallotBox,
			final int numberOfWriteInsPlusOne, final int numberOfVotingCards, final int gracePeriod, final byte[] primesMappingTable) {
		this.ballotBoxId = validateUUID(ballotBoxId);
		this.verificationCardSetEntity = checkNotNull(verificationCardSetEntity);
		this.mixed = false;
		this.testBallotBox = testBallotBox;
		checkArgument(numberOfWriteInsPlusOne > 0);
		this.numberOfWriteInsPlusOne = numberOfWriteInsPlusOne;
		checkArgument(numberOfVotingCards >= 0);
		this.numberOfVotingCards = numberOfVotingCards;
		checkArgument(gracePeriod >= 0);
		this.gracePeriod = gracePeriod;
		this.primesMappingTable = checkNotNull(primesMappingTable);
	}

	public Long getId() {
		return id;
	}

	public String getBallotBoxId() {
		return ballotBoxId;
	}

	public VerificationCardSetEntity getVerificationCardSetEntity() {
		return verificationCardSetEntity;
	}

	public Integer getChangeControlId() {
		return changeControlId;
	}

	public boolean isMixed() {
		return mixed;
	}

	public void setMixed(final boolean mixed) {
		this.mixed = mixed;
	}

	public boolean isTestBallotBox() {
		return testBallotBox;
	}

	public int getNumberOfWriteInsPlusOne() {
		return numberOfWriteInsPlusOne;
	}

	public int getNumberOfVotingCards() {
		return numberOfVotingCards;
	}

	public int getGracePeriod() {
		return gracePeriod;
	}

	public byte[] getPrimesMappingTable() {
		return Arrays.copyOf(primesMappingTable, primesMappingTable.length);
	}

}
