/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.controlcomponent.configuration.setupvoting.PCCAllowListEntryService;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;

@Service
public class VerificationCardSetService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerificationCardSetService.class);

	private final ElectionEventService electionEventService;
	private final PCCAllowListEntryService pccAllowListEntryService;
	private final VerificationCardSetRepository verificationCardSetRepository;
	private final LVCCAllowListEntryService lvccAllowListEntryService;

	public VerificationCardSetService(
			final ElectionEventService electionEventService,
			final PCCAllowListEntryService pccAllowListEntryService,
			final VerificationCardSetRepository verificationCardSetRepository,
			final LVCCAllowListEntryService lvccAllowListEntryService) {
		this.electionEventService = electionEventService;
		this.pccAllowListEntryService = pccAllowListEntryService;
		this.verificationCardSetRepository = verificationCardSetRepository;
		this.lvccAllowListEntryService = lvccAllowListEntryService;
	}

	@Transactional
	public VerificationCardSetEntity save(final VerificationCardSetEntity verificationCardSetEntity) {
		checkNotNull(verificationCardSetEntity);

		return verificationCardSetRepository.save(verificationCardSetEntity);
	}

	@Transactional
	public boolean exists(final String verificationCardSetId) {
		validateUUID(verificationCardSetId);

		return verificationCardSetRepository.existsByVerificationCardSetId(verificationCardSetId);
	}

	@Transactional
	public int numberOfVotingCardSets(final String electionEventId) {
		validateUUID(electionEventId);

		return verificationCardSetRepository.countAllByElectionEventEntity_ElectionEventId(electionEventId);
	}

	@Transactional
	public VerificationCardSetEntity getVerificationCardSet(final String verificationCardSetId) {
		validateUUID(verificationCardSetId);

		return verificationCardSetRepository.findByVerificationCardSetId(verificationCardSetId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Verification card set not found. [verificationCardSetId: %s]", verificationCardSetId)));
	}

	@Transactional
	public VerificationCardSetEntity getOrCreateVerificationCardSet(final String electionEventId, final String verificationCardSetId,
			final CombinedCorrectnessInformation combinedCorrectnessInformation) {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkNotNull(combinedCorrectnessInformation);

		if (exists(verificationCardSetId)) {
			LOGGER.debug("Found existing verification card set. [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
					verificationCardSetId);
			return getVerificationCardSet(verificationCardSetId);
		} else {
			// Create a new verification card set.
			final ElectionEventEntity electionEventEntity = electionEventService.getElectionEventEntity(electionEventId);
			final VerificationCardSetEntity newVerificationCardSetEntity = new VerificationCardSetEntity(verificationCardSetId, electionEventEntity,
					combinedCorrectnessInformation);
			final VerificationCardSetEntity savedVerificationCardSetEntity = save(newVerificationCardSetEntity);
			LOGGER.debug("New verification card set saved. [electionEventId: {}, verificationCardSetId: {}]", electionEventId, verificationCardSetId);

			return savedVerificationCardSetEntity;
		}
	}

	/**
	 * Gets the partial Choice Return Codes allow list for the given verification card set id.
	 * <p>
	 * WARNING: This will not return the complete allow list if called before all chunks have been processed and saved.
	 *
	 * @param verificationCardSetId the verification card set id. Must be a valid UUID.
	 * @return the partial Choice Return Codes allow list.
	 */
	@Transactional
	public PartialChoiceReturnCodeAllowList getPartialChoiceReturnCodesAllowList(final String verificationCardSetId) {
		validateUUID(verificationCardSetId);

		return partialChoiceReturnCode -> pccAllowListEntryService.exists(verificationCardSetId, partialChoiceReturnCode);
	}

	/**
	 * Sets the given long vote cast return codes allow list into the verification card set corresponding to the given verification card set id.
	 *
	 * @param verificationCardSetId            the verification card set id. Must be non-null and a valid UUID.
	 * @param longVoteCastReturnCodesAllowList the long vote cast return codes allow list. Must be non-null.
	 */
	@Transactional
	public void setLongVoteCastReturnCodesAllowList(final String verificationCardSetId, final List<String> longVoteCastReturnCodesAllowList) {
		LOGGER.info("Updating verification card set with long vote cast return codes allow list... [verificationCardSetId: {}]",
				verificationCardSetId);

		validateUUID(verificationCardSetId);
		final List<String> longVoteCastReturnCodesAllowListImmutableCopy = List.copyOf(checkNotNull(longVoteCastReturnCodesAllowList));

		final VerificationCardSetEntity verificationCardSetEntity =
				verificationCardSetRepository.findByVerificationCardSetId(verificationCardSetId)
						.orElseThrow(() -> new IllegalStateException(
								String.format("Could not find any matching verification card set [verificationCardSetId: %s]",
										verificationCardSetId)));

		final List<LVCCAllowListEntryEntity> longVoteCastReturnCodeEntities = longVoteCastReturnCodesAllowListImmutableCopy.stream().parallel()
				.map(castCode -> new LVCCAllowListEntryEntity(verificationCardSetEntity, castCode))
				.toList();
		lvccAllowListEntryService.saveAll(longVoteCastReturnCodeEntities);

		LOGGER.info("Successfully updated verification card set with long vote cast return codes allow list. [verificationCardSetId: {}]",
				verificationCardSetId);
	}

	@Transactional
	public List<String> getLongVoteCastReturnCodesAllowList(final String verificationCardSetId) {
		return lvccAllowListEntryService.getLongVoteCastReturnCodeEntities(verificationCardSetId).stream()
				.map(LVCCAllowListEntryEntity::getLongVoteCastReturnCode).toList();
	}
}
