/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.configuration.setupvoting;

import static ch.post.it.evoting.controlcomponent.ControlComponentsApplicationBootstrap.RABBITMQ_EXCHANGE;
import static ch.post.it.evoting.domain.SharedQueue.LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST_REQUEST_PATTERN;
import static ch.post.it.evoting.domain.SharedQueue.LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST_RESPONSE_PATTERN;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.SignatureException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.controlcomponent.ExactlyOnceCommand;
import ch.post.it.evoting.controlcomponent.ExactlyOnceCommandExecutor;
import ch.post.it.evoting.controlcomponent.Messages;
import ch.post.it.evoting.controlcomponent.VerificationCardSetService;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.setupvoting.LongVoteCastReturnCodesAllowListResponsePayload;
import ch.post.it.evoting.domain.configuration.setupvoting.SetupComponentLVCCAllowListPayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;

@Service
public class LongVoteCastReturnCodesAllowListProcessor {

	public static final Logger LOGGER = LoggerFactory.getLogger(LongVoteCastReturnCodesAllowListProcessor.class);

	private final ObjectMapper objectMapper;
	private final RabbitTemplate rabbitTemplate;
	private final VerificationCardSetService verificationCardSetService;
	private final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor;
	private final SignatureKeystore<Alias> signatureKeystoreService;

	private String responseQueue;

	@Value("${nodeID}")
	private int nodeId;

	public LongVoteCastReturnCodesAllowListProcessor(final ObjectMapper objectMapper, final RabbitTemplate rabbitTemplate,
			final VerificationCardSetService verificationCardSetService, final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor,
			final SignatureKeystore<Alias> signatureKeystoreService) {

		this.objectMapper = objectMapper;
		this.rabbitTemplate = rabbitTemplate;
		this.verificationCardSetService = verificationCardSetService;
		this.exactlyOnceCommandExecutor = exactlyOnceCommandExecutor;
		this.signatureKeystoreService = signatureKeystoreService;
	}

	@PostConstruct
	public void initQueue() {
		responseQueue = String.format("%s%s", LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST_RESPONSE_PATTERN, nodeId);
	}

	@RabbitListener(queues = LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST_REQUEST_PATTERN + "${nodeID}", autoStartup = "false")
	public void onMessage(final Message message) throws IOException {
		checkNotNull(message);
		checkNotNull(message.getMessageProperties());

		final String correlationId = checkNotNull(message.getMessageProperties().getCorrelationId());
		final byte[] messageBytes = checkNotNull(message.getBody());

		final SetupComponentLVCCAllowListPayload setupComponentLVCCAllowListPayload = objectMapper.readValue(messageBytes,
				SetupComponentLVCCAllowListPayload.class);
		verifyPayloadSignature(setupComponentLVCCAllowListPayload);

		final String electionEventId = setupComponentLVCCAllowListPayload.getElectionEventId();
		final String verificationCardSetId = setupComponentLVCCAllowListPayload.getVerificationCardSetId();
		final String contextId = String.join("-", electionEventId, verificationCardSetId);

		LOGGER.info("Received long vote cast return codes allow list request. [contextId: {}, correlationId: {}]", contextId, correlationId);

		final ExactlyOnceCommand exactlyOnceCommand = new ExactlyOnceCommand.Builder()
				.setCorrelationId(correlationId)
				.setContextId(contextId)
				.setContext(Context.CONFIGURATION_SETUP_VOTING_LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST.toString())
				.setTask(() -> createLongVoteCastReturnCodesAllowListResponse(setupComponentLVCCAllowListPayload))
				.setRequestContent(messageBytes).build();

		final byte[] payloadBytes = exactlyOnceCommandExecutor.process(exactlyOnceCommand);

		LOGGER.debug("Long vote cast return codes allow list task successfully processed. [contextId: {}, correlationId: {}]", contextId,
				correlationId);

		final Message responseMessage = Messages.createMessage(correlationId, payloadBytes);

		rabbitTemplate.send(RABBITMQ_EXCHANGE, responseQueue, responseMessage);

		LOGGER.info("Long vote cast return codes allow list response sent. [contextId: {}, correlationId: {}]", contextId, correlationId);
	}

	private void verifyPayloadSignature(final SetupComponentLVCCAllowListPayload setupComponentLVCCAllowListPayload) {
		final String electionEventId = setupComponentLVCCAllowListPayload.getElectionEventId();
		final String verificationCardSetId = setupComponentLVCCAllowListPayload.getVerificationCardSetId();
		final CryptoPrimitivesSignature signature = setupComponentLVCCAllowListPayload.getSignature();

		checkState(signature != null,
				"The signature of setup component LVCC allow list payload is null. [electionEventId: %s, verificationCardSetId: %s]",
				electionEventId, verificationCardSetId);

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentLVCCAllowList(electionEventId, verificationCardSetId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.SDM_CONFIG, setupComponentLVCCAllowListPayload,
					additionalContextData, signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(String.format(
					"Cannot verify the signature of setup component LVCC allow list payload. [electionEventId: %s, verificationCardSetId: %s]",
					electionEventId, verificationCardSetId), e);
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(SetupComponentLVCCAllowListPayload.class,
					String.format("[electionEventId: %s, verificationCardSetId: %s]", electionEventId, verificationCardSetId));
		}

		LOGGER.info(
				"Successfully verified setup component LVCC allow list payload signature. [electionEventId: {}, verificationCardSetId: {}]",
				electionEventId, verificationCardSetId);
	}

	private byte[] createLongVoteCastReturnCodesAllowListResponse(final SetupComponentLVCCAllowListPayload longVoteCastCodeAllowListPayload) {
		final String electionEventId = longVoteCastCodeAllowListPayload.getElectionEventId();
		final String verificationCardSetId = longVoteCastCodeAllowListPayload.getVerificationCardSetId();
		final List<String> longVoteCastReturnCodesAllowList = longVoteCastCodeAllowListPayload.getLongVoteCastReturnCodesAllowList();

		verificationCardSetService.setLongVoteCastReturnCodesAllowList(verificationCardSetId, longVoteCastReturnCodesAllowList);

		LOGGER.info("Saved long vote return codes code allow list. [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);

		final LongVoteCastReturnCodesAllowListResponsePayload longVoteCastReturnCodesAllowListResponsePayload =
				new LongVoteCastReturnCodesAllowListResponsePayload(nodeId, electionEventId, verificationCardSetId);

		try {
			return objectMapper.writeValueAsBytes(longVoteCastReturnCodesAllowListResponsePayload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(String.format(
					"Could not serialize long vote cast return codes allow list response payload. [electionEventId: %s, verificationCardSetId: %s]",
					electionEventId, verificationCardSetId), e);
		}
	}

}
