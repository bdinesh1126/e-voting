/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;

public record VerificationCard(String verificationCardId, String verificationCardSetId, ElGamalMultiRecipientPublicKey verificationCardPublicKey) {

	public VerificationCard {
		validateUUID(verificationCardId);
		validateUUID(verificationCardSetId);
		checkNotNull(verificationCardPublicKey);
	}

}
