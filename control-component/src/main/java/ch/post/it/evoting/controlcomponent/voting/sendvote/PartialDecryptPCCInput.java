/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.cryptoprimitives.utils.Validations.allEqual;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Regroups the inputs needed by the PartialDecryptPCC<sub>j</sub> algorithm.
 *
 * <ul>
 * <li>vc<sub>id</sub>, the verification card id. Not null and a valid UUID.</li>
 * <li>E1, the encrypted vote. Not null.</li>
 * <li>E1_tilde, the exponentiated encrypted vote. Not null.</li>
 * <li>E2, the encrypted partial Choice Return Codes. Not null.</li>
 * <li>(sk<sub>CCRj,0</sub>,...,sk<sub>CCRj,phi−1</sub>), the CCR<sub>j</sub> Choice Return Codes encryption secret key. Not null.</li>
 * <li>(pk<sub>CCRj,0</sub>,...,pk<sub>CCRj,phi−1</sub>), the CCR<sub>j</sub> Choice Return Codes encryption public key. Not null.</li>
 * </ul>
 */
public class PartialDecryptPCCInput {

	private final String verificationCardId;
	private final ElGamalMultiRecipientCiphertext encryptedVote;
	private final ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote;
	private final ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes;
	private final ElGamalMultiRecipientPrivateKey ccrjChoiceReturnCodesEncryptionSecretKey;
	private final ElGamalMultiRecipientPublicKey ccrjChoiceReturnCodesEncryptionPublicKey;

	private PartialDecryptPCCInput(final String verificationCardId,
			final ElGamalMultiRecipientCiphertext encryptedVote,
			final ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote,
			final ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes,
			final ElGamalMultiRecipientPrivateKey ccrjChoiceReturnCodesEncryptionSecretKey,
			final ElGamalMultiRecipientPublicKey ccrjChoiceReturnCodesEncryptionPublicKey) {
		this.verificationCardId = verificationCardId;
		this.encryptedVote = encryptedVote;
		this.exponentiatedEncryptedVote = exponentiatedEncryptedVote;
		this.encryptedPartialChoiceReturnCodes = encryptedPartialChoiceReturnCodes;
		this.ccrjChoiceReturnCodesEncryptionSecretKey = ccrjChoiceReturnCodesEncryptionSecretKey;
		this.ccrjChoiceReturnCodesEncryptionPublicKey = ccrjChoiceReturnCodesEncryptionPublicKey;
	}

	String getVerificationCardId() {
		return verificationCardId;
	}

	ElGamalMultiRecipientCiphertext getEncryptedVote() {
		return encryptedVote;
	}

	ElGamalMultiRecipientCiphertext getExponentiatedEncryptedVote() {
		return exponentiatedEncryptedVote;
	}

	ElGamalMultiRecipientCiphertext getEncryptedPartialChoiceReturnCodes() {
		return encryptedPartialChoiceReturnCodes;
	}

	ElGamalMultiRecipientPrivateKey getCcrjChoiceReturnCodesEncryptionSecretKey() {
		return ccrjChoiceReturnCodesEncryptionSecretKey;
	}

	ElGamalMultiRecipientPublicKey getCcrjChoiceReturnCodesEncryptionPublicKey() {
		return ccrjChoiceReturnCodesEncryptionPublicKey;
	}

	GqGroup getGroup() {
		return encryptedVote.getGroup();
	}

	/**
	 * Builder performing input validations and cross-validations before constructing a {@link PartialDecryptPCCInput}.
	 */
	public static class Builder {

		private String verificationCardId;
		private ElGamalMultiRecipientCiphertext encryptedVote;
		private ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote;
		private ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes;
		private ElGamalMultiRecipientPrivateKey ccrjChoiceReturnCodesEncryptionSecretKey;
		private ElGamalMultiRecipientPublicKey ccrjChoiceReturnCodesEncryptionPublicKey;

		public Builder setVerificationCardId(final String verificationCardId) {
			this.verificationCardId = verificationCardId;
			return this;
		}

		public Builder setEncryptedVote(final ElGamalMultiRecipientCiphertext encryptedVote) {
			this.encryptedVote = encryptedVote;
			return this;
		}

		public Builder setExponentiatedEncryptedVote(final ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote) {
			this.exponentiatedEncryptedVote = exponentiatedEncryptedVote;
			return this;
		}

		public Builder setEncryptedPartialChoiceReturnCodes(final ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes) {
			this.encryptedPartialChoiceReturnCodes = encryptedPartialChoiceReturnCodes;
			return this;
		}

		public Builder setCcrjChoiceReturnCodesEncryptionSecretKey(final ElGamalMultiRecipientPrivateKey ccrjChoiceReturnCodesEncryptionSecretKey) {
			this.ccrjChoiceReturnCodesEncryptionSecretKey = ccrjChoiceReturnCodesEncryptionSecretKey;
			return this;
		}

		public Builder setCcrjChoiceReturnCodesEncryptionPublicKey(final ElGamalMultiRecipientPublicKey ccrjChoiceReturnCodesEncryptionPublicKey) {
			this.ccrjChoiceReturnCodesEncryptionPublicKey = ccrjChoiceReturnCodesEncryptionPublicKey;
			return this;
		}

		/**
		 * Creates the PartialDecryptPCCInput. All fields must have been set and be non-null.
		 *
		 * @return a new PartialDecryptPCCInput.
		 * @throws NullPointerException     if any of the fields is null.
		 * @throws IllegalArgumentException if
		 *                                  <ul>
		 *                                      <li>The encrypted vote and the exponentiated encrypted vote do not have the same size.</li>
		 *                                      <li>The secret key does not have phi elements.</li>
		 *                                      <li>The public key does not have phi elements.</li>
		 *                                      <li>Not all inputs have the same Gq group.</li>
		 *                                      <li>The secret key has a different group order.</li>
		 *                                      <li>The secret and public key do not match.</li>
		 *                                  </ul>
		 */
		public PartialDecryptPCCInput build() {
			validateUUID(verificationCardId);
			checkNotNull(encryptedVote);
			checkNotNull(exponentiatedEncryptedVote);
			checkNotNull(encryptedPartialChoiceReturnCodes);
			checkNotNull(ccrjChoiceReturnCodesEncryptionSecretKey);
			checkNotNull(ccrjChoiceReturnCodesEncryptionPublicKey);

			// Size checks.
			checkArgument(exponentiatedEncryptedVote.size() == 1, "The exponentiated encrypted vote must be of size 1.");
			checkArgument(ccrjChoiceReturnCodesEncryptionSecretKey.size() == ccrjChoiceReturnCodesEncryptionPublicKey.size(),
					"CCRj Choice Return Codes encryption secret key and public key must have the same size.");

			// Cross group checks.
			final List<GqGroup> gqGroups = Arrays.asList(encryptedVote.getGroup(), exponentiatedEncryptedVote.getGroup(),
					encryptedPartialChoiceReturnCodes.getGroup(), ccrjChoiceReturnCodesEncryptionPublicKey.getGroup());
			checkArgument(allEqual(gqGroups.stream(), Function.identity()), "All input encryption groups must be the same.");

			checkArgument(gqGroups.get(0).hasSameOrderAs(ccrjChoiceReturnCodesEncryptionSecretKey.getGroup()),
					"The secret key must have the same group order than the other inputs.");

			// Keypair validation.
			final ElGamalMultiRecipientKeyPair keyPair = ElGamalMultiRecipientKeyPair.from(ccrjChoiceReturnCodesEncryptionSecretKey,
					ccrjChoiceReturnCodesEncryptionPublicKey.getGroup().getGenerator());
			checkArgument(keyPair.getPublicKey().equals(ccrjChoiceReturnCodesEncryptionPublicKey), "The secret and public keys do not match.");

			return new PartialDecryptPCCInput(verificationCardId, encryptedVote, exponentiatedEncryptedVote,
					encryptedPartialChoiceReturnCodes, ccrjChoiceReturnCodesEncryptionSecretKey, ccrjChoiceReturnCodesEncryptionPublicKey);
		}
	}
}