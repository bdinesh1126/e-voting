/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.confirmvote;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;

/**
 * Regroups the inputs needed by the CreateLVCCShare<sub>j</sub> algorithm.
 *
 * <ul>
 * <li>CK<sub>id</sub>, the confirmation key. Not null.</li>
 * <li>k'<sub>j</sub>, the CCRj Return Codes Generation secret key. Not null.</li>
 * <li>vc<sub>id</sub>, the verification card id. Not null and a valid UUID.</li>
 * </ul>
 */
public record CreateLVCCShareInput(GqElement confirmationKey, ZqElement ccrjReturnCodesGenerationSecretKey, String verificationCardId) {

	public CreateLVCCShareInput {
		checkNotNull(confirmationKey);
		checkNotNull(ccrjReturnCodesGenerationSecretKey);
		validateUUID(verificationCardId);

		checkArgument(confirmationKey.getGroup().hasSameOrderAs(ccrjReturnCodesGenerationSecretKey.getGroup()),
				"The confirmation key must have the same order as the CCRj Return Codes Generation secret key.");
	}

}
