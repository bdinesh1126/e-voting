/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Regroups the context values needed by the GenEncLongCodeShares algorithm.
 *
 * <ul>
 *     <li>ee, the election event ID. Not null.</li>
 *     <li>vcs, the verification card set ID. Not null.</li>
 *     <li>(p, q, g), the encryption group. Not null.</li>
 *     <li>j, the CCR’s index. Not null.</li>
 *     <li>n, the number of voting options. Strictly positive.</li>
 * </ul>
 */
public class GenEncLongCodeSharesContext {

	private final String electionEventId;
	private final String verificationCardSetId;
	private final GqGroup encryptionGroup;
	private final int nodeId;
	private final int numberOfVotingOptions;

	private GenEncLongCodeSharesContext(final String electionEventId, final String verificationCardSetId, final GqGroup encryptionGroup,
			final int nodeId, final int numberOfVotingOptions) {
		this.electionEventId = electionEventId;
		this.verificationCardSetId = verificationCardSetId;
		this.encryptionGroup = encryptionGroup;
		this.nodeId = nodeId;
		this.numberOfVotingOptions = numberOfVotingOptions;
	}

	public int getNodeId() {
		return nodeId;
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public String getVerificationCardSetId() {
		return verificationCardSetId;
	}

	public GqGroup getEncryptionGroup() {
		return encryptionGroup;
	}

	public int getNumberOfVotingOptions() {
		return numberOfVotingOptions;
	}

	/**
	 * Builder performing input validations before constructing a {@link GenEncLongCodeSharesContext}.
	 */
	public static class Builder {

		private String electionEventId;
		private String verificationCardSetId;
		private GqGroup encryptionGroup;
		private int nodeId;
		private int numberOfVotingOptions;

		public Builder setElectionEventId(final String electionEventId) {
			this.electionEventId = electionEventId;
			return this;
		}

		public Builder setVerificationCardSetId(final String verificationCardSetId) {
			this.verificationCardSetId = verificationCardSetId;
			return this;
		}

		public Builder setEncryptionGroup(final GqGroup encryptionGroup) {
			this.encryptionGroup = encryptionGroup;
			return this;
		}

		public Builder setNodeId(final int nodeId) {
			this.nodeId = nodeId;
			return this;
		}

		public Builder setNumberOfVotingOptions(final int numberOfVotingOptions) {
			this.numberOfVotingOptions = numberOfVotingOptions;
			return this;
		}

		/**
		 * Creates the GenEncLongCodeSharesContext. All fields must have been set and be non-null.
		 *
		 * @return a new GenEncLongCodeSharesContext.
		 * @throws NullPointerException      if any of the fields is null.
		 * @throws FailedValidationException if any of the election event Id and verification card IDs do not comply with the required UUID format
		 * @throws IllegalArgumentException  if
		 *                                   <ul>
		 *                                       <li>The node id is not part of the know node ids.</li>
		 *                                       <li>The number of voting options is not strictly positive.</li>
		 *                                   </ul>
		 */
		public GenEncLongCodeSharesContext build() {
			validateUUID(electionEventId);
			validateUUID(verificationCardSetId);
			checkNotNull(encryptionGroup);

			checkArgument(NODE_IDS.contains(nodeId), "The node id must be part of the known node ids. [nodeId: %s]", nodeId);
			checkArgument(numberOfVotingOptions > 0, "The number of voting options must be strictly positive.");

			return new GenEncLongCodeSharesContext(electionEventId, verificationCardSetId, encryptionGroup, nodeId, numberOfVotingOptions);
		}
	}
}

