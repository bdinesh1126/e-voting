/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Regroups the context values needed by the DecryptPCC<sub>j</sub> and PartialDecryptPCC<sub>j</sub> algorithms.
 *
 * <ul>
 * <li>encryptionGroup, the {@code GqGroup} with modulus p, cardinality q and generator g. Not null.</li>
 * <li>j, the CCR's index.</li>
 * <li>j_hat, the other CCR's indeces. Not null and not empty.</li>
 * <li>ee, the election event id. Not null and a valid UUID.</li>
 * <li>vcs, the verification card set id. Not null and a valid UUID.</li>
 * <li>psi, the number of selectable voting options. In range [1, 120].</li>
 * <li>delta_hat, the number of allowed write-ins plus one. Strictly positive.</li>
 * </ul>
 */
public class DecryptPCCContext {

	private final GqGroup encryptionGroup;
	private final int nodeId;
	private final List<Integer> otherNodeIds;
	private final String electionEventId;
	private final String verificationCardSetId;
	private final int numberOfSelectableVotingOptions;
	private final int numberOfAllowedWriteInsPlusOne;

	private DecryptPCCContext(final GqGroup encryptionGroup, final int nodeId, final List<Integer> otherNodeIds, final String electionEventId,
			final String verificationCardSetId,
			final int numberOfSelectableVotingOptions, final int numberOfAllowedWriteInsPlusOne) {
		this.encryptionGroup = encryptionGroup;
		this.nodeId = nodeId;
		this.otherNodeIds = otherNodeIds;
		this.electionEventId = electionEventId;
		this.verificationCardSetId = verificationCardSetId;
		this.numberOfSelectableVotingOptions = numberOfSelectableVotingOptions;
		this.numberOfAllowedWriteInsPlusOne = numberOfAllowedWriteInsPlusOne;
	}

	GqGroup getEncryptionGroup() {
		return encryptionGroup;
	}

	int getNodeId() {
		return nodeId;
	}

	List<Integer> getOtherNodeIds() {
		return otherNodeIds;
	}

	String getElectionEventId() {
		return electionEventId;
	}

	String getVerificationCardSetId() {
		return verificationCardSetId;
	}

	int getNumberOfSelectableVotingOptions() {
		return numberOfSelectableVotingOptions;
	}

	int getNumberOfAllowedWriteInsPlusOne() {
		return numberOfAllowedWriteInsPlusOne;
	}

	/**
	 * Builder performing input validations and cross-validations before constructing a {@link DecryptPCCContext}.
	 */
	public static class Builder {

		private GqGroup encryptionGroup;
		private int nodeId;
		private String electionEventId;
		private String verificationCardSetId;
		private int numberOfSelectableVotingOptions;
		private int numberOfAllowedWriteInsPlusOne;

		public Builder setEncryptionGroup(final GqGroup encryptionGroup) {
			this.encryptionGroup = encryptionGroup;
			return this;
		}

		public Builder setNodeId(final int nodeId) {
			this.nodeId = nodeId;
			return this;
		}

		public Builder setElectionEventId(final String electionEventId) {
			this.electionEventId = electionEventId;
			return this;
		}

		public Builder setVerificationCardSetId(final String verificationCardSetId) {
			this.verificationCardSetId = verificationCardSetId;
			return this;
		}

		public Builder setNumberOfSelectableVotingOptions(final int numberOfSelectableVotingOptions) {
			this.numberOfSelectableVotingOptions = numberOfSelectableVotingOptions;
			return this;
		}

		public Builder setNumberOfAllowedWriteInsPlusOne(final int numberOfAllowedWriteInsPlusOne) {
			this.numberOfAllowedWriteInsPlusOne = numberOfAllowedWriteInsPlusOne;
			return this;
		}

		public DecryptPCCContext build() {
			checkNotNull(encryptionGroup);
			checkArgument(NODE_IDS.contains(nodeId), "The node id must be part of the known node ids. [nodeId: %s]", nodeId);
			validateUUID(electionEventId);
			validateUUID(verificationCardSetId);
			checkArgument(numberOfSelectableVotingOptions >= 1 && numberOfSelectableVotingOptions <= MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS,
					"The number of selectable voting options is out of range.");
			checkArgument(numberOfAllowedWriteInsPlusOne > 0, "The number of allowed write-ins + 1 must be strictly positive.");

			final List<Integer> otherNodeIds = NODE_IDS.stream().filter(j -> !j.equals(nodeId)).toList();
			checkArgument(otherNodeIds.size() == NODE_IDS.size() - 1,
					"The size of the other node ids must be equal to the number of known node ids - 1.");

			return new DecryptPCCContext(encryptionGroup, nodeId, otherNodeIds, electionEventId, verificationCardSetId,
					numberOfSelectableVotingOptions, numberOfAllowedWriteInsPlusOne);
		}
	}
}