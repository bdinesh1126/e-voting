/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.math.GroupVector.toGroupVector;
import static com.google.common.base.Preconditions.checkArgument;

import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.controlcomponent.BallotBoxEntity;
import ch.post.it.evoting.controlcomponent.BallotBoxService;
import ch.post.it.evoting.controlcomponent.CcrjReturnCodesKeys;
import ch.post.it.evoting.controlcomponent.CcrjReturnCodesKeysService;
import ch.post.it.evoting.controlcomponent.ElectionEventService;
import ch.post.it.evoting.controlcomponent.EncryptedVerifiableVoteService;
import ch.post.it.evoting.controlcomponent.IdentifierValidationService;
import ch.post.it.evoting.controlcomponent.PartialChoiceReturnCodeAllowList;
import ch.post.it.evoting.controlcomponent.SetupComponentPublicKeysService;
import ch.post.it.evoting.controlcomponent.VerificationCardSetEntity;
import ch.post.it.evoting.controlcomponent.VerificationCardSetService;
import ch.post.it.evoting.cryptoprimitives.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.LongChoiceReturnCodesShare;
import ch.post.it.evoting.domain.voting.sendvote.PartiallyDecryptedEncryptedPCC;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.common.EncryptedVerifiableVote;

/**
 * Decrypts the partially decrypted encrypted Choice Return Codes and creates the CCR_j long Choice Return Code shares.
 */
@Service
public class LCCShareService {

	private static final Logger LOGGER = LoggerFactory.getLogger(LCCShareService.class);

	private final DecryptPCCAlgorithm decryptPCCAlgorithm;
	private final CreateLCCShareAlgorithm createLCCShareAlgorithm;
	private final CcrjReturnCodesKeysService ccrjReturnCodesKeysService;
	private final EncryptedVerifiableVoteService encryptedVerifiableVoteService;
	private final SetupComponentPublicKeysService setupComponentPublicKeysService;
	private final ElectionEventService electionEventService;
	private final VerificationCardSetService verificationCardSetService;
	private final IdentifierValidationService identifierValidationService;
	private final BallotBoxService ballotBoxService;

	@Value("${nodeID}")
	private int nodeId;

	public LCCShareService(
			final DecryptPCCAlgorithm decryptPCCAlgorithm,
			final CreateLCCShareAlgorithm createLCCShareAlgorithm,
			final CcrjReturnCodesKeysService ccrjReturnCodesKeysService,
			final EncryptedVerifiableVoteService encryptedVerifiableVoteService,
			final ElectionEventService electionEventService,
			final VerificationCardSetService verificationCardSetService,
			final IdentifierValidationService identifierValidationService,
			final BallotBoxService ballotBoxService,
			final SetupComponentPublicKeysService setupComponentPublicKeysService) {
		this.decryptPCCAlgorithm = decryptPCCAlgorithm;
		this.createLCCShareAlgorithm = createLCCShareAlgorithm;
		this.ccrjReturnCodesKeysService = ccrjReturnCodesKeysService;
		this.encryptedVerifiableVoteService = encryptedVerifiableVoteService;
		this.setupComponentPublicKeysService = setupComponentPublicKeysService;
		this.electionEventService = electionEventService;
		this.verificationCardSetService = verificationCardSetService;
		this.identifierValidationService = identifierValidationService;
		this.ballotBoxService = ballotBoxService;
	}

	/**
	 * Decrypts the partially decrypted encrypted Choice Return Codes with the DecryptPCC_j algorithm and computes the CCR_j long Choice Return Codes
	 * share with the CreateLCCShare_j algorithm.
	 *
	 * @param controlComponentPartialDecryptPayloads the partially decrypted encrypted node contributions.
	 * @return the Long Choice Return Codes share.
	 */
	LongChoiceReturnCodesShare computeLCCShares(final List<ControlComponentPartialDecryptPayload> controlComponentPartialDecryptPayloads) {
		final ContextIds contextIds = controlComponentPartialDecryptPayloads.get(0).getPartiallyDecryptedEncryptedPCC().contextIds();

		identifierValidationService.validateContextIds(contextIds);
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);

		checkArgument(controlComponentPartialDecryptPayloads.get(0).getEncryptionGroup().equals(encryptionGroup),
				"The control component partial decrypt payloads do not have the expected encryption group.");

		LOGGER.debug("Starting decryption of the partially decrypted encrypted Choice Return Codes. [contextIds: {}]", contextIds);

		final VerificationCardSetEntity verificationCardSetEntity = verificationCardSetService.getVerificationCardSet(verificationCardSetId);
		final BallotBoxEntity ballotBox = ballotBoxService.getBallotBox(verificationCardSetEntity);
		final int numberOfSelectableVotingOptions = verificationCardSetEntity.getCombinedCorrectnessInformation().getTotalNumberOfSelections();
		final int numberOfAllowedWriteInsPlusOne = ballotBox.getNumberOfWriteInsPlusOne();

		// Decrypt.
		final DecryptPCCContext decryptPCCContext = new DecryptPCCContext.Builder()
				.setEncryptionGroup(encryptionGroup)
				.setNodeId(nodeId)
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setNumberOfSelectableVotingOptions(numberOfSelectableVotingOptions)
				.setNumberOfAllowedWriteInsPlusOne(numberOfAllowedWriteInsPlusOne)
				.build();

		final DecryptPCCInput decryptPCCInput = buildDecryptPCCInput(controlComponentPartialDecryptPayloads, contextIds);

		final GroupVector<GqElement, GqGroup> decryptedPartialChoiceReturnCodes = decryptPCCAlgorithm.decryptPCC(decryptPCCContext, decryptPCCInput);

		// Create LCC shares.
		final PartialChoiceReturnCodeAllowList allowList = verificationCardSetService.getPartialChoiceReturnCodesAllowList(verificationCardSetId);
		final CreateLCCShareContext createLCCShareContext = new CreateLCCShareContext(encryptionGroup, nodeId, electionEventId, verificationCardSetId,
				numberOfSelectableVotingOptions, allowList);

		final CcrjReturnCodesKeys ccrjReturnCodesKeys = ccrjReturnCodesKeysService.getCcrjReturnCodesKeys(electionEventId);
		final ZqElement ccrjReturnCodesGenerationSecretKey = ccrjReturnCodesKeys.ccrjReturnCodesGenerationSecretKey();
		final CreateLCCShareInput createLCCShareInput = new CreateLCCShareInput(decryptedPartialChoiceReturnCodes, ccrjReturnCodesGenerationSecretKey,
				verificationCardId);

		final CreateLCCShareOutput createLCCShareOutput = createLCCShareAlgorithm.createLCCShare(createLCCShareContext, createLCCShareInput);

		return new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, nodeId,
				createLCCShareOutput.getLongChoiceReturnCodeShare(), createLCCShareOutput.getExponentiationProof());
	}

	private DecryptPCCInput buildDecryptPCCInput(final List<ControlComponentPartialDecryptPayload> controlComponentPartialDecryptPayloads,
			final ContextIds contextIds) {
		final String verificationCardId = contextIds.verificationCardId();
		final String electionEventId = contextIds.electionEventId();

		final PartiallyDecryptedEncryptedPCC partiallyDecryptedEncryptedPCC = controlComponentPartialDecryptPayloads.stream()
				.map(ControlComponentPartialDecryptPayload::getPartiallyDecryptedEncryptedPCC)
				.filter(pcc -> pcc.nodeId() == nodeId)
				.findAny() // Uniqueness ensured by the combined payload.
				.orElseThrow(() -> new IllegalStateException(
						String.format("Missing node contribution. [contextIds: %s, nodeId: %d]", contextIds, nodeId)));
		final GroupVector<GqElement, GqGroup> exponentiatedGammas = partiallyDecryptedEncryptedPCC.exponentiatedGammas();
		final List<PartiallyDecryptedEncryptedPCC> otherPartiallyDecryptedEncryptedPCC = controlComponentPartialDecryptPayloads.stream()
				.map(ControlComponentPartialDecryptPayload::getPartiallyDecryptedEncryptedPCC)
				.filter(pcc -> pcc.nodeId() != nodeId)
				.sorted(Comparator.comparingInt(PartiallyDecryptedEncryptedPCC::nodeId))
				.toList();
		final GroupVector<GroupVector<GqElement, GqGroup>, GqGroup> otherCcrExponentiatedGammas = otherPartiallyDecryptedEncryptedPCC.stream()
				.map(PartiallyDecryptedEncryptedPCC::exponentiatedGammas)
				.collect(toGroupVector());
		final GroupVector<GroupVector<ExponentiationProof, ZqGroup>, ZqGroup> otherCcrExponentiationProofs = otherPartiallyDecryptedEncryptedPCC.stream()
				.map(PartiallyDecryptedEncryptedPCC::exponentiationProofs)
				.collect(toGroupVector());

		final EncryptedVerifiableVote encryptedVerifiableVote = encryptedVerifiableVoteService.getEncryptedVerifiableVote(verificationCardId);

		final List<ControlComponentPublicKeys> combinedControlComponentPublicKeys = setupComponentPublicKeysService.getCombinedControlComponentPublicKeys(
				electionEventId);
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> otherCcrjEncryptionPublicKeys = combinedControlComponentPublicKeys.stream()
				.filter(ccpk -> ccpk.nodeId() != nodeId)
				.map(ControlComponentPublicKeys::ccrjChoiceReturnCodesEncryptionPublicKey)
				.collect(GroupVector.toGroupVector());

		return new DecryptPCCInput.Builder()
				.setVerificationCardId(verificationCardId)
				.setExponentiatedGammaElements(exponentiatedGammas)
				.setOtherCcrExponentiatedGammaElements(otherCcrExponentiatedGammas)
				.setOtherCcrExponentiationProofs(otherCcrExponentiationProofs)
				.setOtherCcrChoiceReturnCodesEncryptionKeys(otherCcrjEncryptionPublicKeys)
				.setEncryptedVote(encryptedVerifiableVote.encryptedVote())
				.setExponentiatedEncryptedVote(encryptedVerifiableVote.exponentiatedEncryptedVote())
				.setEncryptedPartialChoiceReturnCodes(encryptedVerifiableVote.encryptedPartialChoiceReturnCodes())
				.build();
	}
}
