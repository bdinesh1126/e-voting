/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface ElectionEventRepository extends CrudRepository<ElectionEventEntity, Long> {

	Optional<ElectionEventEntity> findByElectionEventId(final String electionEventId);

	boolean existsByElectionEventId(final String electionEventId);
}
