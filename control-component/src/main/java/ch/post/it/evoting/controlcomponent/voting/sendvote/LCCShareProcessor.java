/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.sendvote;

import static ch.post.it.evoting.controlcomponent.ControlComponentsApplicationBootstrap.RABBITMQ_EXCHANGE;
import static ch.post.it.evoting.domain.SharedQueue.CREATE_LCC_SHARE_REQUEST_PATTERN;
import static ch.post.it.evoting.domain.SharedQueue.CREATE_LCC_SHARE_RESPONSE_PATTERN;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.SignatureException;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Command;
import ch.post.it.evoting.commandmessaging.CommandId;
import ch.post.it.evoting.commandmessaging.CommandService;
import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.controlcomponent.ExactlyOnceCommand;
import ch.post.it.evoting.controlcomponent.ExactlyOnceCommandExecutor;
import ch.post.it.evoting.controlcomponent.Messages;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.voting.sendvote.CombinedControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentLCCSharePayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.LongChoiceReturnCodesShare;
import ch.post.it.evoting.domain.voting.sendvote.PartiallyDecryptedEncryptedPCC;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;

/**
 * Consumes the messages asking for the Long Choice Return Codes Share.
 */
@Service
public class LCCShareProcessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(LCCShareProcessor.class);

	private final CommandService commandService;
	private final RabbitTemplate rabbitTemplate;
	private final LCCShareService lccShareService;
	private final ObjectMapper objectMapper;
	private final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private String responseQueue;

	@Value("${nodeID}")
	private int nodeId;

	public LCCShareProcessor(
			final ObjectMapper objectMapper,
			final RabbitTemplate rabbitTemplate,
			final LCCShareService lccShareService,
			final CommandService commandService,
			final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor,
			final SignatureKeystore<Alias> signatureKeystoreService) {
		this.objectMapper = objectMapper;
		this.rabbitTemplate = rabbitTemplate;
		this.lccShareService = lccShareService;
		this.commandService = commandService;
		this.exactlyOnceCommandExecutor = exactlyOnceCommandExecutor;
		this.signatureKeystoreService = signatureKeystoreService;
	}

	@PostConstruct
	public void initQueue() {
		responseQueue = String.format("%s%s", CREATE_LCC_SHARE_RESPONSE_PATTERN, nodeId);
	}

	@RabbitListener(queues = CREATE_LCC_SHARE_REQUEST_PATTERN + "${nodeID}", autoStartup = "false", concurrency = "4")
	public void onMessage(final Message message) throws IOException {

		final String correlationId = message.getMessageProperties().getCorrelationId();
		checkNotNull(correlationId, "Correlation Id must not be null.");

		// Deserialize message.
		final byte[] messageBytes = message.getBody();
		final CombinedControlComponentPartialDecryptPayload combinedControlComponentPartialDecryptPayload = objectMapper.readValue(messageBytes,
				CombinedControlComponentPartialDecryptPayload.class);

		// Verify payload signature and consistency.
		verifyPayload(combinedControlComponentPartialDecryptPayload);

		// Construct request command.
		final List<ControlComponentPartialDecryptPayload> controlComponentPartialDecryptPayloads = combinedControlComponentPartialDecryptPayload.controlComponentPartialDecryptPayloads();
		final ContextIds contextIds = controlComponentPartialDecryptPayloads.get(0).getPartiallyDecryptedEncryptedPCC().contextIds();
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		final String contextId = String.join("-", electionEventId, verificationCardSetId, verificationCardId);
		LOGGER.info("Received LCC share request. [contextId: {}, correlationId: {}]", contextId, correlationId);

		final ExactlyOnceCommand createLCCShareInput = new ExactlyOnceCommand.Builder()
				.setCorrelationId(correlationId)
				.setContextId(contextId)
				.setContext(Context.VOTING_RETURN_CODES_CREATE_LCC_SHARE.toString())
				.setTask(() -> generateControlComponentLCCSharePayload(combinedControlComponentPartialDecryptPayload))
				.setRequestContent(messageBytes)
				.build();
		final byte[] payloadBytes = exactlyOnceCommandExecutor.process(createLCCShareInput);
		LOGGER.debug("LCC share task successfully processed. [contextId: {}]", electionEventId);

		final Message responseMessage = Messages.createMessage(correlationId, payloadBytes);

		rabbitTemplate.send(RABBITMQ_EXCHANGE, responseQueue, responseMessage);
		LOGGER.info("LCC share response sent. [contextId: {}]", contextId);
	}

	private void verifyPayload(final CombinedControlComponentPartialDecryptPayload combinedControlComponentPartialDecryptPayload)
			throws IOException {

		final ContextIds contextIds = combinedControlComponentPartialDecryptPayload.controlComponentPartialDecryptPayloads().get(0)
				.getPartiallyDecryptedEncryptedPCC().contextIds();
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		// Verify signature of the received ControlComponentPartialDecryptPayloads.
		combinedControlComponentPartialDecryptPayload.controlComponentPartialDecryptPayloads().forEach(this::verifySignature);

		// Retrieve the partially decrypted encrypted PCC previously computed.
		final String contextId = String.join("-", Arrays.asList(electionEventId, verificationCardSetId, verificationCardId));
		final CommandId commandId = CommandId.builder()
				.contextId(contextId)
				.context(Context.VOTING_RETURN_CODES_PARTIAL_DECRYPT_PCC.toString())
				.nodeId(nodeId)
				.build();
		final List<Command> command = commandService.findSemanticallyIdenticalCommand(commandId);
		checkState(command.size() == 1,
				"There was a problem with exactly once processing, multiple semantically identical partial decrypt PCC were saved. "
						+ "[contextId: {}, nodeId: {}]", contextId, nodeId);
		final PartiallyDecryptedEncryptedPCC previouslyComputedPCCPayload = objectMapper.readValue(command.get(0).getResponsePayload(),
						ControlComponentPartialDecryptPayload.class)
				.getPartiallyDecryptedEncryptedPCC();

		// Get the partially decrypted encrypted PCC corresponding to this node id.
		final ControlComponentPartialDecryptPayload receivedPCCPayload = combinedControlComponentPartialDecryptPayload.controlComponentPartialDecryptPayloads()
				.stream().filter(payload -> payload.getPartiallyDecryptedEncryptedPCC().nodeId() == nodeId)
				.findAny() // Uniqueness ensured by the combined payload.
				.orElseThrow(() -> new IllegalStateException("The combined payload does not contain payload for this node id."));

		// Check that they are equal.
		if (!previouslyComputedPCCPayload.equals(receivedPCCPayload.getPartiallyDecryptedEncryptedPCC())) {
			throw new IllegalStateException("The received partially decrypted encrypted PCC is not equal to the previously computed one.");
		}
	}

	private void verifySignature(final ControlComponentPartialDecryptPayload payload) {
		final PartiallyDecryptedEncryptedPCC partiallyDecryptedEncryptedPCC = payload.getPartiallyDecryptedEncryptedPCC();
		final ContextIds contextIds = partiallyDecryptedEncryptedPCC.contextIds();
		final int otherNodeId = partiallyDecryptedEncryptedPCC.nodeId();
		final CryptoPrimitivesSignature signature = payload.getSignature();

		checkState(signature != null, "The signature of the control component partial decrypt payload is null. [contextIds: %s]", contextIds);

		final Hashable additionalContextData = ChannelSecurityContextData.controlComponentPartialDecrypt(otherNodeId,
				contextIds.electionEventId(), contextIds.verificationCardSetId(), contextIds.verificationCardId());

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.getControlComponentByNodeId(otherNodeId), payload,
					additionalContextData, signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not verify the signature of the control component partial decrypt payload. [contextIds: %s]", contextIds));
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(ControlComponentPartialDecryptPayload.class, String.format("[contextIds: %s]", contextIds));
		}
	}

	private byte[] generateControlComponentLCCSharePayload(
			final CombinedControlComponentPartialDecryptPayload combinedControlComponentPartialDecryptPayload) {
		final List<ControlComponentPartialDecryptPayload> controlComponentPartialDecryptPayloads = combinedControlComponentPartialDecryptPayload.controlComponentPartialDecryptPayloads();
		final ContextIds contextIds = controlComponentPartialDecryptPayloads.get(0).getPartiallyDecryptedEncryptedPCC().contextIds();
		final GqGroup gqGroup = controlComponentPartialDecryptPayloads.get(0).getEncryptionGroup();

		// Perform LCC share computation.
		final LongChoiceReturnCodesShare longReturnCodesShare = lccShareService.computeLCCShares(controlComponentPartialDecryptPayloads);
		LOGGER.info("Successfully generated the Long Choice Return Codes Share. [contextIds: {}]", contextIds);

		// Create and sign response payload.
		final ControlComponentLCCSharePayload controlComponentLCCSharePayload =
				new ControlComponentLCCSharePayload(gqGroup, longReturnCodesShare);

		try {
			controlComponentLCCSharePayload.setSignature(getPayloadSignature(controlComponentLCCSharePayload));
			LOGGER.info("Successfully signed Long Return Codes Share payload. [contextIds: {}]", contextIds);

			return objectMapper.writeValueAsBytes(controlComponentLCCSharePayload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(String.format("Could not serialize long return codes share payload. [contextIds: %s]", contextIds), e);
		}
	}

	private CryptoPrimitivesSignature getPayloadSignature(final ControlComponentLCCSharePayload payload) {
		final LongChoiceReturnCodesShare longChoiceReturnCodesShare = payload.getLongChoiceReturnCodesShare();
		final String electionEventId = longChoiceReturnCodesShare.electionEventId();
		final String verificationCardSetId = longChoiceReturnCodesShare.verificationCardSetId();
		final String verificationCardId = longChoiceReturnCodesShare.verificationCardId();

		final Hashable additionalContextData = ChannelSecurityContextData.controlComponentLCCShare(nodeId, electionEventId, verificationCardSetId,
				verificationCardId);

		try {
			final byte[] signature = signatureKeystoreService.generateSignature(payload, additionalContextData);
			return new CryptoPrimitivesSignature(signature);
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not generate payload signature. [electionEventId: %s, verificationCardSetId: %s, verificationCardId: %s]",
							electionEventId,
							verificationCardSetId, verificationCardId));
		}
	}
}
