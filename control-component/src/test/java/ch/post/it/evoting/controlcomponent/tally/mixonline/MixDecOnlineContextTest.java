/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.tally.mixonline;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.SecureRandom;
import java.util.Locale;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;

@DisplayName("Constructing a MixDecryptContext with ")
class MixDecOnlineContextTest {

	private static final SecureRandom RANDOM = new SecureRandom();
	private static final Random RANDOM_SERVICE = RandomFactory.createRandom();
	private static final int UUID_LENGTH = 32;

	private MixDecOnlineContext.Builder mixDecOnlineContextBuilder;

	@BeforeEach
	void setup() {
		final String electionEventId = RANDOM_SERVICE.genRandomBase16String(UUID_LENGTH).toLowerCase(Locale.ENGLISH);
		final String ballotBoxId = RANDOM_SERVICE.genRandomBase16String(UUID_LENGTH).toLowerCase(Locale.ENGLISH);
		final int numberAllowedWriteInsPlusOne = RANDOM.nextInt(10) + 1;
		mixDecOnlineContextBuilder = new MixDecOnlineContext.Builder()
				.setEncryptionGroup(GroupTestData.getGqGroup())
				.setElectionEventId(electionEventId)
				.setBallotBoxId(ballotBoxId)
				.setNumberOfAllowedWriteInsPlusOne(numberAllowedWriteInsPlusOne);
	}

	@Test
	@DisplayName("null arguments throws a NullPointerException")
	void constructWithNullArgumentsThrows() {
		final MixDecOnlineContext.Builder nullEncryptionGroup = mixDecOnlineContextBuilder.setEncryptionGroup(null);
		assertThrows(NullPointerException.class, nullEncryptionGroup::build);

		final MixDecOnlineContext.Builder nullElectionEventId = mixDecOnlineContextBuilder.setElectionEventId(null);
		assertThrows(NullPointerException.class, nullElectionEventId::build);

		final MixDecOnlineContext.Builder nullBallotBoxId = mixDecOnlineContextBuilder.setBallotBoxId(null);
		assertThrows(NullPointerException.class, nullBallotBoxId::build);
	}

	@Test
	@DisplayName("invalid identifiers throws a FailedValidationException")
	void constructWithInvalidIdentifiersThrows() {
		final String badId = "badElectionEventId";
		final MixDecOnlineContext.Builder badElectionEventId = mixDecOnlineContextBuilder.setElectionEventId(badId);
		assertThrows(FailedValidationException.class, badElectionEventId::build);

		final MixDecOnlineContext.Builder badBallotBoxId = mixDecOnlineContextBuilder.setBallotBoxId(badId);
		assertThrows(FailedValidationException.class, badBallotBoxId::build);
	}

	@Test
	@DisplayName("the number of allowed write-ins plus one too small throws an IllegalArgumentException")
	void constructWithTooSmallNumberOfAllowedWriteInsPlusOne() {
		final MixDecOnlineContext.Builder tooSmallNumberOfWriteInsPlusOne = mixDecOnlineContextBuilder.setNumberOfAllowedWriteInsPlusOne(0);
		assertThrows(IllegalArgumentException.class, tooSmallNumberOfWriteInsPlusOne::build);
	}

	@Test
	@DisplayName("valid input is successful")
	void constructWithValidInputDoesNotThrow() {
		assertDoesNotThrow(() -> mixDecOnlineContextBuilder.build());
	}
}