/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionEventContext;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;

@DisplayName("ElectionContextServiceTest")
class ElectionContextServiceTest {
	private static final ElectionContextRepository electionContextRepository = mock(ElectionContextRepository.class);
	private static final ElectionEventService electionEventService = mock(ElectionEventService.class);

	private static ElectionContextService electionContextService;
	private static ElectionEventContext electionEventContext;

	@BeforeAll
	static void setUpAll() {
		String electionEventId = RandomFactory.createRandom().genRandomBase16String(32);

		electionEventContext = mock(ElectionEventContext.class);
		when(electionEventContext.electionEventId()).thenReturn(electionEventId);
		when(electionEventContext.startTime()).thenReturn(LocalDateTime.now());
		when(electionEventContext.finishTime()).thenReturn(LocalDateTime.now().plusDays(2));

		final GqGroup encryptionParameters = GroupTestData.getGqGroup();
		final ElectionEventEntity electionEventEntity = new ElectionEventEntity(electionEventId, encryptionParameters);

		final ElectionContextEntity electionContextEntity = new ElectionContextEntity.Builder()
				.setElectionEventEntity(electionEventEntity)
				.setStartTime(electionEventContext.startTime())
				.setFinishTime(electionEventContext.finishTime()).build();

		final BallotBoxService ballotBoxService = mock(BallotBoxService.class);
		when(ballotBoxService.saveFromContexts(any())).thenReturn(Collections.emptyList());

		when(electionContextRepository.save(any())).thenReturn(new ElectionContextEntity());
		when(electionContextRepository.findByElectionEventId(electionEventContext.electionEventId()))
				.thenReturn(Optional.ofNullable(electionContextEntity));
		when(electionEventService.getElectionEventEntity(electionEventId)).thenReturn(electionEventEntity);
		when(electionEventService.getEncryptionGroup(any())).thenReturn(encryptionParameters);

		electionContextService = new ElectionContextService(ballotBoxService, electionEventService, electionContextRepository);
	}

	@Test
	@DisplayName("saving with valid ElectionEventContext does not throw")
	void savingValidElectionEventContextDoesNotThrow() {
		electionContextService.save(electionEventContext);
		verify(electionContextRepository, times(1)).save(any());
	}

	@Test
	@DisplayName("loading non existing ElectionEventContext throws IllegalStateException")
	void loadNonExistingThrows() {
		final String nonExistingElectionId = "e77dbe3c70874ea584c490a0c6ac0ca4";
		final IllegalStateException exceptionCcm = assertThrows(IllegalStateException.class,
				() -> electionContextService.getElectionContextEntity(nonExistingElectionId));
		assertEquals(String.format("Election context entity not found. [electionEventId: %s]", nonExistingElectionId),
				Throwables.getRootCause(exceptionCcm).getMessage());
	}
}
