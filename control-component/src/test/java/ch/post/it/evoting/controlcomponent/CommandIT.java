/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.transaction.TransactionDefinition.ISOLATION_SERIALIZABLE;
import static org.springframework.transaction.TransactionDefinition.PROPAGATION_REQUIRES_NEW;

import java.security.SecureRandom;
import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import com.google.common.base.Throwables;

import ch.post.it.evoting.commandmessaging.CommandId;
import ch.post.it.evoting.commandmessaging.CommandService;

@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration(initializers = TestKeyStoreInitializer.class)
class CommandIT {

	private static final SecureRandom secureRandom = new SecureRandom();

	@Autowired
	private CommandService commandService;

	@Autowired
	private PlatformTransactionManager transactionManager;

	@Test
	void saveConcurrentlySameContextId() {
		final CommandId commandId1 = CommandId.builder()
				.contextId("context-id-concurrent-1")
				.context("context-concurrent-1")
				.correlationId("correlation-id-1")
				.nodeId(1)
				.build();
		final CommandId commandId2 = CommandId.builder()
				.contextId("context-id-concurrent-1")
				.context("context-concurrent-1")
				.correlationId("correlation-id-2")
				.nodeId(1)
				.build();

		final byte[] requestPayload = new byte[10];
		secureRandom.nextBytes(requestPayload);
		final LocalDateTime requestDateTime = LocalDateTime.now();
		final byte[] responsePayload = new byte[10];
		secureRandom.nextBytes(responsePayload);
		final LocalDateTime responseDateTime = LocalDateTime.now();

		final TransactionTemplate outerTransactionTemplate = new TransactionTemplate(transactionManager);
		outerTransactionTemplate.setIsolationLevel(ISOLATION_SERIALIZABLE);

		final TransactionTemplate innerTransactionTemplate = new TransactionTemplate(transactionManager);
		innerTransactionTemplate.setPropagationBehavior(PROPAGATION_REQUIRES_NEW);
		innerTransactionTemplate.setIsolationLevel(ISOLATION_SERIALIZABLE);

		final DataIntegrityViolationException exception = assertThrows(DataIntegrityViolationException.class,
				() -> outerTransactionTemplate.executeWithoutResult(outerStatus -> {
					commandService.save(commandId1, requestPayload, requestDateTime, responsePayload, responseDateTime);

					innerTransactionTemplate.executeWithoutResult(innerStatus ->
							commandService.save(commandId2, requestPayload, requestDateTime, responsePayload, responseDateTime));
				}));

		assertTrue(Throwables.getRootCause(exception).getMessage().contains("Unique index or primary key violation"));
	}

}
