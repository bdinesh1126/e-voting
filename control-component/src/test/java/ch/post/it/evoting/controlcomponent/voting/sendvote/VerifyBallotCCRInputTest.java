/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Locale;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.PlaintextEqualityProof;

@DisplayName("VerifyBallotCCRInput with")
class VerifyBallotCCRInputTest extends TestGroupSetup {

	private static final int PSI = 5;
	private static final int PHI = MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
	private static final int DELTA = 1;
	private static final int l_ID = 32;

	private final Random random = RandomFactory.createRandom();
	private final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);

	private String verificationCardId;
	private ElGamalMultiRecipientCiphertext encryptedVote;
	private ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote;
	private ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes;
	private GqElement verificationCardPublicKey;
	private ElGamalMultiRecipientPublicKey electionPublicKey;
	private ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey;
	private ExponentiationProof exponentiationProof;
	private PlaintextEqualityProof plaintextEqualityProof;

	@BeforeEach
	void setUp() {
		verificationCardId = random.genRandomBase16String(l_ID).toLowerCase(Locale.ENGLISH);
		encryptedVote = elGamalGenerator.genRandomCiphertext(1);
		final ZqElement k_id = zqGroupGenerator.genRandomZqElementMember();
		exponentiatedEncryptedVote = encryptedVote.getCiphertextExponentiation(k_id);
		encryptedPartialChoiceReturnCodes = elGamalGenerator.genRandomCiphertext(PSI);
		verificationCardPublicKey = gqGroupGenerator.genMember();
		electionPublicKey = new ElGamalMultiRecipientPublicKey(gqGroupGenerator.genRandomGqElementVector(DELTA));
		choiceReturnCodesEncryptionPublicKey = new ElGamalMultiRecipientPublicKey(gqGroupGenerator.genRandomGqElementVector(PHI));
		exponentiationProof = new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(), zqGroupGenerator.genRandomZqElementMember());
		plaintextEqualityProof = new PlaintextEqualityProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementVector(2));
	}

	@Test
	@DisplayName("valid parameters does not throw")
	void validParams() {
		final VerifyBallotCCRInput.Builder builder = new VerifyBallotCCRInput.Builder()
				.setVerificationCardId(verificationCardId)
				.setEncryptedVote(encryptedVote)
				.setExponentiatedEncryptedVote(exponentiatedEncryptedVote)
				.setEncryptedPartialChoiceReturnCodes(encryptedPartialChoiceReturnCodes)
				.setVerificationCardPublicKey(verificationCardPublicKey)
				.setElectionPublicKey(electionPublicKey)
				.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
				.setExponentiationProof(exponentiationProof)
				.setPlaintextEqualityProof(plaintextEqualityProof);

		assertDoesNotThrow(builder::build);
	}

	@Test
	@DisplayName("wrong size encrypted vote throws IllegalArgumentException")
	void wrongSizeEncryptedVote() {
		final ElGamalMultiRecipientCiphertext wrongEncryptedVote = elGamalGenerator.genRandomCiphertext(2);
		final VerifyBallotCCRInput.Builder builder = new VerifyBallotCCRInput.Builder()
				.setVerificationCardId(verificationCardId)
				.setEncryptedVote(wrongEncryptedVote)
				.setExponentiatedEncryptedVote(exponentiatedEncryptedVote)
				.setEncryptedPartialChoiceReturnCodes(encryptedPartialChoiceReturnCodes)
				.setVerificationCardPublicKey(verificationCardPublicKey)
				.setElectionPublicKey(electionPublicKey)
				.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
				.setExponentiationProof(exponentiationProof)
				.setPlaintextEqualityProof(plaintextEqualityProof);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);
		assertEquals(String.format("numberOfAllowedWriteInsPlusOne must be smaller than or equal to delta. [numberOfAllowedWriteInsPlusOne: %s, delta: %s]", wrongEncryptedVote.size(),
						electionPublicKey.size()),
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("different group order exponentiation proof throws IllegalArgumentException")
	void differentOrderExponentiationProof() {
		final ExponentiationProof otherExponentiationProof = new ExponentiationProof(otherZqGroupGenerator.genRandomZqElementMember(),
				otherZqGroupGenerator.genRandomZqElementMember());
		final VerifyBallotCCRInput.Builder shortBuilder = new VerifyBallotCCRInput.Builder()
				.setVerificationCardId(verificationCardId)
				.setEncryptedVote(encryptedVote)
				.setExponentiatedEncryptedVote(exponentiatedEncryptedVote)
				.setEncryptedPartialChoiceReturnCodes(encryptedPartialChoiceReturnCodes)
				.setVerificationCardPublicKey(verificationCardPublicKey)
				.setElectionPublicKey(electionPublicKey)
				.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
				.setExponentiationProof(otherExponentiationProof)
				.setPlaintextEqualityProof(plaintextEqualityProof);

		final IllegalArgumentException shortException = assertThrows(IllegalArgumentException.class, shortBuilder::build);
		assertEquals("The exponentiation proof must have the same group order than the other inputs.",
				Throwables.getRootCause(shortException).getMessage());
	}

	@Test
	@DisplayName("different group order plaintext equality proof throws IllegalArgumentException")
	void differentOrderPlaintextEqualityProof() {
		final PlaintextEqualityProof otherPlaintextEqualityProof = new PlaintextEqualityProof(otherZqGroupGenerator.genRandomZqElementMember(),
				otherZqGroupGenerator.genRandomZqElementVector(2));
		final VerifyBallotCCRInput.Builder shortBuilder = new VerifyBallotCCRInput.Builder()
				.setVerificationCardId(verificationCardId)
				.setEncryptedVote(encryptedVote)
				.setExponentiatedEncryptedVote(exponentiatedEncryptedVote)
				.setEncryptedPartialChoiceReturnCodes(encryptedPartialChoiceReturnCodes)
				.setVerificationCardPublicKey(verificationCardPublicKey)
				.setElectionPublicKey(electionPublicKey)
				.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
				.setExponentiationProof(exponentiationProof)
				.setPlaintextEqualityProof(otherPlaintextEqualityProof);

		final IllegalArgumentException shortException = assertThrows(IllegalArgumentException.class, shortBuilder::build);
		assertEquals("The plaintext equality proof must have the same group order than the other inputs.",
				Throwables.getRootCause(shortException).getMessage());
	}

}