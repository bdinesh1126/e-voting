/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.util.Locale;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;

@SpringBootTest
@ContextConfiguration(initializers = TestKeyStoreInitializer.class)
@ActiveProfiles("test")
@DisplayName("CcrjReturnCodesKeysService")
class CcrjReturnCodesKeysServiceTest {

	private static final Random random = RandomFactory.createRandom();

	private static String electionEventId;
	private static GqGroup encryptionGroup;

	@Autowired
	private CcrjReturnCodesKeysService ccrjReturnCodesKeysService;

	@BeforeAll
	static void setUpAll(
			@Autowired
			final ElectionEventService electionEventService) {

		electionEventId = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
		encryptionGroup = GroupTestData.getGqGroup();
		electionEventService.save(electionEventId, encryptionGroup);
	}

	@Test
	@DisplayName("return codes keys saves to database")
	void save() {
		final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(encryptionGroup));
		final ZqElement ccrjReturnCodesGenerationSecretKey = zqGroupGenerator.genRandomZqElementMember();
		final ElGamalMultiRecipientKeyPair ccrjChoiceReturnCodesEncryptionKeyPair = ElGamalMultiRecipientKeyPair.genKeyPair(encryptionGroup, 1,
				RandomFactory.createRandom());
		final CcrjReturnCodesKeys ccrjReturnCodesKeys = new CcrjReturnCodesKeys(electionEventId, ccrjReturnCodesGenerationSecretKey,
				ccrjChoiceReturnCodesEncryptionKeyPair);

		assertDoesNotThrow(() -> ccrjReturnCodesKeysService.save(ccrjReturnCodesKeys));
	}
}