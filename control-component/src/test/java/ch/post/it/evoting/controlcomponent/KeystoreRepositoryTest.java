/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;

class KeystoreRepositoryTest {

	private static final String KEYSTORE_CONTENT = "keystore-content";
	private static final String KEYSTORE_PASSWORD_CONTENT = "keystore-password-content";
	private static final int NODE_ID = 1;
	@TempDir
	static Path tempKeystorePath;

	static KeystoreRepository keystoreRepository;

	@BeforeAll
	static void setUp() {

		final String keystoreLocation = tempKeystorePath.resolve("signing_keystore_test.p12").toString();
		final String keystorePasswordLocation = tempKeystorePath.resolve("signing_pw_test.txt").toString();

		try {
			Files.write(Paths.get(keystoreLocation), KEYSTORE_CONTENT.getBytes(StandardCharsets.UTF_8));
			Files.write(Paths.get(keystorePasswordLocation), KEYSTORE_PASSWORD_CONTENT.getBytes(StandardCharsets.UTF_8));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		keystoreRepository = new KeystoreRepository(keystoreLocation, keystorePasswordLocation, NODE_ID);
	}

	@Test
	void testGetKeyStore() throws IOException {
		// given

		// when
		String keyStoreContent = IOUtils.toString(keystoreRepository.getKeyStore(), StandardCharsets.UTF_8);

		// then
		assertThat(keyStoreContent).isEqualTo(KEYSTORE_CONTENT);
	}

	@Test
	void testGetKeystorePassword() throws IOException {
		// given

		// when
		char[] passwordContent = keystoreRepository.getKeystorePassword();

		// then
		assertThat(passwordContent).isEqualTo(KEYSTORE_PASSWORD_CONTENT.toCharArray());
	}

	@Test
	void testGetKeystoreAlias() {
		// given

		// when
		Alias alias = keystoreRepository.getKeystoreAlias();

		// then
		assertThat(alias).isEqualTo(Alias.CONTROL_COMPONENT_1);
	}
}
