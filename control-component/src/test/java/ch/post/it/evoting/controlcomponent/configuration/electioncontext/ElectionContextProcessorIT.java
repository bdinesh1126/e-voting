/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.configuration.electioncontext;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SignatureException;
import java.util.Collections;
import java.util.Objects;
import java.util.UUID;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.ElectionEventEntity;
import ch.post.it.evoting.controlcomponent.ElectionEventService;
import ch.post.it.evoting.controlcomponent.MessageBrokerIntegrationTestBase;
import ch.post.it.evoting.controlcomponent.TestDatabaseCleanUpService;
import ch.post.it.evoting.controlcomponent.VerificationCard;
import ch.post.it.evoting.controlcomponent.VerificationCardService;
import ch.post.it.evoting.controlcomponent.VerificationCardSetEntity;
import ch.post.it.evoting.controlcomponent.VerificationCardSetService;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;

@DisplayName("ElectionContextProcessor consuming")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class ElectionContextProcessorIT extends MessageBrokerIntegrationTestBase {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static byte[] modifiedElectionContextPayloadBytes;
	private static byte[] electionContextPayloadBytes;
	private static byte[] invalidElectionContextPayloadBytes;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	private ElectionEventService electionEventService;

	@Autowired
	private VerificationCardSetService verificationCardSetService;

	@Autowired
	private VerificationCardService verificationCardService;

	@Autowired
	private TestDatabaseCleanUpService testDatabaseCleanUpService;

	@MockBean
	private SignatureKeystore<Alias> signatureKeystoreService;

	@BeforeEach
	void setUp() throws IOException, URISyntaxException {

		final String electionEventId = "e3e3c2fd8a16489291c5c24e7b74b26e";
		final String verificationCardSetId = "e77dbe3c70874ea584c490a0c6ac0ca4";
		saveInDatabase(electionEventId, verificationCardSetId, electionEventService, verificationCardSetService, verificationCardService);

		final String otherElectionEventId = "1821675c52bc996ffb4e80fa148928ff";
		final String otherVerificationCardSetId = "0e7b8bf2e03e07696c8d1a741866e7c8";
		saveInDatabase(otherElectionEventId, otherVerificationCardSetId, electionEventService, verificationCardSetService, verificationCardService);

		// Request payload.
		final Path electionContextPayloadPath = Paths.get(Objects.requireNonNull(
				ElectionContextProcessorIT.class.getResource("/configuration/electioncontext/election-event-context-payload.json")).toURI());

		electionContextPayloadBytes = Files.readAllBytes(electionContextPayloadPath);
		final ElectionEventContextPayload electionEventContextPayload = objectMapper.readValue(electionContextPayloadBytes,
				ElectionEventContextPayload.class);
		electionEventContextPayload.setSignature(new CryptoPrimitivesSignature(new byte[] {}));
		modifiedElectionContextPayloadBytes = objectMapper.writeValueAsBytes(electionEventContextPayload);

		// Invalid request payload.
		final Path invalidElectionContextPayloadPath = Paths.get(Objects.requireNonNull(
				ElectionContextProcessorIT.class.getResource("/configuration/electioncontext/invalid-election-event-context-payload.json")).toURI());

		invalidElectionContextPayloadBytes = Files.readAllBytes(invalidElectionContextPayloadPath);
	}

	@AfterEach
	void cleanUp() {

		testDatabaseCleanUpService.cleanUp();
	}

	@Test
	@DisplayName("a request for the first time saves ElectionEventContext")
	void firstTimeCommand() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(true);

		// Send to request queue the ElectionContextPayload.
		final MessageProperties messageProperties = new MessageProperties();
		final String correlationId = UUID.randomUUID().toString();
		messageProperties.setCorrelationId(correlationId);

		final Message message = new Message(electionContextPayloadBytes, messageProperties);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, ELECTION_CONTEXT_REQUEST_QUEUE_1, message);

		// Verifications.
		final Message responseMessage = rabbitTemplate.receive(ELECTION_CONTEXT_RESPONSE_QUEUE_1, 5000);
		assertNotNull(responseMessage);
		assertEquals(correlationId, responseMessage.getMessageProperties().getCorrelationId());
	}

	@Test
	@DisplayName("an identical command sent twice sends previous response")
	void identicalCommandTwice() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(true);

		// Send to request queue the ElectionContextPayload for the first time.
		final MessageProperties messageProperties = new MessageProperties();
		final String correlationId = UUID.randomUUID().toString();
		messageProperties.setCorrelationId(correlationId);
		final Message message = new Message(electionContextPayloadBytes, messageProperties);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, ELECTION_CONTEXT_REQUEST_QUEUE_1, message);
		final Message firstResponseMessage = rabbitTemplate.receive(ELECTION_CONTEXT_RESPONSE_QUEUE_1, 5000);

		// Send to request queue the ElectionContextPayload for the second time.
		rabbitTemplate.send(RABBITMQ_EXCHANGE, ELECTION_CONTEXT_REQUEST_QUEUE_1, message);
		final Message secondResponseMessage = rabbitTemplate.receive(ELECTION_CONTEXT_RESPONSE_QUEUE_1, 5000);

		// Verifications.
		assertNotNull(firstResponseMessage);
		assertNotNull(secondResponseMessage);
		assertEquals(correlationId, firstResponseMessage.getMessageProperties().getCorrelationId());
		assertEquals(correlationId, secondResponseMessage.getMessageProperties().getCorrelationId());
		assertArrayEquals(secondResponseMessage.getBody(), firstResponseMessage.getBody());
	}

	@Test
	@DisplayName("an identical command with different payload is rejected")
	void identicalCommandDifferentPayload() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(true);

		// Send to request queue the ElectionContextPayload.
		final MessageProperties messageProperties = new MessageProperties();
		final String correlationId = UUID.randomUUID().toString();
		messageProperties.setCorrelationId(correlationId);
		final Message message = new Message(electionContextPayloadBytes, messageProperties);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, ELECTION_CONTEXT_REQUEST_QUEUE_1, message);
		final Message firstResponseMessage = rabbitTemplate.receive(ELECTION_CONTEXT_RESPONSE_QUEUE_1, 5000);

		// Send to request queue the modified ElectionContextPayload.
		final Message messageWithDifferentPayload = new Message(modifiedElectionContextPayloadBytes, messageProperties);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, ELECTION_CONTEXT_REQUEST_QUEUE_1, messageWithDifferentPayload);
		final Message secondResponseMessage = rabbitTemplate.receive(ELECTION_CONTEXT_RESPONSE_QUEUE_1, 5000);

		// Verifications.
		assertNotNull(firstResponseMessage);
		assertEquals(correlationId, firstResponseMessage.getMessageProperties().getCorrelationId());
		assertNull(secondResponseMessage);
	}

	@Test
	@DisplayName("a payload with invalid signature sends null response and does not save ElectionEventContext")
	void sendNullResponseWithInvalidSignature() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(false);

		// Send to request queue the ElectionContextPayload.
		final MessageProperties messageProperties = new MessageProperties();
		final String correlationId = UUID.randomUUID().toString();
		messageProperties.setCorrelationId(correlationId);

		final Message message = new Message(electionContextPayloadBytes, messageProperties);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, ELECTION_CONTEXT_REQUEST_QUEUE_1, message);

		// Verifications.
		final Message responseMessage = rabbitTemplate.receive(ELECTION_CONTEXT_RESPONSE_QUEUE_1, 5000);
		assertNull(responseMessage);
	}

	@Test
	@DisplayName("a payload with invalid number of verification card IDs does not save request/response and sends null response")
	void sendNullResponseWithInvalidElectionEventContext() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(true);

		// Send to request queue the ElectionContextPayload.
		final MessageProperties messageProperties = new MessageProperties();
		final String correlationId = UUID.randomUUID().toString();
		messageProperties.setCorrelationId(correlationId);

		final Message message = new Message(invalidElectionContextPayloadBytes, messageProperties);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, ELECTION_CONTEXT_REQUEST_QUEUE_1, message);

		// Verifications.
		final Message responseMessage = rabbitTemplate.receive(ELECTION_CONTEXT_RESPONSE_QUEUE_1, 5000);
		assertNull(responseMessage);
	}

	private static void saveInDatabase(final String electionEventId, final String verificationCardSetId,
			final ElectionEventService electionEventService, final VerificationCardSetService verificationCardSetService,
			final VerificationCardService verificationCardService) {
		final ElectionEventEntity savedElectionEventEntity = electionEventService.save(electionEventId, GroupTestData.getGqGroup());
		final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity(verificationCardSetId,
				savedElectionEventEntity, new CombinedCorrectnessInformation(Collections.emptyList()));
		verificationCardSetService.save(verificationCardSetEntity);
		for (int i = 0; i < 10; i++) {
			final VerificationCard verificationCard = new VerificationCard(RANDOM.genRandomBase16String(32), verificationCardSetId,
					new ElGamalMultiRecipientPublicKey(GroupVector.of(GroupTestData.getGqGroup().getGenerator())));
			verificationCardService.save(verificationCard);
		}
	}
}
