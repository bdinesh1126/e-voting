/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.voting.sendvote;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SignatureException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.ElectionContextService;
import ch.post.it.evoting.controlcomponent.ElectionEventEntity;
import ch.post.it.evoting.controlcomponent.ElectionEventService;
import ch.post.it.evoting.controlcomponent.MessageBrokerIntegrationTestBase;
import ch.post.it.evoting.controlcomponent.TestDatabaseCleanUpService;
import ch.post.it.evoting.controlcomponent.VerificationCard;
import ch.post.it.evoting.controlcomponent.VerificationCardService;
import ch.post.it.evoting.controlcomponent.VerificationCardSetEntity;
import ch.post.it.evoting.controlcomponent.VerificationCardSetService;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.election.CorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentPartialDecryptPayload;

@DisplayName("PartialDecryptProcessor consuming")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class PartialDecryptProcessorIT extends MessageBrokerIntegrationTestBase {

	private static byte[] encryptedVotePayloadBytes;
	private static final List<BigInteger> listOfWriteInOptions = Collections.emptyList();

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private ElectionEventService electionEventService;

	@Autowired
	private VerificationCardSetService verificationCardSetService;

	@Autowired
	private VerificationCardService verificationCardService;

	@Autowired
	private ElectionContextService electionContextService;

	@Autowired
	private TestDatabaseCleanUpService testDatabaseCleanUpService;

	@MockBean
	private SignatureKeystore<Alias> signatureKeystoreService;

	@MockBean
	private PartialDecryptService partialDecryptService;

	@BeforeEach
	void setUp() throws IOException, URISyntaxException {

		// Must match the group in the json.
		final GqGroup encryptionGroup = new GqGroup(BigInteger.valueOf(11), BigInteger.valueOf(5), BigInteger.valueOf(3));

		// Save election event.
		final ElectionEventEntity electionEventEntity = electionEventService.save(electionEventId, encryptionGroup);

		// Save verification card set.
		final CombinedCorrectnessInformation combinedCorrectnessInformation = new CombinedCorrectnessInformation(
				Collections.singletonList(new CorrectnessInformation("1", 5, 5, listOfWriteInOptions)));
		final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity(verificationCardSetId, electionEventEntity,
				combinedCorrectnessInformation);
		verificationCardSetService.save(verificationCardSetEntity);

		// Save verification card.
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(encryptionGroup);
		final ElGamalMultiRecipientPublicKey publicKey = elGamalGenerator.genRandomPublicKey(1);
		final String verificationCardId = "dd4063884c144446a6dfb63c42eb9e86";
		verificationCardService.save(new VerificationCard(verificationCardId, verificationCardSetId, publicKey));

		// Save ElectionEventContext.
		final URL electionEventContextPayloadUrl = PartialDecryptProcessorIT.class.getResource(
				"/voting/sendvote/election-event-context-payload.json");
		final ElectionEventContextPayload electionEventContextPayload = objectMapper.readValue(electionEventContextPayloadUrl,
				ElectionEventContextPayload.class);
		electionContextService.save(electionEventContextPayload.getElectionEventContext());

		// Request payload.
		final Path encryptedVotePayloadPath = Paths.get(
				Objects.requireNonNull(PartialDecryptProcessorIT.class.getResource("/voting/sendvote/encrypted-verifiable-vote-payload.json"))
						.toURI());
		encryptedVotePayloadBytes = Files.readAllBytes(encryptedVotePayloadPath);

		// Response from PartialDecryptService.
		final URL partiallyDecryptedPCCPayloadUrl = PartialDecryptProcessorIT.class.getResource(
				"/voting/sendvote/partially-decrypted-encrypted-pcc-payload.json");
		final ControlComponentPartialDecryptPayload controlComponentPartialDecryptPayload = objectMapper.readValue(partiallyDecryptedPCCPayloadUrl,
				ControlComponentPartialDecryptPayload.class);
		when(partialDecryptService.performPartialDecrypt(any())).thenReturn(
				controlComponentPartialDecryptPayload.getPartiallyDecryptedEncryptedPCC());
	}

	@AfterEach
	void cleanUp() {
		testDatabaseCleanUpService.cleanUp();
	}

	@Test
	@DisplayName("a request for the first time perform calculation")
	void firstTimeCommand() throws SignatureException {
		when(signatureKeystoreService.generateSignature(any(), any())).thenReturn(electionEventId.getBytes(StandardCharsets.UTF_8));
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(true);

		// Send to request queue the VotingServerEncryptedVotePayload.
		final MessageProperties messageProperties = new MessageProperties();
		final String correlationId = UUID.randomUUID().toString();
		messageProperties.setCorrelationId(correlationId);

		final Message message = new Message(encryptedVotePayloadBytes, messageProperties);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, PARTIAL_DECRYPT_REQUEST_QUEUE_1, message);

		// Verifications.
		final Message responseMessage = rabbitTemplate.receive(PARTIAL_DECRYPT_RESPONSE_QUEUE_1, 5000);
		assertNotNull(responseMessage);
		assertEquals(correlationId, responseMessage.getMessageProperties().getCorrelationId());
	}

	@Test
	@DisplayName("an identical command sent twice sends previous response")
	void identicalCommandTwice() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(true);

		// Send to request queue the EncryptedVerifiableVotePayload for the first time.
		final MessageProperties messageProperties = new MessageProperties();
		final String correlationId = UUID.randomUUID().toString();
		messageProperties.setCorrelationId(correlationId);
		final Message message = new Message(encryptedVotePayloadBytes, messageProperties);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, PARTIAL_DECRYPT_REQUEST_QUEUE_1, message);
		final Message firstResponseMessage = rabbitTemplate.receive(PARTIAL_DECRYPT_RESPONSE_QUEUE_1, 5000);

		// Send to request queue the EncryptedVerifiableVotePayload for the second time.
		rabbitTemplate.send(RABBITMQ_EXCHANGE, PARTIAL_DECRYPT_REQUEST_QUEUE_1, message);
		final Message secondResponseMessage = rabbitTemplate.receive(PARTIAL_DECRYPT_RESPONSE_QUEUE_1, 5000);

		// Verifications.
		assertNotNull(firstResponseMessage);
		assertNotNull(secondResponseMessage);
		assertEquals(correlationId, firstResponseMessage.getMessageProperties().getCorrelationId());
		assertEquals(correlationId, secondResponseMessage.getMessageProperties().getCorrelationId());
		assertArrayEquals(secondResponseMessage.getBody(), firstResponseMessage.getBody());
	}

	@Test
	@DisplayName("an identical command with different payload is rejected")
	void identicalCommandDifferentPayload() throws SignatureException, IOException, URISyntaxException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(true);

		// Send to request queue the EncryptedVerifiableVotePayload.
		final MessageProperties messageProperties = new MessageProperties();
		final String correlationId = UUID.randomUUID().toString();
		messageProperties.setCorrelationId(correlationId);
		final Message message = new Message(encryptedVotePayloadBytes, messageProperties);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, PARTIAL_DECRYPT_REQUEST_QUEUE_1, message);
		final Message firstResponseMessage = rabbitTemplate.receive(PARTIAL_DECRYPT_RESPONSE_QUEUE_1, 5000);

		// Send to request queue the modified EncryptedVerifiableVotePayload.
		final Path modifiedPayloadPath = Paths.get(
				Objects.requireNonNull(PartialDecryptProcessorIT.class.getResource("/voting/sendvote/encrypted-verifiable-vote-2.json"))
						.toURI());
		final byte[] modifiedEncryptedVotePayloadBytes = Files.readAllBytes(modifiedPayloadPath);
		final Message messageWithDifferentPayload = new Message(modifiedEncryptedVotePayloadBytes, messageProperties);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, PARTIAL_DECRYPT_REQUEST_QUEUE_1, messageWithDifferentPayload);
		final Message secondResponseMessage = rabbitTemplate.receive(PARTIAL_DECRYPT_RESPONSE_QUEUE_1, 5000);

		// Verifications.
		assertNotNull(firstResponseMessage);
		assertEquals(correlationId, firstResponseMessage.getMessageProperties().getCorrelationId());
		assertNull(secondResponseMessage);
	}

	@Test
	@DisplayName("problem verifying VotingServerEncryptedVotePayload signature sends null message")
	void problemVerifyingPayloadSignatureSendsNullMessage() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenThrow(SignatureException.class);

		// Send to request queue the VotingServerEncryptedVotePayload.
		final MessageProperties messageProperties = new MessageProperties();
		final String correlationId = UUID.randomUUID().toString();
		messageProperties.setCorrelationId(correlationId);

		final Message message = new Message(encryptedVotePayloadBytes, messageProperties);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, PARTIAL_DECRYPT_REQUEST_QUEUE_1, message);

		// There should not be any response.
		assertNull(rabbitTemplate.receive(PARTIAL_DECRYPT_RESPONSE_QUEUE_1, 5000));
	}

	@Test
	@DisplayName("invalid VotingServerEncryptedVotePayload signature sends null message")
	void invalidPayloadSignatureSendsNullMessage() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(false);

		// Send to request queue the VotingServerEncryptedVotePayload.
		final MessageProperties messageProperties = new MessageProperties();
		final String correlationId = UUID.randomUUID().toString();
		messageProperties.setCorrelationId(correlationId);

		final Message message = new Message(encryptedVotePayloadBytes, messageProperties);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, PARTIAL_DECRYPT_REQUEST_QUEUE_1, message);

		// There should not be any response.
		assertNull(rabbitTemplate.receive(PARTIAL_DECRYPT_RESPONSE_QUEUE_1, 5000));
	}

}
