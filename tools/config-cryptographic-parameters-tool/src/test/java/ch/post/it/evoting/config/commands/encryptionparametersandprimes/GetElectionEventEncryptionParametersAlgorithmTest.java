/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.config.commands.encryptionparametersandprimes;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.SecureRandom;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.internal.math.RandomService;

@DisplayName("Calling getElectionEventEncryptionParameters with")
class GetElectionEventEncryptionParametersAlgorithmTest {

	private static final RandomService RANDOM_SERVICE = new RandomService();
	private static final SecureRandom SECURE_RANDOM = new SecureRandom();

	private static GetElectionEventEncryptionParametersAlgorithm encryptionParametersAlgorithm;

	private String seed;
	private int omega;

	@BeforeAll
	static void setupAll() {
		encryptionParametersAlgorithm = new GetElectionEventEncryptionParametersAlgorithm(ElGamalFactory.createElGamal());
	}

	@BeforeEach
	void setup() {
		seed = RANDOM_SERVICE.genRandomBase16String(10);
		omega = SECURE_RANDOM.nextInt(1, 3000);
	}

	@Test
	@DisplayName("null seed or zero omega throws an Exception")
	void getElectionEventEncryptionParametersWithInvalidArgumentsThrows() {
		assertThrows(NullPointerException.class, () -> encryptionParametersAlgorithm.getElectionEventEncryptionParameters(omega, null));
		assertThrows(IllegalArgumentException.class, () -> encryptionParametersAlgorithm.getElectionEventEncryptionParameters(0, seed));
	}

	@Test
	@DisplayName("valid arguments generates output")
	void getElectionEventEncryptionParametersWithValidArgumentsSucceeds() {
		// To avoid a long execution time, we fix the seed here
		assertDoesNotThrow(() -> encryptionParametersAlgorithm.getElectionEventEncryptionParameters(omega, "31"));
	}

}
