/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import ch.post.it.evoting.config.commands.encryptionparametersandprimes.EncryptionParametersAndPrimesCommandProcessor;
import ch.post.it.evoting.config.commands.keystore.KeyStoreCommandProcessor;

/**
 * Tool Configuration
 */
@Configuration
@ComponentScan(basePackages = { "ch.post.it.evoting.config" })
@PropertySource("classpath:config/application.properties")
public class Config {

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertiesResolver() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	ConfigurationCommandLine commandLine(final KeyStoreCommandProcessor keyStoreCommandProcessor,
			final EncryptionParametersAndPrimesCommandProcessor encryptionParametersAndPrimesCommandProcessor) {
		return new ConfigurationCommandLine(keyStoreCommandProcessor, encryptionParametersAndPrimesCommandProcessor);
	}
}
