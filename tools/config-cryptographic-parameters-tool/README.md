# Config Cryptographic Parameters Tool

The Config Cryptographic Parameter Tool helps users with common cryptographic task related to the e-voting process.

The commands it provides are:

- `genEncryptionParametersAndPrimes` to generate the parameters of the ElGamal encryption scheme.
- `keystore` to generate a keystore and the related files for a voting components.

## Generate the ElGamal Encryption Parameters (Command `genEncryptionParametersAndPrimes`)

This command should be run before the actual configuration phase and generates the encryption parameters in a verifiable manner and provides a list of
prime numbers (which have to be members of the group) that can encode the voting options.

We refer the reader to the crypto-primitives and system specification for details on how we verifiably pick the encryption parameters.

```shell
java -jar config-cryptographic-parameters-tool-{VERSION}.jar \
     -genEncryptionParametersAndPrimes \
     -seed_path seed.txt \
     -out output_parameters/
```

Parameters help:

- `genEncryptionParametersAndPrimes` select the current command.
- `seed_path` is the path for the txt file containing the seed as a String.
- `out` is the optional path where the tool persists the output files. Default value is `./output/`.

## Generating Direct Trust Keystore to Implement Channel Security (Command `keystore`)

This command creates three files in the wanted directory (must be empty):

- A keystore.
    - which is protected by a main password.
    - which contains one new private key.
- A password file, which contains the password to unlock the keystore.
- A certificate related to the private key stored in the keystore.

```shell
java -jar config-cryptographic-parameters-tool-{VERSION}.jar \
     -keystore \
     -valid_from <dd/MM/yyyy> \
     -valid_until <dd/MM/yyyy> \
     -certificate_common_name <string> \
     -certificate_country <string> \
     -certificate_state <string> \
     -certificate_organisation <string> \
     -certificate_locality <string> \
     -alias <string> \
     -password_length <int> \
     -out <path> 
```

Parameters help:

- `keystore` select the current command.
- All the parameters below are related to the certificate:
    - `valid_from` date from which the certificate is valid.
    - `valid_until` date until which the certificate is valid.
    - `certificate_common_name` the name of the certificate.
    - `certificate_country` the country of the certificate.
    - `certificate_state` the state of the certificate.
    - `certificate_organisation` the organization of the certificate.
    - `certificate_locality` the locality of the certificate.
- `alias` is the alias name of the component we want to generate files for.
- `password_length` is the optional length of the generated password. Default length is `20`.
- `out` is the optional path where the tool will create the keystore, the password file and the certificate. Default value is `./output/`.

To see how to use this command for the certificates and keys management in e-voting, please follow the section below.

### Usage

The following e-voting participants require a signing keystore.

- Canton
- Setup Component
- Tally Control Component
- Voting Server
- Control Component 1
- Control Component 2
- Control Component 3
- Control Component 4

Each keystore contains the following :

- A private key to sign contents.
- All the trusted certificates of other components to validate the received content.

#### Alias

Each e-voting participant involved in signing is assigned an alias.

| Participant             | Alias               |
|-------------------------|---------------------|
| Canton                  | CANTON              |
| Setup Component         | SDM_CONFIG          |
| Tally Control Component | SDM_TALLY           |
| Voting Server           | VOTING_SERVER       |
| Control Component 1     | CONTROL_COMPONENT_1 |
| Control Component 2     | CONTROL_COMPONENT_2 |
| Control Component 3     | CONTROL_COMPONENT_3 |
| Control Component 4     | CONTROL_COMPONENT_4 |

#### Process

Follow the steps below to set up the e-voting ecosystem:

##### 1. Generate files

As explained in the [README.md](README.md), you can generate the wanted keystore with this command:

```shell
java -jar config-cryptographic-parameters-tool-{VERSION}.jar \
     -keystore \
     -alias <Alias of the participant> \
     -valid_from <dd/MM/yyyy> \
     -valid_until <dd/MM/yyyy> \
     -certificate_common_name <string> \
     -certificate_country <string> \
     -certificate_state <string> \
     -certificate_organisation <string> \
     -certificate_locality <string>
```

This will generate 3 files in the folder `./output/` named according to the alias you have entered.

##### 2. Certificates exchange

We assume the existence of an out-of-band channel that allows the protocol participants to exchange certificates and check their authenticity.

At the end of the process, the `./output/` folder contains all other parties' certificates.

##### 3. Certificates import

Switch to the `./output/` folder and import all the certificates received into the keystore using this command:

```shell
keytool -import \
        -alias <Alias of the participant you are importing> \
        -file <Alias of the participant you are importing>.crt \
        -keystore <Alias of the participant you manage>.p12
```

Now run `keytool -list -keystore <Alias of the participant you manage>.p12` and check you have all the entries listed.

## Development

```bash
mvn clean install
```
