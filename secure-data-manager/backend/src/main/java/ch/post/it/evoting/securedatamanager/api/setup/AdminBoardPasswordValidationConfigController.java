/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api.setup;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.securedatamanager.configuration.AdminBoardConfigService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/sdm-backend/setup/adminboardpasswords")
@Api(value = "Admin board password validation REST API")
@ConditionalOnProperty("role.isSetup")
public class AdminBoardPasswordValidationConfigController {
	private final AdminBoardConfigService adminBoardConfigService;

	public AdminBoardPasswordValidationConfigController(final AdminBoardConfigService adminBoardConfigService) {
		this.adminBoardConfigService = adminBoardConfigService;
	}

	/**
	 * Validates an admin board member's password against its persisted hash.
	 *
	 * @param adminBoardId       the admin board id.
	 * @param memberIndex        the index of the member in the member list.
	 * @param adminBoardPassword the member's password.
	 */
	@PutMapping(value = "adminboards/{adminBoardId}")
	@ApiOperation(value = "Validate admin board password", notes = "Service to validate the admin board password against its hash.")
	public ResponseEntity<Void> validatePassword(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String adminBoardId,
			@ApiParam(value = "index", required = true)
			@RequestParam
			final int memberIndex,
			@ApiParam(value = "adminBoardPassword", required = true)
			@RequestBody
			final char[] adminBoardPassword) {

		validateUUID(adminBoardId);
		checkNotNull(adminBoardPassword);

		if (adminBoardConfigService.verifyAdminBoardMemberPassword(adminBoardId, memberIndex, adminBoardPassword)) {
			return new ResponseEntity<>(HttpStatus.OK);
		}

		return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
	}
}
