/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;

import ch.ech.xmlns.ech_0110._4.Delivery;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.evotinglibraries.xml.XmlNormalizer;
import ch.post.it.evoting.evotinglibraries.xml.mapper.DeliveryMapper;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AuthorizationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingdecrypt.Results;

@Service
@ConditionalOnProperty("role.isTally")
public class TallyComponentEch0110Service {

	private static final Logger LOGGER = LoggerFactory.getLogger(TallyComponentEch0110Service.class);

	private final TallyComponentDecryptService tallyComponentDecryptService;
	private final CantonConfigTallyService cantonConfigTallyService;
	private final TallyComponentEch0110FileRepository tallyComponentEch0110FileRepository;
	private final XmlNormalizer xmlNormalizer;

	public TallyComponentEch0110Service(
			final TallyComponentDecryptService tallyComponentDecryptService,
			final CantonConfigTallyService cantonConfigTallyService,
			final TallyComponentEch0110FileRepository tallyComponentEch0110FileRepository,
			final XmlNormalizer xmlNormalizer) {
		this.tallyComponentDecryptService = tallyComponentDecryptService;
		this.cantonConfigTallyService = cantonConfigTallyService;
		this.tallyComponentEch0110FileRepository = tallyComponentEch0110FileRepository;
		this.xmlNormalizer = xmlNormalizer;
	}

	/**
	 * Generates and persists the delivery tally file.
	 * <p>
	 * Note: this normalizes writeIns before writing to the file.
	 *
	 * @param electionEventId    the election event id. Must be non-null and a valid UUID.
	 * @param authorizationTypes the configuration authorizations to be included in the delivery tally file. Must be non-null and not contain any null
	 *                           entry.
	 * @throws NullPointerException      if any input is null or if {@code authorizationTypes} contains a null entry.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 */
	public void generate(final String electionEventId, final Set<AuthorizationType> authorizationTypes) {
		validateUUID(electionEventId);
		checkNotNull(authorizationTypes);
		authorizationTypes.stream().parallel().forEach(Preconditions::checkNotNull);
		final List<AuthorizationType> authorizations = List.copyOf(authorizationTypes);

		LOGGER.debug("Generating tally component eCH-0110 file... [electionEventId: {}]", electionEventId);

		final Configuration configuration = cantonConfigTallyService.load(electionEventId);
		final Results results = tallyComponentDecryptService.load(electionEventId, configuration.getContest().getContestIdentification());

		final Delivery delivery = DeliveryMapper.INSTANCE.map(electionEventId, configuration, results, authorizations);
		final Delivery normalizedDelivery = xmlNormalizer.normalizeWriteInsEch0110(delivery);

		tallyComponentEch0110FileRepository.save(normalizedDelivery, electionEventId);

		LOGGER.info("Tally component eCH-0110 file successfully generated. [electionEventId: {}]", electionEventId);
	}

	/**
	 * Loads the tally component eCH-0110 for the given election event id and validates its signature.
	 * <p>
	 * The result of this method is cached.
	 *
	 * @param electionEventId       the election event id. Must be non-null and a valid UUID.
	 * @param contestIdentification the contest identification. Must be non-null and non-blank.
	 * @return the tally component eCH-0110 as {@link Delivery}.
	 * @throws NullPointerException      if the election event id or the contest identification is null.
	 * @throws IllegalArgumentException  if the contest identification is blank.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 */
	@Cacheable("tallyComponentEch0110s")
	public Delivery load(final String electionEventId, final String contestIdentification) {
		validateUUID(electionEventId);
		checkNotNull(contestIdentification);
		checkArgument(!contestIdentification.isBlank(), "The contest identification cannot be blank.");

		return tallyComponentEch0110FileRepository.load(electionEventId, contestIdentification)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Could not find the requested tally component eCH-0110 file. [electionEventId: %s, contestIdentification: %s]",
								electionEventId, contestIdentification)));
	}
}
