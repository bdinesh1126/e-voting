/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.commons;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Wrapper around a Retrofit network call that checks that the network request was successful.
 * @param <T> the response body type
 */
public class RemoteService<T> {
	private static final Logger LOGGER = LoggerFactory.getLogger(RemoteService.class);
	private final Call<T> networkCall;
	private String unsuccessfulErrorMessage;
	private String networkErrorMessage;
	private boolean enableUnsuccessfulError = true;

	private RemoteService(final Call<T> networkCall) {
		checkNotNull(networkCall);
		this.networkCall = networkCall;
	}

	public static <T> RemoteService<T> call(final Call<T> networkCall) {
		checkNotNull(networkCall);
		return new RemoteService<>(networkCall);
	}

	public RemoteService<T> networkErrorMessage(final String errorMessageTemplate, final Object... errorMessageArgs) {
		checkNotNull(errorMessageTemplate);
		this.networkErrorMessage = String.format(errorMessageTemplate, errorMessageArgs);
		return this;
	}

	public RemoteService<T> unsuccessfulResponseErrorMessage(final String unsuccessfulMessageTemplate, final Object... unsuccessfulMessageArgs) {
		checkNotNull(unsuccessfulMessageTemplate);
		checkArgument(this.enableUnsuccessfulError, "Cannot set an unsuccessful error message when it is disabled.");
		this.unsuccessfulErrorMessage = String.format(unsuccessfulMessageTemplate, unsuccessfulMessageArgs);
		return this;
	}

	public RemoteService<T> doNotThrowOnUnsuccessfulResponse() {
		checkState(this.unsuccessfulErrorMessage == null, "Cannot disable unsuccessful error when a message is set.");
		this.enableUnsuccessfulError = false;
		return this;
	}

	/**
	 * Executes the network call.
	 * @throws UncheckedIOException if a problem occurred talking to the remote server
	 * @throws IllegalStateException if the response is not successful and this feature is not disabled
	 */
	public Response<T> execute() {
		checkArgument(networkErrorMessage != null, "Missing network error message");
		checkArgument(!enableUnsuccessfulError || unsuccessfulErrorMessage != null, "Unsuccessful error message must be set");
		final Response<T> response;
		try {
			response = networkCall.execute();
		} catch (final IOException e) {
			throw new UncheckedIOException(networkErrorMessage, e);
		}
		if (enableUnsuccessfulError && !response.isSuccessful()) {
			LOGGER.error("Network request unsuccessful. [httpCode: {}]", response.code());
			throw new IllegalStateException(this.unsuccessfulErrorMessage);
		}
		return response;
	}
}
