/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.nio.CharBuffer;

/**
 * Validation of a board password.
 */
public class BoardPasswordValidation {

	private static final int BOARD_PASSWORD_MIN_LENGTH = 24;
	private static final int BOARD_PASSWORD_MAX_LENGTH = 64;

	private BoardPasswordValidation() {
		// Intentionally left blank.
	}

	/**
	 * Validates the password of the board member. <br/> A password is valid if:
	 * <ul>
	 *     <li>the size of the password is equal to or greater than 12;</li>
	 *     <li>the password contains at least one upper case letter;</li>
	 *     <li>the password contains at least one lower case letter;</li>
	 *     <li>the password contains at least one special character;</li>
	 *     <li>the password contains at least one digit.</li>
	 * </ul>
	 *
	 * @param password the board password. Must be non-null.
	 * @throws NullPointerException     if the password is null.
	 * @throws IllegalArgumentException if
	 *                                  <ul>
	 *                                      <li>the size of the password is strictly smaller than 12.</li>
	 *                                      <li>the password does not contain at least one upper case letter.</li>
	 *                                      <li>the password does not contain at least one lower case letter.</li>
	 *                                      <li>the password does not contain at least one special character.</li>
	 *                                      <li>the password does not contain at least one digit.</li>
	 *                                  </ul>
	 */
	public static void validate(final char[] password) {
		checkNotNull(password);
		checkArgument(password.length >= BOARD_PASSWORD_MIN_LENGTH, "The password must be at least of size %s.", BOARD_PASSWORD_MIN_LENGTH);
		checkArgument(password.length <= BOARD_PASSWORD_MAX_LENGTH, "The password must be at most of size %s.", BOARD_PASSWORD_MAX_LENGTH);
		checkArgument(CharBuffer.wrap(password).chars().anyMatch(c -> Character.isAlphabetic(c) && Character.isUpperCase(c)),
				"The password must contain at least one upper case letter.");
		checkArgument(CharBuffer.wrap(password).chars().anyMatch(c -> Character.isAlphabetic(c) && Character.isLowerCase(c)),
				"The password must contain at least one lower case letter.");
		checkArgument(CharBuffer.wrap(password).chars().anyMatch(c -> String.valueOf((char) c).matches("[^A-Za-z0-9]")),
				"The password must contain at least one special character.");
		checkArgument(CharBuffer.wrap(password).chars().anyMatch(Character::isDigit), "The password must contain at least one digit.");
	}
}
