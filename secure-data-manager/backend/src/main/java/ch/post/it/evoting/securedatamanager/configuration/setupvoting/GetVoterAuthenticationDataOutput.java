/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateBase64Encoded;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import ch.post.it.evoting.cryptoprimitives.domain.validations.Validations;
import ch.post.it.evoting.securedatamanager.commons.Constants;

/**
 * @param derivedVoterIdentifiers      credentialID, the vector of derived voter identifiers. Must be non-null and must contain N<sub>E</sub> valid
 *                                     UUIDs.
 * @param baseAuthenticationChallenges hAuth, the vector of base authentication challenges. Must be non-null and must contain N<sub>E</sub> valid
 *                                     Base64 of length l<sub>HB64</sub>.
 */
public record GetVoterAuthenticationDataOutput(List<String> derivedVoterIdentifiers, List<String> baseAuthenticationChallenges) {

	public GetVoterAuthenticationDataOutput(final List<String> derivedVoterIdentifiers, final List<String> baseAuthenticationChallenges) {
		final List<String> derivedVoterIdentifiersCopy = List.copyOf(checkNotNull(derivedVoterIdentifiers));
		checkArgument(!derivedVoterIdentifiersCopy.isEmpty(), "The credentialID must not be empty.");
		derivedVoterIdentifiersCopy.forEach(Validations::validateUUID);

		final List<String> baseAuthenticationChallengesCopy = List.copyOf(checkNotNull(baseAuthenticationChallenges));
		checkArgument(!baseAuthenticationChallengesCopy.isEmpty(), "The hAuth must not be empty.");
		checkArgument(baseAuthenticationChallengesCopy.stream()
						.allMatch(challenge -> validateBase64Encoded(challenge).length() == Constants.BASE64_ENCODED_HASH_OUTPUT_LENGTH),
				"The base authentication challenge must be a valid Base64 string of size l_HB64.");

		checkArgument(derivedVoterIdentifiersCopy.size() == baseAuthenticationChallengesCopy.size(),
				"There must be as many base authentication challenges as derived voter identifiers.");

		this.derivedVoterIdentifiers = derivedVoterIdentifiersCopy;
		this.baseAuthenticationChallenges = baseAuthenticationChallengesCopy;
	}

	@Override
	public List<String> derivedVoterIdentifiers() {
		return List.copyOf(derivedVoterIdentifiers);
	}

	@Override
	public List<String> baseAuthenticationChallenges() {
		return List.copyOf(baseAuthenticationChallenges);
	}
}
