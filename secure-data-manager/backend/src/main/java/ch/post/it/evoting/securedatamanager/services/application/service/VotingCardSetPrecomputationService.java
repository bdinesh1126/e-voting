/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.google.common.annotations.VisibleForTesting;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.securedatamanager.commons.VerificationCardSet;
import ch.post.it.evoting.securedatamanager.configuration.EncryptionParametersConfigService;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.BallotConfigService;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.GenVerDatOutput;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.GenVerDatService;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.InvalidStatusTransitionException;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.Status;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;

/**
 * Service that deals with the pre-computation of voting card sets.
 */
@Service
@ConditionalOnProperty("role.isSetup")
public class VotingCardSetPrecomputationService extends BaseVotingCardSetService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetPrecomputationService.class);

	private final ExecutorService executorService;
	private final BallotConfigService ballotConfigService;
	private final GenVerDatService genVerDatService;
	private final IdleStatusService idleStatusService;
	private final ElectionEventService electionEventService;
	private final BallotBoxService ballotBoxService;
	private final EncryptionParametersConfigService encryptionParametersConfigService;
	private final ConfigurationEntityStatusService configurationEntityStatusService;
	private final VotingCardSetPrecomputationPersistenceService votingCardSetPrecomputationPersistenceService;

	@Autowired
	public VotingCardSetPrecomputationService(
			final ExecutorService executorService,
			final BallotConfigService ballotConfigService,
			final GenVerDatService genVerDatService,
			final IdleStatusService idleStatusService,
			final ElectionEventService electionEventService,
			final BallotBoxService ballotBoxService,
			final EncryptionParametersConfigService encryptionParametersConfigService,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final VotingCardSetPrecomputationPersistenceService votingCardSetPrecomputationPersistenceService) {
		this.executorService = executorService;
		this.ballotConfigService = ballotConfigService;
		this.genVerDatService = genVerDatService;
		this.idleStatusService = idleStatusService;
		this.electionEventService = electionEventService;
		this.ballotBoxService = ballotBoxService;
		this.encryptionParametersConfigService = encryptionParametersConfigService;
		this.configurationEntityStatusService = configurationEntityStatusService;
		this.votingCardSetPrecomputationPersistenceService = votingCardSetPrecomputationPersistenceService;
	}

	public void precomputeVotingCardSets(final String electionEventId, final String adminBoardId) {
		validateUUID(electionEventId);
		validateUUID(adminBoardId);

		// Retrieve all voting card sets not yet pre-computed.
		final List<String> votingCardSetIds = votingCardSetRepository.findAllVotingCardSetIdsByStatus(electionEventId, Status.LOCKED);

		// If there are no voting card sets to pre-compute, do nothing.
		if (votingCardSetIds.isEmpty()) {
			LOGGER.info("No voting card sets to pre-compute. [electionEventId: {}}", electionEventId);
			return;
		}
		LOGGER.info("Found voting card sets to pre-compute. [electionEventId: {}, amount: {}]", electionEventId, votingCardSetIds.size());

		// Run the pre-computations asynchronously.
		final List<CompletableFuture<Void>> precomputeFutures = votingCardSetIds.stream()
				// Prepare the pre-computation tasks.
				.map(votingCardSetId -> (Runnable) () -> {
					try {
						precomputeVotingCardSet(votingCardSetId, electionEventId, adminBoardId);
					} catch (final InvalidStatusTransitionException e) {
						throw new IllegalStateException(
								String.format(
										"Error trying to set voting card set status to precomputed. [electionEventId: %s, votingCardSetId: %s]",
										electionEventId, votingCardSetId), e);
					} catch (final ResourceNotFoundException e) {
						throw new IllegalStateException(
								String.format("Failed to pre-compute voting card set. [electionEventId: %s, votingCardSetId: %s]",
										electionEventId, votingCardSetId), e);
					}
				})
				// Run the pre-computation tasks.
				.map(preComputationTask -> CompletableFuture.runAsync(preComputationTask, executorService))
				.toList();

		// Create new CompletableFuture with all pre-computation futures and wait for all pre-computation tasks to complete, normally or exceptionally.
		try {
			CompletableFuture.allOf(precomputeFutures.toArray(new CompletableFuture[] {})).join();
		} catch (final CompletionException e) {
			// The CompletableFuture.allOf completes exceptionally if at least one precomputeFuture completes exceptionally.
			throw new IllegalStateException(String.format("Some of the pre-computations failed. [electionEventId: %s]", electionEventId), e);
		}
	}

	/**
	 * Pre-compute a voting card set.
	 *
	 * @throws InvalidStatusTransitionException If the original status does not allow pre-computing
	 */
	@VisibleForTesting
	void precomputeVotingCardSet(final String votingCardSetId, final String electionEventId, final String adminBoardId)
			throws ResourceNotFoundException, InvalidStatusTransitionException {

		validateUUID(votingCardSetId);
		validateUUID(electionEventId);

		if (!idleStatusService.getIdLock(votingCardSetId)) {
			return;
		}

		try {
			// Construct the matching verification card set context.
			final String verificationCardSetId = votingCardSetRepository.getVerificationCardSetId(votingCardSetId);
			final String ballotBoxId = votingCardSetRepository.getBallotBoxId(votingCardSetId);

			final VerificationCardSet precomputeContext = new VerificationCardSet(electionEventId, ballotBoxId, votingCardSetId,
					verificationCardSetId, adminBoardId);

			precomputeVerificationCardSet(precomputeContext);
		} finally {
			idleStatusService.freeIdLock(votingCardSetId);
		}
	}

	private void precomputeVerificationCardSet(final VerificationCardSet precomputeContext)
			throws ResourceNotFoundException, InvalidStatusTransitionException {
		final String electionEventId = precomputeContext.electionEventId();
		final String verificationCardSetId = precomputeContext.verificationCardSetId();
		final String votingCardSetId = precomputeContext.votingCardSetId();

		checkArgument(electionEventService.exists(electionEventId), "The election event id of the given context does not exist.");

		LOGGER.debug("Starting pre-computation of voting card set... [electionEventId: {}, verificationCardSetId: {}, votingCardSetId: {}]",
				electionEventId, verificationCardSetId, votingCardSetId);

		final Status fromStatus = Status.LOCKED;
		final Status toStatus = Status.PRECOMPUTED;
		checkVotingCardSetStatusTransition(electionEventId, votingCardSetId, fromStatus, toStatus);

		final int numberOfVotingCardsToGenerate = votingCardSetRepository.getNumberOfVotingCards(electionEventId, votingCardSetId);
		LOGGER.debug(
				"Generating voting cards... [numberOfVotingCardsToGenerate: {}, electionEventId: {}, verificationCardSetId: {}, votingCardSetId: {}]",
				numberOfVotingCardsToGenerate, electionEventId, verificationCardSetId, votingCardSetId);

		final List<GenVerDatOutput> genVerDatOutputs = genVerDatService.genVerDat(precomputeContext, numberOfVotingCardsToGenerate);

		final GqGroup encryptionGroup = encryptionParametersConfigService.loadEncryptionGroup(precomputeContext.electionEventId());
		checkState(genVerDatOutputs.stream().parallel().map(GenVerDatOutput::getGroup).allMatch(group -> group.equals(encryptionGroup)),
				"The group from the GenVerDat outputs does not correspond to the encryption group.");

		final String ballotBoxesByElectionEvent = ballotBoxService.listByElectionEvent(electionEventId);
		final String ballotId = ballotBoxService.getBallotId(precomputeContext.ballotBoxId());
		final String ballotAlias = ballotBoxService.listAliases(ballotId).get(0);
		final Ballot ballot = ballotConfigService.getBallot(precomputeContext.electionEventId(), ballotId);

		// Build and persist payloads to request the return code generation (choice return codes and vote cast return code) from the control components.
		votingCardSetPrecomputationPersistenceService.persistPrecomputationPayloads(precomputeContext, genVerDatOutputs, ballotId, ballotAlias,
				ballotBoxesByElectionEvent, ballot);

		LOGGER.info("Successfully generated voting cards. [numberGenerated: {}, electionEventId: {}, verificationCardSetId: {}, votingCardSetId: {}]",
				numberOfVotingCardsToGenerate, electionEventId, verificationCardSetId, votingCardSetId);

		// Update the voting card set status to 'pre-computed'.
		configurationEntityStatusService.update(toStatus.name(), votingCardSetId, votingCardSetRepository);
		LOGGER.info("Updated voting card set status to pre-computed. [electionEventId: {}, verificationCardSetId: {}, votingCardSetId: {}]",
				electionEventId, verificationCardSetId, votingCardSetId);
	}
}

