/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.security.SignatureException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

import javax.json.JsonArray;
import javax.json.JsonObject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.cryptoprimitives.domain.signature.SignedPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.AdminBoardHashesPayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.securedatamanager.AdminBoardHashesPayloadService;
import ch.post.it.evoting.securedatamanager.BoardPasswordHashService;
import ch.post.it.evoting.securedatamanager.BoardPasswordValidation;
import ch.post.it.evoting.securedatamanager.commons.JsonUtils;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.Status;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.SynchronizeStatus;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.administrationboard.AdministrationBoardRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;

@Service
@ConditionalOnProperty("role.isSetup")
public class AdminBoardConfigService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminBoardConfigService.class);

	private final BoardPasswordHashService boardPasswordHashService;
	private final SignatureKeystore<Alias> signatureKeystoreServiceSdmConfig;
	private final AdminBoardHashesPayloadService adminBoardHashesPayloadService;
	private final ConfigurationEntityStatusService configurationEntityStatusService;
	private final AdministrationBoardRepository administrationBoardRepository;

	public AdminBoardConfigService(
			final BoardPasswordHashService boardPasswordHashService,
			@Qualifier("keystoreServiceSdmConfig")
			final SignatureKeystore<Alias> signatureKeystoreServiceSdmConfig,
			final AdminBoardHashesPayloadService adminBoardHashesPayloadService,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final AdministrationBoardRepository administrationBoardRepository) {
		this.boardPasswordHashService = boardPasswordHashService;
		this.signatureKeystoreServiceSdmConfig = signatureKeystoreServiceSdmConfig;
		this.adminBoardHashesPayloadService = adminBoardHashesPayloadService;
		this.configurationEntityStatusService = configurationEntityStatusService;
		this.administrationBoardRepository = administrationBoardRepository;
	}

	/**
	 * Constitutes the Admin board.
	 * <p>
	 * The passwords are hashed and saved in an AdminBoardHashesPayload.
	 *
	 * @param adminBoardId        the admin board id.
	 * @param adminBoardPasswords the admin board passwords.
	 */
	@Transactional
	public void constitute(final String adminBoardId, final List<char[]> adminBoardPasswords)
			throws ResourceNotFoundException {
		LOGGER.debug("Constituting Admin board... [adminBoardId: {}]", adminBoardId);

		validateUUID(adminBoardId);

		checkNotNull(adminBoardPasswords);
		adminBoardPasswords.forEach(BoardPasswordValidation::validate);
		final JsonObject administrationBoard = getAdminBoardJsonObject(adminBoardId);
		final JsonArray administrationBoardMembers = administrationBoard.getJsonArray(JsonConstants.BOARD_MEMBERS);
		checkArgument(adminBoardPasswords.size() == administrationBoardMembers.size());

		final List<byte[]> hashPasswords = boardPasswordHashService.hashPasswords(adminBoardPasswords);

		// Wipe the passwords after usage
		adminBoardPasswords.forEach(pw -> Arrays.fill(pw, '\u0000'));

		final int minimumThreshold = Integer.parseInt(administrationBoard.getString(JsonConstants.MINIMUM_THRESHOLD));
		final AdminBoardHashesPayload adminBoardHashesPayload = createAdminBoardHashesPayload(adminBoardId, hashPasswords, minimumThreshold);

		adminBoardHashesPayloadService.save(adminBoardHashesPayload);

		configurationEntityStatusService.updateWithSynchronizedStatus(Status.CONSTITUTED.name(), adminBoardId, administrationBoardRepository,
				SynchronizeStatus.PENDING);

		LOGGER.info("Admin board constituted. [adminBoardId: {}]", adminBoardId);
	}

	/**
	 * Activates the Admin board.
	 * <p>
	 * The passwords are hashed and compared to the ones saved in a AdminBoardHashesPayload. The number of provided passwords must be greater than or
	 * equal to the threshold.
	 *
	 * @param adminBoardId        the admin board id.
	 * @param adminBoardPasswords the admin board passwords.
	 */
	public void activate(final String adminBoardId, final List<char[]> adminBoardPasswords) {
		LOGGER.debug("Activating Admin board... [adminBoardId: {}]", adminBoardId);

		validateUUID(adminBoardId);
		final AdminBoardHashesPayload adminBoardHashesPayload = adminBoardHashesPayloadService.load(adminBoardId);
		checkArgument(adminBoardId.equals(adminBoardHashesPayload.getAdminBoardId()));

		// Verify signature
		if (!verifySignature(adminBoardHashesPayload)) {
			throw new InvalidPayloadSignatureException(
					AdminBoardHashesPayload.class, String.format("[adminBoardId: %s]", adminBoardId));
		}
		LOGGER.info("Validated signature of admin board hashes payload. [adminBoardId: {}]", adminBoardId);

		// Verify password with hash
		final List<byte[]> adminBoardMembersHashes = adminBoardHashesPayload.getAdminBoardHashes();
		checkArgument(adminBoardMembersHashes.size() == adminBoardPasswords.size());

		final long countValidPassword = IntStream.range(0, adminBoardMembersHashes.size())
				.filter(i -> adminBoardPasswords.get(i) != null && adminBoardPasswords.get(i).length > 0 && boardPasswordHashService.verifyPassword(
						adminBoardPasswords.get(i), adminBoardMembersHashes.get(i)))
				.count();

		checkArgument(countValidPassword >= adminBoardHashesPayload.getThreshold());

		// Wipe the passwords after usage
		adminBoardPasswords.stream().filter(Objects::nonNull).forEach(pw -> Arrays.fill(pw, '\u0000'));
	}

	/**
	 * Verifies the given electoral board member's password.
	 *
	 * @param adminBoardId             the identifier of the admin board. Must be non-null and a valid UUID.
	 * @param adminBoardMemberIdx      the index of the admin board member. Must be positive and strictly smaller than the number of stored ABHashes.
	 * @param adminBoardMemberPassword the password of the admin board member. Must be non-null and a valid EBPassword.
	 * @return true if the given password matches the stored hash of the admin board member, false otherwise.
	 * @throws NullPointerException             if any input is null.
	 * @throws FailedValidationException        if {@code adminBoardId} is not a valid UUID.
	 * @throws IllegalArgumentException         if the {@code adminBoardMemberIdx} is negative or greater than the number of stored ABHashes.
	 * @throws InvalidPayloadSignatureException if the signature of the admin board hashes payload is invalid.
	 * @throws IllegalStateException            if the signature verification of the admin board hashes was not possible.
	 */
	public boolean verifyAdminBoardMemberPassword(final String adminBoardId, final int adminBoardMemberIdx,
			final char[] adminBoardMemberPassword) {
		validateUUID(adminBoardId);
		checkArgument(adminBoardMemberIdx >= 0);
		checkNotNull(adminBoardMemberPassword);

		final AdminBoardHashesPayload adminBoardHashesPayload = adminBoardHashesPayloadService.load(adminBoardId);

		// Verify signature
		if (!verifySignature(adminBoardHashesPayload)) {
			throw new InvalidPayloadSignatureException(
					AdminBoardHashesPayload.class, String.format("[adminBoardId: %s]", adminBoardId));
		}
		LOGGER.info("Validated signature of admin board hashes payload. [adminBoardId: {}]", adminBoardId);

		final List<byte[]> adminBoardMembersHashes = adminBoardHashesPayload.getAdminBoardHashes();
		checkArgument(adminBoardMemberIdx < adminBoardMembersHashes.size());
		final byte[] adminBoardMemberArgonHash = adminBoardMembersHashes.get(adminBoardMemberIdx);

		return boardPasswordHashService.verifyPassword(adminBoardMemberPassword, adminBoardMemberArgonHash);
	}

	private boolean verifySignature(final AdminBoardHashesPayload adminBoardHashesPayload) {
		final String adminBoardId = adminBoardHashesPayload.getAdminBoardId();
		final CryptoPrimitivesSignature signature = adminBoardHashesPayload.getSignature();

		checkState(signature != null, "The signature of the admin board hashes payload is null. [adminBoardId: %s]",
				adminBoardHashesPayload.getAdminBoardId());

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentAdminBoardHashes(adminBoardId);

		try {
			return signatureKeystoreServiceSdmConfig.verifySignature(Alias.SDM_CONFIG, adminBoardHashesPayload, additionalContextData,
					signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not verify the signature of the admin board hashes payload. [adminBoardId: %s]", adminBoardId));
		}
	}

	/**
	 * Gets the admin board json object.
	 *
	 * @param adminBoardId the admin board id
	 * @return the admin board json object
	 * @throws ResourceNotFoundException the resource not found exception
	 */
	private JsonObject getAdminBoardJsonObject(final String adminBoardId) throws ResourceNotFoundException {

		// get number of members, threshold
		final String administrationBoardJSON = administrationBoardRepository.find(adminBoardId);

		if (StringUtils.isEmpty(administrationBoardJSON) || JsonConstants.EMPTY_OBJECT.equals(administrationBoardJSON)) {
			throw new ResourceNotFoundException("Administration Board not found");
		}

		return JsonUtils.getJsonObject(administrationBoardJSON);
	}

	private AdminBoardHashesPayload createAdminBoardHashesPayload(final String adminBoardId, final List<byte[]> immutableHashes,
			final int threshold) {
		final AdminBoardHashesPayload adminBoardHashesPayload = new AdminBoardHashesPayload(adminBoardId, immutableHashes, threshold);

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentAdminBoardHashes(adminBoardId);
		final CryptoPrimitivesSignature adminBoardHashesPayloadSignature = getPayloadSignature(adminBoardHashesPayload, additionalContextData);
		adminBoardHashesPayload.setSignature(adminBoardHashesPayloadSignature);

		return adminBoardHashesPayload;
	}

	private CryptoPrimitivesSignature getPayloadSignature(final SignedPayload payload, final Hashable additionalContextData) {
		try {
			final byte[] signature = signatureKeystoreServiceSdmConfig.generateSignature(payload, additionalContextData);
			return new CryptoPrimitivesSignature(signature);
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Failed to generate payload signature. [%s, %s]", payload.getClass().getName(), additionalContextData));
		}
	}
}
