/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

public record GenCredDatOutput(List<String> verificationCardKeystores) {

	public GenCredDatOutput(final List<String> verificationCardKeystores) {

		checkNotNull(verificationCardKeystores);
		checkArgument(!verificationCardKeystores.isEmpty(), "Vector of verification card keystores must not be empty.");

		this.verificationCardKeystores = List.copyOf(verificationCardKeystores);
	}
}
