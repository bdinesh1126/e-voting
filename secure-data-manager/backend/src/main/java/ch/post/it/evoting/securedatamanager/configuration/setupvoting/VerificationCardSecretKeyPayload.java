/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.hasNoDuplicates;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Preconditions;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;

@JsonDeserialize(using = VerificationCardSecretKeyPayloadDeserializer.class)
public record VerificationCardSecretKeyPayload(GqGroup encryptionGroup,
											   String electionEventId,
											   String verificationCardSetId,
											   List<VerificationCardSecretKey> verificationCardSecretKeys
) {
	public VerificationCardSecretKeyPayload(final GqGroup encryptionGroup,
			final String electionEventId,
			final String verificationCardSetId,
			final List<VerificationCardSecretKey> verificationCardSecretKeys) {
		this.encryptionGroup = checkNotNull(encryptionGroup);
		this.electionEventId = validateUUID(electionEventId);
		this.verificationCardSetId = validateUUID(verificationCardSetId);
		checkNotNull(verificationCardSecretKeys).forEach(Preconditions::checkNotNull);
		this.verificationCardSecretKeys = List.copyOf(verificationCardSecretKeys);

		checkArgument(!this.verificationCardSecretKeys.isEmpty(), "There must be at least one verification card secret key.");

		final GroupVector<ElGamalMultiRecipientPrivateKey, ZqGroup> secretKeys = this.verificationCardSecretKeys.stream()
				.map(VerificationCardSecretKey::privateKey)
				.collect(GroupVector.toGroupVector());
		checkArgument(secretKeys.getGroup().hasSameOrderAs(encryptionGroup),
				"The verification card secret keys' group must be of same order as the encryption group.");

		final List<String> verificationCardIds = this.verificationCardSecretKeys.stream().map(VerificationCardSecretKey::verificationCardId).toList();
		checkArgument(hasNoDuplicates(verificationCardIds), "There must not be any duplicate verification card ids.");
	}

	@Override
	public List<VerificationCardSecretKey> verificationCardSecretKeys() {
		return List.copyOf(verificationCardSecretKeys);
	}
}
