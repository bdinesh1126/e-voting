/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api.online;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.application.service.EncryptedLongReturnCodeSharesService;
import ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetComputationService;
import ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetDownloadService;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.InvalidStatusTransitionException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * The voting card set end-point for the online SDM.
 */
@RestController
@RequestMapping("/sdm-backend/online/votingcardsets")
@Api(value = "Voting card set REST API")
public class VotingCardSetOnlineController {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetOnlineController.class);
	private final VotingCardSetDownloadService votingCardSetDownloadService;
	private final VotingCardSetComputationService votingCardSetComputationService;
	private final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService;
	private final boolean isVotingPortalEnabled;

	public VotingCardSetOnlineController(final VotingCardSetDownloadService votingCardSetDownloadService,
			final VotingCardSetComputationService votingCardSetComputationService,
			final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService,
			@Value("${voting.portal.enabled}")
			final boolean isVotingPortalEnabled) {
		this.votingCardSetDownloadService = votingCardSetDownloadService;
		this.votingCardSetComputationService = votingCardSetComputationService;
		this.encryptedLongReturnCodeSharesService = encryptedLongReturnCodeSharesService;
		this.isVotingPortalEnabled = isVotingPortalEnabled;
	}

	/**
	 * Changes the status of a list of voting card sets by performing the compute operation. The HTTP call uses a PUT request to the voting card set
	 * endpoint. If the requested status cannot be transitioned to computing from the current one, the call will fail.
	 *
	 * @param electionEventId the election event id.
	 * @param votingCardSetId the voting card set id.
	 * @return a list of ids of the created voting card sets.
	 */
	@PutMapping(value = "/electionevent/{electionEventId}/votingcardset/{votingCardSetId}" + "/compute", produces = "application/json")
	@ApiOperation(value = "Change the status of a voting card set to computing",
			notes = "Change the status of a voting card set to computing, performing the compute operation")
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Not Found") })
	public ResponseEntity<Object> setVotingCardSetStatusComputing(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId,
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String votingCardSetId) {

		validateUUID(electionEventId);
		validateUUID(votingCardSetId);

		ResponseEntity<Object> response;
		try {
			votingCardSetComputationService.compute(votingCardSetId, electionEventId);
			response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (final InvalidStatusTransitionException e) {
			LOGGER.info("Error trying to set voting card set status to computing. [electionEventId: {}, votingCardSetId: {}]", electionEventId,
					votingCardSetId, e);
			response = ResponseEntity.badRequest().build();
		}
		return response;
	}

	/**
	 * Check if computed choice codes are ready.
	 *
	 * @throws IOException if something fails checking if the control components finished the generation of the encrypted long Return Code Shares.
	 */
	@PostMapping(value = "/electionevent/{electionEventId}/status", produces = "application/json")
	@ResponseStatus(value = HttpStatus.OK)
	@ApiOperation(value = "Check if computed choice codes are ready and update its status", notes = "Service to check the status of computed choice codes.")
	public void updateChoiceCodesComputationStatus(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId) throws IOException {

		if (!isVotingPortalEnabled) {
			LOGGER.warn("The application is configured to not have connectivity to Voter Portal, check if this is the expected behavior");
			return;
		}

		validateUUID(electionEventId);

		encryptedLongReturnCodeSharesService.updateVotingCardSetsComputationStatus(electionEventId);
	}

	/**
	 * Changes the status of a list of voting card sets by performing the download operation. The HTTP call uses a PUT request to the voting card set
	 * endpoint. If the requested status cannot be transitioned to computing from the current one, the call will fail.
	 *
	 * @param electionEventId the election event id.
	 * @param votingCardSetId the voting card set id.
	 * @return a list of ids of the created voting card sets.
	 */
	@PutMapping(value = "/electionevent/{electionEventId}/votingcardset/{votingCardSetId}" + "/download", produces = "application/json")
	@ApiOperation(value = "Change the status of a voting card set to downloaded",
			notes = "Change the status of a voting card set to downloaded, performing the download operation")
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Not Found") })
	public ResponseEntity<Object> setVotingCardSetStatusDownloaded(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId,
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String votingCardSetId) throws IOException, ResourceNotFoundException {

		validateUUID(electionEventId);
		validateUUID(votingCardSetId);

		ResponseEntity<Object> response;
		try {
			votingCardSetDownloadService.download(votingCardSetId, electionEventId);
			response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (final InvalidStatusTransitionException e) {
			LOGGER.info("Error trying to set voting card set status to downloaded. [electionEventId: {}, votingCardSetId: {}]", electionEventId,
					votingCardSetId, e);
			response = ResponseEntity.badRequest().build();
		}
		return response;
	}
}
