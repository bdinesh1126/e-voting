/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.evotinglibraries.domain.tally.TallyComponentVotesPayload;

@Service
public class TallyComponentVotesService {

	private final TallyComponentVotesFileRepository tallyComponentVotesFileRepository;

	public TallyComponentVotesService(final TallyComponentVotesFileRepository tallyComponentVotesFileRepository) {
		this.tallyComponentVotesFileRepository = tallyComponentVotesFileRepository;
	}

	public void save(final TallyComponentVotesPayload payload) {
		checkNotNull(payload);

		final String electionEventId = payload.getElectionEventId();
		final String ballotId = payload.getBallotId();
		final String ballotBoxId = payload.getBallotBoxId();

		checkState(!tallyComponentVotesFileRepository.exists(electionEventId, ballotId, ballotBoxId),
				"Requested tally component votes payload already exists. [electionEventId: %s, ballotId: %s, ballotBoxId: %s]",
				electionEventId, ballotId, ballotBoxId);

		tallyComponentVotesFileRepository.save(payload);
	}

	public TallyComponentVotesPayload load(final String electionEventId, final String ballotId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotId);
		validateUUID(ballotBoxId);

		return tallyComponentVotesFileRepository.load(electionEventId, ballotId, ballotBoxId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Requested tally component votes payload is not present. [electionEventId: %s, ballotId: %s, ballotBoxId: %s]",
								electionEventId, ballotId, ballotBoxId)));
	}
}
