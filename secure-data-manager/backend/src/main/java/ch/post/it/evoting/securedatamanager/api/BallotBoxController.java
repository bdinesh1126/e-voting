/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;

import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.securedatamanager.services.application.service.BallotBoxService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * The ballot box end-point.
 */
@RestController
@RequestMapping("/sdm-backend/ballotboxes")
@Api(value = "Ballot Box REST API")
public class BallotBoxController {

	private final BallotBoxService ballotBoxService;

	public BallotBoxController(final BallotBoxService ballotBoxService) {
		this.ballotBoxService = ballotBoxService;
	}

	/**
	 * Returns an ballot box identified by election event and its id.
	 *
	 * @param electionEventId the election event id.
	 * @param ballotBoxId     the ballot box id.
	 * @return An ballot box identified by election event and its id.
	 */
	@GetMapping(value = "/electionevent/{electionEventId}/ballotbox/{ballotBoxId}", produces = "application/json")
	@ResponseBody
	@ApiOperation(value = "Get Ballot box", notes = "Service to retrieve a given ballot box.", response = String.class)
	public ResponseEntity<String> getBallotBox(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId,
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String ballotBoxId) {

		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		final JsonNode ballotBox = ballotBoxService.getBallotBox(ballotBoxId);

		if (!ballotBox.isEmpty()) {
			return new ResponseEntity<>(ballotBox.toString(), HttpStatus.OK);
		}

		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	/**
	 * Returns a list of all ballot boxes.
	 *
	 * @param electionEventId the election event id.
	 * @return The list of ballot boxes.
	 */
	@GetMapping(value = "/electionevent/{electionEventId}", produces = "application/json")
	@ResponseBody
	@ApiOperation(value = "List of ballot boxes", notes = "Service to retrieve the list of ballot boxes.", response = String.class)
	public String getBallotBoxes(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId) {

		validateUUID(electionEventId);

		return ballotBoxService.getBallotBoxes(electionEventId);
	}

	/**
	 * Change the state of the ballot box from ready to signed for a given election event and ballot box id.
	 *
	 * @param electionEventId the election event id.
	 * @param ballotBoxId     the ballot box id.
	 * @return HTTP status code 200 - If the ballot box is successfully signed. HTTP status code 404 - If the resource is not found. HTTP status code
	 * 412 - If the ballot box is already signed.
	 */
	@PutMapping(value = "/electionevent/{electionEventId}/ballotbox/{ballotBoxId}")
	@ApiOperation(value = "Sign ballot box", notes = "Service to change the state of the ballot box from ready to signed for a given election event and ballot box id.")
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 412, message = "Precondition Failed") })
	public ResponseEntity<Void> signBallotBox(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId,
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String ballotBoxId) {

		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		ballotBoxService.sign(ballotBoxId);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping(value = "/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/status", produces = "application/json")
	@ResponseBody
	public ResponseEntity<BallotBoxStatus> getBallotBoxStatus(
			@ApiParam(
					name = "electionEventId",
					type = "String",
					value = "Unique identifier for election event",
					example = "bf731ed5b4f24cf8a926d744015118ae",
					required = true)
			@PathVariable
			final String electionEventId,
			@ApiParam(
					name = "ballotBoxId",
					type = "String",
					value = "Unique identifier for ballot box",
					example = "51a21bbc884749fca630b8037cd29424",
					required = true)
			@PathVariable
			final String ballotBoxId) {

		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		final BallotBoxStatus ballotBoxStatus = ballotBoxService.getBallotBoxStatus(ballotBoxId);

		return new ResponseEntity<>(ballotBoxStatus, HttpStatus.OK);
	}

}
