/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.stringToInteger;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.google.common.annotations.VisibleForTesting;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionAttributesAliasConstants;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionObjectValidations;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.domain.validations.Validations;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientMessage;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.evotinglibraries.domain.common.Alphabet;
import ch.post.it.evoting.evotinglibraries.domain.common.StartVotingKeyAlphabet;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.commons.VerificationCardSet;
import ch.post.it.evoting.securedatamanager.sdmsetup.configuration.setupvoting.GenRandomSVKAlgorithm;
import ch.post.it.evoting.securedatamanager.services.application.service.BallotBoxService;

/**
 * Implements the GenVerDat algorithm described in the cryptographic protocol.
 */
@Service
@ConditionalOnProperty("role.isSetup")
public class GenVerDatAlgorithm {

	private final ElGamal elGamal;
	private final Hash hash;
	private final BallotConfigService ballotConfigService;
	private final Random random;
	private final BallotBoxService ballotBoxService;
	private final Base64 base64;
	private final GenRandomSVKAlgorithm genRandomSVKAlgorithm;

	public GenVerDatAlgorithm(
			final ElGamal elGamal,
			final Hash hash,
			final BallotConfigService ballotConfigService,
			final Random random,
			final BallotBoxService ballotBoxService,
			final Base64 base64,
			final GenRandomSVKAlgorithm genRandomSVKAlgorithm) {
		this.elGamal = elGamal;
		this.hash = hash;
		this.ballotConfigService = ballotConfigService;
		this.random = random;
		this.ballotBoxService = ballotBoxService;
		this.base64 = base64;
		this.genRandomSVKAlgorithm = genRandomSVKAlgorithm;
	}

	/**
	 * Initialize the control components' computation of the return codes.
	 *
	 * @param eligibleVoters       N<sub>E</sub>, the number of eligible voters. Must be strictly greater than 0.
	 * @param encodedVotingOptions p&#771;, the encoded voting options as prime numbers. Must be non-null.
	 * @param actualVotingOptions  v&#771;, the actual voting options. Must be non-null.
	 * @param semanticInformation  sigma, the semantic information. Must be non-null.
	 * @param verificationCardSet  the corresponding verification card set. Must be non-null.
	 * @param setupPublicKey       pk<sub>setup</sub>, the setup public key. Must be non-null.
	 * @return the generated verification data as a {@link GenVerDatOutput}.
	 * @throws NullPointerException      if any non-nullable input is null.
	 * @throws FailedValidationException if
	 *                                   <ul>
	 *                                    	<li>{@code actualVotingOptions} is not a valid xs:token or a concatenation of two valid xs:token joined
	 *                                    		with {@value ElectionAttributesAliasConstants#ALIAS_JOIN_DELIMITER}.</li>
	 *                                    	<li>{@code semanticInformation} is not a valid non blank UTF8 string.</li>
	 *                                   </ul>
	 * @throws IllegalArgumentException  if
	 *                                   <ul>
	 *                                       <li>{@code encodedVotingOptions}, {@code actualVotingOptions} or {@code semanticInformation} contains any null element.</li>
	 *                                       <li>{@code encodedVotingOptions}, {@code actualVotingOptions} and {@code semanticInformation} do not have the same size.</li>
	 *                                       <li>{@code eligibleVoters} is not strictly greater than 0.</li>
	 *                                       <li>The number of voting options is greater than the secret key length.</li>
	 *                                   </ul>
	 */
	@SuppressWarnings("java:S117")
	public GenVerDatOutput genVerDat(final int eligibleVoters, final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions,
			final List<String> actualVotingOptions, final List<String> semanticInformation, final VerificationCardSet verificationCardSet,
			final ElGamalMultiRecipientPublicKey setupPublicKey) {

		final int N_E = eligibleVoters;
		checkNotNull(encodedVotingOptions);
		checkNotNull(actualVotingOptions);
		checkNotNull(semanticInformation);
		actualVotingOptions.stream().parallel().forEach(ElectionObjectValidations::validateActualVotingOption);
		semanticInformation.stream().parallel().forEach(Validations::validateNonBlankUCS);

		final List<PrimeGqElement> p_tilde = List.copyOf(encodedVotingOptions);
		final List<String> v_tilde = List.copyOf(actualVotingOptions);
		final List<String> sigma = List.copyOf(semanticInformation);

		final ElGamalMultiRecipientPublicKey pk_setup = checkNotNull(setupPublicKey);
		final GqGroup gqGroup = setupPublicKey.getGroup();
		final String ee = checkNotNull(verificationCardSet).electionEventId();
		final int l_ID = Constants.BASE16_ID_LENGTH;
		final int l_SVK = Constants.SVK_LENGTH;
		final StartVotingKeyAlphabet A_SVK = StartVotingKeyAlphabet.getInstance();
		final int n = p_tilde.size();
		final int omega = pk_setup.size();

		checkArgument(eligibleVoters > 0, "The number of eligible voters must be strictly greater than 0.");

		checkArgument(n == v_tilde.size() && n == sigma.size(),
				"The encoded voting options, the actual voting options and the semantic information must have the same size [p_tilde_size: %s, v_tilde_size: %s, sigma_size: %s]",
				n, v_tilde.size(), sigma.size());

		// Require.
		checkArgument(n <= omega, "The number of voting options must be smaller than or equal to the setup public key length.");

		final ZqGroup zqGroup = ZqGroup.sameOrderAs(gqGroup);
		final CombinedCorrectnessInformation combinedCorrectnessInformation = new CombinedCorrectnessInformation(getBallot(verificationCardSet));

		// Output variables.
		record VerificationData(String vc_id,
								String SVK_id,
								ElGamalMultiRecipientKeyPair keyPair_id,
								List<String> L_pCC_id,
								String BCK_id,
								ElGamalMultiRecipientCiphertext c_pCC_id,
								ElGamalMultiRecipientCiphertext c_ck_id) {
		}

		// Algorithm.
		final List<VerificationData> verificationData = IntStream.range(0, N_E).parallel()
				.mapToObj(id -> {
					final String vc_id = random.genRandomBase16String(l_ID);
					final String SVK_id = genRandomSVKAlgorithm.genRandomSVK(l_SVK, A_SVK);
					final ElGamalMultiRecipientKeyPair K_id_k_id = ElGamalMultiRecipientKeyPair.genKeyPair(gqGroup, 1, random);

					// Compute hpCC_id.
					final List<GqElement> hpCC_id_elements = new ArrayList<>();
					final ZqElement k_id = K_id_k_id.getPrivateKey().get(0);
					final List<String> L_pCC_id = new ArrayList<>();
					for (int k = 0; k < n; k++) {
						final PrimeGqElement p_k_tilde = p_tilde.get(k);
						final GqElement pCC_id_k = p_k_tilde.exponentiate(k_id);
						final GqElement hpCC_id_k = hash.hashAndSquare(pCC_id_k.getValue(), gqGroup);
						final String ci = combinedCorrectnessInformation.getCorrectnessIdForVotingOptionIndex(k);
						final byte[] lpCC_id_k = hash.recursiveHash(hpCC_id_k, HashableString.from(vc_id), HashableString.from(ee),
								HashableString.from(ci));
						L_pCC_id.add(base64.base64Encode(lpCC_id_k));

						hpCC_id_elements.add(hpCC_id_k);
					}
					final ElGamalMultiRecipientMessage hpCC_id = new ElGamalMultiRecipientMessage(GroupVector.from(hpCC_id_elements));

					// Compute c_pCC_id.
					final ZqElement hpCC_id_exponent = ZqElement.create(random.genRandomInteger(gqGroup.getQ()), zqGroup);
					final ElGamalMultiRecipientCiphertext c_pCC_id = elGamal.getCiphertext(hpCC_id, hpCC_id_exponent, pk_setup);

					// Generate BCK_id.
					final int l_BCK = 9;
					String BCK_id;

					do {
						BCK_id = random.genUniqueDecimalStrings(l_BCK, 1).get(0);
					} while (stringToInteger(BCK_id).equals(BigInteger.ZERO));

					// Compute c_ck_id.
					final GqElement hBCK_id = hash.hashAndSquare(stringToInteger(BCK_id), gqGroup);
					final GqElement CK_id = hBCK_id.exponentiate(k_id);

					final ElGamalMultiRecipientMessage hCK_id = new ElGamalMultiRecipientMessage(
							GroupVector.of(hash.hashAndSquare(CK_id.getValue(), gqGroup)));

					final ZqElement hCKExponent = ZqElement.create(random.genRandomInteger(gqGroup.getQ()), zqGroup);
					final ElGamalMultiRecipientCiphertext c_ck_id = elGamal.getCiphertext(hCK_id, hCKExponent, pk_setup);

					return new VerificationData(vc_id, SVK_id, K_id_k_id, L_pCC_id, BCK_id, c_pCC_id, c_ck_id);
				})
				.toList();

		List<String> L_pCC = verificationData.stream().flatMap(v -> v.L_pCC_id.stream()).toList();
		L_pCC = order(new ArrayList<>(L_pCC));
		final List<PrimesMappingTableEntry> pTable_entries = IntStream.rangeClosed(0, n - 1)
				.mapToObj(i -> new PrimesMappingTableEntry(v_tilde.get(i), p_tilde.get(i), sigma.get(i)))
				.toList();

		// Outputs.
		final List<String> vc = verificationData.stream().map(VerificationData::vc_id).toList();
		final List<String> SVK = verificationData.stream().map(VerificationData::SVK_id).toList();
		final List<String> BCK = verificationData.stream().map(VerificationData::BCK_id).toList();
		final List<ElGamalMultiRecipientCiphertext> c_pCC = verificationData.stream().map(VerificationData::c_pCC_id)
				.toList();
		final List<ElGamalMultiRecipientCiphertext> c_ck = verificationData.stream().map(VerificationData::c_ck_id)
				.toList();

		// The object regroups the public and secret keys of the verification card key pair
		final List<ElGamalMultiRecipientKeyPair> verificationCardKeyPairs = verificationData.stream().map(VerificationData::keyPair_id).toList();

		return new GenVerDatOutput.Builder()
				.setVerificationCardIds(vc)
				.setStartVotingKeys(SVK)
				.setVerificationCardKeyPairs(verificationCardKeyPairs)
				.setPartialChoiceReturnCodesAllowList(L_pCC)
				.setBallotCastingKeys(BCK)
				.setEncryptedHashedPartialChoiceReturnCodes(GroupVector.from(c_pCC))
				.setEncryptedHashedConfirmationKeys(GroupVector.from(c_ck))
				.setPTable(PrimesMappingTable.from(pTable_entries))
				.build();
	}

	/**
	 * Orders a list of strings lexicographically.
	 *
	 * @param toOrder the list to order.
	 * @return the lexicographically ordered list of strings.
	 */
	@VisibleForTesting
	protected List<String> order(final List<String> toOrder) {
		Collections.sort(toOrder);

		return toOrder;
	}

	private Ballot getBallot(final VerificationCardSet precomputeContext) {
		final String ballotId = ballotBoxService.getBallotId(precomputeContext.ballotBoxId());
		return ballotConfigService.getBallot(precomputeContext.electionEventId(), ballotId);
	}

}

