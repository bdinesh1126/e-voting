/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkState;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.domain.configuration.setupvoting.ComputingStatus;
import ch.post.it.evoting.securedatamanager.commons.RemoteService;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.InvalidStatusTransitionException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.SetupComponentVerificationDataPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.MessageBrokerOrchestratorClient;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;

import retrofit2.Response;

/**
 * This is an application service that deals with the computation of voting card data.
 */
@Service
public class VotingCardSetComputationService extends BaseVotingCardSetService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetComputationService.class);

	private final IdleStatusService idleStatusService;
	private final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient;
	private final ConfigurationEntityStatusService configurationEntityStatusService;
	private final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService;
	private final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository;

	public VotingCardSetComputationService(final IdleStatusService idleStatusService,
			final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService,
			final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository) {
		this.idleStatusService = idleStatusService;
		this.messageBrokerOrchestratorClient = messageBrokerOrchestratorClient;
		this.configurationEntityStatusService = configurationEntityStatusService;
		this.encryptedLongReturnCodeSharesService = encryptedLongReturnCodeSharesService;
		this.setupComponentVerificationDataPayloadFileRepository = setupComponentVerificationDataPayloadFileRepository;
	}

	/**
	 * Compute a voting card set.
	 *
	 * @param votingCardSetId the identifier of the voting card set
	 * @param electionEventId the identifier of the election event
	 * @throws InvalidStatusTransitionException if the original status does not allow computing
	 */
	public void compute(final String votingCardSetId, final String electionEventId)
			throws InvalidStatusTransitionException {

		if (!idleStatusService.getIdLock(votingCardSetId)) {
			return;
		}

		try {
			LOGGER.info("Starting computation of voting card set {}...", votingCardSetId);

			validateUUID(votingCardSetId);
			validateUUID(electionEventId);

			final ComputingStatus toStatus = ComputingStatus.COMPUTING;

			final String verificationCardSetId = votingCardSetRepository.getVerificationCardSetId(votingCardSetId);

			final int chunkCount = setupComponentVerificationDataPayloadFileRepository.getCount(electionEventId, verificationCardSetId);

			final List<Integer> queuedChunkIds = getQueuedComputeChunkIds(electionEventId, verificationCardSetId);
			if (!queuedChunkIds.isEmpty()) {
				final String queuedChunkIdsString = queuedChunkIds.stream().sorted().map(Object::toString).collect(Collectors.joining(","));
				LOGGER.warn(
						"Ignoring some chunks as they were already queued for computation. [electionEventId: {}, verificationCardSetId: {}, chunkIds: {}]",
						electionEventId, verificationCardSetId, queuedChunkIdsString);
			}

			IntStream.range(0, chunkCount)
					.filter(chunkId -> !queuedChunkIds.contains(chunkId))
					.parallel()
					.forEach(chunkId -> {
								// Retrieve the payload.
								final SetupComponentVerificationDataPayload payload = setupComponentVerificationDataPayloadFileRepository.retrieve(
										electionEventId, verificationCardSetId, chunkId);

								// Send chunk for processing.
								encryptedLongReturnCodeSharesService.computeGenEncLongCodeShares(payload);

								LOGGER.info(
										"Successfully requested GenEncLongCodeShares chunk computation. [electionEventId: {}, verificationCardSetId: {}, chunkId: {}]",
										electionEventId, verificationCardSetId, chunkId);
							}
					);

			LOGGER.info("Successfully requested all GenEncLongCodeShares computations. [electionEventId: {}, verificationCardSetId: {}]",
					electionEventId, verificationCardSetId);

			// All chunks have been sent, update status.
			configurationEntityStatusService.update(toStatus.name(), votingCardSetId, votingCardSetRepository);
			LOGGER.info("Computation of voting card set {} started", votingCardSetId);

		} finally {
			idleStatusService.freeIdLock(votingCardSetId);
		}
	}

	private List<Integer> getQueuedComputeChunkIds(final String electionEventId, final String verificationCardSetId) {
		final Response<List<Integer>> response = RemoteService.call(
						messageBrokerOrchestratorClient.queuedComputeChunkIds(electionEventId, verificationCardSetId))
				.networkErrorMessage("Failed to communicate with orchestrator")
				.unsuccessfulResponseErrorMessage(
						"Request for queued compute chunk ids unsuccessful. [electionEventId: %s, verificationCardSetId: %s]", electionEventId,
						verificationCardSetId)
				.execute();

		LOGGER.debug("Successfully requested queued compute chunk ids. [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);

		final List<Integer> queuedComputeChunkIds = response.body();
		checkState(Objects.nonNull(queuedComputeChunkIds),
				"Queued compute chunk ids cannot be null. [electionEventId: %s, verificationCardSetId: %s]", electionEventId, verificationCardSetId);

		return queuedComputeChunkIds;
	}
}
