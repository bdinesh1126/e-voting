/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.SetupComponentVerificationCardKeystoresPayload;

/**
 * Allows performing operations with the setup component verification card keystores payload. The payload is persisted/retrieved to/from the file
 * system of the SDM, in its workspace.
 */
@Service
public class SetupComponentVerificationCardKeystoresPayloadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentVerificationCardKeystoresPayloadService.class);

	private final SetupComponentVerificationCardKeystoresPayloadFileRepository setupComponentVerificationCardKeystoresPayloadFileRepository;
	private final SetupComponentVerificationCardKeystoresPayloadUploadRepository setupComponentVerificationCardKeystoresPayloadUploadRepository;

	public SetupComponentVerificationCardKeystoresPayloadService(
			final SetupComponentVerificationCardKeystoresPayloadFileRepository setupComponentVerificationCardKeystoresPayloadFileRepository,
			final SetupComponentVerificationCardKeystoresPayloadUploadRepository setupComponentVerificationCardKeystoresPayloadUploadRepository) {
		this.setupComponentVerificationCardKeystoresPayloadFileRepository = setupComponentVerificationCardKeystoresPayloadFileRepository;
		this.setupComponentVerificationCardKeystoresPayloadUploadRepository = setupComponentVerificationCardKeystoresPayloadUploadRepository;
	}

	/**
	 * Saves a setup component verification card keystores payload for the given {@code electionEventId} and {@code verificationCardSetId}.
	 *
	 * @param payload the setup component verification card keystores payload to save.
	 * @throws FailedValidationException if any of the ids are invalid.
	 * @throws NullPointerException      if any of {@code electionEventId}, {@code verificationCardSetId} or {@code value} is null.
	 * @throws IllegalStateException     if the verification card secret key cannot be saved.
	 */
	public void save(final SetupComponentVerificationCardKeystoresPayload payload) {
		checkNotNull(payload);

		setupComponentVerificationCardKeystoresPayloadFileRepository.save(payload);

		LOGGER.info("Successfully persisted setup component verification card keystores payload. [electionEventId: {}, verificationCardSetId: {}]",
				payload.getElectionEventId(), payload.getVerificationCardSetId());
	}

	/**
	 * Checks if the setup component verification card keystores payload is present for the given election event id.
	 *
	 * @param electionEventId       the election event id to check. Must be non-null and a valid UUID.
	 * @param verificationCardSetId the verification card set id. Must be non-null and a valid UUID.
	 * @return {@code true} if the setup component verification card keystores payload is present, {@code false} otherwise.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if any input is not a valid UUID.
	 */
	public boolean exist(final String electionEventId, final String verificationCardSetId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		return setupComponentVerificationCardKeystoresPayloadFileRepository.existsById(electionEventId, verificationCardSetId);
	}

	/**
	 * Loads the setup component verification card keystores payload for the given {@code electionEventId} and {@code verificationCardSetId}.
	 *
	 * @param electionEventId       the payload's election event id.
	 * @param verificationCardSetId the payload's verification card set id.
	 * @return the setup component verification card keystores payload for the given ids.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if any input is not a valid UUID.
	 */
	public SetupComponentVerificationCardKeystoresPayload load(final String electionEventId, final String verificationCardSetId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		final SetupComponentVerificationCardKeystoresPayload setupComponentVerificationCardKeystoresPayload =
				setupComponentVerificationCardKeystoresPayloadFileRepository.findById(electionEventId, verificationCardSetId)
						.orElseThrow(() -> new IllegalStateException(String.format(
								"Requested setup component verification card keystores payload is not present. [electionEventId: %s, verificationCardSetId: %s]",
								electionEventId, verificationCardSetId)));

		LOGGER.info("Loaded setup component verification card keystores payload. [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);

		return setupComponentVerificationCardKeystoresPayload;
	}

	/**
	 * Uploads the list of setup component verification card keystores payloads to the voting-server.
	 *
	 * @param setupComponentVerificationCardKeystoresPayloads the list of setup component verification card keystores payloads. Must be non-null.
	 * @throws NullPointerException if the input is null.
	 */
	public void upload(final List<SetupComponentVerificationCardKeystoresPayload> setupComponentVerificationCardKeystoresPayloads) {
		final List<SetupComponentVerificationCardKeystoresPayload> setupComponentVerificationCardKeystoresPayloadsCopy =
				List.copyOf(checkNotNull(setupComponentVerificationCardKeystoresPayloads));

		setupComponentVerificationCardKeystoresPayloadsCopy.forEach(setupComponentVerificationCardKeystoresPayload -> {
			final String electionEventId = setupComponentVerificationCardKeystoresPayload.getElectionEventId();
			final String verificationCardSetId = setupComponentVerificationCardKeystoresPayload.getVerificationCardSetId();

			LOGGER.info("Uploading setup component verification card keystores payload... [electionEventId: {}, verificationCardSetId: {}]",
					electionEventId, verificationCardSetId);

			setupComponentVerificationCardKeystoresPayloadUploadRepository.upload(setupComponentVerificationCardKeystoresPayload);

			LOGGER.info("Successfully uploaded setup component verification card keystores payload. [electionEventId: {}, verificationCardSetId: {}]",
					electionEventId, verificationCardSetId);
		});

	}
}
