/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.security.SignatureException;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionEventContext;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.SignedPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.securedatamanager.configuration.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.configuration.PrimesMappingTableService;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.BallotConfigService;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballotbox.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

@Service
@ConditionalOnProperty("role.isSetup")
public class ElectionEventContextPersistenceService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElectionEventContextPersistenceService.class);

	private final BallotBoxRepository ballotBoxRepository;
	private final BallotConfigService ballotConfigService;
	private final ElectionEventService electionEventService;
	private final VotingCardSetRepository votingCardSetRepository;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final PrimesMappingTableService primesMappingTableService;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;

	public ElectionEventContextPersistenceService(
			final BallotBoxRepository ballotBoxRepository,
			final BallotConfigService ballotConfigService,
			final ElectionEventService electionEventService,
			final VotingCardSetRepository votingCardSetRepository,
			@Qualifier("keystoreServiceSdmConfig")
			final SignatureKeystore<Alias> signatureKeystoreService,
			final PrimesMappingTableService primesMappingTableService,
			final ElectionEventContextPayloadService electionEventContextPayloadService) {
		this.ballotBoxRepository = ballotBoxRepository;
		this.ballotConfigService = ballotConfigService;
		this.electionEventService = electionEventService;
		this.votingCardSetRepository = votingCardSetRepository;
		this.signatureKeystoreService = signatureKeystoreService;
		this.primesMappingTableService = primesMappingTableService;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
	}

	/**
	 * Persists the election event context.
	 *
	 * @param electionEventId the election event id for which to persist the context.
	 */
	public void persist(final String electionEventId) {
		validateUUID(electionEventId);

		LOGGER.debug("Building Verification Card Set Contexts... [electionEventId: {}]", electionEventId);

		final List<VerificationCardSetContext> verificationCardSetContexts = buildVerificationCardSetContexts(electionEventId);

		final String electionEventAlias = electionEventService.getElectionEventAlias(electionEventId);
		final String electionEventDescription = electionEventService.getElectionEventDescription(electionEventId);
		final LocalDateTime startTime = electionEventService.getDateFrom(electionEventId);
		final LocalDateTime finishTime = electionEventService.getDateTo(electionEventId);

		final ElectionEventContext electionEventContext = new ElectionEventContext(electionEventId, electionEventAlias, electionEventDescription,
				verificationCardSetContexts, startTime, finishTime);
		final GqGroup encryptionGroup = verificationCardSetContexts.get(0).primesMappingTable().getPTable().getGroup();
		final ElectionEventContextPayload electionEventContextPayload = createElectionEventContextPayload(encryptionGroup, electionEventContext);
		electionEventContextPayloadService.save(electionEventContextPayload);

		LOGGER.debug("Built Verification Card Set Contexts. [electionEventId: {}]", electionEventId);
	}

	/**
	 * Builds the list of {@link VerificationCardSetContext} for the given election event id.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @return the list of {@link VerificationCardSetContext}.
	 * @throws NullPointerException      if the input is null.
	 * @throws FailedValidationException if the input is not a valid UUID.
	 */
	private List<VerificationCardSetContext> buildVerificationCardSetContexts(final String electionEventId) {
		validateUUID(electionEventId);

		final List<String> votingCardSetIds = votingCardSetRepository.findAllVotingCardSetIds(electionEventId);

		return votingCardSetIds.stream().parallel()
				.map(votingCardSetId -> {
					final String verificationCardSetId = votingCardSetRepository.getVerificationCardSetId(votingCardSetId);
					final String votingCardSetAlias = votingCardSetRepository.getVotingCardSetAlias(votingCardSetId);
					final String votingCardSetDefaultDescription = votingCardSetRepository.getVotingCardSetDefaultDescription(votingCardSetId);
					final String ballotBoxId = votingCardSetRepository.getBallotBoxId(votingCardSetId);
					final LocalDateTime ballotBoxDateFrom = ballotBoxRepository.getDateFrom(ballotBoxId);
					final LocalDateTime ballotBoxDateTo = ballotBoxRepository.getDateTo(ballotBoxId);
					final String ballotId = ballotBoxRepository.getBallotId(ballotBoxId);
					final Ballot ballot = ballotConfigService.getBallot(electionEventId, ballotId);

					final boolean testBallotBox = ballotBoxRepository.isTestBallotBox(ballotBoxId);
					final int numberOfWriteInFields = new CombinedCorrectnessInformation(ballot).getTotalNumberOfWriteInOptions();
					final int gracePeriod = ballotBoxRepository.getGracePeriod(ballotBoxId);
					final PrimesMappingTable primesMappingTable = primesMappingTableService.load(electionEventId, verificationCardSetId);

					final int numberOfVotingCards;
					try {
						numberOfVotingCards = votingCardSetRepository.getNumberOfVotingCards(electionEventId, votingCardSetId);
					} catch (final ResourceNotFoundException e) {
						throw new IllegalStateException(
								String.format("Number of voting cards not found. [electionEventId: %s, votingCardSetId: %s]", electionEventId,
										votingCardSetId));
					}

					return new VerificationCardSetContext(verificationCardSetId, votingCardSetAlias, votingCardSetDefaultDescription, ballotBoxId,
							ballotBoxDateFrom, ballotBoxDateTo, testBallotBox, numberOfWriteInFields,
							numberOfVotingCards, gracePeriod, primesMappingTable);
				})
				.toList();
	}

	private ElectionEventContextPayload createElectionEventContextPayload(final GqGroup group, final ElectionEventContext electionEventContext) {
		final String electionEventId = electionEventContext.electionEventId();
		final ElectionEventContextPayload electionEventContextPayload = new ElectionEventContextPayload(group, electionEventContext);

		final Hashable additionalContextData = ChannelSecurityContextData.electionEventContext(electionEventId);

		final CryptoPrimitivesSignature electionEventContextPayloadSignature = getPayloadSignature(electionEventContextPayload,
				additionalContextData);
		electionEventContextPayload.setSignature(electionEventContextPayloadSignature);

		return electionEventContextPayload;
	}

	private CryptoPrimitivesSignature getPayloadSignature(final SignedPayload payload, final Hashable additionalContextData) {

		final byte[] signature;
		try {
			signature = signatureKeystoreService.generateSignature(payload, additionalContextData);
		} catch (final SignatureException e) {
			throw new IllegalStateException(String.format("Failed to generate payload signature. [name: %s]", payload.getClass().getName()));
		}

		return new CryptoPrimitivesSignature(signature);
	}

}
