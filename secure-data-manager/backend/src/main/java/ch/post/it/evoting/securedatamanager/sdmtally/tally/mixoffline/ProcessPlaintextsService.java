/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_WRITE_IN_OPTION_LENGTH;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.security.SignatureException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.TallyComponentShufflePayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientMessage;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.tally.TallyComponentVotesPayload;

/**
 * Handles the processing plaintexts step of the offline mixing.
 */
@Service
@ConditionalOnProperty("role.isTally")
public class ProcessPlaintextsService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessPlaintextsService.class);

	private final ProcessPlaintextsAlgorithm processPlaintextsAlgorithm;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final TallyComponentVotesService tallyComponentVotesService;
	private final EncryptionParametersTallyService encryptionParametersTallyService;

	public ProcessPlaintextsService(final ProcessPlaintextsAlgorithm processPlaintextsAlgorithm,
			@Qualifier("keystoreServiceSdmTally")
			final SignatureKeystore<Alias> signatureKeystoreService,
			final TallyComponentVotesService tallyComponentVotesService,
			final EncryptionParametersTallyService encryptionParametersTallyService) {
		this.processPlaintextsAlgorithm = processPlaintextsAlgorithm;
		this.signatureKeystoreService = signatureKeystoreService;
		this.tallyComponentVotesService = tallyComponentVotesService;
		this.encryptionParametersTallyService = encryptionParametersTallyService;
	}

	/**
	 * Factorizes each plaintext vote and saves the list of selected encoded voting options.
	 *
	 * @param electionEventId                 the identifier of the election. Must be non-null and a valid UUID.
	 * @param ballotId                        the identifier of the ballot. Must be non-null and a valid UUID.
	 * @param ballotBoxId                     the identifier of the ballot box. Must be non-null and a valid UUID.
	 * @param numberOfSelectableVotingOptions the number of selectable voting options. Must be between 1 and 120 included.
	 * @param numberOfAllowedWriteInsPlusOne  the number of write-ins + 1. Must be greater or equal to 1.
	 * @param finalPayload                    the tally component shuffle payload. Must be non-null.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if any of the IDs is not a valid UUID.
	 * @throws IllegalArgumentException  if number of write-ins + 1 is strictly smaller than 1 or the number of selectable voting options is not in
	 *                                   range [1, 120].
	 */
	public void processPlaintexts(final String electionEventId, final String ballotId, final String ballotBoxId,
			final int numberOfSelectableVotingOptions, final int numberOfAllowedWriteInsPlusOne, final TallyComponentShufflePayload finalPayload,
			final PrimesMappingTable primesMappingTable, final GroupVector<PrimeGqElement, GqGroup> writeInVotingOptions) {

		validateUUID(electionEventId);
		validateUUID(ballotId);
		validateUUID(ballotBoxId);
		checkNotNull(finalPayload);
		checkNotNull(primesMappingTable);

		checkArgument(numberOfAllowedWriteInsPlusOne >= 1, "The number of allowed write-ins + 1 must be at least 1.");
		checkArgument(numberOfSelectableVotingOptions >= 1,
				"The number of selectable voting options must be greater or equal to 1. [numberOfSelectableVotingOptions: %s]",
				numberOfSelectableVotingOptions);
		checkArgument(numberOfSelectableVotingOptions <= MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS,
				"The number of selectable voting options must be smaller or equal to %s. [numberOfSelectableVotingOptions: %s]",
				MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS, numberOfSelectableVotingOptions);

		checkArgument(electionEventId.equals(finalPayload.getElectionEventId()),
				"The final payload's election event ID must be the same as the given election event ID.");
		checkArgument(ballotBoxId.equals(finalPayload.getBallotBoxId()),
				"The final payload's ballot box ID must be the same as the given election event ID.");
		checkArgument(finalPayload.getEncryptionGroup().equals(primesMappingTable.getPTable().getGroup()),
				"The final payload and primes mapping table must have the same group.");

		final GroupVector<ElGamalMultiRecipientMessage, GqGroup> decryptedVotes = finalPayload.getVerifiablePlaintextDecryption().getDecryptedVotes();
		final GqGroup encryptionGroup = encryptionParametersTallyService.loadEncryptionGroup(electionEventId);
		final ProcessPlaintextsContext processPlaintextsContext = new ProcessPlaintextsContext(encryptionGroup, primesMappingTable);
		final ProcessPlaintextsInput processPlaintextsInput = new ProcessPlaintextsInput.Builder()
				.setPlaintextVotes(decryptedVotes)
				.setWriteInVotingOptions(writeInVotingOptions)
				.setNumberOfSelectableVotingOptions(numberOfSelectableVotingOptions)
				.setNumberOfAllowedWriteInsPlusOne(numberOfAllowedWriteInsPlusOne)
				.build();

		final ProcessPlaintextsAlgorithmOutput processPlaintextsAlgorithmOutput = processPlaintextsAlgorithm.processPlaintexts(
				processPlaintextsContext, processPlaintextsInput);

		final GroupVector<GroupVector<PrimeGqElement, GqGroup>, GqGroup> encodedSelectedVotingOptions = processPlaintextsAlgorithmOutput.L_votes();
		final List<List<String>> actualSelectedVotingOptions = processPlaintextsAlgorithmOutput.L_decodedVotes();
		final List<List<String>> decodedWriteInVotes = processPlaintextsAlgorithmOutput.L_writeIns();

		final TallyComponentVotesPayload tallyComponentVotesPayload = createTallyComponentVotesPayload(electionEventId, ballotId, ballotBoxId,
				decryptedVotes.getGroup(), encodedSelectedVotingOptions, actualSelectedVotingOptions, decodedWriteInVotes);
		tallyComponentVotesService.save(tallyComponentVotesPayload);
		LOGGER.info("Persisted tally component votes. [electionEventId: {}, ballotId: {}, ballotBoxId: {}]", electionEventId, ballotId, ballotBoxId);
	}

	private TallyComponentVotesPayload createTallyComponentVotesPayload(final String electionEventId, final String ballotId, final String ballotBoxId,
			final GqGroup encryptionGroup, final GroupVector<GroupVector<PrimeGqElement, GqGroup>, GqGroup> encodedSelectedVotingOptions,
			final List<List<String>> actualSelectedVotingOptions, final List<List<String>> decodedWriteInVotes) {

		final List<List<String>> sanitizedDecodedWriteInVotes = decodedWriteInVotes.stream()
				.map(decodedWriteInVote -> decodedWriteInVote.stream()
						.map(decodedWriteIn -> {
							if (decodedWriteIn.length() > MAXIMUM_WRITE_IN_OPTION_LENGTH) {
								LOGGER.warn("Write-in voting option with length exceeding maximum. "
												+ "It has been truncated to maximum supported size. [maximum: {}, write-in's length: {}]",
										MAXIMUM_WRITE_IN_OPTION_LENGTH, decodedWriteIn.length());
								return decodedWriteIn.substring(0, MAXIMUM_WRITE_IN_OPTION_LENGTH);
							}
							return decodedWriteIn;
						})
						.toList())
				.toList();

		final TallyComponentVotesPayload tallyComponentVotesPayload = new TallyComponentVotesPayload(electionEventId, ballotId, ballotBoxId,
				encryptionGroup, encodedSelectedVotingOptions, actualSelectedVotingOptions, sanitizedDecodedWriteInVotes);

		final CryptoPrimitivesSignature signature = getPayloadSignature(tallyComponentVotesPayload);
		tallyComponentVotesPayload.setSignature(signature);

		return tallyComponentVotesPayload;
	}

	private CryptoPrimitivesSignature getPayloadSignature(final TallyComponentVotesPayload payload) {
		final String electionEventId = payload.getElectionEventId();
		final String ballotBoxId = payload.getBallotBoxId();

		final Hashable additionalContextData = ChannelSecurityContextData.tallyComponentVotes(electionEventId, ballotBoxId);

		try {
			return new CryptoPrimitivesSignature(signatureKeystoreService.generateSignature(payload, additionalContextData));
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format(
							"Could not generate the tally component votes payload signature. [electionEventId: %s, ballotId: %s, ballotBoxId: %s]",
							electionEventId, payload.getBallotId(), ballotBoxId));
		}
	}
}
