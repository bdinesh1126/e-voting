/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;

public record VerificationCardSecretKey(String verificationCardId,
										ElGamalMultiRecipientPrivateKey privateKey
) {
	public VerificationCardSecretKey {
		validateUUID(verificationCardId);
		checkNotNull(privateKey);
	}
}
