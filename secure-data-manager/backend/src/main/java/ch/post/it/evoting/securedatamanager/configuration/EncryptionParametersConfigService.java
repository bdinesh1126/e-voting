/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkState;

import java.security.SignatureException;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.EncryptionParametersPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.securedatamanager.EncryptionParametersPayloadFileRepository;

/**
 * Allows loading the encryption parameters associated to an election event.
 */
@Service
@ConditionalOnProperty("role.isSetup")
public class EncryptionParametersConfigService {

	private final EncryptionParametersPayloadFileRepository encryptionParametersPayloadFileRepository;
	private final SignatureKeystore<Alias> signatureKeystore;

	public EncryptionParametersConfigService(final EncryptionParametersPayloadFileRepository encryptionParametersPayloadFileRepository,
			@Qualifier("keystoreServiceSdmConfig")
			final SignatureKeystore<Alias> signatureKeystore) {
		this.encryptionParametersPayloadFileRepository = encryptionParametersPayloadFileRepository;
		this.signatureKeystore = signatureKeystore;
	}

	/**
	 * Loads the encryption parameters for the given {@code electionEventId}. The result of this method is cached.
	 *
	 * @param electionEventId the election event id for which to get the encryption parameters.
	 * @return the encryption parameters.
	 * @throws FailedValidationException if {@code electionEventId} is invalid.
	 * @throws IllegalStateException     if the encryption parameters are not found for this {@code electionEventId}.
	 */
	@Cacheable("gqGroups")
	public GqGroup loadEncryptionGroup(final String electionEventId) {
		validateUUID(electionEventId);

		final EncryptionParametersPayload encryptionParametersPayload = encryptionParametersPayloadFileRepository.findById(electionEventId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Requested encryption parameters payload is not present. [electionEventId: %s]", electionEventId)));

		validateSignature(encryptionParametersPayload);

		return encryptionParametersPayload.getEncryptionGroup();
	}

	private void validateSignature(final EncryptionParametersPayload encryptionParametersPayload) {
		final CryptoPrimitivesSignature signature = encryptionParametersPayload.getSignature();

		checkState(signature != null, "The signature of the encryption parameters payload is null.");

		try {
			final boolean isSignatureValid = signatureKeystore.verifySignature(Alias.SDM_CONFIG, encryptionParametersPayload,
					ChannelSecurityContextData.setupComponentEncryptionParameters(),
					encryptionParametersPayload.getSignature().signatureContents());

			if (!isSignatureValid) {
				throw new InvalidPayloadSignatureException(EncryptionParametersPayload.class);
			}
		} catch (final SignatureException e) {
			throw new IllegalStateException("Unable to verify signature of EncryptionParametersPayload", e);
		}
	}

}
