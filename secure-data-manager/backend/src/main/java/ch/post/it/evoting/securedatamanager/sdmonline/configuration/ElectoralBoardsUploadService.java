/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmonline.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.SetupComponentVerificationCardKeystoresPayload;
import ch.post.it.evoting.securedatamanager.commons.JsonUtils;
import ch.post.it.evoting.securedatamanager.configuration.SetupComponentPublicKeysPayloadService;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.SetupComponentVerificationCardKeystoresPayloadService;
import ch.post.it.evoting.securedatamanager.services.application.exception.DatabaseException;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.Status;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.SynchronizeStatus;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.electoralboard.ElectoralBoardRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.electoralboard.ElectoralBoardUploadRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

/**
 * Service which uploads files to voter portal after creating the electoral board
 */
@Service
public class ElectoralBoardsUploadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElectoralBoardsUploadService.class);
	private final ElectoralBoardRepository electoralBoardRepository;
	private final ElectoralBoardUploadRepository electoralBoardUploadRepository;
	private final SetupComponentPublicKeysPayloadService setupComponentPublicKeysPayloadService;
	private final VotingCardSetRepository votingCardSetRepository;
	private final SetupComponentVerificationCardKeystoresPayloadService setupComponentVerificationCardKeystoresPayloadService;
	private final boolean isVotingPortalEnabled;

	public ElectoralBoardsUploadService(
			final ElectoralBoardRepository electoralBoardRepository,
			final ElectoralBoardUploadRepository electoralBoardUploadRepository,
			final SetupComponentPublicKeysPayloadService setupComponentPublicKeysPayloadService,
			final VotingCardSetRepository votingCardSetRepository,
			final SetupComponentVerificationCardKeystoresPayloadService setupComponentVerificationCardKeystoresPayloadService,
			@Value("${voting.portal.enabled}")
			final boolean isVotingPortalEnabled) {
		this.electoralBoardRepository = electoralBoardRepository;
		this.electoralBoardUploadRepository = electoralBoardUploadRepository;
		this.setupComponentPublicKeysPayloadService = setupComponentPublicKeysPayloadService;
		this.votingCardSetRepository = votingCardSetRepository;
		this.setupComponentVerificationCardKeystoresPayloadService = setupComponentVerificationCardKeystoresPayloadService;
		this.isVotingPortalEnabled = isVotingPortalEnabled;
	}

	/**
	 * Uploads the available electoral boards texts to the voter portal.
	 */
	public void uploadSynchronizableElectoralBoards(final String electionEventId) {
		validateUUID(electionEventId);
		checkState(isVotingPortalEnabled, "The voting-portal connection is not enabled.");

		final Map<String, Object> params = new HashMap<>();
		params.put(JsonConstants.STATUS, Status.SIGNED.name());
		params.put(JsonConstants.SYNCHRONIZED, SynchronizeStatus.PENDING.getIsSynchronized().toString());
		params.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);

		final String documents = electoralBoardRepository.list(params);
		final JsonArray electoralBoards = JsonUtils.getJsonObject(documents).getJsonArray(JsonConstants.RESULT);

		for (int i = 0; i < electoralBoards.size(); i++) {

			final JsonObject electoralBoardsInArray = electoralBoards.getJsonObject(i);
			final String electoralBoardId = electoralBoardsInArray.getString(JsonConstants.ID);
			checkArgument(
					electionEventId.equals(electoralBoardsInArray.getJsonObject(JsonConstants.ELECTION_EVENT).getString(JsonConstants.ID)));

			LOGGER.debug("Uploading the signed authentication context configuration. [electionEventId: {}, electoralBoardId: {}]",
					electionEventId, electoralBoardId);

			final JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
			jsonObjectBuilder.add(JsonConstants.ID, electoralBoardId);
			jsonObjectBuilder.add(JsonConstants.SYNCHRONIZED, SynchronizeStatus.SYNCHRONIZED.getIsSynchronized().toString());
			jsonObjectBuilder.add(JsonConstants.DETAILS, SynchronizeStatus.SYNCHRONIZED.getStatus());
			LOGGER.info("The electoral board was uploaded successfully. [electionEventId: {}, electoralBoardId: {}]", electionEventId,
					electoralBoardId);

			try {
				electoralBoardRepository.update(jsonObjectBuilder.build().toString());
			} catch (final DatabaseException ex) {
				LOGGER.error(String.format(
						"An error occurred while updating the signed electoral board. [electionEventId: %s, electoralBoardId: %s]",
						electionEventId, electoralBoardId), ex);
			}

			uploadSetupComponentPublicKeysData(electionEventId);
			uploadSetupComponentVerificationCardKeystoresPayloads(electionEventId);
		}

	}

	/**
	 * Uploads the setup component verification card keystores payloads.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 * @throws NullPointerException      if the election event id is null.
	 */
	private void uploadSetupComponentVerificationCardKeystoresPayloads(final String electionEventId) {
		validateUUID(electionEventId);

		LOGGER.info("Uploading setup component verification card keystores payloads... [electionEventId: {}]", electionEventId);

		final Map<String, Object> votingCardSetsCriteria = new HashMap<>();
		votingCardSetsCriteria.put(JsonConstants.STATUS, Status.SIGNED.name());
		votingCardSetsCriteria.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);

		final String votingCardSetsEntities = votingCardSetRepository.list(votingCardSetsCriteria);
		final JsonArray votingCardSets = JsonUtils.getJsonObject(votingCardSetsEntities).getJsonArray(JsonConstants.RESULT);

		final List<SetupComponentVerificationCardKeystoresPayload> setupComponentVerificationCardKeystoresPayloads = IntStream.range(0,
						votingCardSets.size())
				.mapToObj(votingCardSets::getJsonObject)
				.map(votingCardSet -> votingCardSet.getString(JsonConstants.VERIFICATION_CARD_SET_ID))
				.map(verificationCardId -> setupComponentVerificationCardKeystoresPayloadService.load(electionEventId, verificationCardId))
				.toList();

		setupComponentVerificationCardKeystoresPayloadService.upload(setupComponentVerificationCardKeystoresPayloads);

		LOGGER.info("Successfully uploaded setup component verification card keystores payloads. [electionEventId: {}]", electionEventId);
	}

	/**
	 * Uploads the setup component public keys payload to the vote verification.
	 *
	 * @param electionEventId the election event id.
	 * @throws IllegalStateException            if an error occurred while verifying the signature of the setup component public keys payload or the
	 *                                          request for uploading the setup component public keys failed.
	 * @throws InvalidPayloadSignatureException if the signature of the setup component public keys payload is invalid.
	 */
	private void uploadSetupComponentPublicKeysData(final String electionEventId) {

		LOGGER.info("Uploading setup component public keys payload... [electionEventId: {}]", electionEventId);

		final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload = setupComponentPublicKeysPayloadService.load(electionEventId);

		electoralBoardUploadRepository.uploadSetupComponentPublicKeysData(setupComponentPublicKeysPayload);
		LOGGER.info("Successfully uploaded setup component public keys payload. [electionEventId: {}]", electionEventId);
	}

}
