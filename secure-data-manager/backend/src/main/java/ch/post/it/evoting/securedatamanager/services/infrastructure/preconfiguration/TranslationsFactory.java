/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.function.Predicate.not;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.cryptoprimitives.domain.election.Contest;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AnswerInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.BallotQuestionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.BallotQuestionType.BallotQuestionInfo;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.CandidatePositionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.CandidateTextInformationType.CandidateTextInfo;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.CandidateType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionDescriptionInformationType.ElectionDescriptionInfo;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionGroupBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.IncumbentTextInfoType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.LanguageType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ListDescriptionInformationType.ListDescriptionInfo;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ListType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ListUnionDescriptionType.ListUnionDescriptionInfo;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ListUnionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.StandardAnswerType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.StandardBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.StandardQuestionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.TieBreakQuestionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.TiebreakAnswerType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VariantBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteDescriptionInformationType.VoteDescriptionInfo;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteType;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration.BallotContestFactory.ContestWithPosition;
import ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration.ElectionsConfigFactory.ElectionTypeAttributeIds;
import ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration.ElectionsConfigFactory.VoteTypeAttributeIds;

/**
 * Creates the translations of the elections_config.json.
 */
public class TranslationsFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(TranslationsFactory.class);
	private static final String TYPE_TEXT = "text";
	private static final String SUFFIX_LANGUAGE_CH = "-CH";
	private static final Set<String> ANSWER_INFO_BLANK = Set.of("Leer", "Blanc", "Bianco", "Vid");
	private static final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();

	private TranslationsFactory() {
		// static usage only.
	}

	/**
	 * Creates the translations without ballotId for all elections and votes.
	 *
	 * @param configuration the {@link Configuration} representing the configuration-anonymized. Must be non-null.
	 * @return the mapping of the domain of influence of the election or vote to the translations.
	 */
	public static Map<String, List<Translation>> createTranslationsWithoutBallotId(final Configuration configuration,
			final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap,
			final Map<String, List<ContestWithPosition>> domainOfInfluenceToContests,
			final Map<String, ElectionTypeAttributeIds> electionTypeAttributeIdsMap) {
		checkNotNull(configuration);
		checkNotNull(voteTypeAttributeIdsMap);
		checkNotNull(domainOfInfluenceToContests);
		checkNotNull(electionTypeAttributeIdsMap);

		// Create translations for all elections
		final Map<String, List<Translation>> electionTranslations = configuration.getContest().getElectionGroupBallot().stream()
				.collect(Collectors.groupingBy(
						ElectionGroupBallotType::getDomainOfInfluence,
						Collectors.flatMapping(
								electionGroupBallotType -> {
									final ElectionInformationType electionInformationType = electionGroupBallotType.getElectionInformation().stream()
											.collect(MoreCollectors.onlyElement());

									final ElectionTypeAttributeIds electionTypeAttributeIds = checkNotNull(electionTypeAttributeIdsMap
											.get(electionInformationType.getElection().getElectionIdentification()));
									return createElectionTranslations(electionInformationType, domainOfInfluenceToContests, electionTypeAttributeIds,
											electionGroupBallotType.getDomainOfInfluence());
								},
								Collectors.toList())
				));

		// Create translations for all votes
		final Map<String, List<Translation>> voteTranslations = configuration.getContest().getVoteInformation().stream()
				.map(VoteInformationType::getVote)
				.collect(Collectors.groupingBy(
						VoteType::getDomainOfInfluence,
						Collectors.flatMapping(voteType -> createVoteTranslations(voteType, domainOfInfluenceToContests, voteTypeAttributeIdsMap),
								Collectors.toList())
				));

		final Map<String, List<Translation>> domainOfInfluenceToTranslations = Stream.of(electionTranslations, voteTranslations)
				.flatMap(map -> map.entrySet().stream())
				.collect(Collectors.toMap(
						Map.Entry::getKey,
						entry -> new ArrayList<>(entry.getValue()),
						(contestsLeft, contestsRight) -> {
							contestsLeft.addAll(contestsRight);
							return contestsLeft;
						})
				);

		LOGGER.info("Successfully created the translations.");

		return domainOfInfluenceToTranslations;
	}

	private static Stream<Translation> createElectionTranslations(final ElectionInformationType electionInformationType,
			final Map<String, List<ContestWithPosition>> domainOfInfluenceToContests, final ElectionTypeAttributeIds electionTypeAttributeIds,
			final String domainOfInfluence) {
		final ElectionType electionType = electionInformationType.getElection();
		final Map<String, String> candidateAttributeIdsMap = electionTypeAttributeIds.candidateAttributeIdsMap();
		final Map<String, String> listAttributeIdsMap = electionTypeAttributeIds.listAttributeIdsMap();
		final boolean electionWithLists = electionInformationType.getList().stream().anyMatch(not(ListType::isListEmpty));

		return Arrays.stream(LanguageType.values())
				.map(languageType -> {
					final ObjectNode texts = objectMapper.createObjectNode();

					if (electionWithLists) {
						texts.setAll(electionInformationType.getList().stream()
								.filter(not(ListType::isListEmpty))
								.map(listType -> {
									// create a candidate text with list information
									final ObjectNode textsListsElection = createGenericElectionTranslationTextListCandidate(electionInformationType,
											listType.getCandidatePosition(), electionTypeAttributeIds, languageType);

									// create a list text
									final String listAttributeId = listAttributeIdsMap.get(listType.getListIdentification());
									return (ObjectNode) textsListsElection
											.set(listAttributeId, createElectionTranslationTextList(electionInformationType, listType, languageType));
								})
								.reduce(objectMapper.createObjectNode(), ObjectNode::setAll));
					} else {
						texts.setAll(electionInformationType.getCandidate().stream()
								.map(candidateType -> {
									// create a candidate text
									final String candidateIdentification = candidateType.getCandidateIdentification();
									final String candidateAttributeId = candidateAttributeIdsMap.get(candidateIdentification);
									final ArrayNode textCandidate = createGenericElectionTranslationTextCandidate(candidateType, languageType);
									final ObjectNode textLists = objectMapper.createObjectNode().set(candidateAttributeId, textCandidate);

									// create a candidate's dummy list text
									return (ObjectNode) textLists
											.set(listAttributeIdsMap.get(candidateIdentification), objectMapper.createArrayNode()
													.add(objectMapper.createObjectNode().put(JsonConstants.LIST_TYPE_ATTRIBUTE_1,
															textCandidate.get(0).get(JsonConstants.CANDIDATE_TYPE_ATTRIBUTE_1).asText())));
								})
								.reduce(objectMapper.createObjectNode(), ObjectNode::setAll));
					}

					final ObjectNode textContest = createElectionTranslationTextContest(electionInformationType, languageType,
							electionWithLists);
					final String contestId = getContestId(domainOfInfluence, electionType.getElectionIdentification(),
							domainOfInfluenceToContests);
					texts.set(contestId, textContest);

					texts.setAll(createElectionTranslationTextBlank(electionInformationType, electionTypeAttributeIds, languageType,
							electionWithLists));

					return createGenericTranslationWithoutBallotId(texts, languageType);
				});
	}

	private static ObjectNode createGenericElectionTranslationTextListCandidate(final ElectionInformationType electionInformationType,
			final List<CandidatePositionType> candidatePositions, final ElectionTypeAttributeIds electionTypeAttributeIds,
			final LanguageType languageType) {
		final Map<String, List<CandidatePositionType>> candidatePositionMap = candidatePositions.stream()
				.collect(Collectors.groupingBy(CandidatePositionType::getCandidateIdentification));
		final Map<String, String> candidateAttributeIdsMap = electionTypeAttributeIds.candidateAttributeIdsMap();

		return candidatePositions.stream()
				.map(candidatePosition -> {
					final String candidateIdentification = candidatePosition.getCandidateIdentification();
					// find the candidate type corresponding to the candidate position
					final CandidateType candidateType = electionInformationType.getCandidate().stream()
							.filter(candidate -> candidate.getCandidateIdentification().equals(candidateIdentification))
							.collect(MoreCollectors.onlyElement());

					final List<CandidatePositionType> candidatePositionTypes = candidatePositionMap.get(candidateIdentification);
					final String candidateTypeInitialAccumulation = Integer.toString(candidatePositionTypes.size());
					final String candidateTypePositionOnList = candidatePositionTypes.stream()
							.map(CandidatePositionType::getPositionOnList)
							.map(pos -> String.format("%d", pos))
							.collect(Collectors.joining(","));

					final ArrayNode textCandidate = createGenericElectionTranslationTextCandidate(candidateType, languageType);
					textCandidate.addObject().put(JsonConstants.CANDIDATE_TYPE_INITIAL_ACCUMULATION, candidateTypeInitialAccumulation);
					textCandidate.addObject().put(JsonConstants.CANDIDATE_TYPE_POSITION_ON_LIST, candidateTypePositionOnList);

					final String candidateAttributeId = candidateAttributeIdsMap.get(candidateIdentification);
					return (ObjectNode) objectMapper.createObjectNode().set(candidateAttributeId, textCandidate);
				}).reduce(objectMapper.createObjectNode(), ObjectNode::setAll);
	}

	private static ArrayNode createGenericElectionTranslationTextCandidate(final CandidateType candidateType, final LanguageType languageType) {
		final String candidateTypeAttribute1 = String.format("%s %s %s", candidateType.getReferenceOnPosition(), candidateType.getFamilyName(),
				candidateType.getCallName());

		final String candidateTypeAttribute2 = getInLanguage(languageType, candidateType.getCandidateText().getCandidateTextInfo(),
				CandidateTextInfo::getLanguage, CandidateTextInfo::getCandidateText);

		final String candidateTypeAttribute3 = candidateType.getIncumbent().isIncumbent()
				? getInLanguage(languageType, candidateType.getIncumbent().getIncumbentText().getIncumbentTextInfo(),
				IncumbentTextInfoType::getLanguage, IncumbentTextInfoType::getIncumbentText)
				: "";

		final ArrayNode textCandidate = objectMapper.createArrayNode();
		textCandidate.addObject().put(JsonConstants.CANDIDATE_TYPE_ATTRIBUTE_1, candidateTypeAttribute1);
		textCandidate.addObject().put(JsonConstants.CANDIDATE_TYPE_ATTRIBUTE_2, candidateTypeAttribute2);
		textCandidate.addObject().put(JsonConstants.CANDIDATE_TYPE_ATTRIBUTE_3, candidateTypeAttribute3);

		return textCandidate;
	}

	private static ArrayNode createElectionTranslationTextList(final ElectionInformationType electionInformationType, final ListType listType,
			final LanguageType languageType) {
		final String listDescription = getInLanguage(languageType, listType.getListDescription().getListDescriptionInfo(),
				ListDescriptionInfo::getLanguage, ListDescriptionInfo::getListDescription);
		final String listTypeAttribute1 = String.format("%s %s", listType.getListIndentureNumber(), listDescription);
		final ArrayNode textList = objectMapper.createArrayNode()
				.add(objectMapper.createObjectNode().put(JsonConstants.LIST_TYPE_ATTRIBUTE_1, listTypeAttribute1));

		final String listIdentification = listType.getListIdentification();
		final List<ListUnionType> listUnionTypes = electionInformationType.getListUnion();
		if (!listUnionTypes.isEmpty()) {
			textList.add(createElectionTranslationTextListUnion(JsonConstants.LIST_TYPE_APPARENTMENT, BigInteger.ONE, listUnionTypes,
					listIdentification, languageType));
			textList.add(createElectionTranslationTextListUnion(JsonConstants.LIST_TYPE_SOUS_APPARENTMENT, BigInteger.TWO, listUnionTypes,
					listIdentification, languageType));
		}

		return textList;
	}

	private static ObjectNode createElectionTranslationTextListUnion(final String listUnionAttribute, final BigInteger listUnionType,
			final List<ListUnionType> listUnionTypes, final String listIdentification, final LanguageType languageType) {
		final String unionDescription = listUnionTypes.stream()
				.filter(type -> type.getListUnionType().equals(listUnionType))
				.filter(listUnion -> listUnion.getReferencedList().stream().anyMatch(listIdentification::equals))
				.map(listUnion -> getInLanguage(languageType, listUnion.getListUnionDescription().getListUnionDescriptionInfo(),
						ListUnionDescriptionInfo::getLanguage, ListUnionDescriptionInfo::getListUnionDescription))
				.findAny()
				.orElse("");
		return objectMapper.createObjectNode()
				.put(listUnionAttribute, unionDescription);
	}

	private static ObjectNode createElectionTranslationTextContest(final ElectionInformationType electionInformationType,
			final LanguageType languageType, final boolean electionWithLists) {
		final ElectionType electionType = electionInformationType.getElection();
		final String electionDescription = getInLanguage(languageType, electionType.getElectionDescription().getElectionDescriptionInfo(),
				ElectionDescriptionInfo::getLanguage, ElectionDescriptionInfo::getElectionDescription);
		final ObjectNode textContest = objectMapper.createObjectNode()
				.put(JsonConstants.TITLE, electionDescription)
				.put(JsonConstants.DESCRIPTION, electionDescription)
				.put(JsonConstants.HOW_TO_VOTE, (String) null);

		final ObjectNode attributeKeys = objectMapper.createObjectNode();
		attributeKeys.setAll(IntStream.range(1, 4)
				.mapToObj(i -> {
					final String candidateTypeAttribute = switch (i) {
						case 1 -> "Candidate text";
						case 2 -> "Candidate Text";
						case 3 -> "Incumbent";
						default -> throw new IllegalStateException(String.format("Unknown index. [index: %s]", i));
					};
					return objectMapper.createObjectNode().put(JsonConstants.CANDIDATE_TYPE_ATTRIBUTE + i, candidateTypeAttribute);
				})
				.reduce(objectMapper.createObjectNode(), ObjectNode::setAll));

		if (electionWithLists) {
			attributeKeys
					.put(JsonConstants.CANDIDATE_TYPE_INITIAL_ACCUMULATION,
							switch (languageType) {
								case DE -> "Initiales Kumulieren";
								case FR -> "Cumul initial";
								case IT -> "Cumulo iniziale";
								case RM -> "Cumular inizial";
							})
					.put(JsonConstants.CANDIDATE_TYPE_POSITION_ON_LIST,
							switch (languageType) {
								case DE -> "Initiale Position";
								case FR -> "Position initiale";
								case IT -> "Posizione initiale";
								case RM -> "Posiziun inizial";
							});
		}

		attributeKeys.put(JsonConstants.LIST_TYPE_ATTRIBUTE_1, "Party Name");

		textContest.set(JsonConstants.ATTRIBUTE_KEYS, attributeKeys);

		return textContest;
	}

	private static ObjectNode createElectionTranslationTextBlank(final ElectionInformationType electionInformationType,
			final ElectionTypeAttributeIds electionTypeAttributeIds, final LanguageType languageType, final boolean electionWithLists) {
		return electionInformationType.getList().stream()
				.filter(ListType::isListEmpty)
				.map(emptyList -> {
					// blank candidate
					final CandidatePositionType emptyListCandidate = emptyList.getCandidatePosition().get(0);
					final String blankCandidateId = electionTypeAttributeIds.candidateAttributeIdsMap()
							.get(emptyListCandidate.getCandidateListIdentification());
					final String textCandidate = getInLanguage(languageType, emptyListCandidate.getCandidateTextOnPosition().getCandidateTextInfo(),
							CandidateTextInfo::getLanguage, CandidateTextInfo::getCandidateText);
					final ObjectNode textBlank = objectMapper.createObjectNode();
					textBlank.putArray(blankCandidateId).addObject().put(JsonConstants.TEXT, textCandidate);

					// blank list
					if (electionWithLists) {
						final String blankListId = electionTypeAttributeIds.listAttributeIdsMap().get(emptyList.getListIdentification());
						final String textList = getInLanguage(languageType, emptyList.getListDescription().getListDescriptionInfo(),
								ListDescriptionInfo::getLanguage, ListDescriptionInfo::getListDescription);
						final ObjectNode textBlankList = objectMapper.createObjectNode();
						textBlankList.putArray(blankListId).addObject().put(JsonConstants.TEXT, textList);

						textBlank.setAll(textBlankList);
					}

					return textBlank;
				}).collect(MoreCollectors.onlyElement());
	}

	private static Stream<Translation> createVoteTranslations(final VoteType voteType,
			final Map<String, List<ContestWithPosition>> domainOfInfluenceToContests,
			final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap) {
		return Arrays.stream(LanguageType.values())
				.map(languageType -> {
					final ObjectNode texts = voteType.getBallot().stream()
							.map(ballotType -> {
								final ObjectNode textQuestionAnswer = objectMapper.createObjectNode();

								final StandardBallotType standardBallotType = ballotType.getStandardBallot();
								if (standardBallotType != null) {
									textQuestionAnswer.setAll(createGenericVoteTranslationText(Stream.of(standardBallotType),
											StandardBallotType::getQuestionIdentification, StandardBallotType::getBallotQuestion,
											StandardBallotType::getAnswer, StandardAnswerType::getAnswerIdentification,
											StandardAnswerType::getAnswerInfo, voteTypeAttributeIdsMap, languageType));
								}

								final VariantBallotType variantBallot = ballotType.getVariantBallot();
								if (variantBallot != null) {
									textQuestionAnswer.setAll(createGenericVoteTranslationText(variantBallot.getStandardQuestion().stream(),
											StandardQuestionType::getQuestionIdentification, StandardQuestionType::getBallotQuestion,
											StandardQuestionType::getAnswer, StandardAnswerType::getAnswerIdentification,
											StandardAnswerType::getAnswerInfo, voteTypeAttributeIdsMap, languageType));

									final List<TieBreakQuestionType> tieBreakQuestion = variantBallot.getTieBreakQuestion();
									if (tieBreakQuestion != null) {
										textQuestionAnswer.setAll(createGenericVoteTranslationText(variantBallot.getTieBreakQuestion().stream(),
												TieBreakQuestionType::getQuestionIdentification, TieBreakQuestionType::getBallotQuestion,
												TieBreakQuestionType::getAnswer, TiebreakAnswerType::getAnswerIdentification,
												TiebreakAnswerType::getAnswerInfo, voteTypeAttributeIdsMap, languageType));
									}
								}

								return textQuestionAnswer;
							})
							.reduce(objectMapper.createObjectNode(), ObjectNode::setAll);

					final ObjectNode textContest = createVoteTranslationTextContest(voteType, languageType);
					texts.set(getContestId(voteType.getDomainOfInfluence(), voteType.getVoteIdentification(), domainOfInfluenceToContests),
							textContest);

					return createGenericTranslationWithoutBallotId(texts, languageType);
				});
	}

	private static <S, T> ObjectNode createGenericVoteTranslationText(final Stream<S> questions, final Function<S, String> getQuestionIdentification,
			final Function<S, BallotQuestionType> getBallotQuestion, final Function<S, List<T>> getAnswer,
			final Function<T, String> getAnswerIdentification, final Function<T, List<AnswerInformationType>> getAnswerInfo,
			final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap, final LanguageType languageType) {
		return questions
				.map(question -> {
					final String questionIdentification = getQuestionIdentification.apply(question);
					final VoteTypeAttributeIds attributeIds = voteTypeAttributeIdsMap.get(questionIdentification);

					final ObjectNode texts = createGenericVoteTranslationTextAnswer(getAnswer.apply(question).stream(), getAnswerIdentification,
							getAnswerInfo, attributeIds, languageType);

					final String attributeQuestionId = attributeIds.attributeQuestionId();
					final String ballotQuestion = getInLanguage(languageType, getBallotQuestion.apply(question).getBallotQuestionInfo(),
							BallotQuestionInfo::getLanguage, BallotQuestionInfo::getBallotQuestion);

					final ArrayNode textQuestion = objectMapper.createArrayNode()
							.add(objectMapper.createObjectNode()
									.put(JsonConstants.QUESTION_TYPE_TEXT, ballotQuestion));

					return (ObjectNode) texts.set(attributeQuestionId, textQuestion);
				}).reduce(objectMapper.createObjectNode(), ObjectNode::setAll);
	}

	private static <T> ObjectNode createGenericVoteTranslationTextAnswer(final Stream<T> answers, final Function<T, String> getAnswerIdentification,
			final Function<T, List<AnswerInformationType>> getAnswerInfo, final VoteTypeAttributeIds voteTypeAttributeIds,
			final LanguageType languageType) {
		return answers
				.map(answer -> {
					final String attributeAnswerId = voteTypeAttributeIds.attributeAnswerId().get(getAnswerIdentification.apply(answer));
					final String answerInfo = getInLanguage(languageType, getAnswerInfo.apply(answer), AnswerInformationType::getLanguage,
							AnswerInformationType::getAnswer);

					final ArrayNode textAnswer = objectMapper.createArrayNode();
					final boolean isBlank = ANSWER_INFO_BLANK.contains(answerInfo);
					textAnswer.add(objectMapper.createObjectNode()
							.put(isBlank ? JsonConstants.TEXT : JsonConstants.ANSWER_TYPE_TEXT, answerInfo));

					return (ObjectNode) objectMapper.createObjectNode()
							.set(attributeAnswerId, textAnswer);
				}).reduce(objectMapper.createObjectNode(), ObjectNode::setAll);
	}

	private static ObjectNode createVoteTranslationTextContest(final VoteType voteType, final LanguageType languageType) {
		final String voteDescription = getInLanguage(languageType, voteType.getVoteDescription().getVoteDescriptionInfo(),
				VoteDescriptionInfo::getLanguage, VoteDescriptionInfo::getVoteDescription);
		final ObjectNode textContest = objectMapper.createObjectNode()
				.put(JsonConstants.TITLE, voteDescription)
				.put(JsonConstants.DESCRIPTION, voteDescription)
				.put(JsonConstants.HOW_TO_VOTE, (String) null);
		final ObjectNode attributeKeys = objectMapper.createObjectNode()
				.put(JsonConstants.ANSWER_TYPE_TEXT, TYPE_TEXT)
				.put(JsonConstants.QUESTION_TYPE_TEXT, TYPE_TEXT);
		textContest.set(JsonConstants.ATTRIBUTE_KEYS, attributeKeys);
		return textContest;
	}

	private static String getContestId(final String domainOfInfluence, final String identification,
			final Map<String, List<ContestWithPosition>> domainOfInfluenceToContests) {
		final List<ContestWithPosition> contests = checkNotNull(domainOfInfluenceToContests.get(domainOfInfluence),
				"The contest for the given domain of influence does not exist.");
		return contests.stream()
				.map(ContestWithPosition::contest)
				.filter(contest -> contest.alias().equals(identification))
				.map(Contest::id)
				.collect(MoreCollectors.onlyElement());
	}

	public static <T> String getInLanguage(final LanguageType languageType, final List<T> descriptionInfo,
			final Function<T, LanguageType> getLanguage, final Function<T, String> getDescription) {
		return descriptionInfo.stream()
				.filter(info -> getLanguage.apply(info).equals(languageType))
				.map(getDescription)
				.collect(MoreCollectors.onlyElement());
	}

	private static Translation createGenericTranslationWithoutBallotId(final ObjectNode texts, final LanguageType languageType) {
		return new Translation(languageType.value() + SUFFIX_LANGUAGE_CH, texts);
	}

	record Translation(String locale, ObjectNode texts) {
	}
}
