/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmonline.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkState;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.SetupComponentCMTablePayloadChunks;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.SetupComponentCMTablePayloadService;

/**
 * Service which uploads setup component CMTable payload files to the voter portal.
 */
@Service
public class SetupComponentCMTablePayloadUploadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentCMTablePayloadUploadService.class);

	private final SetupComponentCMTablePayloadService setupComponentCMTablePayloadService;
	private final SetupComponentCMTablePayloadUploadRepository setupComponentCMTablePayloadUploadRepository;
	private final boolean isVotingPortalEnabled;

	public SetupComponentCMTablePayloadUploadService(
			final SetupComponentCMTablePayloadService setupComponentCMTablePayloadService,
			final SetupComponentCMTablePayloadUploadRepository setupComponentCMTablePayloadUploadRepository,
			@Value("${voting.portal.enabled}")
			final boolean isVotingPortalEnabled) {
		this.setupComponentCMTablePayloadService = setupComponentCMTablePayloadService;
		this.setupComponentCMTablePayloadUploadRepository = setupComponentCMTablePayloadUploadRepository;
		this.isVotingPortalEnabled = isVotingPortalEnabled;
	}

	/**
	 * Uploads the available return codes mapping tables to the voter portal.
	 *
	 * @param electionEventId       the election event id. Must be non-null and a valid UUID.
	 * @param verificationCardSetId the verification card set id. Must be non-null and a valid UUID.
	 * @throws FailedValidationException if {@code electionEventId} or {@code verificationCardSetId} is invalid.
	 */
	public void upload(final String electionEventId, final String verificationCardSetId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkState(isVotingPortalEnabled, "The voting-portal connection is not enabled.");

		LOGGER.debug("Uploading setup component CMTable payloads... [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);

		final SetupComponentCMTablePayloadChunks setupComponentCMTablePayloadChunks = setupComponentCMTablePayloadService.load(electionEventId,
				verificationCardSetId);

		setupComponentCMTablePayloadUploadRepository.uploadReturnCodesMappingTable(electionEventId, verificationCardSetId,
				setupComponentCMTablePayloadChunks);

		LOGGER.info("Successfully uploaded setup component CMTable payloads. [electionEventId: {}, verificationCardSetId: {}, chunkCount{}]",
				electionEventId, verificationCardSetId, setupComponentCMTablePayloadChunks.getChunkCount());
	}

}
