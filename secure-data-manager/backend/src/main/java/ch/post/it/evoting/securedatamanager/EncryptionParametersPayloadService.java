/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.EncryptionParametersPayload;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Allows loading the encryption parameters associated to an election event.
 */
@Service
public class EncryptionParametersPayloadService {

	private final EncryptionParametersPayloadFileRepository encryptionParametersPayloadFileRepository;

	public EncryptionParametersPayloadService(final EncryptionParametersPayloadFileRepository encryptionParametersPayloadFileRepository) {
		this.encryptionParametersPayloadFileRepository = encryptionParametersPayloadFileRepository;
	}

	/**
	 * Saves an encryption parameters payload in the corresponding election event folder.
	 *
	 * @param electionEventId             the election event id. Must be non-null and a valid UUID.
	 * @param encryptionParametersPayload the encryption parameters payload to save. Must be non-null.
	 * @throws NullPointerException if the payload is null.
	 */
	public void save(final String electionEventId, final EncryptionParametersPayload encryptionParametersPayload) {
		validateUUID(electionEventId);
		checkNotNull(encryptionParametersPayload);

		encryptionParametersPayloadFileRepository.save(electionEventId, encryptionParametersPayload);
	}

	/**
	 * Loads the encryption parameters for the given {@code electionEventId}.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @return the encryption parameters for this {@code electionEventId}.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if {@code electionEventId} is an invalid UUID.
	 * @throws IllegalStateException     if the requested encryption parameters payload is not present. </li>
	 */
	public EncryptionParametersPayload load(final String electionEventId) {
		validateUUID(electionEventId);

		return encryptionParametersPayloadFileRepository.findById(electionEventId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Requested encryption parameters payload is not present. [electionEventId: %s]", electionEventId)));
	}

	/**
	 * Loads the encryption parameters the {@value Constants#SDM_INPUT_FILES_BASE_DIR} directory for the pre-configuration phase.
	 *
	 * @return the encryption parameters.
	 * @throws IllegalStateException if the requested encryption parameters payload is not present. </li>
	 */
	public EncryptionParametersPayload loadPreconfiguration() {
		return encryptionParametersPayloadFileRepository.find()
				.orElseThrow(() -> new IllegalStateException(
						"An encryptionParameters.json file in sdm/input is needed for the election event pre-configuration."));
	}

	/**
	 * Renames the encryption parameters file from {@value Constants#CONFIG_FILE_NAME_ENCRYPTION_PARAMETERS_PAYLOAD_JSON} to
	 * {ee}-{@value Constants#CONFIG_FILE_NAME_ENCRYPTION_PARAMETERS_PAYLOAD_JSON} at the end of the pre-configuration phase.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 */
	public void renameEncryptionParametersFile(final String electionEventId) throws IOException {
		validateUUID(electionEventId);
		encryptionParametersPayloadFileRepository.renameEncryptionParametersFile(electionEventId);
	}

	/**
	 * Loads the encryption parameters for the given {@code electionEventId}. The result of this method is cached.
	 *
	 * @param electionEventId the election event id for which to get the encryption parameters.
	 * @return the encryption parameters.
	 * @throws FailedValidationException if {@code electionEventId} is invalid.
	 * @throws IllegalStateException     if the encryption parameters are not found for this {@code electionEventId}.
	 */
	@Cacheable("gqGroups")
	public GqGroup loadEncryptionGroup(final String electionEventId) {
		validateUUID(electionEventId);

		return encryptionParametersPayloadFileRepository.findById(electionEventId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Requested encryption parameters payload is not present. [electionEventId: %s]", electionEventId)))
				.getEncryptionGroup();
	}

}
