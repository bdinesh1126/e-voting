/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.services.infrastructure.administrationboard.AdministrationBoardRepository;

@Service
public class AdminBoardService {

	private final AdministrationBoardRepository administrationBoardRepository;

	public AdminBoardService(final AdministrationBoardRepository administrationBoardRepository) {
		this.administrationBoardRepository = administrationBoardRepository;
	}

	/**
	 * Gets the admin boards.
	 *
	 * @return the admin boards
	 */
	public String getAdminBoards() {
		return administrationBoardRepository.list();
	}
}
