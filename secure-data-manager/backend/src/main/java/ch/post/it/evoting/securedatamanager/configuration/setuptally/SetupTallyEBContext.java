/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setuptally;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Regroups the context needed by the SetupTallyEB algorithm.
 *
 * @param encryptionGroup (p, q, g), the group. Must be non-null.
 * @param electionEventId ee, the election event id. Must be non-null and a valid UUID.
 */
public record SetupTallyEBContext(GqGroup encryptionGroup, String electionEventId) {

	public SetupTallyEBContext {
		checkNotNull(encryptionGroup);
		validateUUID(electionEventId);
	}
}
