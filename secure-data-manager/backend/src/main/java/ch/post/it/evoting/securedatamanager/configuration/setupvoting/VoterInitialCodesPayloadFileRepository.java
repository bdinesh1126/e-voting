/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.commons.Constants.CONFIG_FILE_NAME_VOTER_INITIAL_CODES_PAYLOAD;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.setupvoting.VoterInitialCodesPayload;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

/**
 * Allows performing operations with the voter initial codes payload. The voter initial codes payload is persisted/retrieved to/from the file system
 * of the SDM, in its workspace.
 */
@Repository
public class VoterInitialCodesPayloadFileRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(VoterInitialCodesPayloadFileRepository.class);

	private final ObjectMapper objectMapper;
	private final PathResolver pathResolver;

	public VoterInitialCodesPayloadFileRepository(final ObjectMapper objectMapper, final PathResolver pathResolver) {
		this.objectMapper = objectMapper;
		this.pathResolver = pathResolver;
	}

	/**
	 * Persists a voter initial codes payload to the file system.
	 *
	 * @param voterInitialCodesPayload the voter initial codes payload to persist. Must be non-null.
	 * @param votingCardSetId          the voting card set id. Must be non-null and a valid UUID.
	 * @return the path where the voter initial codes payload has been successfully persisted.
	 * @throws NullPointerException      if any parameter is null.
	 * @throws FailedValidationException if the voting card set id is not a valid UUID.
	 * @throws UncheckedIOException      if the serialization of the voter initial codes payload fails.
	 */
	public Path save(final VoterInitialCodesPayload voterInitialCodesPayload, final String votingCardSetId) {
		checkNotNull(voterInitialCodesPayload);
		validateUUID(votingCardSetId);

		final String electionEventId = voterInitialCodesPayload.electionEventId();

		final Path votingCardSetIdPath = pathResolver.resolvePrintingPath(electionEventId).resolve(votingCardSetId);
		try {
			Files.createDirectory(votingCardSetIdPath);
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Failed to create voting card set id directory in printing. [path: %s]", votingCardSetIdPath),
					e);
		}
		final Path payloadPath = votingCardSetIdPath.resolve(CONFIG_FILE_NAME_VOTER_INITIAL_CODES_PAYLOAD);
		try {
			final byte[] payloadBytes = objectMapper.writeValueAsBytes(voterInitialCodesPayload);

			final Path writePath = Files.write(payloadPath, payloadBytes);
			LOGGER.info("Successfully persisted voter initial codes payload. [electionEventId: {}, votingCardSetId: {}, path: {}]", electionEventId,
					votingCardSetId, payloadPath);

			return writePath;
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Failed to serialize voter initial codes payload. [electionEventId: %s, votingCardSetId: %s, path: %s]",
							electionEventId, votingCardSetId, payloadPath), e);
		}
	}

	/**
	 * Retrieves from the file system a voter initial codes payload by election event id and voting card set id.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @param votingCardSetId the voting card set id. Must be non-null and a valid UUID.
	 * @return the voter initial codes payload with the given id or {@link Optional#empty} if none found.
	 * @throws NullPointerException      if any parameter is null.
	 * @throws FailedValidationException if any parameter is not a valid UUID.
	 * @throws UncheckedIOException      if the deserialization of the voter initial codes payload fails.
	 */
	public Optional<VoterInitialCodesPayload> findByElectionEventIdAndVotingCardSetId(final String electionEventId, final String votingCardSetId) {
		validateUUID(electionEventId);
		validateUUID(votingCardSetId);

		final Path payloadPath = getPayloadPath(electionEventId, votingCardSetId);
		if (!Files.exists(payloadPath)) {
			LOGGER.debug("Requested voter initial codes payload does not exist. [electionEventId: {}, votingCardSetId: {}, path: {}]",
					electionEventId, votingCardSetId, payloadPath);
			return Optional.empty();
		}

		return Optional.of(deserializeVoterInitialCodesPayload(electionEventId, payloadPath));
	}

	/**
	 * Retrieves from the file system all voter initial codes payloads by election event id.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @return the list of voter initial codes payloads, empty if none found.
	 * @throws NullPointerException      if the election event id is null.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 * @throws UncheckedIOException      if the deserialization of the voter initial codes payloads fails.
	 */
	public List<VoterInitialCodesPayload> findAllByElectionEventId(final String electionEventId) {
		final List<Path> votingCarSetIdPaths;
		final Path printingPath = pathResolver.resolvePrintingPath(electionEventId);
		try (final Stream<Path> paths = Files.walk(printingPath, 1)) {
			votingCarSetIdPaths = paths
					.filter(path -> !printingPath.equals(path))
					.filter(Files::isDirectory)
					.toList();
		} catch (final IOException e) {
			LOGGER.info("Unable to find voter initial codes payload. [electionEventId: {}, path: {}]", electionEventId, printingPath);
			return List.of();
		}

		return votingCarSetIdPaths.stream().parallel()
				.map(votingCarSetIdPath -> votingCarSetIdPath.resolve(CONFIG_FILE_NAME_VOTER_INITIAL_CODES_PAYLOAD))
				.map(payloadPath -> deserializeVoterInitialCodesPayload(electionEventId, payloadPath))
				.toList();
	}

	private VoterInitialCodesPayload deserializeVoterInitialCodesPayload(final String electionEventId, final Path payloadPath) {
		try {
			return objectMapper.readValue(payloadPath.toFile(), VoterInitialCodesPayload.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Failed to deserialize voter initial codes payload. [electionEventId: %s, path: %s]", electionEventId, payloadPath),
					e);
		}
	}

	private Path getPayloadPath(final String electionEventId, final String votingCardSetId) {
		final Path printingPath = pathResolver.resolvePrintingPath(electionEventId);
		return printingPath.resolve(votingCardSetId).resolve(CONFIG_FILE_NAME_VOTER_INITIAL_CODES_PAYLOAD);
	}

}
