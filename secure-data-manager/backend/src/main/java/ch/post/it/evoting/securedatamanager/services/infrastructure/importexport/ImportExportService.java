/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.importexport;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

import ch.post.it.evoting.evotinglibraries.domain.validations.PasswordValidation;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;

@Service
public class ImportExportService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ImportExportService.class);
	private static final byte[] ASSOCIATED_DATA = new byte[] {};
	private final ImportExportFileSystemService filesystemService;
	private final ImportExportOrientDbService orientDbService;
	private final CompressionService compressionService;
	private final StreamedEncryptionService streamedEncryptionService;
	private final char[] importExportZipPassword;

	public ImportExportService(final CompressionService compressionService,
			final ImportExportOrientDbService orientDbService,
			final ImportExportFileSystemService filesystemService,
			final StreamedEncryptionService streamedEncryptionService,
			@Value("${import.export.zip.password}")
			final char[] importExportZipPassword) {
		this.compressionService = compressionService;
		this.orientDbService = orientDbService;
		this.filesystemService = filesystemService;
		this.streamedEncryptionService = streamedEncryptionService;
		this.importExportZipPassword = importExportZipPassword;

		PasswordValidation.validate(importExportZipPassword, "importExportZipPassword");
	}

	public void importSdmData(final InputStream inputStream) {
		checkNotNull(inputStream);
		try (final InputStream decryptedStream = streamedEncryptionService.decrypt(inputStream, importExportZipPassword, ASSOCIATED_DATA)) {
			final Path unzipDirectory = unzip(decryptedStream);
			final Path sdmDirectory = unzipDirectory.resolve(Constants.SDM_DIR_NAME);

			importOrientDb(unzipDirectory);
			filesystemService.importFileSystem(sdmDirectory);

			deleteDirectory(unzipDirectory);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	private static void deleteDirectory(final Path directory) {
		try {
			FileSystemUtils.deleteRecursively(directory);
		} catch (final IOException e) {
			LOGGER.warn("Fail to remove directory. [directory: {}]", directory);
		}
	}

	private Path unzip(final InputStream inputStream) {
		try {
			final Path unzipDirectory = createTemporaryDirectory();
			compressionService.unzipToDirectory(inputStream, unzipDirectory);
			return unzipDirectory;
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot unzip the file.", e);
		}
	}

	private void importOrientDb(final Path directory) {
		try {
			final Path dbDump = directory.resolve(Constants.DBDUMP_FILE_NAME);
			orientDbService.importOrientDb(dbDump);
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot import orient DB.", e);
		}
	}

	public void exportSdmData(final OutputStream outputStream, final String electionEventId) {
		checkNotNull(outputStream);
		validateUUID(electionEventId);

		final Path zipDirectory = createTemporaryDirectory();
		final Path filesystem = zipDirectory.resolve(Constants.SDM_DIR_NAME);

		try {
			Files.createDirectories(filesystem);

			exportOrientDb(zipDirectory, electionEventId);
			filesystemService.exportFileSystem(electionEventId, filesystem);

			try (final PipedOutputStream zipPipedOutputStream = new PipedOutputStream();
					final PipedInputStream zipPipedInputStream = new PipedInputStream(zipPipedOutputStream)) {
				final Thread t = new Thread(() -> {
					try {
						compressionService.zipDirectory(zipPipedOutputStream, zipDirectory);
					} catch (final IOException e) {
						throw new UncheckedIOException(e);
					}
				});
				t.start();
				streamedEncryptionService.encrypt(outputStream, zipPipedInputStream, importExportZipPassword, ASSOCIATED_DATA);
			}
		} catch (final IOException e) {
			throw new UncheckedIOException("Error during export.", e);
		}
		deleteDirectory(zipDirectory);
	}

	private static Path createTemporaryDirectory() {
		try {
			final Path temporaryDirectory = Files.createTempDirectory(UUID.randomUUID().toString()).toAbsolutePath();

			LOGGER.info("Temporary directory created. [path: {}]", temporaryDirectory);

			return temporaryDirectory;
		} catch (final IOException e) {
			throw new UncheckedIOException("Unable to create temporary directory", e);
		}
	}

	private void exportOrientDb(final Path directory, final String electionEventId) {
		try {
			final Path dbDump = directory.resolve(Constants.DBDUMP_FILE_NAME);
			orientDbService.exportOrientDb(dbDump, electionEventId);
		} catch (final IOException | ResourceNotFoundException e) {
			throw new IllegalStateException("Cannot export orient DB.", e);
		}
	}
}
