/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmonline.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.UncheckedIOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.retry.RetryContext;
import org.springframework.retry.annotation.Retryable;
import org.springframework.retry.support.RetrySynchronizationManager;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.SetupComponentCMTablePayload;
import ch.post.it.evoting.domain.configuration.SetupComponentCMTablePayloadChunks;
import ch.post.it.evoting.securedatamanager.commons.ChunkedRequestBodyCreator;
import ch.post.it.evoting.securedatamanager.commons.RemoteService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.VoteVerificationClient;

import okhttp3.RequestBody;

@Repository
public class SetupComponentCMTablePayloadUploadRepository {
	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentCMTablePayloadUploadRepository.class);

	private final ObjectMapper objectMapper;

	private final boolean isVotingPortalEnabled;
	private final RetryableRemoteCall retryableRemoteCall;

	public SetupComponentCMTablePayloadUploadRepository(
			final ObjectMapper objectMapper,
			@Value("${voting.portal.enabled}")
			final boolean isVotingPortalEnabled,
			final RetryableRemoteCall retryableRemoteCall) {
		this.objectMapper = objectMapper;
		this.isVotingPortalEnabled = isVotingPortalEnabled;
		this.retryableRemoteCall = retryableRemoteCall;
	}

	/**
	 * Uploads the setup component CMTable payload to the vote verification.
	 *
	 * @param electionEventId                    the election event id. Must be non-null and a valid UUID.
	 * @param verificationCardSetId              the verification card set id. Must be non-null and a valid UUID.
	 * @param setupComponentCMTablePayloadChunks the list of {@link SetupComponentCMTablePayload} to upload. Must be non-null.
	 * @throws NullPointerException      if any of the inputs is null.
	 * @throws FailedValidationException if {@code electionEventId} or {@code verificationCardSetId} is invalid.
	 */
	public void uploadReturnCodesMappingTable(final String electionEventId, final String verificationCardSetId,
			final SetupComponentCMTablePayloadChunks setupComponentCMTablePayloadChunks) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkNotNull(setupComponentCMTablePayloadChunks);
		checkState(isVotingPortalEnabled, "The voting-portal connection is not enabled.");

		setupComponentCMTablePayloadChunks.payloads().stream()
				.parallel()
				.forEach(setupComponentCMTablePayload -> {
					final int chunkId = setupComponentCMTablePayload.getChunkId();
					LOGGER.debug("Uploading CMTable chunk. [electionEventId: {}, verificationCardSetId: {}, chunkId: {}]",
							electionEventId, verificationCardSetId, chunkId);

					final byte[] setupComponentCMTablePayloadBytes;
					try {
						setupComponentCMTablePayloadBytes = objectMapper.writeValueAsBytes(setupComponentCMTablePayload);
					} catch (final JsonProcessingException e) {
						throw new UncheckedIOException(
								String.format(
										"Failed to serialize SetupComponentCMTablePayload. [electionEventId: %s, verificationCardSetId: %s, chunkId: %s]",
										electionEventId, verificationCardSetId, chunkId), e);
					}
					final RequestBody requestBody = ChunkedRequestBodyCreator.forJsonPayload(setupComponentCMTablePayloadBytes);

					retryableRemoteCall.remoteCall(electionEventId, verificationCardSetId, chunkId, requestBody);
				});

		LOGGER.info("Successfully uploaded setup component CMTable payloads. [electionEventId: {}, verificationCardSetId: {}, chunkCount: {}]",
				electionEventId, verificationCardSetId, setupComponentCMTablePayloadChunks.getChunkCount());
	}

	@Service
	static class RetryableRemoteCall {
		private static final Logger LOGGER = LoggerFactory.getLogger(RetryableRemoteCall.class);
		private final VoteVerificationClient voteVerificationClient;

		public RetryableRemoteCall(final VoteVerificationClient voteVerificationClient) {
			this.voteVerificationClient = voteVerificationClient;
		}

		@Retryable(retryFor = UncheckedIOException.class, maxAttemptsExpression = "${cmtable-upload.retryable-calls.max-attempts}")
		void remoteCall(final String electionEventId, final String verificationCardSetId, final int chunkId, final RequestBody requestBody) {
			validateUUID(electionEventId);
			validateUUID(verificationCardSetId);
			checkArgument(chunkId >= 0);
			checkNotNull(requestBody);

			final RetryContext retryContext = RetrySynchronizationManager.getContext();
			final int retryNumber = retryContext.getRetryCount();
			if (retryNumber != 0) {
				LOGGER.warn(
						"Retrying of request for uploading setup component CMTable payload. [retryNumber: {}, electionEventId: {}, verificationCardSetId: {}, chunkId: {}]",
						retryNumber, electionEventId, verificationCardSetId, chunkId);
			}

			RemoteService.call(voteVerificationClient.saveReturnCodeMappingTable(electionEventId, verificationCardSetId, chunkId, requestBody))
					.networkErrorMessage("Failed to communicate with vote verification.")
					.unsuccessfulResponseErrorMessage(
							"Request for uploading setup component CMTable payloads failed. [electionEventId: %s, verificationCardSetId: %s, chunkId: %s]",
							electionEventId, verificationCardSetId, chunkId)
					.execute();
		}
	}
}
