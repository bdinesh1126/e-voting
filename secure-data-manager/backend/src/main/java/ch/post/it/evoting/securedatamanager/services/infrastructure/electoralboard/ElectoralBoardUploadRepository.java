/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.electoralboard;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.securedatamanager.commons.RemoteService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.VoteVerificationClient;

/**
 * Implements the repository for electoral board uploadDataInContext.
 */
@Repository
public class ElectoralBoardUploadRepository {

	private static final String VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE = "The voting-portal connection is not enabled.";

	private final VoteVerificationClient voteVerificationClient;

	private final boolean isVotingPortalEnabled;

	public ElectoralBoardUploadRepository(
			final VoteVerificationClient voteVerificationClient,
			@Value("${voting.portal.enabled}")
			final boolean isVotingPortalEnabled) {

		this.voteVerificationClient = voteVerificationClient;
		this.isVotingPortalEnabled = isVotingPortalEnabled;
	}

	/**
	 * Uploads the setup component public keys payload to the vote verification.
	 *
	 * @param setupComponentPublicKeysPayload the setup component public keys payload to upload. Must be non-null.
	 * @throws IllegalStateException if the upload was unsuccessful.
	 * @throws NullPointerException  if the setup component public keys payload is null.
	 */
	public void uploadSetupComponentPublicKeysData(final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload) {
		checkNotNull(setupComponentPublicKeysPayload);

		checkState(isVotingPortalEnabled, VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		final String electionEventId = setupComponentPublicKeysPayload.getElectionEventId();
		RemoteService.call(voteVerificationClient.uploadSetupComponentPublicKeys(electionEventId, setupComponentPublicKeysPayload))
				.networkErrorMessage("Failed to communicate with vote verification.")
				.unsuccessfulResponseErrorMessage("Request for uploading setup component public keys failed. [electionEventId: %s]", electionEventId)
				.execute();
	}

}
