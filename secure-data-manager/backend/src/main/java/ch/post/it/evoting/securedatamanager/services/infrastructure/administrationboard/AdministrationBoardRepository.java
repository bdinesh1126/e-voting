/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.administrationboard;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.cryptoprimitives.domain.election.AdministrationBoard;
import ch.post.it.evoting.securedatamanager.services.infrastructure.AbstractEntityRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.DatabaseManager;

/**
 * Implementation of operations with administration board.
 */
@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class AdministrationBoardRepository extends AbstractEntityRepository {

	/**
	 * Constructor.
	 *
	 * @param databaseManager
	 */
	@Autowired
	public AdministrationBoardRepository(final DatabaseManager databaseManager) {
		super(databaseManager);
	}

	@PostConstruct
	@Override
	public void initialize() {
		super.initialize();
	}

	@Override
	protected String entityName() {
		return AdministrationBoard.class.getSimpleName();
	}
}
