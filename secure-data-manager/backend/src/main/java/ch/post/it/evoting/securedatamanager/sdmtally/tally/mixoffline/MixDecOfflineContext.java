/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Regroups the context values needed by MixDecOffline algorithm.
 *
 * <ul>
 *     <li>(p, q, g), the encryption group. Non-null.</li>
 *     <li>ee, the election event id. A valid UUID.</li>
 *     <li>bb, the ballot box id. A valid UUID.</li>
 *     <li>delta_hat, the number of allowed write-ins + 1. Strictly positive.</li>
 * </ul>
 */
public class MixDecOfflineContext {

	private final GqGroup encryptionGroup;
	private final String electionEventId;
	private final String ballotBoxId;
	private final int numberOfAllowedWriteInsPlusOne;

	private MixDecOfflineContext(final GqGroup encryptionGroup, final String electionEventId, final String ballotBoxId,
			final int numberOfAllowedWriteInsPlusOne) {
		this.encryptionGroup = encryptionGroup;
		this.electionEventId = electionEventId;
		this.ballotBoxId = ballotBoxId;
		this.numberOfAllowedWriteInsPlusOne = numberOfAllowedWriteInsPlusOne;
	}

	public GqGroup getEncryptionGroup() {
		return encryptionGroup;
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public String getBallotBoxId() {
		return ballotBoxId;
	}

	public int getNumberOfAllowedWriteInsPlusOne() {
		return numberOfAllowedWriteInsPlusOne;
	}

	public static class Builder {

		private GqGroup encryptionGroup;
		private String electionEventId;
		private String ballotBoxId;
		private int numberOfAllowedWriteInsPlusOne;

		public Builder setEncryptionGroup(final GqGroup encryptionGroup) {
			this.encryptionGroup = encryptionGroup;
			return this;
		}

		public Builder setElectionEventId(final String electionEventId) {
			this.electionEventId = electionEventId;
			return this;
		}

		public Builder setBallotBoxId(final String ballotBoxId) {
			this.ballotBoxId = ballotBoxId;
			return this;
		}

		public Builder setNumberOfAllowedWriteInsPlusOne(final int numberOfAllowedWriteInsPlusOne) {
			this.numberOfAllowedWriteInsPlusOne = numberOfAllowedWriteInsPlusOne;
			return this;
		}

		/**
		 * Creates a MixDecOfflineContext object.
		 *
		 * @throws NullPointerException      if the encryption group, the election event id or the ballot box id is null.
		 * @throws FailedValidationException if the election event id or the ballot box id is not a valid UUID.
		 * @throws IllegalArgumentException  if the number of allowed write-ins + 1 is smaller or equal to 0.
		 */
		public MixDecOfflineContext build() {
			checkNotNull(encryptionGroup);
			validateUUID(electionEventId);
			validateUUID(ballotBoxId);
			checkArgument(numberOfAllowedWriteInsPlusOne > 0, "The number of allowed write-ins + 1 must be strictly greater than 0. [delta_hat: %s]",
					numberOfAllowedWriteInsPlusOne);

			return new MixDecOfflineContext(encryptionGroup, electionEventId, ballotBoxId, numberOfAllowedWriteInsPlusOne);
		}
	}
}
