/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.security.SignatureException;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.TallyComponentShufflePayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.securedatamanager.services.application.service.IdentifierValidationService;

/**
 * Handles the mixing and decryption step of the offline mixing.
 */
@Service
@ConditionalOnProperty("role.isTally")
public class MixDecOfflineService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MixDecOfflineService.class);

	private final TallyComponentShufflePayloadFileRepository tallyComponentShufflePayloadFileRepository;
	private final EncryptionParametersTallyService encryptionParametersTallyService;
	private final MixDecOfflineAlgorithm mixDecOfflineAlgorithm;
	private final SignatureKeystore<Alias> signatureKeystoreServiceSdmTally;
	private final IdentifierValidationService identifierValidationService;

	public MixDecOfflineService(final TallyComponentShufflePayloadFileRepository tallyComponentShufflePayloadFileRepository,
			final EncryptionParametersTallyService encryptionParametersTallyService,
			final MixDecOfflineAlgorithm mixDecOfflineAlgorithm,
			@Qualifier("keystoreServiceSdmTally")
			final SignatureKeystore<Alias> signatureKeystoreServiceSdmTally,
			final IdentifierValidationService identifierValidationService) {
		this.tallyComponentShufflePayloadFileRepository = tallyComponentShufflePayloadFileRepository;
		this.encryptionParametersTallyService = encryptionParametersTallyService;
		this.mixDecOfflineAlgorithm = mixDecOfflineAlgorithm;
		this.signatureKeystoreServiceSdmTally = signatureKeystoreServiceSdmTally;
		this.identifierValidationService = identifierValidationService;
	}

	/**
	 * Mixes and decrypts the votes in the specified ballot box and saves the {@code TallyComponentShufflePayload} file.
	 *
	 * @param electionEventId                the identifier of the election. Must be non-null and a valid UUID.
	 * @param ballotId                       the identifier of the ballot. Must be non-null and a valid UUID.
	 * @param ballotBoxId                    the identifier of the ballot box. Must be non-null and a valid UUID.
	 * @param numberOfAllowedWriteInsPlusOne the number of write-ins + 1. Must be greater or equal to 1.
	 * @param lastNodePayload                the control component shuffle payload of the last node. Must be non-null.
	 * @return the tally component shuffle payload containing the output of the algorithm MixDecOffline.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if any of the IDs is not a valid UUID.
	 * @throws IllegalArgumentException  if number of write-ins + 1 is strictly smaller than 1.
	 */
	public TallyComponentShufflePayload mixDecrypt(final String electionEventId, final String ballotId, final String ballotBoxId,
			final int numberOfAllowedWriteInsPlusOne, final ControlComponentShufflePayload lastNodePayload,
			final List<char[]> electoralBoardMembersPasswords) {
		validateUUID(electionEventId);
		validateUUID(ballotId);
		validateUUID(ballotBoxId);
		identifierValidationService.validateBallotBoxRelatedIds(electionEventId, ballotBoxId);
		identifierValidationService.validateBallotId(ballotId, ballotBoxId);
		checkNotNull(lastNodePayload);
		checkArgument(numberOfAllowedWriteInsPlusOne >= 1, "The number of allowed write-ins + 1 must be at least 1.");

		checkNotNull(electoralBoardMembersPasswords);
		electoralBoardMembersPasswords.forEach(Preconditions::checkNotNull);
		final List<char[]> passwords = electoralBoardMembersPasswords.stream().map(char[]::clone).toList();
		checkArgument(passwords.size() >= 2);
		// Wipe the passwords after usage
		electoralBoardMembersPasswords.forEach(pw -> Arrays.fill(pw, '0'));

		LOGGER.info("Mixing and decrypting. [electionEventId: {}, ballotBoxId: {}]", electionEventId, ballotBoxId);
		final GqGroup encryptionGroup = encryptionParametersTallyService.loadEncryptionGroup(electionEventId);
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> ciphertexts = lastNodePayload.getVerifiableDecryptions().getCiphertexts();

		final MixDecOfflineContext mixDecOfflineContext = new MixDecOfflineContext.Builder()
				.setEncryptionGroup(encryptionGroup)
				.setElectionEventId(electionEventId)
				.setBallotBoxId(ballotBoxId)
				.setNumberOfAllowedWriteInsPlusOne(numberOfAllowedWriteInsPlusOne)
				.build();
		final MixDecOfflineInput mixDecOfflineInput = new MixDecOfflineInput(ciphertexts, passwords);

		final Instant start = Instant.now();

		final MixDecOfflineOutput mixDecOfflineOutput = mixDecOfflineAlgorithm.mixDecOffline(mixDecOfflineContext, mixDecOfflineInput);
		LOGGER.info("Ballot box mixed and decrypted. [electionEventId: {}, ballotId: {}, ballotBoxId: {}, duration: {}]", electionEventId, ballotId,
				ballotBoxId, Duration.between(start, Instant.now()));

		final TallyComponentShufflePayload finalPayload = new TallyComponentShufflePayload(encryptionGroup, electionEventId, ballotBoxId,
				mixDecOfflineOutput.getVerifiableShuffle(), mixDecOfflineOutput.getVerifiablePlaintextDecryption());
		finalPayload.setSignature(getPayloadSignature(finalPayload));

		tallyComponentShufflePayloadFileRepository.savePayload(electionEventId, ballotId, ballotBoxId, finalPayload);
		LOGGER.info("Final payload persisted. [electionEventId: {}, ballotId: {}, ballotBoxId: {}]", electionEventId, ballotId, ballotBoxId);

		return finalPayload;
	}

	private CryptoPrimitivesSignature getPayloadSignature(final TallyComponentShufflePayload payload) {
		final String electionEventId = payload.getElectionEventId();
		final String ballotBoxId = payload.getBallotBoxId();

		final Hashable additionalContextData = ChannelSecurityContextData.tallyComponentShuffle(electionEventId, ballotBoxId);

		try {

			final byte[] signature = signatureKeystoreServiceSdmTally.generateSignature(payload, additionalContextData);

			return new CryptoPrimitivesSignature(signature);

		} catch (final SignatureException se) {
			throw new IllegalStateException(
					String.format("Failed to generate payload signature [%s, %s]", payload.getClass().getSimpleName(), additionalContextData), se);
		}
	}
}
