/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api.setup;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.io.IOException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.post.it.evoting.securedatamanager.services.application.exception.CCKeysNotExistException;
import ch.post.it.evoting.securedatamanager.services.application.service.ElectionEventService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * The election event end-point.
 */
@RestController
@RequestMapping("/sdm-backend/electionevents")
@Api(value = "Election event REST API")
@ConditionalOnProperty("role.isSetup")
public class ElectionEventSetupController {

	private static final String KEYS_NOT_EXIST = "KEYS_NOT_EXIST";
	private static final String KEYS_NOT_EXIST_DESCRIPTION = "The Control Components keys do not exist for this election event.";

	private final ElectionEventService electionEventService;

	public ElectionEventSetupController(
			final ElectionEventService electionEventService) {
		this.electionEventService = electionEventService;
	}

	/**
	 * Runs the securization of an election event.
	 *
	 * @param electionEventId the election event id.
	 * @return a list of ids of the created election events.
	 */
	@PostMapping(value = "/{electionEventId}", produces = "application/json")
	@ApiOperation(value = "Secure election event", notes = "Service to secure an election event.")
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Not Found") })
	public ResponseEntity<String> secureElectionEvent(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final
			String electionEventId) throws IOException {

		validateUUID(electionEventId);

		final boolean result;
		try {
			result = electionEventService.create(electionEventId);
		} catch (final CCKeysNotExistException e) {
			final ObjectNode errorJson = prepareJsonError(electionEventId, KEYS_NOT_EXIST, KEYS_NOT_EXIST_DESCRIPTION);
			return new ResponseEntity<>(errorJson.toString(), HttpStatus.BAD_REQUEST);
		}

		if (result) {
			return new ResponseEntity<>(HttpStatus.CREATED);
		}

		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	private ObjectNode prepareJsonError(final String electionEventId, final String code, final String description) {
		final ObjectNode rootNode = JsonNodeFactory.instance.objectNode();
		final ObjectNode errorNode = rootNode.putObject("error");
		errorNode.put("code", code);
		errorNode.put("description", description);
		final ObjectNode context = errorNode.putObject("context");
		context.put("electionEventId", electionEventId);

		return rootNode;
	}
}
