/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.EncryptionParametersPayload;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@Repository
public class EncryptionParametersPayloadFileRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(EncryptionParametersPayloadFileRepository.class);

	private final PathResolver pathResolver;

	private final ObjectMapper objectMapper;

	public EncryptionParametersPayloadFileRepository(final PathResolver pathResolver, final ObjectMapper objectMapper) {
		this.pathResolver = pathResolver;
		this.objectMapper = objectMapper;
	}

	/**
	 * Persists an encryption parameters payload to the file system.
	 *
	 * @param electionEventId             the election event id. Must be non-null and a valid UUID.
	 * @param encryptionParametersPayload the encryption parameters payload to persist. Must be non-null.
	 * @return the path where the encryption parameters payload has been successfully persisted.
	 * @throws NullPointerException if {@code encryptionParametersPayload} is null.
	 * @throws UncheckedIOException if the serialization of the encryption parameters payload fails.
	 */
	public Path save(final String electionEventId, final EncryptionParametersPayload encryptionParametersPayload) {
		validateUUID(electionEventId);
		checkNotNull(encryptionParametersPayload);

		final Path filePath = getStorageFilePath(electionEventId);

		try {
			final byte[] payloadBytes = objectMapper.writeValueAsBytes(encryptionParametersPayload);

			final Path writePath = Files.write(filePath, payloadBytes);
			LOGGER.info("EncryptionParameters payload saved. [path: {}]", writePath);

			return writePath;
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Failed to serialize encryption parameters payload. [electionEventId: %s, path: %s]", electionEventId, filePath),
					e);
		}
	}

	/**
	 * Retrieves from the file system an encryption parameters payload by election event id.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @return the encryption parameters payload with the given id or {@link Optional#empty} if none found.
	 * @throws FailedValidationException if {@code electionEventId} is null or not a valid UUID.
	 * @throws UncheckedIOException      if the deserialization of the encryption parameters payload fails.
	 */
	public Optional<EncryptionParametersPayload> findById(final String electionEventId) {
		validateUUID(electionEventId);

		final Path payloadPath = getStorageFilePath(electionEventId);

		if (!Files.exists(payloadPath)) {
			LOGGER.info("Requested encryption parameters payload does not exist. [electionEventId: {}, path: {}]", electionEventId, payloadPath);
			return Optional.empty();
		}

		try {
			return Optional.of(objectMapper.readValue(payloadPath.toFile(), EncryptionParametersPayload.class));
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Could not deserialize encryption parameters payload. [electionEventId: %s]", electionEventId), e);
		}
	}

	/**
	 * Retrieves from the {@value Constants#SDM_INPUT_FILES_BASE_DIR} directory an encryption parameters payload.
	 *
	 * @return the encryption parameters payload or {@link Optional#empty} if none found.
	 * @throws UncheckedIOException if the deserialization of the encryption parameters payload fails.
	 */
	public Optional<EncryptionParametersPayload> find() {
		final Path payloadPath = pathResolver.resolveInputPath().resolve(Constants.CONFIG_FILE_NAME_ENCRYPTION_PARAMETERS_PAYLOAD_JSON);

		if (!Files.exists(payloadPath)) {
			LOGGER.info("Requested encryption parameters payload does not exist. [path: {}]", payloadPath);
			return Optional.empty();
		}

		try {
			return Optional.of(objectMapper.readValue(payloadPath.toFile(), EncryptionParametersPayload.class));
		} catch (final IOException e) {
			throw new UncheckedIOException("Could not deserialize encryption parameters payload.", e);
		}
	}

	/**
	 * Renames the encryption parameters file from {@value Constants#CONFIG_FILE_NAME_ENCRYPTION_PARAMETERS_PAYLOAD_JSON} to
	 * {ee}-{@value Constants#CONFIG_FILE_NAME_ENCRYPTION_PARAMETERS_PAYLOAD_JSON}.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 */
	public void renameEncryptionParametersFile(final String electionEventId) throws IOException {
		validateUUID(electionEventId);
		Files.move(pathResolver.resolveInputPath().resolve(Constants.CONFIG_FILE_NAME_ENCRYPTION_PARAMETERS_PAYLOAD_JSON),
				pathResolver.resolveInputPath().resolve(electionEventId + "-" + Constants.CONFIG_FILE_NAME_ENCRYPTION_PARAMETERS_PAYLOAD_JSON));
	}

	private Path getStorageFilePath(final String electionEventId) {
		return pathResolver.resolveInputPath(electionEventId).resolve(Constants.CONFIG_FILE_NAME_ENCRYPTION_PARAMETERS_PAYLOAD_JSON);
	}
}
