/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

/**
 * Allows performing operations with the node contributions responses.
 */
@Repository
public class NodeContributionsResponsesFileRepository {

	private static final Pattern FILE_PATTERN = Pattern.compile("^controlComponentCodeSharesPayload\\.([\\d]+)\\.json$");
	private static final ToIntFunction<Path> EXTRACT_CHUNK_ID_FROM_PATH = chunkFilePath -> {
		final Matcher matcher = FILE_PATTERN.matcher(chunkFilePath.getFileName().toString());
		if (matcher.find()) {
			return Integer.parseInt(matcher.group(1));
		}
		throw new IllegalStateException(String.format("No chunk id found. [path: %s]", chunkFilePath));
	};
	private static final Logger LOGGER = LoggerFactory.getLogger(NodeContributionsResponsesFileRepository.class);

	private final ObjectMapper objectMapper;
	private final PathResolver pathResolver;

	public NodeContributionsResponsesFileRepository(
			final ObjectMapper objectMapper,
			final PathResolver pathResolver) {
		this.objectMapper = objectMapper;
		this.pathResolver = pathResolver;
	}

	/**
	 * Retrieves all node contributions response chunk paths corresponding to the given election event id and verification card set id.
	 *
	 * @param electionEventId       the node contributions responses' election event id. Must be non-null and a valid UUID.
	 * @param verificationCardSetId the node contributions responses' verification card set id. Must be non-null and a valid UUID.
	 * @return all node contributions response chunk paths.
	 * @throws FailedValidationException if {@code electionEventId} or {@code verificationCardSetId} is invalid.
	 * @throws UncheckedIOException      if the deserialization of the payload fails.
	 */
	public List<Path> findAllPathsOrderByChunkId(final String electionEventId, final String verificationCardSetId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		final Path verificationCardSetPath = pathResolver.resolveVerificationCardSetPath(electionEventId, verificationCardSetId);
		final Predicate<String> patternPredicate = FILE_PATTERN.asPredicate();

		if (!Files.exists(verificationCardSetPath)) {
			LOGGER.warn("Requested verification card set directory does not exist. [electionEventId: {}, verificationCardSetId: {}, path: {}]",
					electionEventId, verificationCardSetId, verificationCardSetPath);
			return List.of();
		}

		try (final Stream<Path> paths = Files.walk(verificationCardSetPath, 1)) {
			return paths.filter(path -> patternPredicate.test(path.getFileName().toString()))
					.sorted(Comparator.comparingInt(EXTRACT_CHUNK_ID_FROM_PATH))
					.toList();
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Failed to walk verification card set directory. [electionEventId: %s, verificationCardSetId: %s, path: %s]",
							electionEventId, verificationCardSetId, verificationCardSetPath), e);
		}
	}

	public int getChunkId(Path path) {
		return EXTRACT_CHUNK_ID_FROM_PATH.applyAsInt(path);
	}

	public List<ControlComponentCodeSharesPayload> getControlComponentCodeSharesPayloads(final Path path) {
		try {
			final List<ControlComponentCodeSharesPayload> controlComponentCodeSharesPayloads = objectMapper.readValue(
					path.toFile(),
					new TypeReference<>() {
					});
			checkState(getChunkId(path) == controlComponentCodeSharesPayloads.get(0).getChunkId());
			return controlComponentCodeSharesPayloads.stream()
					.sorted(Comparator.comparingInt(ControlComponentCodeSharesPayload::getNodeId))
					.toList();
		} catch (final IOException e) {
			throw new UncheckedIOException(String.format("Failed to deserialize the ControlComponentCodeSharesPayloads. [path: %s]", path), e);
		}
	}

}
