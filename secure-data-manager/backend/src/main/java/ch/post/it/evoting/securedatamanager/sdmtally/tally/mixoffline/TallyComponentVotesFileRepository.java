/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.evotinglibraries.domain.tally.TallyComponentVotesPayload;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@Repository
public class TallyComponentVotesFileRepository {
	private static final Logger LOGGER = LoggerFactory.getLogger(TallyComponentVotesFileRepository.class);
	private static final String TALLY_COMPONENT_VOTES_FILENAME = "tallyComponentVotesPayload" + Constants.JSON;
	private final PathResolver pathResolver;
	private final ObjectMapper objectMapper;

	public TallyComponentVotesFileRepository(final PathResolver pathResolver, final ObjectMapper objectMapper) {
		this.pathResolver = pathResolver;
		this.objectMapper = objectMapper;
	}

	/**
	 * Checks if the file {@value TALLY_COMPONENT_VOTES_FILENAME} exists for the given parameters.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID
	 * @param ballotId        the ballot id. Must be non-null and a valid UUID
	 * @param ballotBoxId     the ballot box id. Must be non-null and a valid UUID
	 * @return true is the file exists
	 * @throws IllegalArgumentException if the ballot id, the ballot box id or the election event id is not a valid UUID.
	 */
	public boolean exists(final String electionEventId, final String ballotId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotId);
		validateUUID(ballotBoxId);

		final Path path = pathResolver.resolveBallotBoxPath(electionEventId, ballotId, ballotBoxId).resolve(TALLY_COMPONENT_VOTES_FILENAME);
		return Files.exists(path);
	}

	/**
	 * Persists the payload as a file to the corresponding directory
	 *
	 * @param payload the payload to persist.
	 * @return the path where the file has been persisted.
	 * @throws IllegalStateException if the payload file already exists
	 */
	public Path save(TallyComponentVotesPayload payload) {
		checkNotNull(payload);

		try {
			final Path payloadPath = pathResolver.resolveBallotBoxPath(payload.getElectionEventId(), payload.getBallotId(), payload.getBallotBoxId())
					.resolve(TALLY_COMPONENT_VOTES_FILENAME);
			final byte[] payloadBytes = objectMapper.writeValueAsBytes(payload);
			final Path writePath = Files.write(payloadPath, payloadBytes);
			LOGGER.debug("Successfully persisted tally component votes payload. [electionEventId: {}, ballotId: {}, ballotBoxId: {}, path: {}]",
					payload.getElectionEventId(), payload.getBallotId(), payload.getBallotBoxId(), payloadPath);
			return writePath;
		} catch (IOException e) {
			throw new UncheckedIOException(
					String.format("Failed to serialize tally component votes payload. [electionEventId: %s, ballotId: %s, ballotBoxId: %s]",
							payload.getElectionEventId(), payload.getBallotId(), payload.getBallotBoxId()), e);
		}
	}

	/**
	 * Reads the payload from file {@value TALLY_COMPONENT_VOTES_FILENAME}
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID
	 * @param ballotId        the ballot id. Must be non-null and a valid UUID
	 * @param ballotBoxId     the ballot box id. Must be non-null and a valid UUID
	 * @return the payload as {@link Optional}
	 * @throws NullPointerException      if the parameters are null
	 * @throws UncheckedIOException      if unable to deserialize the payload
	 * @throws FailedValidationException if the parameters are not valid UUID
	 */
	public Optional<TallyComponentVotesPayload> load(final String electionEventId, final String ballotId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotId);
		validateUUID(ballotBoxId);

		final Path filePath = pathResolver.resolveBallotBoxPath(electionEventId, ballotId, ballotBoxId).resolve(TALLY_COMPONENT_VOTES_FILENAME);

		if (!Files.exists(filePath)) {
			LOGGER.debug(
					"Requested tally component votes payload does not exist. [electionEventId: {}, ballotId: {}, ballotBoxId: {}, path: {}]",
					electionEventId, ballotId, ballotBoxId, filePath);
			return Optional.empty();
		}

		try {
			return Optional.of(objectMapper.readValue(filePath.toFile(), TallyComponentVotesPayload.class));
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Failed to deserialize tally component votes payload. [electionEventId: %s, ballotId: %s, ballotBoxId: %s, path: %s]",
							electionEventId, ballotId, ballotBoxId, filePath), e);
		}
	}
}
