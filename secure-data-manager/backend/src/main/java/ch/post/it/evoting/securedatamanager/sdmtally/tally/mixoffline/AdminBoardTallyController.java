/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;

/**
 * The admin board end-point.
 */
@RestController
@RequestMapping("/sdm-backend/tally/adminboard-management")
@Api(value = "Administration Board REST API")
@ConditionalOnProperty("role.isTally")
public class AdminBoardTallyController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminBoardTallyController.class);

	private final AdminBoardTallyService adminBoardTallyService;

	public AdminBoardTallyController(final AdminBoardTallyService adminBoardTallyService) {
		this.adminBoardTallyService = adminBoardTallyService;
	}

	/**
	 * Activates the adminBoards.
	 */
	@PutMapping(value = "adminboards/{adminBoardId}")
	@ResponseStatus(HttpStatus.OK)
	public void activate(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String adminBoardId,
			@ApiParam(value = "adminBoardPasswords", required = true)
			@RequestBody
			final List<char[]> adminBoardPasswords) {

		validateUUID(adminBoardId);
		checkNotNull(adminBoardPasswords);

		LOGGER.info("Received request to constitute administration board. [adminBoardId: {}]", adminBoardId);

		adminBoardTallyService.activate(adminBoardId, adminBoardPasswords);
	}
}
