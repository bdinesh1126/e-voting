/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * The context of the ProcessPlaintexts algorithm.
 *
 * @param encryptionGroup    (p,q,g), the encryption group of the election. Non-null.
 * @param primesMappingTable pTable, the primes mapping table. Non-null Its prime elements belong to the encryption group.
 */
public record ProcessPlaintextsContext(GqGroup encryptionGroup, PrimesMappingTable primesMappingTable) {

	public ProcessPlaintextsContext {
		checkNotNull(encryptionGroup);
		checkNotNull(primesMappingTable);
		checkArgument(primesMappingTable.getPTable().getGroup().equals(encryptionGroup),
				"The primes mapping table's entries must belong to the encryption group.");
	}
}
