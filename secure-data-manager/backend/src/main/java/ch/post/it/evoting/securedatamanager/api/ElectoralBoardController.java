/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.securedatamanager.services.infrastructure.electoralboard.ElectoralBoardRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * The REST endpoint for accessing electoral board data.
 */
@RestController
@RequestMapping("/sdm-backend/electoralboard-management")
@Api(value = "Electoral board REST API")
public class ElectoralBoardController {
	private final ElectoralBoardRepository electoralBoardRepository;

	public ElectoralBoardController(final ElectoralBoardRepository electoralBoardRepository) {
		this.electoralBoardRepository = electoralBoardRepository;
	}

	/**
	 * Returns a list of electoral boards identified by an election event identifier.
	 *
	 * @param electionEventId the election event id.
	 * @return a list of electoral boards belong to an election event.
	 */
	@GetMapping(value = "electionevents/{electionEventId}/electoralboards", produces = "application/json")
	@ResponseBody
	@ApiOperation(value = "List electoral boards", response = String.class, notes = "Service to retrieve the list of electoral boards for a given election event.")
	public String getElectoralBoards(
			@PathVariable
			final String electionEventId) {

		validateUUID(electionEventId);

		return electoralBoardRepository.listByElectionEvent(electionEventId);
	}
}
