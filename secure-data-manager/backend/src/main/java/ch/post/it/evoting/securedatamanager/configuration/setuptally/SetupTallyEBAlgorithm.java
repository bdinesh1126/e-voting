/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setuptally;

import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.integerToString;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import com.google.common.collect.Streams;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableBigInteger;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableByteArray;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;

/**
 * Implements the SetupTallyEB algorithm.
 */
@Service
public class SetupTallyEBAlgorithm {

	private static final String SETUP_TALLY_CCM = "SetupTallyCCM";
	private final Hash hash;
	private final ElGamal elGamal;
	private final ZeroKnowledgeProof zeroKnowledgeProof;

	public SetupTallyEBAlgorithm(final Hash hash, final ElGamal elGamal,
			final ZeroKnowledgeProof zeroKnowledgeProof) {
		this.hash = hash;
		this.elGamal = elGamal;
		this.zeroKnowledgeProof = zeroKnowledgeProof;
	}

	/**
	 * Generates the last key pair (electoral board key pair (EB<sub>pk</sub>, EB<sub>sk</sub>)) and combines the CCMs' election public keys to yield
	 * the election public key (EL<sub>pk</sub>).
	 *
	 * @param context the {@link SetupTallyEBContext} containing all needed context. Non-null.
	 * @param input   the {@link SetupTallyEBInput} containing all needed inputs. Non-null.
	 * @return the election public key and the electoral board public key encapsulated in a {@link SetupTallyEBOutput}.
	 * @throws NullPointerException     if any parameter is null.
	 * @throws IllegalArgumentException if
	 *                                  <ul>
	 *                                      <li>δ is not smaller or equal to μ.</li>
	 *                                      <li>k is not greater or equal to 2.</li>
	 *                                  </ul>
	 */
	@SuppressWarnings("java:S117")
	public SetupTallyEBOutput setupTallyEB(final SetupTallyEBContext context, final SetupTallyEBInput input) {
		checkNotNull(context);
		checkNotNull(input);

		// Context.
		final GqGroup encryptionGroup = context.encryptionGroup();
		final BigInteger q = encryptionGroup.getQ();
		final String ee = context.electionEventId();

		// Variables.
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> EL_pk_list = input.ccmElectionPublicKeys();
		final GroupVector<GroupVector<SchnorrProof, ZqGroup>, ZqGroup> pi_EL_pk = input.schnorrProofs();
		final int delta = input.maxWriteInsInAllVerificationCardSets() + 1;
		final List<HashableByteArray> PW = input.electoralBoardMembersPasswords().stream()
				.map(PW_i -> HashableByteArray.from(StandardCharsets.UTF_8.encode(CharBuffer.wrap(PW_i)).array()))
				.toList();
		final int k = PW.size();
		final int mu = EL_pk_list.getElementSize();

		// Group cross-check
		checkArgument(EL_pk_list.getGroup().equals(encryptionGroup),
				"The group of the CCM election public keys must be equal to the context group.");

		// Require.
		checkArgument(delta <= mu, "Requires delta <= mu.");
		checkArgument(k >= 2, "Requires k >= 2.");

		// Operation.
		final List<ElGamalMultiRecipientPublicKey> EL_pk_prime = new ArrayList<>(4);
		final List<List<String>> i_aux = new ArrayList<>(4);
		IntStream.range(0, 4)
				.forEach(j -> {
					final ElGamalMultiRecipientPublicKey EL_pk_j_prime =
							new ElGamalMultiRecipientPublicKey(GroupVector.from(EL_pk_list.get(j).getKeyElements().subList(0, delta)));
					final List<String> i_aux_j = Arrays.asList(ee, SETUP_TALLY_CCM, integerToString(j + 1));

					EL_pk_prime.add(EL_pk_j_prime);
					i_aux.add(i_aux_j);
				});

		final List<String> i_aux_EB = Arrays.asList(ee, "SetupTallyEB");

		final List<GqElement> EB_pk_elements = new ArrayList<>();
		final List<SchnorrProof> pi_EB_elements = new ArrayList<>();
		for (int i = 0; i < delta; i++) {
			// Since we have 0-indexing in Java, the indexes 1-4 become indexes 0-3 in the lines below
			final boolean verifSch_1 = zeroKnowledgeProof.verifySchnorrProof(pi_EL_pk.get(0).get(i), EL_pk_list.get(0).get(i), i_aux.get(0));
			final boolean verifSch_2 = zeroKnowledgeProof.verifySchnorrProof(pi_EL_pk.get(1).get(i), EL_pk_list.get(1).get(i), i_aux.get(1));
			final boolean verifSch_3 = zeroKnowledgeProof.verifySchnorrProof(pi_EL_pk.get(2).get(i), EL_pk_list.get(2).get(i), i_aux.get(2));
			final boolean verifSch_4 = zeroKnowledgeProof.verifySchnorrProof(pi_EL_pk.get(3).get(i), EL_pk_list.get(3).get(i), i_aux.get(3));

			if (!(verifSch_1 && verifSch_2 && verifSch_3 && verifSch_4)) {
				throw new IllegalStateException(String.format(
						"Schnorr proofs verification failed. [electionEventId: %s, verifSch_1: %s, verifSch_2: %s, verifSch_3: %s, verifSch_4: %s]",
						ee, verifSch_1, verifSch_2, verifSch_3, verifSch_4));
			}

			final ZqElement EB_sk_i = hash.recursiveHashToZq(q,
					Streams.concat(Stream.of(HashableString.from("ElectoralBoardSecretKey"), HashableString.from(ee),
							HashableBigInteger.from(BigInteger.valueOf(i))), PW.stream()).collect(HashableList.toHashableList()));
			final GqElement EB_pk_i = encryptionGroup.getGenerator().exponentiate(EB_sk_i);
			final SchnorrProof pi_EB_i = zeroKnowledgeProof.genSchnorrProof(EB_sk_i, EB_pk_i, i_aux_EB);

			EB_pk_elements.add(EB_pk_i);
			pi_EB_elements.add(pi_EB_i);
		}

		final ElGamalMultiRecipientPublicKey EB_pk = new ElGamalMultiRecipientPublicKey(GroupVector.from(EB_pk_elements));
		final GroupVector<SchnorrProof, ZqGroup> pi_EB = GroupVector.from(pi_EB_elements);
		// Since we have 0-indexing in Java, the indexes 1-4 become indexes 0-3
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> EL_pk_prime_vector_EB_pk = GroupVector.of(EL_pk_prime.get(0), EL_pk_prime.get(1),
				EL_pk_prime.get(2), EL_pk_prime.get(3), EB_pk);
		final ElGamalMultiRecipientPublicKey EL_pk = elGamal.combinePublicKeys(EL_pk_prime_vector_EB_pk);

		// Wipe the passwords after usage
		PW.forEach(PW_i -> Arrays.fill(PW_i.toHashableForm(), (byte) 0));
		input.electoralBoardMembersPasswords().forEach(PW_i -> Arrays.fill(PW_i, '0'));

		// Output.
		return new SetupTallyEBOutput(EL_pk, EB_pk, pi_EB);
	}
}
