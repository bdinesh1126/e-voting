/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballot.BallotRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Endpoint for managing ballot information.
 */
@RestController
@RequestMapping("/sdm-backend/ballots")
@Api(value = "Ballot REST API")
public class BallotController {

	private final BallotRepository ballotRepository;

	public BallotController(final BallotRepository ballotRepository) {
		this.ballotRepository = ballotRepository;
	}

	/**
	 * Returns a list of ballots identified by an election event identifier.
	 *
	 * @param electionEventId the election event id. Must be non-null and valid UUID.
	 * @return list of ballots identified by an election event identifier.
	 * @throws NullPointerException      if the election event id is null.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 */
	@GetMapping(value = "/electionevent/{electionEventId}", produces = "application/json")
	@ResponseBody
	@ApiOperation(value = "List of ballots", notes = "Service to retrieve the list of ballots.", response = String.class)
	public String getBallots(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final
			String electionEventId) {

		validateUUID(electionEventId);

		return ballotRepository.listByElectionEvent(electionEventId);
	}
}
