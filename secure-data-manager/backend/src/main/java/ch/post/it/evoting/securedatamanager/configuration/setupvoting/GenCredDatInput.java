/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import ch.post.it.evoting.cryptoprimitives.domain.validations.Validations;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.securedatamanager.commons.Constants;

public record GenCredDatInput(List<String> verificationCardIds, GroupVector<ZqElement, ZqGroup> verificationCardSecretKeys,
							  List<String> startVotingKeys) {

	public GenCredDatInput(final List<String> verificationCardIds, final GroupVector<ZqElement, ZqGroup> verificationCardSecretKeys,
			final List<String> startVotingKeys) {
		final int l_SVK = Constants.SVK_LENGTH;

		checkNotNull(verificationCardIds);
		checkNotNull(verificationCardSecretKeys);
		checkNotNull(startVotingKeys);

		final List<String> verificationCardIdsCopy = verificationCardIds.stream().map(Validations::validateUUID).toList();
		final List<String> startVotingKeysCopy = startVotingKeys.stream().map(Validations::validateSVK).toList();

		checkArgument(!verificationCardIdsCopy.isEmpty(), "The vector of verification card ID must not be empty.");
		checkArgument(!verificationCardSecretKeys.isEmpty(), "The vector of verification card secret key must not be empty.");
		checkArgument(!startVotingKeysCopy.isEmpty(), "The start voting keys must not be empty.");
		checkArgument(startVotingKeysCopy.stream().allMatch(vc -> vc.length() == l_SVK),
				"The elements of the start voting keys must have the correct size.");

		final int N_E = verificationCardIdsCopy.size();
		checkArgument(N_E == verificationCardSecretKeys.size() && N_E == startVotingKeysCopy.size(),
				"All vectors must have the same size.");

		this.verificationCardIds = verificationCardIdsCopy;
		this.verificationCardSecretKeys = verificationCardSecretKeys;
		this.startVotingKeys = startVotingKeysCopy;
	}
}
