/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;

@Service
@ConditionalOnProperty("role.isTally")
public class CantonConfigTallyService {

	private final CantonConfigTallyFileRepository cantonConfigTallyFileRepository;

	public CantonConfigTallyService(final CantonConfigTallyFileRepository cantonConfigTallyFileRepository) {
		this.cantonConfigTallyFileRepository = cantonConfigTallyFileRepository;
	}

	/**
	 * Loads the canton config for the given election event id and validates its signature.
	 * <p>
	 * The result of this method is cached.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @return the canton config.
	 * @throws NullPointerException      if the election event id is null.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 */
	@Cacheable("cantonConfigTallys")
	public Configuration load(final String electionEventId) {
		validateUUID(electionEventId);

		return cantonConfigTallyFileRepository.load(electionEventId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Could not find the requested canton config file. [electionEventId: %s]", electionEventId)));
	}
}
