/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.util.HashMap;
import java.util.Map;

import javax.json.JsonObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.securedatamanager.commons.JsonUtils;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * The voting card set end-point.
 */
@RestController
@RequestMapping("/sdm-backend/votingcardsets")
@Api(value = "Voting card set REST API")
public class VotingCardSetController {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetController.class);

	private final VotingCardSetRepository votingCardSetRepository;

	public VotingCardSetController(final VotingCardSetRepository votingCardSetRepository) {
		this.votingCardSetRepository = votingCardSetRepository;
	}

	/**
	 * Returns a voting card set identified by election event and its id.
	 *
	 * @param electionEventId the election event id.
	 * @param votingCardSetId the voting card set id.
	 * @return An voting card set identified by election event and its id.
	 */
	@GetMapping(value = "/electionevent/{electionEventId}/votingcardset/{votingCardSetId}", produces = "application/json")
	@ResponseBody
	@ApiOperation(value = "Get voting card set", notes = "Service to retrieve a given voting card set.", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Not Found") })
	public ResponseEntity<String> getVotingCardSet(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId,
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String votingCardSetId) {

		validateUUID(electionEventId);
		validateUUID(votingCardSetId);

		final Map<String, Object> attributeValueMap = new HashMap<>();
		attributeValueMap.put(JsonConstants.ID, votingCardSetId);
		attributeValueMap.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);
		final String result = votingCardSetRepository.find(attributeValueMap);
		final JsonObject jsonObject = JsonUtils.getJsonObject(result);
		if (!jsonObject.isEmpty()) {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
		LOGGER.info("No voting card set found. [electionEventId: {}, votingCardSetId: {}]", electionEventId, votingCardSetId);
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	/**
	 * Returns a list of all voting card sets.
	 *
	 * @param electionEventId the election event id.
	 * @return The list of voting card sets.
	 */
	@GetMapping(value = "/electionevent/{electionEventId}", produces = "application/json")
	@ResponseBody
	@ApiOperation(value = "List voting card sets", notes = "Service to retrieve the list of voting card sets.", response = String.class)
	public String getVotingCardSets(
			@PathVariable
			final String electionEventId) {
		validateUUID(electionEventId);

		final Map<String, Object> attributeValueMap = new HashMap<>();
		attributeValueMap.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);
		return votingCardSetRepository.list(attributeValueMap);
	}
}
