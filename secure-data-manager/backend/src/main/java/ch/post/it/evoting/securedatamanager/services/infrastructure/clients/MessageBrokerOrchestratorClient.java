/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.clients;

import java.util.List;

import javax.ws.rs.core.MediaType;

import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.domain.configuration.ControlComponentKeyGenerationRequestPayload;
import ch.post.it.evoting.domain.configuration.setupvoting.ComputingStatus;
import ch.post.it.evoting.domain.configuration.setupvoting.SetupComponentLVCCAllowListPayload;
import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.domain.tally.MixDecryptOnlinePayload;
import ch.post.it.evoting.evotinglibraries.domain.configuration.ControlComponentPublicKeysPayload;
import ch.post.it.evoting.securedatamanager.commons.Constants;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface MessageBrokerOrchestratorClient {

	@POST("api/v1/configuration/setupvoting/keygeneration/electionevent/{electionEventId}")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<List<ControlComponentPublicKeysPayload>> generateCCKeys(
			@Path(Constants.ELECTION_EVENT_ID)
			final String electionEventId,
			@Body
			final ControlComponentKeyGenerationRequestPayload controlComponentKeyGenerationRequestPayload);

	@POST("api/v1/configuration/setupvoting/longvotecastreturncodesallowlist/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<Void> uploadLongVoteCastReturnCodesAllowList(
			@Path(Constants.ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.VERIFICATION_CARD_SET_ID_PARAM)
			final String verificationCardSetId,
			@Body
			final SetupComponentLVCCAllowListPayload setupComponentLVCCAllowListPayload);

	@PUT("api/v1/tally/mixonline/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/mix")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<Void> createMixDecryptOnline(
			@Path(Constants.ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.BALLOT_BOX_ID)
			final String ballotBoxId);

	@GET("api/v1/tally/mixonline/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/status")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<BallotBoxStatus> checkMixDecryptOnlineStatus(
			@Path(Constants.ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.BALLOT_BOX_ID)
			final String ballotBoxId);

	@GET("api/v1/tally/mixonline/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/download")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<MixDecryptOnlinePayload> downloadMixDecryptOnline(
			@Path(Constants.ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.BALLOT_BOX_ID)
			final String ballotBoxId);

	@PUT("api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/chunk/{chunkId}/computegenenclongcodeshares")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<Void> computeGenEncLongCodeShares(
			@Path(Constants.ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.VERIFICATION_CARD_SET_ID_PARAM)
			final String verificationCardSetId,
			@Path(Constants.CHUNK_ID_PARAM)
			final Integer chunkId,
			@Body
			final SetupComponentVerificationDataPayload setupComponentVerificationDataPayload);

	@GET("api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/chunk/{chunkId}/download")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<List<ControlComponentCodeSharesPayload>> downloadGenEncLongCodeShares(
			@Path(Constants.ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.VERIFICATION_CARD_SET_ID_PARAM)
			final String verificationCardSetId,
			@Path(Constants.CHUNK_ID_PARAM)
			final Integer chunkId);

	@GET("api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/chunkcount/{chunkCount}/status")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<ComputingStatus> checkGenEncLongCodeSharesStatus(
			@Path(Constants.ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.VERIFICATION_CARD_SET_ID_PARAM)
			final String verificationCardSetId,
			@Path(Constants.CHUNK_COUNT_PARAM)
			final Integer chunkCount);

	@GET("api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/queuedcomputechunkids")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<List<Integer>> queuedComputeChunkIds(
			@Path(Constants.ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.VERIFICATION_CARD_SET_ID_PARAM)
			final String verificationCardSetId);
}
