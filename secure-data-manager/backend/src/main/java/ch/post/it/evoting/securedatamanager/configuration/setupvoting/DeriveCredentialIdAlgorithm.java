/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateSVK;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.stringToByteArray;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.internal.utils.ByteArrays;
import ch.post.it.evoting.cryptoprimitives.math.Base16;
import ch.post.it.evoting.securedatamanager.commons.Constants;

/**
 * Implements the DeriveCredentialId algorithm.
 */
@Service
public class DeriveCredentialIdAlgorithm {

	private final Hash hash;
	private final Base16 base16;
	private final Argon2 argon2;

	public DeriveCredentialIdAlgorithm(final Hash hash,
			final Base16 base16,
			@Qualifier("argon2LessMemory")
			final Argon2 argon2) {
		this.hash = hash;
		this.base16 = base16;
		this.argon2 = argon2;
	}

	/**
	 * Derives a voter's identifier credentialID<sub>id</sub> from the Start Voting Key SVK<sub>id</sub>.
	 *
	 * @param electionEventId ee, the election event id. Must be non-null and a valid UUID.
	 * @param startVotingKey  SVK<sub>id</sub>, the Start Voting Key. Must be non-null and a valid Base32 string of size l<sub>SVK</sub>.
	 * @return the derived credentialID<sub>id</sub>.
	 * @throws NullPointerException      if any parameter is null.
	 * @throws IllegalArgumentException  if the Start Voting Key's size is not l<sub>SVK</sub>={@value Constants#SVK_LENGTH}.
	 * @throws FailedValidationException if the election event id is not a valid UUID or the start voting key is not a valid Base32 string.
	 */
	@SuppressWarnings("java:S117")
	public String deriveCredentialId(final String electionEventId, final String startVotingKey) {
		final String SVK_id = validateSVK(startVotingKey);
		final String ee = validateUUID(electionEventId);

		final int securityLevel = 128;
		final byte[] recursiveHash = hash.recursiveHash(HashableList.of(HashableString.from(ee), HashableString.from("credentialId")));
		final byte[] salt = ByteArrays.cutToBitLength(recursiveHash, securityLevel);

		final byte[] bCredentialID_id = argon2.getArgon2id(stringToByteArray(SVK_id), salt);

		return base16.base16Encode(ByteArrays.cutToBitLength(bCredentialID_id, securityLevel));
	}

}
