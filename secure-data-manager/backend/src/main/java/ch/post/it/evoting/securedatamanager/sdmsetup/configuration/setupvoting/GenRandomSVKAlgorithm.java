/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmsetup.configuration.setupvoting;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.evotinglibraries.domain.common.Alphabet;
import ch.post.it.evoting.evotinglibraries.domain.common.StartVotingKeyAlphabet;

/**
 * Implements the GenRandomSVK algorithm.
 */
@Service
@ConditionalOnProperty("role.isSetup")
public class GenRandomSVKAlgorithm {

	private final Random random;

	public GenRandomSVKAlgorithm(final Random random) {
		this.random = random;
	}

	/**
	 * Generates a random string of length &#119897; of the given alphabet.
	 *
	 * @param length   &#119897; &#8712; &#8469;<sup>+</sup>, the desired length of string. Must be strictly positive.
	 * @param alphabet &#120120;<sub>SVK</sub>=(&#119878;<sub>0</sub>,...,&#119878;<sub>&#119896;-1</sub>), the alphabet from which to choose the
	 *                 string. Must be non-null.
	 * @return &#119878;' &#8712; (&#120120;<sub>SVK</sub>)<sup>&#119897;</sup>, a random string of length &#119897; of the given alphabet.
	 * @throws NullPointerException     if any input is null.
	 * @throws IllegalArgumentException if &#119897; is not strictly positive.
	 */
	@SuppressWarnings("java:S117")
	public String genRandomSVK(final int length, final StartVotingKeyAlphabet alphabet) {

		checkArgument(length > 0, "The desired length of string must be strictly positive. [length: %s]", length);
		checkNotNull(alphabet);

		// Input
		final int l = length;
		final Alphabet A_SVK = alphabet;
		final int k = A_SVK.size();

		// Operation
		return IntStream.range(0, l)
				.mapToObj(i -> {
					final int m = random.genRandomInteger(BigInteger.valueOf(k)).intValueExact();
					return A_SVK.get(m);
				})
				.collect(Collectors.joining());
	}

}

