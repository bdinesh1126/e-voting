/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.retry.RetryContext;
import org.springframework.retry.annotation.Retryable;
import org.springframework.retry.support.RetrySynchronizationManager;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.domain.configuration.setupvoting.ComputingStatus;
import ch.post.it.evoting.securedatamanager.commons.JsonUtils;
import ch.post.it.evoting.securedatamanager.commons.RemoteService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.SetupComponentVerificationDataPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.MessageBrokerOrchestratorClient;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

import retrofit2.Response;

@Service
public class EncryptedLongReturnCodeSharesService {

	private static final String VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE = "The voting-portal connection is not enabled.";
	private final VotingCardSetRepository votingCardSetRepository;
	private final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient;
	private final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository;
	private final RetryableRemoteCall retryableRemoteCall;
	private final boolean isVotingPortalEnabled;

	public EncryptedLongReturnCodeSharesService(
			final VotingCardSetRepository votingCardSetRepository,
			final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient,
			final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository,
			final RetryableRemoteCall retryableRemoteCall,
			@Value("${voting.portal.enabled}")
			final boolean isVotingPortalEnabled) {
		this.votingCardSetRepository = votingCardSetRepository;
		this.messageBrokerOrchestratorClient = messageBrokerOrchestratorClient;
		this.setupComponentVerificationDataPayloadFileRepository = setupComponentVerificationDataPayloadFileRepository;
		this.retryableRemoteCall = retryableRemoteCall;
		this.isVotingPortalEnabled = isVotingPortalEnabled;
	}

	/**
	 * Send the Setup Component verification data to the control components to generate the encrypted long Return Code Shares (encrypted long Choice
	 * Return Code shares and encrypted long Vote Cast Return Code shares).
	 */
	public void computeGenEncLongCodeShares(final SetupComponentVerificationDataPayload setupComponentVerificationDataPayload) {

		checkNotNull(setupComponentVerificationDataPayload);
		checkState(isVotingPortalEnabled, VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		retryableRemoteCall.computeGenEncLongCodeShares(setupComponentVerificationDataPayload);
	}

	/**
	 * Check if the control components finished the generation of the encrypted long Return Code Shares and update the status of the voting card set
	 * they belong to. The SDM checks if the number of generated encrypted long Return Code Shares (number of chunks) in the voting server corresponds
	 * to the number of Setup Component verification data (number of chunks) in the SDM.
	 */
	public void updateVotingCardSetsComputationStatus(final String electionEventId) {

		validateUUID(electionEventId);
		checkState(isVotingPortalEnabled, VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		final Map<String, Object> votingCardSetsParams = new HashMap<>();
		votingCardSetsParams.put(JsonConstants.STATUS, ComputingStatus.COMPUTING.name());
		votingCardSetsParams.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);

		final String votingCardSetJSON = votingCardSetRepository.list(votingCardSetsParams);
		final JsonArray votingCardSets = JsonUtils.getJsonObject(votingCardSetJSON).getJsonArray(JsonConstants.RESULT);

		for (int i = 0; i < votingCardSets.size(); i++) {
			final JsonObject votingCardSetInArray = votingCardSets.getJsonObject(i);
			final String verificationCardSetId = votingCardSetInArray.getString(JsonConstants.VERIFICATION_CARD_SET_ID);

			final int chunkCount = setupComponentVerificationDataPayloadFileRepository.getCount(electionEventId, verificationCardSetId);

			final Response<ComputingStatus> computingStatusResponse = RemoteService.call(
							messageBrokerOrchestratorClient.checkGenEncLongCodeSharesStatus(electionEventId,
									verificationCardSetId, chunkCount))
					.networkErrorMessage("Failed to communicate with orchestrator")
					.unsuccessfulResponseErrorMessage("Get status unsuccessful. [electionEventId: %s, verificationCardSetId: %s, chunkCount: %s]",
							electionEventId,
							verificationCardSetId, chunkCount)
					.execute();

			checkNotNull(computingStatusResponse.body(), "Returned status is null. [electionEventId: %s, verificationCardSetId: %s, chunkCount: %s]",
					electionEventId, verificationCardSetId, chunkCount);

			final ComputingStatus computingStatus = computingStatusResponse.body();

			final JsonObjectBuilder builder = Json.createObjectBuilder(votingCardSetInArray);
			builder.add(JsonConstants.STATUS, computingStatus.toString());

			votingCardSetRepository.update(builder.build().toString());
		}
	}

	/**
	 * Download the control components' encrypted long Choice Return codes and the encrypted long Vote Cast Return code shares.
	 */
	public List<ControlComponentCodeSharesPayload> downloadGenEncLongCodeShares(final String electionEventId, final String verificationCardSetId,
			final int chunkId) {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkArgument(chunkId >= 0);

		checkState(isVotingPortalEnabled, VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		final Response<List<ControlComponentCodeSharesPayload>> response = retryableRemoteCall.downloadGenEncLongCodeShares(electionEventId,
				verificationCardSetId, chunkId);

		checkNotNull(response.body(), "Download response null. [electionEventId: %s, verificationCardSetId: %s, chunkId: %s]",
				electionEventId, verificationCardSetId, chunkId);

		final List<ControlComponentCodeSharesPayload> controlComponentCodeSharesPayloads = response.body().stream()
				.sorted(Comparator.comparingInt(ControlComponentCodeSharesPayload::getNodeId))
				.toList();

		checkState(ControlComponentConstants.NODE_IDS.equals(controlComponentCodeSharesPayloads.stream()
						.parallel()
						.map(controlComponentCodeSharesPayload -> {

							checkState(controlComponentCodeSharesPayload.getElectionEventId().equals(electionEventId),
									"The controlComponentCodeSharesPayload's election event id does not correspond to the election event id of the request");

							checkState(controlComponentCodeSharesPayload.getVerificationCardSetId().equals(verificationCardSetId),
									"The controlComponentCodeSharesPayload's verification card set id does not correspond to the verification card set id of the request");

							checkState(controlComponentCodeSharesPayload.getChunkId() == chunkId,
									"The control component code shares payloads must have the expected chunk id");

							return controlComponentCodeSharesPayload.getNodeId();
						})
						.collect(Collectors.toSet())),
				"There must be exactly the expected number of control component code shares payloads with the expected node ids.");

		return controlComponentCodeSharesPayloads;
	}

	@Service
	static class RetryableRemoteCall {
		private static final Logger LOGGER = LoggerFactory.getLogger(EncryptedLongReturnCodeSharesService.RetryableRemoteCall.class);
		private final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient;

		public RetryableRemoteCall(final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient) {
			this.messageBrokerOrchestratorClient = messageBrokerOrchestratorClient;
		}

		@Retryable(retryFor = { UncheckedIOException.class,
				IOException.class }, maxAttemptsExpression = "${compute-genenclongcodeshares.retryable-calls.max-attempts}")
		public void computeGenEncLongCodeShares(final SetupComponentVerificationDataPayload setupComponentVerificationDataPayload) {

			checkNotNull(setupComponentVerificationDataPayload);

			final String electionEventId = setupComponentVerificationDataPayload.getElectionEventId();
			final String verificationCardSetId = setupComponentVerificationDataPayload.getVerificationCardSetId();
			final int chunkId = setupComponentVerificationDataPayload.getChunkId();

			final RetryContext retryContext = RetrySynchronizationManager.getContext();
			final int retryNumber = retryContext.getRetryCount();
			if (retryNumber != 0) {
				LOGGER.warn(
						"Retrying of request for computing the GenEncLongCodeShares. [retryNumber: {}, electionEventId: {}, verificationCardSetId: {}, chunkId: {}]",
						retryNumber, electionEventId, verificationCardSetId, chunkId);
			}

			RemoteService.call(messageBrokerOrchestratorClient.computeGenEncLongCodeShares(electionEventId, verificationCardSetId, chunkId,
							setupComponentVerificationDataPayload))
					.networkErrorMessage("Failed to communicate with orchestrator")
					.unsuccessfulResponseErrorMessage("Compute unsuccessful. [electionEventId: {}, verificationCardSetId: {}, chunkId: {}]",
							electionEventId,
							verificationCardSetId, chunkId)
					.execute();
		}

		@Retryable(retryFor = { UncheckedIOException.class,
				IOException.class }, maxAttemptsExpression = "${download-genenclongcodeshares.retryable-calls.max-attempts}")
		Response<List<ControlComponentCodeSharesPayload>> downloadGenEncLongCodeShares(final String electionEventId,
				final String verificationCardSetId, final int chunkId) {
			validateUUID(electionEventId);
			validateUUID(verificationCardSetId);
			checkArgument(chunkId >= 0);

			final RetryContext retryContext = RetrySynchronizationManager.getContext();
			final int retryNumber = retryContext.getRetryCount();
			if (retryNumber != 0) {
				LOGGER.warn(
						"Retrying of request for downloading the GenEncLongCodeShares. [retryNumber: {}, electionEventId: {}, verificationCardSetId: {}, chunkId: {}]",
						retryNumber, electionEventId, verificationCardSetId, chunkId);
			}

			return RemoteService.call(
							messageBrokerOrchestratorClient.downloadGenEncLongCodeShares(
									electionEventId, verificationCardSetId, chunkId))
					.networkErrorMessage("Failed to communicated with orchestrator")
					.unsuccessfulResponseErrorMessage("Download unsuccessful. [electionEventId: %s, verificationCardSetId: %s, chunkId: %s]",
							electionEventId, verificationCardSetId, chunkId)
					.execute();
		}
	}
}
