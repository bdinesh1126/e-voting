/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.hashing.Argon2;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Context;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Factory;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Profile;

class BoardPasswordHashServiceTest {

	private static final char[] BOARD_PASSWORD_1 = "Password_LongBoardPassword1".toCharArray();
	private static final char[] BOARD_PASSWORD_2 = "Password_LongBoardPassword2".toCharArray();
	private static final List<char[]> BOARD_PASSWORDS = List.of(BOARD_PASSWORD_1, BOARD_PASSWORD_2);

	private static BoardPasswordHashService boardPasswordHashService;

	@BeforeAll
	static void setUpAll() {
		final Argon2Context context = Argon2Profile.TEST.getContext();
		final Argon2 argon2 = Argon2Factory.createArgon2(context);
		boardPasswordHashService = new BoardPasswordHashService(argon2);
	}

	@Test
	void cyclicTest() {
		final List<byte[]> hashes = assertDoesNotThrow(() -> boardPasswordHashService.hashPasswords(BOARD_PASSWORDS));
		assertAll(
				() -> assertTrue(boardPasswordHashService.verifyPassword(BOARD_PASSWORD_1, hashes.get(0))),
				() -> assertTrue(boardPasswordHashService.verifyPassword(BOARD_PASSWORD_2, hashes.get(1)))
		);
	}

	@Nested
	class HashPasswords {
		@Test
		void hashPasswordsHappyPath() {
			final List<byte[]> hashes = assertDoesNotThrow(() -> boardPasswordHashService.hashPasswords(BOARD_PASSWORDS));
			assertEquals(2, hashes.size());
		}

		@Test
		void hashPasswordsNullPasswordsThrows() {
			assertThrows(NullPointerException.class, () -> boardPasswordHashService.hashPasswords(null));
		}
	}

	@Nested
	class VerifyPassword {
		private final List<byte[]> hashes = boardPasswordHashService.hashPasswords(BOARD_PASSWORDS);

		@Test
		void verifyPasswordHappyPath() {
			assertAll(
					() -> assertTrue(
							boardPasswordHashService.verifyPassword(BOARD_PASSWORD_1, hashes.get(0))),
					() -> assertTrue(
							boardPasswordHashService.verifyPassword(BOARD_PASSWORD_2, hashes.get(1)))
			);
		}

		@Test
		void verifyPasswordNullBoardMemberHashThrows() {
			assertThrows(NullPointerException.class, () -> boardPasswordHashService.verifyPassword(BOARD_PASSWORD_1, null));
		}

		@Test
		void verifyPasswordInvalidBoardMemberHashThrows() {
			final char[] invalidPassword = "invalid".toCharArray();
			final byte[] passwordHash = hashes.get(0);
			assertThrows(IllegalArgumentException.class, () -> boardPasswordHashService.verifyPassword(invalidPassword, passwordHash));
		}
	}

}