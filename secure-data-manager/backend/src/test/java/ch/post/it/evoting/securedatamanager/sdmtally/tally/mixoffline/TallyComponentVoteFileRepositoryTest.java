/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.evotinglibraries.domain.tally.TallyComponentVotesPayload;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

class TallyComponentVoteFileRepositoryTest {

	private static final PathResolver pathResolverMock = mock(PathResolver.class);

	private static TallyComponentVotesFileRepository tallyComponentVotesFileRepository;

	private final GqGroup gqGroup = GroupTestData.getGroupP59();
	private final String ballotId = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private final String ballotBoxId = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private final String electionEventId = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature("".getBytes(StandardCharsets.UTF_8));

	private GroupVector<GroupVector<PrimeGqElement, GqGroup>, GqGroup> votes;
	private List<List<String>> actualSelectedVotingOptions;

	private List<List<String>> decodedWriteInVotes;
	private TallyComponentVotesPayload tallyComponentVotesPayload;

	@BeforeAll
	static void setUpAll() {
		final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();

		tallyComponentVotesFileRepository = new TallyComponentVotesFileRepository(pathResolverMock, objectMapper);
	}

	@BeforeEach
	void setUp() {
		final int desiredNumberOfPrimes = 3;
		final GroupVector<PrimeGqElement, GqGroup> primeGroupMembers =
				PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(gqGroup, desiredNumberOfPrimes);

		votes = GroupVector.of(GroupVector.from(primeGroupMembers), GroupVector.from(primeGroupMembers));
		actualSelectedVotingOptions = votes.stream()
				.map(actualSelectedVotingOption -> actualSelectedVotingOption.stream().map(v -> "124124aa-154153").toList()).toList();

		decodedWriteInVotes = votes.stream()
				.map(actualSelectedVotingOption -> actualSelectedVotingOption.stream().map(v -> "James Bond").toList()).toList();

		tallyComponentVotesPayload = new TallyComponentVotesPayload(electionEventId, ballotId, ballotBoxId, votes.getGroup(), votes,
				actualSelectedVotingOptions, decodedWriteInVotes, signature);
	}

	@Test
	void persistTallyComponentVotesTest(
			@TempDir
			final Path tempDir) {
		when(pathResolverMock.resolveBallotBoxPath(anyString(), anyString(), anyString())).thenReturn(tempDir);

		assertEquals(Optional.empty(), tallyComponentVotesFileRepository.load(electionEventId, ballotId, ballotBoxId));

		assertDoesNotThrow(() -> tallyComponentVotesFileRepository.save(tallyComponentVotesPayload));

		final Optional<TallyComponentVotesPayload> optionalPayload = tallyComponentVotesFileRepository.load(electionEventId, ballotId, ballotBoxId);
		assertTrue(optionalPayload.isPresent());

		final TallyComponentVotesPayload payload = optionalPayload.get();
		assertNotNull(payload);
		assertAll(
				() -> assertEquals(electionEventId, payload.getElectionEventId()),
				() -> assertEquals(ballotBoxId, payload.getBallotBoxId()),
				() -> assertEquals(ballotId, payload.getBallotId()),
				() -> assertEquals(votes.getGroup(), payload.getEncryptionGroup()),
				() -> assertEquals(votes.size(), payload.getVotes().size()),
				() -> assertEquals(actualSelectedVotingOptions, payload.getActualSelectedVotingOptions()),
				() -> assertEquals(signature, payload.getSignature())
		);
	}

	@Test
	void persistTallyComponentVotesNullTest() {
		assertThrows(NullPointerException.class, () -> tallyComponentVotesFileRepository.save(null));
	}

	@Test
	void loadTallyComponentVotesNullTest() {
		assertAll(
				() -> assertThrows(NullPointerException.class, () -> tallyComponentVotesFileRepository.load(null, ballotId, ballotBoxId)),
				() -> assertThrows(NullPointerException.class, () -> tallyComponentVotesFileRepository.load(electionEventId, null, ballotBoxId)),
				() -> assertThrows(NullPointerException.class, () -> tallyComponentVotesFileRepository.load(electionEventId, ballotId, null))
		);
	}

	@Test
	void loadTallyComponentVotesInvalidUUIDTest() {
		assertAll(
				() -> assertThrows(FailedValidationException.class, () -> tallyComponentVotesFileRepository.load("123", ballotId, ballotBoxId)),
				() -> assertThrows(FailedValidationException.class,
						() -> tallyComponentVotesFileRepository.load(electionEventId, "123", ballotBoxId)),
				() -> assertThrows(FailedValidationException.class, () -> tallyComponentVotesFileRepository.load(electionEventId, ballotId, "123"))
		);

	}
}
