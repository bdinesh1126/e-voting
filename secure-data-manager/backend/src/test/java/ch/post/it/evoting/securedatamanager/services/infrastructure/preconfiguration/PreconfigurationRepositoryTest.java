/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.preconfiguration;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.json.JsonObject;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OSchema;

import ch.post.it.evoting.securedatamanager.TestKeyStoreInitializer;
import ch.post.it.evoting.securedatamanager.commons.JsonUtils;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.CantonConfigConfigFileRepository;
import ch.post.it.evoting.securedatamanager.services.domain.config.Config;
import ch.post.it.evoting.securedatamanager.services.infrastructure.DatabaseManager;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.VoteVerificationClient;
import ch.post.it.evoting.securedatamanager.services.infrastructure.importexport.StreamedEncryptionService;

@SpringBootTest
@ExtendWith({ SpringExtension.class, MockitoExtension.class })
@ContextConfiguration(classes = Config.class, loader = AnnotationConfigContextLoader.class, initializers = TestKeyStoreInitializer.class)
@ActiveProfiles("test")
class PreconfigurationRepositoryTest {
	private static final Set<String> SYSTEM_CLASSES = new HashSet<>(
			asList("OFunction", "OIdentity", "ORestricted", "ORIDs", "ORole", "OSchedule", "OTriggered", "OUser", "OSecurityPolicy"));
	@Autowired
	private DatabaseManager manager;
	@SpyBean
	private PathResolver pathResolver;

	@MockBean
	private VoteVerificationClient voteVerificationClient;

	@MockBean
	private CantonConfigConfigFileRepository cantonConfigConfigFileRepository;

	@MockBean
	private StreamedEncryptionService streamedEncryptionService;

	@Autowired
	private PreconfigurationRepository repository;

	@AfterEach
	void tearDown() throws IOException {
		try (final ODatabaseDocument database = manager.openDatabase()) {
			final OSchema schema = database.getMetadata().getSchema();
			for (final OClass oClass : schema.getClasses()) {
				if (!SYSTEM_CLASSES.contains(oClass.getName())) {
					oClass.truncate();
				}
			}
		}
	}

	@Test
	void readFromFileAndSave() throws IOException, URISyntaxException {
		doReturn(Paths.get(Objects.requireNonNull(getClass().getClassLoader().getResource("sdmConfig")).toURI()))
				.when(pathResolver).resolveSdmConfigPath();
		final String result = repository.readFromFileAndSave();

		assertNotNull(result);
		final JsonObject jsonObject = JsonUtils.getJsonObject(result);
		assertNotNull(jsonObject);
		assertFalse(jsonObject.isEmpty());
		assertFalse(jsonObject.getJsonArray(JsonConstants.ADMINISTRATION_BOARDS).isEmpty());
		assertFalse(jsonObject.getJsonArray(JsonConstants.ELECTION_EVENTS).isEmpty());
		assertFalse(jsonObject.getJsonArray(JsonConstants.VOTING_CARD_SETS).isEmpty());
		assertFalse(jsonObject.getJsonArray(JsonConstants.BALLOT_BOXES).isEmpty());
	}

	@Test
	void readFromFileAndSaveExceptionDuplicated() throws IOException, URISyntaxException {
		doReturn(Paths.get(Objects.requireNonNull(getClass().getClassLoader().getResource("sdmConfig")).toURI()))
				.when(pathResolver).resolveSdmConfigPath();
		repository.readFromFileAndSave();
		final String result = repository.readFromFileAndSave();

		assertNotNull(result);
		final JsonObject jsonObject = JsonUtils.getJsonObject(result);
		assertNotNull(jsonObject);
		assertFalse(jsonObject.isEmpty());
		assertTrue(jsonObject.getJsonArray(JsonConstants.ADMINISTRATION_BOARDS).isEmpty());
		assertTrue(jsonObject.getJsonArray(JsonConstants.ELECTION_EVENTS).isEmpty());
		assertTrue(jsonObject.getJsonArray(JsonConstants.VOTING_CARD_SETS).isEmpty());
		assertTrue(jsonObject.getJsonArray(JsonConstants.BALLOT_BOXES).isEmpty());
	}
}
