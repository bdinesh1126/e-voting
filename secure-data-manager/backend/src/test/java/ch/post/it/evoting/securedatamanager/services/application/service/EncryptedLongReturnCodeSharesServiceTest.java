/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static java.nio.file.Files.exists;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.securedatamanager.VotingCardSetServiceTestBase;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.InvalidStatusTransitionException;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.Status;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.SetupComponentVerificationDataPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

@ExtendWith(MockitoExtension.class)
@SpringJUnitConfig(VotingCardSetServiceTestSpringConfig.class)
class EncryptedLongReturnCodeSharesServiceTest extends VotingCardSetServiceTestBase {

	private static final String ELECTION_EVENT_ID = "a3d790fd1ac543f9b0a05ca79a20c9e2";
	private static final String VERIFICATION_CARD_SET_ID = "e8c82c55701c4bbdbef2cd8a675549d1";
	private static final String PRECOMPUTED_VALUES_PATH = "computeTest";
	private static final String ONLINE_PATH = "ONLINE";
	private static final String VOTE_VERIFICATION_FOLDER = "voteVerification";
	@Autowired
	ObjectMapper objectMapper;
	@Autowired
	private VotingCardSetDownloadService votingCardSetDownloadService;
	@Autowired
	private VotingCardSetRepository votingCardSetRepositoryMock;
	@Autowired
	private IdleStatusService idleStatusService;
	@Autowired
	private PathResolver pathResolver;
	@Autowired
	private ConfigurationEntityStatusService configurationEntityStatusServiceMock;
	@Autowired
	private EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesServiceMock;
	@Autowired
	private SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository;

	@Test
	void download() throws ResourceNotFoundException, URISyntaxException, IOException {
		setStatusForVotingCardSetFromRepository(Status.COMPUTED.name(), votingCardSetRepositoryMock);

		final Path basePath = getPathOfFileInResources(Paths.get(PRECOMPUTED_VALUES_PATH));
		final Path voteVerificationPath = basePath.resolve(ELECTION_EVENT_ID).resolve(ONLINE_PATH).resolve(VOTE_VERIFICATION_FOLDER);
		final Path verificationCardSetPath = voteVerificationPath.resolve(VERIFICATION_CARD_SET_ID);

		final Resource contributions = new ClassPathResource("GenEncLongCodeSharesServiceTest/nodeContributions.0.json");

		final List<ControlComponentCodeSharesPayload> payloads = objectMapper.readValue(contributions.getFile(),
				new TypeReference<>() {
				});

		when(pathResolver.resolveVoteVerificationPath(ELECTION_EVENT_ID)).thenReturn(voteVerificationPath);
		when(configurationEntityStatusServiceMock.update(anyString(), anyString(), any())).thenReturn("");
		when(setupComponentVerificationDataPayloadFileRepository.getCount(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID)).thenReturn(3);
		when(encryptedLongReturnCodeSharesServiceMock.downloadGenEncLongCodeShares(anyString(), anyString(), anyInt()))
				.thenReturn(payloads);
		when(idleStatusService.getIdLock(anyString())).thenReturn(true);

		assertAll(() -> assertDoesNotThrow(() -> votingCardSetDownloadService.download(VOTING_CARD_SET_ID, ELECTION_EVENT_ID)),
				() -> assertTrue(exists(verificationCardSetPath.resolve(Constants.CONFIG_FILE_CONTROL_COMPONENT_CODE_SHARES_PAYLOAD + ".0" + Constants.JSON))),
				() -> assertTrue(exists(verificationCardSetPath.resolve(Constants.CONFIG_FILE_CONTROL_COMPONENT_CODE_SHARES_PAYLOAD + ".1" + Constants.JSON))),
				() -> assertTrue(exists(verificationCardSetPath.resolve(Constants.CONFIG_FILE_CONTROL_COMPONENT_CODE_SHARES_PAYLOAD + ".2" + Constants.JSON))));

	}

	@Test
	void downloadInvalidStatus() throws ResourceNotFoundException {
		setStatusForVotingCardSetFromRepository("SIGNED", votingCardSetRepositoryMock);

		when(idleStatusService.getIdLock(anyString())).thenReturn(true);

		assertThrows(InvalidStatusTransitionException.class, () -> votingCardSetDownloadService.download(VOTING_CARD_SET_ID, ELECTION_EVENT_ID));
	}
}
