/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants;
import ch.post.it.evoting.cryptoprimitives.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.evotinglibraries.domain.SerializationUtils;
import ch.post.it.evoting.evotinglibraries.domain.configuration.ControlComponentPublicKeysPayload;
import ch.post.it.evoting.securedatamanager.ControlComponentPublicKeysService;
import ch.post.it.evoting.securedatamanager.EncryptionParametersPayloadService;
import ch.post.it.evoting.securedatamanager.SetupKeyPairService;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.GenSetupEncryptionKeysAlgorithm;
import ch.post.it.evoting.securedatamanager.services.application.exception.CCKeysAlreadyExistException;
import ch.post.it.evoting.securedatamanager.services.application.exception.CCKeysNotExistException;
import ch.post.it.evoting.securedatamanager.services.domain.service.ElectionEventDataGeneratorService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.MessageBrokerOrchestratorClient;
import ch.post.it.evoting.securedatamanager.services.infrastructure.electionevent.ElectionEventRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;

import retrofit2.Call;
import retrofit2.Response;

@DisplayName("An electionEventService")
@ExtendWith(MockitoExtension.class)
class ElectionEventServiceTest {

	private static final Random random = RandomFactory.createRandom();
	private static final String ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);

	private static GqGroup gqGroup;
	private static ElGamalMultiRecipientKeyPair keyPair;

	@Spy
	private final ObjectMapper mapper = DomainObjectMapper.getNewInstance();

	@Mock
	private PathResolver pathResolver;
	@Mock
	private SetupKeyPairService setupKeyPairService;
	@Mock
	private EncryptionParametersPayloadService encryptionParametersPayloadServiceMock;
	@Mock
	private GenSetupEncryptionKeysAlgorithm genSetupEncryptionKeysAlgorithm;
	@Mock
	private ConfigurationEntityStatusService configurationEntityStatusService;
	@Mock
	private ControlComponentPublicKeysService controlComponentPublicKeysService;
	@Mock
	private ElectionEventDataGeneratorService electionEventDataGeneratorServiceMock;
	@Mock
	private MessageBrokerOrchestratorClient messageBrokerOrchestratorClient;

	private ElectionEventService electionEventService;

	@BeforeEach
	void setUp() {
		electionEventService = new ElectionEventService(mapper, pathResolver, messageBrokerOrchestratorClient, setupKeyPairService, mock(
				ElectionEventRepository.class), encryptionParametersPayloadServiceMock, genSetupEncryptionKeysAlgorithm,
				configurationEntityStatusService, electionEventDataGeneratorServiceMock, controlComponentPublicKeysService, true);
	}

	@BeforeAll
	static void setUpAll() {
		gqGroup = SerializationUtils.getGqGroup();
		keyPair = ElGamalMultiRecipientKeyPair.genKeyPair(gqGroup, 1, random);
	}

	@DisplayName("executing create(), with an unsuccessful election event generation, returns an unsuccessful response.")
	@Test
	void createElectionEventGenerateFails() throws IOException, CCKeysNotExistException {
		when(controlComponentPublicKeysService.exist(anyString())).thenReturn(true);
		when(pathResolver.resolveOfflinePath(anyString())).thenReturn(Paths.get("test"));
		when(encryptionParametersPayloadServiceMock.loadEncryptionGroup(any())).thenReturn(gqGroup);
		when(genSetupEncryptionKeysAlgorithm.genSetupEncryptionKeys(any())).thenReturn(keyPair);

		doNothing().when(setupKeyPairService).save(anyString(), any());
		doNothing().when(mapper).writeValue((File) any(), any());

		when(electionEventDataGeneratorServiceMock.generate(anyString())).thenReturn(false);

		assertFalse(electionEventService.create(""));
	}

	@DisplayName("executing create(), with a successful election event generation, returns a successful response and write the related file.")
	@Test
	void create(
			@TempDir
			final Path tempDir) throws IOException, CCKeysNotExistException {

		when(controlComponentPublicKeysService.exist(anyString())).thenReturn(true);

		final Path offlinePath = tempDir.resolve(Constants.CONFIG_DIR_NAME_OFFLINE);
		Files.createDirectories(offlinePath);
		when(pathResolver.resolveOfflinePath(anyString())).thenReturn(offlinePath);

		when(encryptionParametersPayloadServiceMock.loadEncryptionGroup(any())).thenReturn(gqGroup);
		when(genSetupEncryptionKeysAlgorithm.genSetupEncryptionKeys(any())).thenReturn(keyPair);
		when(configurationEntityStatusService.update(anyString(), anyString(), any())).thenReturn("");

		doNothing().when(setupKeyPairService).save(anyString(), any());

		when(electionEventDataGeneratorServiceMock.generate(anyString())).thenReturn(true);

		assertTrue(electionEventService.create(""));
		assertTrue(Files.exists(offlinePath.resolve(Constants.SETUP_SECRET_KEY_FILE_NAME)));
	}

	@DisplayName("executing create(), with non-existing keys, throws an CCKeysNotExistException.")
	@Test
	void createNotExist() {

		when(controlComponentPublicKeysService.exist(anyString())).thenReturn(false);

		final CCKeysNotExistException ccKeysNotExistException =
				assertThrows(CCKeysNotExistException.class, () -> electionEventService.create(ELECTION_EVENT_ID));

		final String expectedMessage = String.format("The Control Components keys do not exist for this election event. [electionEventId: %s]",
				ELECTION_EVENT_ID);

		assertEquals(expectedMessage, Throwables.getRootCause(ccKeysNotExistException).getMessage());
	}

	@DisplayName("executing requestCCKeys(), with an invalid number of keys retrieved, throws an IllegalStateException.")
	@Test
	void requestCCKeysInvalidTotalExpectedKeys() throws IOException {

		when(controlComponentPublicKeysService.exist(anyString())).thenReturn(false);
		when(encryptionParametersPayloadServiceMock.loadEncryptionGroup(any())).thenReturn(gqGroup);

		final List<ControlComponentPublicKeysPayload> controlComponentPublicKeysPayloads = ControlComponentConstants.NODE_IDS.stream()
				.skip(1) // skips the first element to have an invalid number of returned payloads.
				.map(this::createControlComponentPayload)
				.toList();

		@SuppressWarnings("unchecked")
		final Call<List<ControlComponentPublicKeysPayload>> responseBody = mock(Call.class);
		when(responseBody.execute()).thenReturn(Response.success(controlComponentPublicKeysPayloads));
		when(messageBrokerOrchestratorClient.generateCCKeys(eq(ELECTION_EVENT_ID), any())).thenReturn(responseBody);

		final IllegalStateException illegalStateException =
				assertThrows(IllegalStateException.class, () -> electionEventService.requestCCKeys(ELECTION_EVENT_ID));

		final String expectedMessage = String.format(
				"There number of control component public keys payloads expected is incorrect. [received: %s, expected: %s, electionEventId: %s]",
				controlComponentPublicKeysPayloads.size(), ControlComponentConstants.NODE_IDS.size(), ELECTION_EVENT_ID);

		assertEquals(expectedMessage, Throwables.getRootCause(illegalStateException).getMessage());
	}

	@DisplayName("executing requestCCKeys(), with keys retrieved from invalid nodes, throws an IllegalStateException.")
	@Test
	void requestCCKeysInvalidExpectedKeys() throws IOException {

		when(controlComponentPublicKeysService.exist(anyString())).thenReturn(false);
		when(encryptionParametersPayloadServiceMock.loadEncryptionGroup(any())).thenReturn(gqGroup);

		final List<ControlComponentPublicKeysPayload> controlComponentPublicKeysPayloads = ControlComponentConstants.NODE_IDS.stream()
				.skip(1)
				.map(this::createControlComponentPayload)
				.collect(Collectors.toList());
		controlComponentPublicKeysPayloads.add(createControlComponentPayload(ControlComponentConstants.NODE_IDS.last()));

		@SuppressWarnings("unchecked")
		final Call<List<ControlComponentPublicKeysPayload>> responseBody = mock(Call.class);
		when(responseBody.execute()).thenReturn(Response.success(controlComponentPublicKeysPayloads));

		when(messageBrokerOrchestratorClient.generateCCKeys(eq(ELECTION_EVENT_ID), any())).thenReturn(responseBody);

		final IllegalStateException illegalStateException =
				assertThrows(IllegalStateException.class, () -> electionEventService.requestCCKeys(ELECTION_EVENT_ID));

		final String expectedMessage = "The control component public keys payloads node ids do not match the expected node ids.";

		assertTrue(Throwables.getRootCause(illegalStateException).getMessage().startsWith(expectedMessage));
	}

	@DisplayName("executing requestCCKeys(), with already existing keys retrieved, throws an CCKeysAlreadyExistException.")
	@Test
	void requestCCKeysAlreadyExist() {

		when(controlComponentPublicKeysService.exist(anyString())).thenReturn(true);

		final CCKeysAlreadyExistException ccKeysAlreadyExistException =
				assertThrows(CCKeysAlreadyExistException.class, () -> electionEventService.requestCCKeys(ELECTION_EVENT_ID));

		final String expectedMessage = String.format("The Control Components keys already exist for this election event. [electionEventId: %s]",
				ELECTION_EVENT_ID);

		assertEquals(expectedMessage, Throwables.getRootCause(ccKeysAlreadyExistException).getMessage());
	}

	@DisplayName("executing requestCCKeys(), with valid keys retrieved, does not throw and saves.")
	@Test
	void requestCCKeysHappyPath() throws IOException {

		when(controlComponentPublicKeysService.exist(anyString())).thenReturn(false);
		when(encryptionParametersPayloadServiceMock.loadEncryptionGroup(any())).thenReturn(gqGroup);

		final List<ControlComponentPublicKeysPayload> controlComponentPublicKeysPayloads = ControlComponentConstants.NODE_IDS.stream()
				.map(this::createControlComponentPayload)
				.collect(Collectors.toList());

		@SuppressWarnings("unchecked")
		final Call<List<ControlComponentPublicKeysPayload>> responseBody = mock(Call.class);
		when(responseBody.execute()).thenReturn(Response.success(controlComponentPublicKeysPayloads));

		when(messageBrokerOrchestratorClient.generateCCKeys(eq(ELECTION_EVENT_ID), any())).thenReturn(responseBody);

		assertDoesNotThrow(() -> electionEventService.requestCCKeys(ELECTION_EVENT_ID));
		verify(controlComponentPublicKeysService, times(ControlComponentConstants.NODE_IDS.size())).save(any());
	}

	private ControlComponentPublicKeysPayload createControlComponentPayload(final int nodeId) {
		final ElGamalMultiRecipientPublicKey ccrChoiceReturnCodesEncryptionPublicKey = SerializationUtils.getPublicKey();
		final ElGamalMultiRecipientPublicKey ccmElectionPublicKey = SerializationUtils.getPublicKey();
		final GroupVector<SchnorrProof, ZqGroup> schnorrProofs = SerializationUtils.getSchnorrProofs(2);
		final ControlComponentPublicKeys controlComponentPublicKeys = new ControlComponentPublicKeys(nodeId, ccrChoiceReturnCodesEncryptionPublicKey,
				schnorrProofs, ccmElectionPublicKey, schnorrProofs);

		return new ControlComponentPublicKeysPayload(gqGroup, ELECTION_EVENT_ID, controlComponentPublicKeys);
	}
}
