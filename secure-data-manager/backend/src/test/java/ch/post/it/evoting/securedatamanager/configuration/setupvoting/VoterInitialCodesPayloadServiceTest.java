/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.securedatamanager.commons.Constants.CONFIG_FILE_NAME_VOTER_INITIAL_CODES_PAYLOAD;
import static ch.post.it.evoting.securedatamanager.commons.Constants.SVK_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Locale;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.configuration.setupvoting.VoterInitialCodes;
import ch.post.it.evoting.domain.configuration.setupvoting.VoterInitialCodesPayload;
import ch.post.it.evoting.evotinglibraries.domain.common.Alphabet;
import ch.post.it.evoting.evotinglibraries.domain.common.StartVotingKeyAlphabet;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.sdmsetup.configuration.setupvoting.GenRandomSVKAlgorithm;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@DisplayName("A VoterInitialCodesPayloadService")
class VoterInitialCodesPayloadServiceTest {
	private static final Random random = RandomFactory.createRandom();

	private static final String NON_EXISTING_ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String EXISTING_ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String EXISTING_VOTING_CARD_SET_ID = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
	private static final String INVALID_ID = "invalidId";
	private static final int NUMBER_OF_VOTER_INITIAL_CODES = 3;

	private static ObjectMapper objectMapper;
	private static PathResolver pathResolver;
	private static VoterInitialCodesPayloadService voterInitialCodesPayloadService;

	@BeforeAll
	static void setUpAll(
			@TempDir
			final Path tempDir) throws IOException {
		objectMapper = DomainObjectMapper.getNewInstance();

		createDirectories(tempDir);

		final PathResolver pathResolver = new PathResolver(tempDir.toString());

		final VoterInitialCodesPayloadFileRepository voterInitialCodesPayloadFileRepository =
				new VoterInitialCodesPayloadFileRepository(objectMapper, pathResolver);

		final VoterInitialCodesPayload voterInitialCodesPayload = validVoterInitialCodesPayload();
		voterInitialCodesPayloadFileRepository.save(voterInitialCodesPayload, EXISTING_VOTING_CARD_SET_ID);

		voterInitialCodesPayloadService = new VoterInitialCodesPayloadService(voterInitialCodesPayloadFileRepository);
	}

	private static VoterInitialCodesPayload validVoterInitialCodesPayload() {
		final List<String> voterIdentifications = random.genUniqueDecimalStrings(8, NUMBER_OF_VOTER_INITIAL_CODES);
		final List<String> UUIDs = List.of(
				random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH),
				random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH),
				random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH));
		final GenRandomSVKAlgorithm genRandomSVKAlgorithm = new GenRandomSVKAlgorithm(random);
		final StartVotingKeyAlphabet A_SVK = StartVotingKeyAlphabet.getInstance();
		final List<String> SVKs = List.of(
				genRandomSVKAlgorithm.genRandomSVK(Constants.SVK_LENGTH, A_SVK),
				genRandomSVKAlgorithm.genRandomSVK(Constants.SVK_LENGTH, A_SVK),
				genRandomSVKAlgorithm.genRandomSVK(Constants.SVK_LENGTH, A_SVK));
		final List<String> extendedAuthenticationFactor = random.genUniqueDecimalStrings(8, NUMBER_OF_VOTER_INITIAL_CODES);
		final List<String> BCKs = random.genUniqueDecimalStrings(9, NUMBER_OF_VOTER_INITIAL_CODES);

		final List<VoterInitialCodes> voterInitialCodes = IntStream.range(0, NUMBER_OF_VOTER_INITIAL_CODES).mapToObj(i -> new VoterInitialCodes(
				voterIdentifications.get(i), UUIDs.get(i), UUIDs.get(i), SVKs.get(i), extendedAuthenticationFactor.get(i), BCKs.get(i))
		).toList();

		return new VoterInitialCodesPayload(EXISTING_ELECTION_EVENT_ID, EXISTING_VOTING_CARD_SET_ID, voterInitialCodes);
	}

	private static void createDirectories(final Path tempDir) throws IOException {
		Files.createDirectories(
				tempDir.resolve("sdm/config").resolve(EXISTING_ELECTION_EVENT_ID).resolve("ONLINE").resolve("voteVerification")
						.resolve(EXISTING_VOTING_CARD_SET_ID));
	}

	@Nested
	@DisplayName("saving")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SaveTest {

		private VoterInitialCodesPayload voterInitialCodesPayload;

		private VoterInitialCodesPayloadService voterInitialCodesPayloadServiceTemp;

		@BeforeAll
		void setUpAll(
				@TempDir
				final Path tempDir) throws IOException {

			createDirectories(tempDir);

			pathResolver = new PathResolver(tempDir.toString());

			final VoterInitialCodesPayloadFileRepository voterInitialCodesPayloadFileRepositoryTemp =
					new VoterInitialCodesPayloadFileRepository(objectMapper, pathResolver);

			voterInitialCodesPayloadServiceTemp = new VoterInitialCodesPayloadService(voterInitialCodesPayloadFileRepositoryTemp);
		}

		@BeforeEach
		void setUp() {
			voterInitialCodesPayload = validVoterInitialCodesPayload();
		}

		@Test
		@DisplayName("a valid payload does not throw")
		void saveValidPayload() {
			assertDoesNotThrow(() -> voterInitialCodesPayloadServiceTemp.save(voterInitialCodesPayload, EXISTING_VOTING_CARD_SET_ID));

			assertTrue(Files.exists(pathResolver.resolvePrintingPath(EXISTING_ELECTION_EVENT_ID).resolve(EXISTING_VOTING_CARD_SET_ID)
					.resolve(CONFIG_FILE_NAME_VOTER_INITIAL_CODES_PAYLOAD)));
		}

		@Test
		@DisplayName("a null parameters throws")
		void saveNullPayload() {
			assertThrows(NullPointerException.class, () -> voterInitialCodesPayloadServiceTemp.save(null, EXISTING_VOTING_CARD_SET_ID));
			assertThrows(NullPointerException.class, () -> voterInitialCodesPayloadServiceTemp.save(voterInitialCodesPayload, null));
		}
	}

	@Nested
	@DisplayName("loading")
	@TestInstance(TestInstance.Lifecycle.PER_METHOD)
	class LoadTest {

		@Test
		@DisplayName("existing election event and verification card set returns expected voter initial codes payload")
		void loadExistingElectionEventValidSignature() {
			final VoterInitialCodesPayload voterInitialCodesPayload = voterInitialCodesPayloadService.load(EXISTING_ELECTION_EVENT_ID,
					EXISTING_VOTING_CARD_SET_ID);

			assertEquals(EXISTING_ELECTION_EVENT_ID, voterInitialCodesPayload.electionEventId());
		}

		@Test
		@DisplayName("null input throws NullPointerException")
		void loadNullInput() {
			assertThrows(NullPointerException.class, () -> voterInitialCodesPayloadService.load(null, EXISTING_VOTING_CARD_SET_ID));
			assertThrows(NullPointerException.class, () -> voterInitialCodesPayloadService.load(EXISTING_ELECTION_EVENT_ID, null));
		}

		@Test
		@DisplayName("invalid input throws FailedValidationException")
		void loadInvalidInput() {
			assertThrows(FailedValidationException.class, () -> voterInitialCodesPayloadService.load(INVALID_ID, EXISTING_VOTING_CARD_SET_ID));
			assertThrows(FailedValidationException.class, () -> voterInitialCodesPayloadService.load(EXISTING_ELECTION_EVENT_ID, INVALID_ID));
		}

		@Test
		@DisplayName("existing election event and verification card set but with missing payload throws IllegalStateException")
		void loadMissingPayload() {
			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> voterInitialCodesPayloadService.load(NON_EXISTING_ELECTION_EVENT_ID, EXISTING_VOTING_CARD_SET_ID));

			final String errorMessage = String.format(
					"Requested voter initial codes payload is not present. [electionEventId: %s, votingCardSetId: %s]",
					NON_EXISTING_ELECTION_EVENT_ID, EXISTING_VOTING_CARD_SET_ID);
			assertEquals(errorMessage, Throwables.getRootCause(exception).getMessage());
		}

	}

}
