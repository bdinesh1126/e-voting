/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.commons.io.output.NullOutputStream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.post.it.evoting.securedatamanager.services.application.service.ElectionEventService;
import ch.post.it.evoting.securedatamanager.services.domain.model.operation.OperationResult;
import ch.post.it.evoting.securedatamanager.services.domain.model.operation.OperationsOutputCode;
import ch.post.it.evoting.securedatamanager.services.infrastructure.importexport.ImportExportService;

@ExtendWith(MockitoExtension.class)
class OperationsControllerTest {

	private static final String ELECTION_EVENT_ALIAS = "eeAlias";
	private static final String ELECTION_EVENT_ID = "a9d805a0d9ef4deeb4b14838f7e8ed8a";
	@Mock
	private ImportExportService importExportService;
	@Mock
	private ElectionEventService electionEventService;

	@InjectMocks
	private OperationsController operationsController;

	@Test
	void testExportOperationElectionEventNotFound() {
		// given
		final String electionEventId = "17ccbe962cf341bc93208c26e911090c";

		assertThrows(IllegalStateException.class, () -> operationsController.exportOperation(electionEventId));
	}

	@Test
	void testExportOperationErrorInService() throws IOException {
		when(electionEventService.getElectionEventAlias(ELECTION_EVENT_ID)).thenReturn(ELECTION_EVENT_ALIAS);
		doThrow(new IllegalStateException("test")).when(importExportService).exportSdmData(any(), anyString());

		final StreamingResponseBody body = operationsController.exportOperation(ELECTION_EVENT_ID).getBody();
		assertNotNull(body);
		try (final NullOutputStream fos = NullOutputStream.NULL_OUTPUT_STREAM) {
			assertThrows(IllegalStateException.class, () -> body.writeTo(fos));
		}
	}

	@Test
	void testExportOperationHappyPath() throws IOException {

		when(electionEventService.getElectionEventAlias(ELECTION_EVENT_ID)).thenReturn(ELECTION_EVENT_ALIAS);

		final ResponseEntity<StreamingResponseBody> exportOperation = operationsController.exportOperation(ELECTION_EVENT_ID);

		assertEquals(HttpStatus.OK, exportOperation.getStatusCode());
		try (final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
			exportOperation.getBody().writeTo(byteArrayOutputStream);
			assertEquals(0, byteArrayOutputStream.toByteArray().length);
		}
	}

	@Test
	void testImportOperationErrorInService() {
		doThrow(new IllegalStateException("test")).when(importExportService).importSdmData(any());

		final MockMultipartFile multipartFile = new MockMultipartFile("test", new byte[] { 1, 2, 3, 4 });
		final ResponseEntity<OperationResult> importOperation = operationsController.importOperation(multipartFile);

		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, importOperation.getStatusCode());
		assertEquals(OperationsOutputCode.GENERAL_ERROR.value(), importOperation.getBody().getError());
	}

	@Test
	void testImportOperationHappyPath() {
		final MockMultipartFile multipartFile = new MockMultipartFile("test", new byte[] { 1, 2, 3, 4 });
		final ResponseEntity<OperationResult> importOperation = operationsController.importOperation(multipartFile);

		assertEquals(HttpStatus.OK, importOperation.getStatusCode());
		assertNull(importOperation.getBody());
	}
}
