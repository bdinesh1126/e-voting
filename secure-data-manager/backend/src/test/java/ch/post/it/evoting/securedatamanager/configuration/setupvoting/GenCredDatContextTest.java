/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.security.SecureRandom;
import java.util.List;
import java.util.Locale;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.securedatamanager.commons.Constants;

class GenCredDatContextTest {

	private final static int bound = 12;
	private final static int start = 21;
	private final static int differentStart = 41;
	private final static int maxSize = 4;

	private final Random rand = RandomFactory.createRandom();
	private final SecureRandom srand = new SecureRandom();
	private final int delta = srand.nextInt(bound) + 1;
	private final int psi = srand.nextInt(bound) + 1;

	private final GqGroup gqGroup = GroupTestData.getGqGroup();
	private final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);

	private String electionEventId;
	private String verificationCardSetId;
	private ElGamalMultiRecipientPublicKey electionPublicKey;
	private ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey;
	private PrimesMappingTable primesMappingTable;
	private List<String> actualVotingOptions;
	private List<String> ciSelections;
	private List<String> ciVotingOptions;

	@BeforeEach
	void setup() {
		electionEventId = rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase(Locale.ENGLISH);
		verificationCardSetId = rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase(Locale.ENGLISH);

		electionPublicKey = elGamalGenerator.genRandomPublicKey(delta);
		choiceReturnCodesEncryptionPublicKey = elGamalGenerator.genRandomPublicKey(psi);

		final GqGroupGenerator generator = new GqGroupGenerator(electionPublicKey.getGroup());
		final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions = Stream.generate(generator::genSmallPrimeMember).limit(maxSize)
				.distinct()
				.collect(GroupVector.toGroupVector());
		actualVotingOptions = Stream.generate(() -> rand.genRandomBase16String(Constants.BASE16_ID_LENGTH)).limit(encodedVotingOptions.size())
				.toList();
		primesMappingTable = PrimesMappingTable.from(IntStream.range(0, encodedVotingOptions.size())
				.mapToObj(i -> new PrimesMappingTableEntry(actualVotingOptions.get(i), encodedVotingOptions.get(i), "semantic information"))
				.toList());

		ciSelections = Stream.generate(() -> rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase(Locale.ENGLISH)).limit(maxSize)
				.toList();
		ciVotingOptions = Stream.generate(() -> rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase(Locale.ENGLISH)).limit(maxSize)
				.toList();
	}

	@Test
	@DisplayName("happyPath")
	void happyPath() {
		GenCredDatContext context = assertDoesNotThrow(() -> new GenCredDatContext.Builder()
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setElectionPublicKey(electionPublicKey)
				.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
				.setPrimesMappingTable(primesMappingTable)
				.setCorrectnessInformationSelections(ciSelections)
				.setCorrectnessInformationVotingOptions(ciVotingOptions)
				.build());

		assertEquals(context.electionEventId(), electionEventId, "Context electionEventId no equal");
		assertEquals(context.verificationCardSetId(), verificationCardSetId, "Context verificationCardSetId no equal");
		assertEquals(context.electionPublicKey().getGroup(), electionPublicKey.getGroup(),
				"Input electionPublicKey has different GqGroup");
		assertTrue(context.electionPublicKey().stream().allMatch(elt -> electionPublicKey.stream().anyMatch(elt2 -> elt2.equals(elt))),
				"Input electionPublicKey does not contains all elements");
		assertTrue(context.primesMappingTable().getPTable().containsAll(primesMappingTable.getPTable()),
				"Input primes mapping table does not contains all elements");
	}

	@Nested
	@DisplayName("A null arguments throws a NullPointerException")
	class NullArgumentThrowsNullPointerException {

		@Test
		@DisplayName("all arguments null")
		void constructWithNullArguments() {
			final GenCredDatContext.Builder builder = new GenCredDatContext.Builder();

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			final String expectedMessage = null;

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("electionEventId argument null")
		void constructWithElectionEventId() {
			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setVerificationCardSetId(verificationCardSetId)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setPrimesMappingTable(primesMappingTable);

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			final String expectedMessage = null;

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("verificationCardSetId argument null")
		void constructWithVerificationCardSetId() {
			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setPrimesMappingTable(primesMappingTable);

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			final String expectedMessage = null;

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("electionPublicKey argument null")
		void constructWithElectionPublicKey() {
			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setPrimesMappingTable(primesMappingTable);

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			final String expectedMessage = null;

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("choiceReturnCodesEncryptionPublicKey argument null")
		void constructWithChoiceReturnCodesEncryptionPublicKey() {
			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setElectionPublicKey(electionPublicKey)
					.setPrimesMappingTable(primesMappingTable);

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			final String expectedMessage = null;

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("primesMappingTable argument null")
		void constructWithEncodedVotingOptions() {
			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey);

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			final String expectedMessage = null;

			assertEquals(expectedMessage, ex.getMessage());
		}
	}

	@Nested
	@DisplayName("Invalid arguments throws an Exception")
	class InvalidArgumentThrowsException {

		private ElGamalGenerator elGamalGeneratorGroup2;

		@BeforeEach
		void setUp() {
			GqGroup differentGroup = GroupTestData.getDifferentGqGroup(gqGroup);
			elGamalGeneratorGroup2 = new ElGamalGenerator(differentGroup);
		}

		@Test
		@DisplayName("electionEventId argument invalid")
		void invalidElectionEventIdTest() {
			final String invalidElectionEventId = electionEventId + "X";

			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(invalidElectionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setPrimesMappingTable(primesMappingTable);

			FailedValidationException ex = assertThrows(FailedValidationException.class, builder::build);

			final String expectedMessage =
					String.format("The given string does not comply with the required format. [string: %s, format: ^[0123456789abcdefABCDEF]{32}$].",
							invalidElectionEventId);

			assertEquals(expectedMessage, ex.getMessage());

		}

		@Test
		@DisplayName("verificationCardSetId argument invalid")
		void invalidVerificationCardSetIdTest() {
			final String invalidVerificationCardSetId = verificationCardSetId + "X";

			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(invalidVerificationCardSetId)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setPrimesMappingTable(primesMappingTable)
					.setCorrectnessInformationSelections(ciSelections)
					.setCorrectnessInformationVotingOptions(ciVotingOptions);

			final FailedValidationException ex = assertThrows(FailedValidationException.class, builder::build);

			final String expectedMessage =
					String.format("The given string does not comply with the required format. [string: %s, format: ^[0123456789abcdefABCDEF]{32}$].",
							invalidVerificationCardSetId);

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("electionPublicKey argument invalid")
		void invalidElectionPublicKeyTest() {
			final ElGamalMultiRecipientPublicKey electionPublicKey2 = elGamalGeneratorGroup2.genRandomPublicKey(srand.nextInt(bound) + start);

			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setElectionPublicKey(electionPublicKey2)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setPrimesMappingTable(primesMappingTable)
					.setCorrectnessInformationSelections(ciSelections)
					.setCorrectnessInformationVotingOptions(ciVotingOptions);

			final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "The choiceReturnCodesEncryptionPublicKey and electionPublicKey must have the same group.";

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("choiceReturnCodesEncryptionPublicKey invalid")
		void invalidChoiceReturnCodesEncryptionPublicKeyTest() {
			final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey2 =
					elGamalGeneratorGroup2.genRandomPublicKey(srand.nextInt(bound) + differentStart);

			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey2)
					.setPrimesMappingTable(primesMappingTable)
					.setCorrectnessInformationSelections(ciSelections)
					.setCorrectnessInformationVotingOptions(ciVotingOptions);

			final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "The choiceReturnCodesEncryptionPublicKey and electionPublicKey must have the same group.";

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("primesMappingTable and electionPublicKey have the same GqGroup.")
		void encodedVotingOptionsGqGroupTest() {
			GqGroup gqGroup2 = GroupTestData.getGqGroup();

			while (gqGroup2.equals(gqGroup)) {
				gqGroup2 = GroupTestData.getGqGroup();
			}
			final GqGroupGenerator gqGroupGenerator2 = new GqGroupGenerator(gqGroup2);

			final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptionsDifferentGqGroup =
					Stream.generate(gqGroupGenerator2::genSmallPrimeMember).limit(actualVotingOptions.size()).distinct().collect(GroupVector.toGroupVector());
			final PrimesMappingTable primesMappingTableDifferentGqGroup = PrimesMappingTable.from(
					IntStream.range(0, encodedVotingOptionsDifferentGqGroup.size())
							.mapToObj(i -> new PrimesMappingTableEntry(actualVotingOptions.get(i), encodedVotingOptionsDifferentGqGroup.get(i),
									"semantic information"))
							.toList());

			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setPrimesMappingTable(primesMappingTableDifferentGqGroup)
					.setCorrectnessInformationSelections(ciSelections)
					.setCorrectnessInformationVotingOptions(ciVotingOptions);

			final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "The primesMappingTable and electionPublicKey must have the same group.";

			assertEquals(expectedMessage, ex.getMessage());
		}
	}
}
