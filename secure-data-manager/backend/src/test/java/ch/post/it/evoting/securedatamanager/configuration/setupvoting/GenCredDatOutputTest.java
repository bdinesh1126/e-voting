/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.security.SecureRandom;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;

class GenCredDatOutputTest {
	private final Random RAND = RandomFactory.createRandom();
	private final SecureRandom SRAND = new SecureRandom();
	private final int SIZE = SRAND.nextInt(12) + 1;

	private final List<String> verificationCardKeystores = List.copyOf(
			Stream.generate(() -> RAND.genRandomBase16String(32)).limit(SIZE).toList());

	@Test
	@DisplayName("happyPath")
	void happyPath() {
		AtomicReference<GenCredDatOutput> refOutput = new AtomicReference<>();

		assertDoesNotThrow(() -> refOutput.set(new GenCredDatOutput(verificationCardKeystores)));

		GenCredDatOutput output = refOutput.get();

		assertTrue(output.verificationCardKeystores().containsAll(verificationCardKeystores),
				"Input verificationCardKeystores does not contains all elements");
	}

	@Test
	@DisplayName("argument null")
	void nullArgumentTest() {
		final NullPointerException ex =
				assertThrows(NullPointerException.class, () -> new GenCredDatOutput(null));

		final String expectedMessage = null;

		assertEquals(expectedMessage, ex.getMessage());
	}

	@Test
	@DisplayName("argument empty")
	void emptyArgumentTest() {
		List<String> EMPTY_LIST = List.of();

		final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> new GenCredDatOutput(EMPTY_LIST));

		final String expectedMessage = "Vector of verification card keystores must not be empty.";

		assertEquals(expectedMessage, ex.getMessage());
	}
}
