/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Locale;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Factory;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Profile;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.common.Alphabet;
import ch.post.it.evoting.evotinglibraries.domain.common.StartVotingKeyAlphabet;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.sdmsetup.configuration.setupvoting.GenRandomSVKAlgorithm;

@DisplayName("Calling deriveExtendedAuthenticationChallenge")
class DeriveBaseAuthenticationChallengeAlgorithmTest {

	private static final Random random = RandomFactory.createRandom();

	private static DeriveBaseAuthenticationChallengeAlgorithm algorithm;

	private String electionEventId;
	private String startVotingKey;
	private String extendedAuthenticationFactor;

	@BeforeAll
	static void setupAll() {
		final Hash hash = HashFactory.createHash();
		final Argon2 argon2 = Argon2Factory.createArgon2(Argon2Profile.TEST.getContext());
		final Base64 base64 = BaseEncodingFactory.createBase64();
		algorithm = new DeriveBaseAuthenticationChallengeAlgorithm(hash, argon2, base64);
	}

	@BeforeEach
	void setup() {
		electionEventId = random.genRandomBase16String(Constants.BASE16_ID_LENGTH);
		startVotingKey = new GenRandomSVKAlgorithm(random).genRandomSVK(Constants.SVK_LENGTH, StartVotingKeyAlphabet.getInstance());
		extendedAuthenticationFactor = random.genUniqueDecimalStrings(8, 1).get(0);
	}

	@Test
	@DisplayName("with null arguments throws a NullPointerException")
	void deriveBaseAuthenticationChallengeWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class,
				() -> algorithm.deriveBaseAuthenticationChallenge(null, startVotingKey, extendedAuthenticationFactor));
		assertThrows(NullPointerException.class,
				() -> algorithm.deriveBaseAuthenticationChallenge(electionEventId, null, extendedAuthenticationFactor));
		assertThrows(NullPointerException.class, () -> algorithm.deriveBaseAuthenticationChallenge(electionEventId, startVotingKey, null));
	}

	@Test
	@DisplayName("with invalid election event id throws a FailedValidationException")
	void deriveBaseAuthenticationChallengeWithInvalidElectionEventIdThrows() {
		final String invalidElectionEventId = electionEventId + "$";
		assertThrows(FailedValidationException.class,
				() -> algorithm.deriveBaseAuthenticationChallenge(invalidElectionEventId, startVotingKey, extendedAuthenticationFactor));
	}

	@Test
	@DisplayName("with invalid start voting key throws a FailedValidationException")
	void deriveBaseAuthenticationChallengeWithInvalidStartVotingKeyThrows() {
		final String invalidStartVotingKey = startVotingKey.toUpperCase(Locale.ENGLISH);
		assertThrows(FailedValidationException.class,
				() -> algorithm.deriveBaseAuthenticationChallenge(electionEventId, invalidStartVotingKey, extendedAuthenticationFactor));
	}

	@Test
	@DisplayName("with invalid start voting key throws a FailedValidationException")
	void deriveBaseAuthenticationChallengeWithStartVotingKeyWrongSizeThrows() {
		final String tooLongStartVotingKey = startVotingKey + "a";
		assertThrows(FailedValidationException.class,
				() -> algorithm.deriveBaseAuthenticationChallenge(electionEventId, tooLongStartVotingKey, extendedAuthenticationFactor));

		final String tooShortStartVotingKey = startVotingKey.substring(1);
		assertThrows(FailedValidationException.class,
				() -> algorithm.deriveBaseAuthenticationChallenge(electionEventId, tooShortStartVotingKey, extendedAuthenticationFactor));
	}

	@Test
	@DisplayName("with extended authentication factor not a digit throws an IllegalArgumentException")
	void deriveBaseAuthenticationChallengeWithInvalidExtendedAuthenticationFactorThrows() {
		final String invalidExtendedAuthenticationFactor = "a" + extendedAuthenticationFactor.substring(1);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> algorithm.deriveBaseAuthenticationChallenge(electionEventId, startVotingKey, invalidExtendedAuthenticationFactor));
		assertEquals("The extended authentication factor must be a digit of correct size.", exception.getMessage());
	}

	@Test
	@DisplayName("with extended authentication factor of bad size throws an IllegalArgumentException")
	void deriveBaseAuthenticationChallengeWithExtendedAuthenticationFactorBadSizeThrows() {
		final String invalidExtendedAuthenticationFactor = extendedAuthenticationFactor + "1";
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> algorithm.deriveBaseAuthenticationChallenge(electionEventId, startVotingKey, invalidExtendedAuthenticationFactor));
		assertEquals("The extended authentication factor must be a digit of correct size.", exception.getMessage());
	}

	@Test
	@DisplayName("with valid arguments does not throw")
	void deriveBaseAuthenticationChallengeWithValidArgumentsDoesNotThrow() {
		assertDoesNotThrow(() -> algorithm.deriveBaseAuthenticationChallenge(electionEventId, startVotingKey, extendedAuthenticationFactor));

		final String shortExtendedAuthenticationFactor = extendedAuthenticationFactor.substring(4);
		assertDoesNotThrow(() -> algorithm.deriveBaseAuthenticationChallenge(electionEventId, startVotingKey, shortExtendedAuthenticationFactor));
	}
}
