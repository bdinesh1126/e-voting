/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmsetup.configuration.setupvoting;

import static ch.post.it.evoting.securedatamanager.commons.Constants.SVK_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.common.StartVotingKeyAlphabet;

@DisplayName("GenRandomSVKAlgorithm calling GenRandomSVK with ")
class GenRandomSVKAlgorithmTest {

	private static final Random random = RandomFactory.createRandom();
	private static final StartVotingKeyAlphabet startVotingKeyAlphabet = StartVotingKeyAlphabet.getInstance();
	private static GenRandomSVKAlgorithm genRandomSVKAlgorithm;

	@BeforeAll
	static void setup() {
		genRandomSVKAlgorithm = new GenRandomSVKAlgorithm(random);
	}

	@ParameterizedTest
	@CsvSource({ "-1", "0" })
	@DisplayName("an invalid length throws an IllegalArgumentException")
	void invalidLengthThrows(final int length) {
		final IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
				() -> genRandomSVKAlgorithm.genRandomSVK(length, startVotingKeyAlphabet));

		assertEquals(String.format("The desired length of string must be strictly positive. [length: %s]", length),
				Throwables.getRootCause(illegalArgumentException).getMessage());
	}

	@Test
	@DisplayName("a null alphabet throws a NullPointerException")
	void nullAlphabetThrows() {
		assertThrows(NullPointerException.class, () -> genRandomSVKAlgorithm.genRandomSVK(SVK_LENGTH, null));
	}

	@Test
	@DisplayName("valid input behaves as expected")
	void happyPath() {

		final String S_prime = assertDoesNotThrow(() -> genRandomSVKAlgorithm.genRandomSVK(SVK_LENGTH, startVotingKeyAlphabet));

		// S_prime must have length l.
		assertEquals(SVK_LENGTH, S_prime.length());

		// each element of S_prime must be part of the Alphabet.
		final char[] chars = S_prime.toCharArray();
		for (final char S_prime_i_char : chars) {
			final String S_prime_i = String.valueOf(S_prime_i_char);
			assertTrue(startVotingKeyAlphabet.contains(S_prime_i));
		}

		// a second call should return a different S_prime
		assertNotEquals(S_prime, genRandomSVKAlgorithm.genRandomSVK(SVK_LENGTH, startVotingKeyAlphabet));
	}
}