/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.importexport;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ElectionEventContextRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.administrationboard.AdministrationBoardRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballot.BallotRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballotbox.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballottext.BallotTextRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.electionevent.ElectionEventRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.electoralboard.ElectoralBoardRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

@ExtendWith(MockitoExtension.class)
class ImportExportOrientDbServiceTest {

	private static final String EE_ID = "eeid";
	private static final String EXPORT = "export";

	@Mock
	private VotingCardSetRepository votingCardSetRepository;
	@Mock
	private BallotRepository ballotRepository;
	@Mock
	private AdministrationBoardRepository administrationBoardRepository;
	@Mock
	private ElectionEventContextRepository electionEventContextRepository;
	@Mock
	private ElectionEventRepository electionEventRepository;
	@Mock
	private BallotTextRepository ballotTextRepository;
	@Mock
	private BallotBoxRepository ballotBoxRepository;
	@Mock
	private ElectoralBoardRepository electoralBoardRepository;
	@TempDir
	private Path workingDirectory;

	private String votingCardSetJson;
	private String administrationBoardJson;
	private String electionEventJson;
	private String ballotJson;
	private String ballotTextJson;
	private String ballotBoxJson;
	private String electoralBoardJson;
	private ImportExportOrientDbService importExportOrientDbService;

	private static Path getPathOfFileInResources(final String path) throws URISyntaxException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		URL resource = classLoader.getResource(path);
		return Paths.get(resource.toURI());
	}

	@BeforeEach
	public void setUp() throws URISyntaxException {
		importExportOrientDbService = spy(new ImportExportOrientDbService(
				electionEventRepository,
				ballotBoxRepository,
				ballotRepository,
				ballotTextRepository,
				votingCardSetRepository,
				electoralBoardRepository,
				administrationBoardRepository,
				electionEventContextRepository));
	}

	@Test
	void exportOrientDb() throws IOException, ResourceNotFoundException {

		// given
		when(administrationBoardRepository.list()).thenReturn(getAdministrationBoardJson());
		when(electionEventRepository.find(anyString())).thenReturn(getElectionEventJson());
		when(ballotRepository.listByElectionEvent(anyString())).thenReturn(getBallotJson());
		when(ballotTextRepository.list(any())).thenReturn(getBallotTextJson());
		when(ballotBoxRepository.listByElectionEvent(any())).thenReturn(getBallotBoxJson());
		when(electoralBoardRepository.listByElectionEvent(anyString())).thenReturn(getElectoralBoardJson());
		when(votingCardSetRepository.listByElectionEvent(anyString())).thenReturn(getVotingCardSetJson());

		// when
		importExportOrientDbService.exportOrientDb(workingDirectory.resolve("db-export.json"), EE_ID);

		// then
		assertThat(Files.exists(workingDirectory.resolve("db-export.json"))).isTrue();
	}

	/**
	 * Returns a sample voting card set JSON string.
	 */
	private String getVotingCardSetJson() throws IOException {
		if (null == votingCardSetJson) {
			votingCardSetJson = readJson("votingCardSet.json");
		}

		return votingCardSetJson;
	}

	private String getAdministrationBoardJson() throws IOException {
		if (null == administrationBoardJson) {
			administrationBoardJson = readJson("administrationBoards.json");
		}

		return administrationBoardJson;
	}

	private String getElectionEventJson() throws IOException {
		if (null == electionEventJson) {
			electionEventJson = readJson("electionEventRepository.json");
		}

		return electionEventJson;
	}

	private String getBallotJson() throws IOException {
		if (null == ballotJson) {
			ballotJson = readJson("ballot_result_with_representations.json");
		}

		return ballotJson;
	}

	private String getBallotTextJson() throws IOException {
		if (null == ballotTextJson) {
			ballotTextJson = readJson("ballotText_result.json");
		}

		return ballotTextJson;
	}

	private String getBallotBoxJson() throws IOException {
		if (null == ballotBoxJson) {
			ballotBoxJson = readJson("ballotBox_result.json");
		}

		return ballotBoxJson;
	}

	private String getElectoralBoardJson() throws IOException {
		if (null == electoralBoardJson) {
			electoralBoardJson = readJson("electoralBoard_result.json");
		}

		return electoralBoardJson;
	}

	private String readJson(String fileName) throws IOException {
		try (InputStream is = getClass().getResourceAsStream("/" + fileName); Scanner scanner = new Scanner(is, StandardCharsets.UTF_8)) {
			return scanner.useDelimiter("\\A").next();
		}
	}
}