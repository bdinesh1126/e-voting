/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static ch.post.it.evoting.securedatamanager.configuration.ElectionEventContextPayloadFileRepository.PAYLOAD_FILE_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionEventContext;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.evotinglibraries.domain.SerializationUtils;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@DisplayName("ElectionEventContextPayloadFileRepository")
class ElectionEventContextPayloadFileRepositoryTest {

	private static final String NON_EXISTING_ELECTION_EVENT_ID = "414bd34dcf6e4de4b771a92fa3849d3d";
	private static final String EXISTING_ELECTION_EVENT_ID = "b643acd93ccc453db5b1ed3ed910f4b2";
	private static final String CORRUPTED_ELECTION_EVENT_ID = "514bd34dcf6e4de4b771a92fa3849d3d";
	private static final String EXISTING_ELECTION_EVENT_ALIAS = "b643acd93ccc453db5b1ed3ed910f4b2_alias";
	private static final String EXISTING_ELECTION_EVENT_DESCRIPTION = "b643acd93ccc453db5b1ed3ed910f4b2_description";
	private static final Random RANDOM = RandomFactory.createRandom();

	private static PathResolver pathResolver;
	private static ObjectMapper objectMapper;
	private static ElectionEventContextPayloadFileRepository electionEventContextPayloadFileRepository;

	@BeforeAll
	static void setUpAll() throws URISyntaxException {
		objectMapper = DomainObjectMapper.getNewInstance();

		final Path path = Paths.get(
				Objects.requireNonNull(ElectionEventContextPayloadFileRepositoryTest.class.getClassLoader()
								.getResource("ElectionEventContextPayloadFileRepositoryTest/"))
						.toURI());
		pathResolver = new PathResolver(path.toString());
		electionEventContextPayloadFileRepository = new ElectionEventContextPayloadFileRepository(objectMapper, pathResolver);
	}

	private ElectionEventContextPayload validElectionEventContextPayload() {
		final List<VerificationCardSetContext> verificationCardSetContexts = new ArrayList<>();

		IntStream.rangeClosed(1, 2).forEach((i) -> verificationCardSetContexts.add(generatedVerificationCardSetContext()));

		final GqGroup gqGroup = SerializationUtils.getGqGroup();

		final LocalDateTime startTime = LocalDateTime.now().minusMinutes(15);
		final LocalDateTime finishTime = startTime.plusWeeks(1);

		final ElectionEventContext electionEventContext = new ElectionEventContext(EXISTING_ELECTION_EVENT_ID, EXISTING_ELECTION_EVENT_ALIAS,
				EXISTING_ELECTION_EVENT_DESCRIPTION, verificationCardSetContexts, startTime, finishTime);

		return new ElectionEventContextPayload(gqGroup, electionEventContext);
	}

	private VerificationCardSetContext generatedVerificationCardSetContext() {
		final String verificationCardSetId = RANDOM.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
		final String verificationCardSetAlias = verificationCardSetId + "_alias";
		final String verificationCardSetDescription = verificationCardSetId + "_description";
		final String ballotBoxId = RANDOM.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
		final LocalDateTime startTime = LocalDateTime.now();
		final LocalDateTime finishTime = startTime.plusDays(1);
		final boolean testBallotBox = Math.random() < 0.5;
		final int numberOfWriteInFields = 1;
		final int numberOfVotingCards = 10;
		final int gracePeriod = 900;
		final GroupVector<PrimeGqElement, GqGroup> smallPrimeGroupMembers = PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(
				SerializationUtils.getGqGroup(), 1);
		final PrimesMappingTable primesMappingTable = PrimesMappingTable.from(
				List.of(new PrimesMappingTableEntry("actualVotingOption", smallPrimeGroupMembers.get(0), "semantic")));

		return new VerificationCardSetContext(verificationCardSetId, verificationCardSetAlias, verificationCardSetDescription, ballotBoxId, startTime,
				finishTime, testBallotBox, numberOfWriteInFields, numberOfVotingCards, gracePeriod, primesMappingTable);
	}

	private ControlComponentPublicKeys generateCombinedControlComponentPublicKeys(final int nodeId) {
		final ElGamalMultiRecipientPublicKey ccrChoiceReturnCodesEncryptionPublicKey = SerializationUtils.getPublicKey();
		final ElGamalMultiRecipientPublicKey ccmElectionPublicKey = SerializationUtils.getPublicKey();
		final GroupVector<SchnorrProof, ZqGroup> schnorrProofs = SerializationUtils.getSchnorrProofs(2);
		return new ControlComponentPublicKeys(nodeId, ccrChoiceReturnCodesEncryptionPublicKey, schnorrProofs, ccmElectionPublicKey, schnorrProofs);
	}

	@Nested
	@DisplayName("saving")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SaveTest {

		private ElectionEventContextPayloadFileRepository electionEventContextPayloadFileRepositoryTemp;

		private ElectionEventContextPayload electionEventContextPayload;

		@BeforeAll
		void setUpAll(
				@TempDir
				final Path tempDir) throws IOException {

			Files.createDirectories(tempDir.resolve("sdm/config").resolve(EXISTING_ELECTION_EVENT_ID));

			final PathResolver pathResolver = new PathResolver(tempDir.toString());
			electionEventContextPayloadFileRepositoryTemp = new ElectionEventContextPayloadFileRepository(objectMapper, pathResolver);
		}

		@BeforeEach
		void setUp() {
			electionEventContextPayload = validElectionEventContextPayload();
		}

		@Test
		@DisplayName("valid election event context payload creates file")
		void save() {
			final Path savedPath = electionEventContextPayloadFileRepositoryTemp.save(electionEventContextPayload);

			assertTrue(Files.exists(savedPath));
		}

		@Test
		@DisplayName("null election event context payload throws NullPointerException")
		void saveNullElectionEventContext() {
			assertThrows(NullPointerException.class, () -> electionEventContextPayloadFileRepositoryTemp.save(null));
		}

		@Test
		@DisplayName("invalid path throws UncheckedIOException")
		void invalidPath() {
			final PathResolver pathResolver = new PathResolver("invalidPath");
			final ElectionEventContextPayloadFileRepository repository = new ElectionEventContextPayloadFileRepository(
					DomainObjectMapper.getNewInstance(), pathResolver);

			final UncheckedIOException exception = assertThrows(UncheckedIOException.class, () -> repository.save(electionEventContextPayload));

			final Path electionEventContextPath = pathResolver.resolveElectionEventPath(EXISTING_ELECTION_EVENT_ID).resolve(PAYLOAD_FILE_NAME);
			final String errorMessage = String.format(
					"Failed to serialize election event context payload. [electionEventId: %s, path: %s]", EXISTING_ELECTION_EVENT_ID,
					electionEventContextPath);

			assertEquals(errorMessage, exception.getMessage());
		}

	}

	@Nested
	@DisplayName("calling existsById")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class ExistsByIdTest {

		@Test
		@DisplayName("for existing election event context payload returns true")
		void existingElectionEventContext() {
			assertTrue(electionEventContextPayloadFileRepository.existsById(EXISTING_ELECTION_EVENT_ID));
		}

		@Test
		@DisplayName("with invalid election event id throws FailedValidationException")
		void invalidElectionEventId() {
			assertThrows(FailedValidationException.class, () -> electionEventContextPayloadFileRepository.existsById("invalidId"));
		}

		@Test
		@DisplayName("for non existing election event context payload returns false")
		void nonExistingElectionEventContext() {
			assertFalse(electionEventContextPayloadFileRepository.existsById(NON_EXISTING_ELECTION_EVENT_ID));
		}

	}

	@Nested
	@DisplayName("calling findById")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class FindByIdTest {

		@Test
		@DisplayName("for existing election event context payload returns it")
		void existingElectionEventContext() {
			assertTrue(electionEventContextPayloadFileRepository.findById(EXISTING_ELECTION_EVENT_ID).isPresent());
		}

		@Test
		@DisplayName("for non existing election event context payload return empty optional")
		void nonExistingElectionEventContext() {
			assertFalse(electionEventContextPayloadFileRepository.findById(NON_EXISTING_ELECTION_EVENT_ID).isPresent());
		}

		@Test
		@DisplayName("for corrupted election event context payload throws UncheckedIOException")
		void corruptedElectionEventContext() {
			final UncheckedIOException exception = assertThrows(UncheckedIOException.class,
					() -> electionEventContextPayloadFileRepository.findById(CORRUPTED_ELECTION_EVENT_ID));

			final Path electionEventPath = pathResolver.resolveElectionEventPath(CORRUPTED_ELECTION_EVENT_ID);
			final Path electionEventContextPath = electionEventPath.resolve(PAYLOAD_FILE_NAME);
			final String errorMessage = String.format("Failed to deserialize election event context payload. [electionEventId: %s, path: %s]",
					CORRUPTED_ELECTION_EVENT_ID, electionEventContextPath);

			assertEquals(errorMessage, exception.getMessage());
		}

	}

}
