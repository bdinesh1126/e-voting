/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.domain.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.file.Path;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;
import ch.post.it.evoting.securedatamanager.services.infrastructure.electionevent.ElectionEventRepository;

@ExtendWith(MockitoExtension.class)
class ElectionEventDataGeneratorServiceTest {

	@InjectMocks
	@Spy
	private ElectionEventDataGeneratorService electionEventDataGeneratorService;

	@Mock
	private PathResolver pathResolverMock;

	@Mock
	private Path configPathMock;

	@Mock
	private ElectionEventRepository electionEventRepositoryMock;

	private String electionEventId;

	@Test
	void generateWithIdNull() {
		assertFalse(electionEventDataGeneratorService.generate(null));
	}

	@Test
	void generateWithIdEmpty() {
		electionEventId = "";

		assertFalse(electionEventDataGeneratorService.generate(electionEventId));
	}

	@Test
	void generateElectionEventNotFound() throws IOException {
		electionEventId = "123";

		when(pathResolverMock.resolveConfigPath()).thenReturn(configPathMock);
		doReturn(configPathMock).when(electionEventDataGeneratorService).makePath(configPathMock);
		when(electionEventRepositoryMock.find(electionEventId)).thenReturn(JsonConstants.EMPTY_OBJECT);

		assertFalse(electionEventDataGeneratorService.generate(electionEventId));
	}

}
