/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.SecureRandom;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;

class MixDecOfflineInputTest {

	private static final SecureRandom RANDOM = new SecureRandom();
	private static final GqGroup GQ_GROUP = GroupTestData.getLargeGqGroup();

	private GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> partiallyDecryptedVotes;
	private List<char[]> electoralBoardMembersPasswords;

	@BeforeEach
	void setupEach() {
		final int n = RANDOM.nextInt(4) + 2;
		final int numberOfAllowedWriteInsPlusOne = RANDOM.nextInt(5) + 1;
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(GQ_GROUP);
		partiallyDecryptedVotes = elGamalGenerator.genRandomCiphertextVector(n, numberOfAllowedWriteInsPlusOne);
		electoralBoardMembersPasswords = Stream.of("Password_ElectoralBoard1_2".toCharArray(), "Password_ElectoralBoard2_2".toCharArray())
				.toList();
	}

	@Test
	void constructMixDecOfflineInputWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class, () -> new MixDecOfflineInput(null, electoralBoardMembersPasswords));
		assertThrows(NullPointerException.class, () -> new MixDecOfflineInput(partiallyDecryptedVotes, null));
	}

	@Test
	void constructWithInvalidPasswordsThrows() {
		final List<char[]> invalidElectoralBoardMembersPasswords = Stream.of("invalid".toCharArray(), "Password_ElectoralBoard2_2".toCharArray())
				.toList();
		assertThrows(IllegalArgumentException.class, () -> new MixDecOfflineInput(partiallyDecryptedVotes, invalidElectoralBoardMembersPasswords));
	}
}
