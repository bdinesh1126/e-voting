/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_ACTUAL_VOTING_OPTION_LENGTH;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline.ProcessPlaintextsAlgorithmOutput;

/**
 * Tests of ProcessPlaintextsAlgorithmOutput.
 */
@DisplayName("A ProcessPlaintextsAlgorithmOutput built with")
class ProcessPlaintextsAlgorithmOutputTest {

	private static final GqGroup gqGroup = GroupTestData.getGroupP59();
	private static final int desiredNumberOfPrimes = 3;
	private static final List<List<String>> L_decodedVotes = List.of(
			List.of(
					"57a30570-1722-3a7e-a8f9-7dd643d7f33",
					"b9c57a40-a555-35e1-972c-a1a6b7e03381",
					"1-3"),
			List.of(
					"8814c2b6-8c73-38e8-99e6-830bffdf32c6",
					"b9c57a40-a555-35e1-972c-a1a6b7e03381",
					"1-2"));

	private static GroupVector<GroupVector<PrimeGqElement, GqGroup>, GqGroup> L_votes;

	@BeforeAll
	static void setUp() {
		L_votes = GroupVector.from(
				GroupVector.of(
						PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(gqGroup, desiredNumberOfPrimes),
						PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(gqGroup, desiredNumberOfPrimes)));
	}

	@Test
	@DisplayName("any null input throws a NullPointerException.")
	void nullInputThrows() {
		final List<List<String>> emptyList = List.of();
		assertAll(
				() -> assertThrows(NullPointerException.class, () -> new ProcessPlaintextsAlgorithmOutput(null, L_decodedVotes, emptyList)),
				() -> assertThrows(NullPointerException.class, () -> new ProcessPlaintextsAlgorithmOutput(L_votes, null, emptyList))
		);
	}

	@Test
	@DisplayName("inputs of different sizes throws an IllegalArgumentException.")
	void differentSizesInputThrows() {
		final GroupVector<GroupVector<PrimeGqElement, GqGroup>, GqGroup> smallerL_votes = GroupVector.of(L_votes.get(0));
		final List<List<String>> emptyList = List.of();
		final IllegalArgumentException illegalArgumentException1 = assertThrows(IllegalArgumentException.class,
				() -> new ProcessPlaintextsAlgorithmOutput(smallerL_votes, L_decodedVotes, emptyList));

		assertEquals(String.format(
				"The size of the list of all selected encoded voting options and the size of the list of all selected decoded voting options must be equal. [L_votes_size: %s, L_decodedVotes_size: %s]",
				smallerL_votes.size(), L_decodedVotes.size()), Throwables.getRootCause(illegalArgumentException1).getMessage());

		final List<List<String>> smallerL_decodedVotes = List.of(L_decodedVotes.get(0));
		final IllegalArgumentException illegalArgumentException2 = assertThrows(IllegalArgumentException.class,
				() -> new ProcessPlaintextsAlgorithmOutput(L_votes, smallerL_decodedVotes, emptyList));

		assertEquals(String.format(
				"The size of the list of all selected encoded voting options and the size of the list of all selected decoded voting options must be equal. [L_votes_size: %s, L_decodedVotes_size: %s]",
				L_votes.size(), smallerL_decodedVotes.size()), Throwables.getRootCause(illegalArgumentException2).getMessage());
	}

	@Test
	@DisplayName("inputs with elements of different sizes throws an IllegalArgumentException.")
	void differentElementSizesInputThrows() {
		final List<List<String>> differentElementSizesL_decodedVotes = List.of(
				L_decodedVotes.get(0),
				L_decodedVotes.get(1).subList(0, 1));
		final List<List<String>> emptyLists = List.of(List.of(), List.of());
		final IllegalArgumentException illegalArgumentException2 = assertThrows(IllegalArgumentException.class,
				() -> new ProcessPlaintextsAlgorithmOutput(L_votes, differentElementSizesL_decodedVotes, emptyLists));

		assertEquals("All selected decoded voting options must have the same size.", Throwables.getRootCause(illegalArgumentException2).getMessage());
	}

	@Test
	@DisplayName("inputs having different elements sizes throws an IllegalArgumentException.")
	void differentElementSizesInputsThrows() {
		final List<List<String>> differentElementSizesL_decodedVotes = List.of(
				L_decodedVotes.get(0).subList(0, 1),
				L_decodedVotes.get(1).subList(0, 1));
		final List<List<String>> emptyLists = List.of(List.of(), List.of());
		final IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
				() -> new ProcessPlaintextsAlgorithmOutput(L_votes, differentElementSizesL_decodedVotes, emptyLists));

		final int encodedVotesSize = L_votes.getElementSize();
		final int decodedVotesSize = differentElementSizesL_decodedVotes.get(0).size();

		assertEquals(String.format(
				"The size of the elements of the list of all selected encoded voting options must be equal to the size of the list of all selected decoded voting options. [encodedVotesSize: %s, decodedVotesSize: %s]",
				encodedVotesSize, decodedVotesSize), Throwables.getRootCause(illegalArgumentException).getMessage());
	}

	@Test
	@DisplayName("valid inputs behaves as expected.")
	void happyPath() {
		final ProcessPlaintextsAlgorithmOutput processPlaintextsAlgorithmOutput = assertDoesNotThrow(
				() -> new ProcessPlaintextsAlgorithmOutput(L_votes, L_decodedVotes, List.of(List.of(), List.of())));

		assertEquals(L_votes, processPlaintextsAlgorithmOutput.L_votes());
		assertEquals(L_decodedVotes, processPlaintextsAlgorithmOutput.L_decodedVotes());
	}

	@ParameterizedTest(name = "inputs with {0} decoded voting options throws an IllegalArgumentException.")
	@MethodSource("provideInvalidDecodedVotingOptions")
	void invalidDecodedVotingOptionsSizesInputsThrows(final String usecase, final List<List<String>> invalidDecodedVotingOptions) {
		final List<List<String>> emptyLists = List.of(List.of(), List.of());
		final IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
				() -> new ProcessPlaintextsAlgorithmOutput(L_votes, invalidDecodedVotingOptions, emptyLists));

		assertEquals(String.format("The selected decoded voting options must be non-blank strings and their length must not exceed %s.",
				MAXIMUM_ACTUAL_VOTING_OPTION_LENGTH), Throwables.getRootCause(illegalArgumentException).getMessage());
	}

	private static Stream<Arguments> provideInvalidDecodedVotingOptions() {
		return Stream.of(
				Arguments.of("empty",
						List.of(
								List.of(
										"",
										"b9c57a40-a555-35e1-972c-a1a6b7e03381",
										"1-3"),
								List.of(
										"8814c2b6-8c73-38e8-99e6-830bffdf32c6",
										"b9c57a40-a555-35e1-972c-a1a6b7e03381",
										"1-2"))),
				Arguments.of("blank",
						List.of(
								List.of(
										" ",
										"b9c57a40-a555-35e1-972c-a1a6b7e03381",
										"1-3"),
								List.of(
										"8814c2b6-8c73-38e8-99e6-830bffdf32c6",
										"b9c57a40-a555-35e1-972c-a1a6b7e03381",
										"1-2"))),
				Arguments.of("larger than authorized",
						List.of(
								List.of(
										"8KU8zJ0OR0ZDgLTDopHrVRAg4c55w8gqdjHPYWx6kh8CIkRQNbF",
										"b9c57a40-a555-35e1-972c-a1a6b7e03381",
										"1-3"),
								List.of(
										"8814c2b6-8c73-38e8-99e6-830bffdf32c6",
										"b9c57a40-a555-35e1-972c-a1a6b7e03381",
										"1-2")))
		);
	}

}