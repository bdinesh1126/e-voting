/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmtally.tally.mixoffline;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.security.SecureRandom;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.mixnet.MixnetFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProofFactory;
import ch.post.it.evoting.securedatamanager.services.application.service.BallotBoxService;

/**
 * Tests of MixDecOfflineAlgorithm.
 */
@DisplayName("MixDecOfflineAlgorithm calling mixDecOffline with")
class MixDecOfflineAlgorithmTest {

	private static final Random RANDOM_SERVICE = RandomFactory.createRandom();
	private static final SecureRandom RANDOM = new SecureRandom();
	private static final int ID_LENGTH = 32;
	private static final GqGroup GQ_GROUP = GroupTestData.getLargeGqGroup();

	private static MixDecOfflineAlgorithm mixDecOfflineAlgorithm;
	private static BallotBoxService ballotBoxService;

	private String electionEventId;
	private String ballotBoxId;
	private int numberOfAllowedWriteInsPlusOne;
	private GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> partiallyDecryptedVotes;
	private List<char[]> electoralBoardMembersPasswords;
	private MixDecOfflineContext mixDecOfflineContext;
	private MixDecOfflineInput mixDecOfflineInput;

	@BeforeAll
	static void setupAll() {
		ballotBoxService = mock(BallotBoxService.class);

		mixDecOfflineAlgorithm = new MixDecOfflineAlgorithm(HashFactory.createHash(), MixnetFactory.createMixnet(), ballotBoxService,
				ZeroKnowledgeProofFactory.createZeroKnowledgeProof()
		);
	}

	@BeforeEach
	void setup() {
		electionEventId = RANDOM_SERVICE.genRandomBase16String(ID_LENGTH).toLowerCase(Locale.ENGLISH);
		ballotBoxId = RANDOM_SERVICE.genRandomBase16String(ID_LENGTH).toLowerCase(Locale.ENGLISH);
		numberOfAllowedWriteInsPlusOne = RANDOM.nextInt(5) + 1;
		mixDecOfflineContext = new MixDecOfflineContext.Builder()
				.setEncryptionGroup(GQ_GROUP)
				.setElectionEventId(electionEventId)
				.setBallotBoxId(ballotBoxId)
				.setNumberOfAllowedWriteInsPlusOne(numberOfAllowedWriteInsPlusOne)
				.build();

		final int n = RANDOM.nextInt(4) + 2;
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(GQ_GROUP);
		partiallyDecryptedVotes = elGamalGenerator.genRandomCiphertextVector(n, numberOfAllowedWriteInsPlusOne);
		electoralBoardMembersPasswords = Stream.of("Password_ElectoralBoard1_1".toCharArray(), "Password_ElectoralBoard2_2".toCharArray())
				.toList();
		mixDecOfflineInput = new MixDecOfflineInput(partiallyDecryptedVotes, electoralBoardMembersPasswords);
	}

	@Test
	@DisplayName("null arguments throws a NullPointerException")
	void mixDecOfflineWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class,
				() -> mixDecOfflineAlgorithm.mixDecOffline(null, mixDecOfflineInput));
		assertThrows(NullPointerException.class,
				() -> mixDecOfflineAlgorithm.mixDecOffline(mixDecOfflineContext, null));
	}

	@Test
	@DisplayName("context and input having different groups throws an IllegalArgumentException")
	void mixDecOfflineWithDifferentGroupsThrows() {
		final MixDecOfflineContext contextWithDifferentGroup = new MixDecOfflineContext.Builder()
				.setEncryptionGroup(GroupTestData.getDifferentGqGroup(GQ_GROUP))
				.setElectionEventId(electionEventId)
				.setBallotBoxId(ballotBoxId)
				.setNumberOfAllowedWriteInsPlusOne(numberOfAllowedWriteInsPlusOne)
				.build();
		assertThrows(IllegalArgumentException.class, () -> mixDecOfflineAlgorithm.mixDecOffline(contextWithDifferentGroup, mixDecOfflineInput));
	}

	@Test
	@DisplayName("too few partially decrypted votes throws an IllegalArgumentException")
	void mixDecOfflineWithTooFewPartiallyDecryptedVotesThrows() {
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> tooFewPartiallyDecryptedVotes = new ElGamalGenerator(
				GQ_GROUP).genRandomCiphertextVector(1,
				partiallyDecryptedVotes.getElementSize());
		final MixDecOfflineInput tooSmallMixDecOfflineInput = new MixDecOfflineInput(tooFewPartiallyDecryptedVotes, electoralBoardMembersPasswords);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> mixDecOfflineAlgorithm.mixDecOffline(mixDecOfflineContext, tooSmallMixDecOfflineInput));
		assertEquals(String.format("There must be at least 2 partially decrypted votes. [N_c_hat: %s]", tooFewPartiallyDecryptedVotes.size()),
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("number of allowed write-ins + 1 different from number of elements of the partially decrypted votes throws an IllegalArgumentException")
	void mixDecOfflineWithPartiallyDecryptedVotesIncorrectNumberOfElementsThrows() {
		final int biggerNumberOfAllowedWriteInsPlusOne = numberOfAllowedWriteInsPlusOne + 1;
		final MixDecOfflineContext tooBigMixDecOfflineContext = new MixDecOfflineContext.Builder()
				.setEncryptionGroup(GQ_GROUP)
				.setElectionEventId(electionEventId)
				.setBallotBoxId(ballotBoxId)
				.setNumberOfAllowedWriteInsPlusOne(biggerNumberOfAllowedWriteInsPlusOne)
				.build();
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> mixDecOfflineAlgorithm.mixDecOffline(tooBigMixDecOfflineContext, mixDecOfflineInput));
		assertEquals(String.format(
				"The number of elements in the partially decrypted votes must correspond to the number of allowed write-ins + 1. [l: %s, delta_hat: %s]",
				partiallyDecryptedVotes.getElementSize(), biggerNumberOfAllowedWriteInsPlusOne), Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("on already decrypted ballot box throws IllegalArgumentException")
	void alreadyDecryptedThrows() {
		when(ballotBoxService.isDecrypted(ballotBoxId)).thenReturn(true);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> mixDecOfflineAlgorithm.mixDecOffline(mixDecOfflineContext, mixDecOfflineInput));

		final String errorMessage = String.format("The ballot box has already been decrypted. [ballotBoxId: %s]", ballotBoxId);
		assertEquals(errorMessage, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("valid inputs does not throw")
	void mixDecOfflineWithValidParametersDoesNotThrow() {
		final MixDecOfflineOutput mixDecOfflineOutput = mixDecOfflineAlgorithm.mixDecOffline(mixDecOfflineContext, mixDecOfflineInput);

		assertEquals(numberOfAllowedWriteInsPlusOne, mixDecOfflineOutput.getVerifiableShuffle().shuffledCiphertexts().getElementSize());
		assertEquals(partiallyDecryptedVotes.size(), mixDecOfflineOutput.getVerifiableShuffle().shuffledCiphertexts().size());
		assertEquals(numberOfAllowedWriteInsPlusOne, mixDecOfflineOutput.getVerifiablePlaintextDecryption().getDecryptedVotes().getElementSize());
		assertEquals(partiallyDecryptedVotes.size(), mixDecOfflineOutput.getVerifiablePlaintextDecryption().getDecryptedVotes().size());
	}
}
