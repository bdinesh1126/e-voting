/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@DisplayName("SetupKeyPairService")
class SetupKeyPairServiceTest {

	private static final String ELECTION_EVENT_ID = "be658216a38b421b943457846c6a68f9";
	private static final String WRONG_ELECTION_EVENT_ID = "ce658216a38b421b943457846c6a68f9";
	private static final String INVALID_ELECTION_EVENT_ID = "invalidId";
	private static final SecureRandom secureRandom = new SecureRandom();
	private static final Random random = RandomFactory.createRandom();

	private static GqGroup gqGroup;
	private static ObjectMapper objectMapper;
	private static SetupKeyPairService setupKeyPairService;
	private static EncryptionParametersPayloadService encryptionParametersPayloadService;

	@BeforeAll
	static void setUpAll() throws URISyntaxException {
		final Path path = Paths.get(SetupKeyPairServiceTest.class.getResource("/setupKeyPairTest/").toURI());

		gqGroup = GroupTestData.getGqGroup();
		objectMapper = DomainObjectMapper.getNewInstance();
		final PathResolver pathResolver = new PathResolver(path.toString());

		encryptionParametersPayloadService = new EncryptionParametersPayloadService(
				new EncryptionParametersPayloadFileRepository(pathResolver, objectMapper));
		final SetupKeyPairFileRepository setupKeyPairFileRepository = new SetupKeyPairFileRepository(objectMapper, pathResolver,
				encryptionParametersPayloadService);
		setupKeyPairService = new SetupKeyPairService(setupKeyPairFileRepository);
	}

	@Nested
	@DisplayName("saving")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SaveTest {

		private ElGamalMultiRecipientKeyPair setupKeyPair;
		private SetupKeyPairService setupKeyPairServiceTempDir;

		@BeforeAll
		void setUpAll(
				@TempDir
				final Path tempDir) throws IOException {

			Files.createDirectories(tempDir.resolve("sdm/config").resolve(ELECTION_EVENT_ID).resolve(Constants.CONFIG_DIR_NAME_OFFLINE));

			final PathResolver pathResolver = new PathResolver(tempDir.toString());

			final SetupKeyPairFileRepository setupKeyPairFileRepository = new SetupKeyPairFileRepository(objectMapper, pathResolver,
					encryptionParametersPayloadService);
			setupKeyPairServiceTempDir = new SetupKeyPairService(setupKeyPairFileRepository);
		}

		@BeforeEach
		void setUp() {
			final int numElements = secureRandom.nextInt(10) + 1;
			setupKeyPair = ElGamalMultiRecipientKeyPair.genKeyPair(gqGroup, numElements, random);
		}

		@Test
		@DisplayName("a valid key pair does not throw")
		void saveValidKeyPair() {
			assertDoesNotThrow(() -> setupKeyPairServiceTempDir.save(ELECTION_EVENT_ID, setupKeyPair));
		}

		@Test
		@DisplayName("with invalid election event id throws FailedValidationException")
		void saveInvalidElectionEventId() {
			assertThrows(FailedValidationException.class, () -> setupKeyPairServiceTempDir.save(INVALID_ELECTION_EVENT_ID, setupKeyPair));
		}

		@Test
		@DisplayName("with null setup key pair throws NullPointerException")
		void saveNullSetupKeyPair() {
			assertThrows(NullPointerException.class, () -> setupKeyPairServiceTempDir.save(ELECTION_EVENT_ID, null));
		}

	}

	@Nested
	@DisplayName("loading")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class LoadTest {

		@Test
		@DisplayName("existing key pair returns it")
		void loadExistingKeyPair() {
			assertNotNull(setupKeyPairService.load(ELECTION_EVENT_ID));
		}

		@Test
		@DisplayName("invalid election event id")
		void loadInvalidElectionEventId() {
			assertThrows(FailedValidationException.class, () -> setupKeyPairService.load(INVALID_ELECTION_EVENT_ID));
		}

		@Test
		@DisplayName("non existing key pair throws IllegalArgumentException")
		void loadNonExistingKeyPair() {
			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> setupKeyPairService.load(WRONG_ELECTION_EVENT_ID));

			final String errorMessage = String.format("Setup key pair not found. [electionEventId: %s]", WRONG_ELECTION_EVENT_ID);
			assertEquals(errorMessage, Throwables.getRootCause(exception).getMessage());
		}

	}

}