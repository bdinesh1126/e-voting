/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setuptally;

import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_WRITE_IN_OPTIONS;
import static ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair.genKeyPair;
import static ch.post.it.evoting.cryptoprimitives.math.GroupVector.toGroupVector;
import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.integerToString;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProofFactory;

/**
 * Tests of SetupTallyEBAlgorithm.
 */
@DisplayName("A SetupTallyEBAlgorithm calling setupTallyEB with")
class SetupTallyEBAlgorithmTest {

	private static final int MU = MAXIMUM_NUMBER_OF_WRITE_IN_OPTIONS + 1;
	private static final int MAX_WRITE_INS_IN_ALL_VERIFICATION_CARD_SETS = 1;
	private static final int DELTA = MAX_WRITE_INS_IN_ALL_VERIFICATION_CARD_SETS + 1;

	private static final String ELECTION_EVENT_ID = "b5f8fddd30234a12bb3b544af46d4fc4";
	private static final String SETUP_TALLY_CCM = "SetupTallyCCM";
	private static SetupTallyEBAlgorithm setupTallyEBAlgorithm;
	private static GqGroup gqGroup;
	private static GqGroupGenerator gqGroupGenerator;
	private static GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys;
	private static GroupVector<GroupVector<SchnorrProof, ZqGroup>, ZqGroup> schnorrProofs;
	private static ZeroKnowledgeProof zeroKnowledgeProof;
	private static Random random;
	private List<char[]> passwords;

	@BeforeAll
	static void setUpAll() {
		random = RandomFactory.createRandom();
		gqGroup = GroupTestData.getLargeGqGroup();
		zeroKnowledgeProof = ZeroKnowledgeProofFactory.createZeroKnowledgeProof();
		final ElGamal elGamal = ElGamalFactory.createElGamal();
		setupTallyEBAlgorithm = new SetupTallyEBAlgorithm(HashFactory.createHash(), elGamal, zeroKnowledgeProof);
		gqGroupGenerator = new GqGroupGenerator(gqGroup);

		final ElGamalMultiRecipientKeyPair keypair1 = genKeyPair(gqGroup, MU, random);
		final ElGamalMultiRecipientPublicKey ccmElectionPublicKey1 = keypair1.getPublicKey();

		final ElGamalMultiRecipientKeyPair keypair2 = genKeyPair(gqGroup, MU, random);
		final ElGamalMultiRecipientPublicKey ccmElectionPublicKey2 = keypair2.getPublicKey();

		final ElGamalMultiRecipientKeyPair keypair3 = genKeyPair(gqGroup, MU, random);
		final ElGamalMultiRecipientPublicKey ccmElectionPublicKey3 = keypair3.getPublicKey();

		final ElGamalMultiRecipientKeyPair keypair4 = genKeyPair(gqGroup, MU, random);
		final ElGamalMultiRecipientPublicKey ccmElectionPublicKey4 = keypair4.getPublicKey();

		final GroupVector<SchnorrProof, ZqGroup> schnorrProofs1 = generateSchnorrProofs(ELECTION_EVENT_ID, 1, keypair1);
		final GroupVector<SchnorrProof, ZqGroup> schnorrProofs2 = generateSchnorrProofs(ELECTION_EVENT_ID, 2, keypair2);
		final GroupVector<SchnorrProof, ZqGroup> schnorrProofs3 = generateSchnorrProofs(ELECTION_EVENT_ID, 3, keypair3);
		final GroupVector<SchnorrProof, ZqGroup> schnorrProofs4 = generateSchnorrProofs(ELECTION_EVENT_ID, 4, keypair4);

		ccmElectionPublicKeys = GroupVector.of(ccmElectionPublicKey1, ccmElectionPublicKey2, ccmElectionPublicKey3, ccmElectionPublicKey4);

		schnorrProofs = GroupVector.of(schnorrProofs1, schnorrProofs2, schnorrProofs3, schnorrProofs4);
	}

	@BeforeEach
	void setup() {
		passwords = List.of("Password_ElectoralBoard1".toCharArray(), "Password_ElectoralBoard2".toCharArray());
	}

	@Test
	@DisplayName("a valid SetupTallyEBInput does not throw any Exception.")
	void validParamDoesNotThrow() {

		final SetupTallyEBContext context = new SetupTallyEBContext(gqGroup, ELECTION_EVENT_ID);
		final SetupTallyEBInput input = new SetupTallyEBInput(ccmElectionPublicKeys, schnorrProofs, MAX_WRITE_INS_IN_ALL_VERIFICATION_CARD_SETS,
				passwords);

		assertDoesNotThrow(() -> setupTallyEBAlgorithm.setupTallyEB(context, input));
	}

	@Test
	@DisplayName("an invalid SetupTallyEBInput throws IllegalStateException")
	void invalidParamThrowIllegalStateException() {

		final GroupVector<SchnorrProof, ZqGroup> invalidSchnorrProof = generateSchnorrProofs("8bh8257gc32142bb8ee0ed1bb70f362g", 55,
				genKeyPair(gqGroup, MU, random));

		final GroupVector<GroupVector<SchnorrProof, ZqGroup>, ZqGroup> invalidSchnorrProofs = GroupVector.of(schnorrProofs.get(0),
				schnorrProofs.get(1),
				schnorrProofs.get(2), invalidSchnorrProof);

		final SetupTallyEBContext context = new SetupTallyEBContext(gqGroup, ELECTION_EVENT_ID);
		final SetupTallyEBInput input =
				new SetupTallyEBInput(ccmElectionPublicKeys, invalidSchnorrProofs,
						MAX_WRITE_INS_IN_ALL_VERIFICATION_CARD_SETS, passwords);

		final IllegalStateException illegalStateException = assertThrows(IllegalStateException.class,
				() -> setupTallyEBAlgorithm.setupTallyEB(context, input));

		Assertions.assertEquals(illegalStateException.getMessage(), String.format(
				"Schnorr proofs verification failed. [electionEventId: %s, verifSch_1: %s, verifSch_2: %s, verifSch_3: %s, verifSch_4: %s]",
				ELECTION_EVENT_ID, true, true, true, false));
	}

	@Test
	@DisplayName("a valid SetupTallyEBInput returns a non-null SetupTallyEBOutput with expected content.")
	void nonNullOutput() {
		final SetupTallyEBContext context = new SetupTallyEBContext(gqGroup, ELECTION_EVENT_ID);
		final SetupTallyEBInput input = new SetupTallyEBInput(ccmElectionPublicKeys, schnorrProofs, MAX_WRITE_INS_IN_ALL_VERIFICATION_CARD_SETS,
				passwords);

		final SetupTallyEBOutput output = assertDoesNotThrow(() -> setupTallyEBAlgorithm.setupTallyEB(context, input));
		final ElGamalMultiRecipientPublicKey electionPublicKey = output.getElectionPublicKey();
		final ElGamalMultiRecipientPublicKey electoralBoardPublicKey = output.getElectoralBoardPublicKey();

		assertNotNull(output);
		assertNotNull(electoralBoardPublicKey);
		assertEquals(DELTA, electionPublicKey.size());
		assertEquals(DELTA, electoralBoardPublicKey.size());
	}

	@Test
	@DisplayName("non-matching groups throws an IllegalArgumentException.")
	void crossGroupChecksFailures() {
		final SetupTallyEBContext context = new SetupTallyEBContext(GroupTestData.getGqGroup(), ELECTION_EVENT_ID);

		final SetupTallyEBInput input = new SetupTallyEBInput(ccmElectionPublicKeys, schnorrProofs, MAX_WRITE_INS_IN_ALL_VERIFICATION_CARD_SETS,
				passwords);

		final IllegalArgumentException illegalArgumentException =
				assertThrows(IllegalArgumentException.class, () -> setupTallyEBAlgorithm.setupTallyEB(context, input));

		final String errorMessage = "The group of the CCM election public keys must be equal to the context group.";
		assertEquals(errorMessage, Throwables.getRootCause(illegalArgumentException).getMessage());
	}

	@Test
	@DisplayName("a null parameter throws a NullPointerException.")
	void nullParamThrowsANullPointer() {
		final SetupTallyEBContext context = new SetupTallyEBContext(gqGroup, ELECTION_EVENT_ID);
		final SetupTallyEBInput input = new SetupTallyEBInput(ccmElectionPublicKeys, schnorrProofs, MAX_WRITE_INS_IN_ALL_VERIFICATION_CARD_SETS,
				passwords);
		assertAll(
				() -> assertThrows(NullPointerException.class, () -> setupTallyEBAlgorithm.setupTallyEB(context, null)),
				() -> assertThrows(NullPointerException.class, () -> setupTallyEBAlgorithm.setupTallyEB(null, input))
		);
	}

	private static GroupVector<SchnorrProof, ZqGroup> generateSchnorrProofs(final String electionEventId, final int nodeId,
			final ElGamalMultiRecipientKeyPair elGamalMultiRecipientKeyPair) {

		// Operation.
		final ElGamalMultiRecipientPrivateKey EL_sk_j = elGamalMultiRecipientKeyPair.getPrivateKey();
		final ElGamalMultiRecipientPublicKey EL_pk_j = elGamalMultiRecipientKeyPair.getPublicKey();
		final List<String> i_aux = Arrays.asList(electionEventId, SETUP_TALLY_CCM, integerToString(nodeId));

		return IntStream.range(0, MU).mapToObj(i -> {
			final ZqElement EL_sk_j_i = EL_sk_j.get(i);
			final GqElement EL_pk_j_i = EL_pk_j.get(i);
			return zeroKnowledgeProof.genSchnorrProof(EL_sk_j_i, EL_pk_j_i, i_aux);
		}).collect(toGroupVector());
	}

	@Nested
	@DisplayName("a SetupTallyEBInput built with")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SetupTallyEBInputTest {

		@Test
		@DisplayName("null election public keys throws a NullPointerException.")
		void nullEPKThrowsANullPointer() {
			assertThrows(NullPointerException.class,
					() -> new SetupTallyEBInput(null, schnorrProofs, MAX_WRITE_INS_IN_ALL_VERIFICATION_CARD_SETS, passwords));
		}

		@Test
		@DisplayName("election public keys of size different than μ throws an IllegalArgumentException.")
		void nonValidSizeEPKThrowsAnIllegalArgument() {

			final GqGroup otherGqGroup = GroupTestData.getDifferentGqGroup(gqGroup);
			final GqGroupGenerator otherGqGroupGenerator = new GqGroupGenerator(otherGqGroup);
			final int otherSize = MU + 1;

			final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeysOtherSize = GroupVector.of(
					new ElGamalMultiRecipientPublicKey(otherGqGroupGenerator.genRandomGqElementVector(otherSize)),
					new ElGamalMultiRecipientPublicKey(otherGqGroupGenerator.genRandomGqElementVector(otherSize)),
					new ElGamalMultiRecipientPublicKey(otherGqGroupGenerator.genRandomGqElementVector(otherSize)));

			assertThrows(IllegalArgumentException.class,
					() -> new SetupTallyEBInput(ccmElectionPublicKeysOtherSize, schnorrProofs, MAX_WRITE_INS_IN_ALL_VERIFICATION_CARD_SETS,
							passwords));
		}

		@Test
		@DisplayName("a number election public keys different than 4 throws an IllegalArgumentException.")
		void nonValidNumberOfEPKThrowsAnIllegalArgument() {

			final ElGamalMultiRecipientPublicKey ccmElectionPublicKey1 =
					new ElGamalMultiRecipientPublicKey(gqGroupGenerator.genRandomGqElementVector(MU));
			final ElGamalMultiRecipientPublicKey ccmElectionPublicKey2 =
					new ElGamalMultiRecipientPublicKey(gqGroupGenerator.genRandomGqElementVector(MU));
			final ElGamalMultiRecipientPublicKey ccmElectionPublicKey3 =
					new ElGamalMultiRecipientPublicKey(gqGroupGenerator.genRandomGqElementVector(MU));
			final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> smallCcmElectionPublicKeys =
					GroupVector.of(ccmElectionPublicKey1, ccmElectionPublicKey2, ccmElectionPublicKey3);

			assertThrows(IllegalArgumentException.class,
					() -> new SetupTallyEBInput(smallCcmElectionPublicKeys, schnorrProofs, MAX_WRITE_INS_IN_ALL_VERIFICATION_CARD_SETS, passwords));

			final ElGamalMultiRecipientPublicKey ccmElectionPublicKey4 =
					new ElGamalMultiRecipientPublicKey(gqGroupGenerator.genRandomGqElementVector(MU));
			final ElGamalMultiRecipientPublicKey ccmElectionPublicKey5 =
					new ElGamalMultiRecipientPublicKey(gqGroupGenerator.genRandomGqElementVector(MU));

			final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> bigCcmElectionPublicKeys =
					GroupVector.of(ccmElectionPublicKey1, ccmElectionPublicKey2, ccmElectionPublicKey3, ccmElectionPublicKey4, ccmElectionPublicKey5);

			assertThrows(IllegalArgumentException.class,
					() -> new SetupTallyEBInput(bigCcmElectionPublicKeys, schnorrProofs, MAX_WRITE_INS_IN_ALL_VERIFICATION_CARD_SETS, passwords));
		}

		@Test
		@DisplayName("a negative maximum number of write-ins in all verification card sets throws an IllegalArgumentException.")
		void nonValidMaxThrowsAnIllegalArgument() {

			final int nonValidMaxWriteInsInAllVerificationCardSets = -1;

			assertThrows(IllegalArgumentException.class,
					() -> new SetupTallyEBInput(ccmElectionPublicKeys, schnorrProofs, nonValidMaxWriteInsInAllVerificationCardSets, passwords));
		}

		@Test
		@DisplayName("a null list of passwords of k electoral board members throws a NullPointerException.")
		void nullPasswordsThrowsANullPointer() {
			assertThrows(NullPointerException.class,
					() -> new SetupTallyEBInput(ccmElectionPublicKeys, schnorrProofs, MAX_WRITE_INS_IN_ALL_VERIFICATION_CARD_SETS, null));
		}

		@Test
		@DisplayName("less than 2 electoral board members throws an IllegalArgumentException.")
		void nonValidNumberOfElectoralBoardMembersThrowsAnIllegalArgument() {
			final List<char[]> nonValidK = Stream.of("Password_ElectoralBoard1".toCharArray()).map(char[].class::cast).toList();

			assertThrows(IllegalArgumentException.class,
					() -> new SetupTallyEBInput(ccmElectionPublicKeys, schnorrProofs, MAX_WRITE_INS_IN_ALL_VERIFICATION_CARD_SETS, nonValidK));
		}
	}

	@Nested
	@DisplayName("a SetupTallyEBContext built with")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SetupTallyEBContextTest {

		@Test
		@DisplayName("a null parameter throws a NullPointerException.")
		void nullParameterThrowsANullPointer() {
			assertAll(
					() -> assertThrows(NullPointerException.class, () -> new SetupTallyEBContext(null, ELECTION_EVENT_ID)),
					() -> assertThrows(NullPointerException.class, () -> new SetupTallyEBContext(gqGroup, null))
			);
		}

		@Test
		@DisplayName("a non-valid UUID election event id throws a FailedValidationException.")
		void nonValidUUIDThrows() {
			assertThrows(FailedValidationException.class, () -> new SetupTallyEBContext(gqGroup, "electionEventId"));
		}
	}
}
