/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.commons;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.UncheckedIOException;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

class RemoteServiceTest {

	Call<?> mockCall = Mockito.mock(Call.class);

	@Test
	void callCanNotBeNull() {
		assertThrows(NullPointerException.class, () -> RemoteService.call(null));
	}

	@Test
	void networkErrorMessageCanNotBeNull() {
		final RemoteService<?> remoteCall = RemoteService.call(mockCall);
		assertThrows(NullPointerException.class, () -> remoteCall.networkErrorMessage(null));
	}

	@Test
	void networkErrorMessageIsRequired() {
		final RemoteService<?> remoteCall = RemoteService.call(mockCall).doNotThrowOnUnsuccessfulResponse();
		assertThrows(IllegalArgumentException.class, remoteCall::execute);
	}

	@Test
	void unsuccessfulResponseErrorMessageCanNotBeNull() {
		final RemoteService<?> remoteCall = RemoteService.call(mockCall);
		assertThrows(NullPointerException.class, () -> remoteCall.unsuccessfulResponseErrorMessage(null));
	}

	@Test
	void unsuccessfulResponseErrorCanBeAbsentIfDisabled() {
		final RemoteService<?> remoteCall = RemoteService.call(mockCall).networkErrorMessage("Network error").doNotThrowOnUnsuccessfulResponse();
		assertDoesNotThrow(remoteCall::execute);
	}
	@Test
	void executeThrowsOnNetworkError() throws IOException {
		when(mockCall.execute()).thenThrow(new IOException("Network error"));
		final RemoteService<?> remoteCall = RemoteService.call(mockCall).networkErrorMessage("Network error").doNotThrowOnUnsuccessfulResponse();
		assertThrows(UncheckedIOException.class, remoteCall::execute);
	}

	@Test
	void executeThrowsOnUnsuccessfulResponse() throws IOException {
		when(mockCall.execute()).thenReturn(Response.error(500, ResponseBody.create("", MediaType.parse("application/json"))));
		final RemoteService<?> remoteCall = RemoteService.call(mockCall).networkErrorMessage("Network error")
				.unsuccessfulResponseErrorMessage("Unsuccessful reponse");
		assertThrows(IllegalStateException.class, remoteCall::execute);
	}

	@Test
	void executeDoesNotThrowOnUnsuccessfulResponseWhenDisabled() throws IOException {
		when(mockCall.execute()).thenReturn(Response.error(500, ResponseBody.create("", MediaType.parse("application/json"))));
		final RemoteService<?> remoteCall = RemoteService.call(mockCall).networkErrorMessage("Network Error").doNotThrowOnUnsuccessfulResponse();
		assertDoesNotThrow(remoteCall::execute);
	}

	@Test
	void canNotSetUnsuccessfulErrorMessageAndDisable() {
		final RemoteService<?> remoteCall = RemoteService.call(mockCall)
				.networkErrorMessage("Network error")
				.doNotThrowOnUnsuccessfulResponse();
		assertThrows(IllegalArgumentException.class, () -> remoteCall.unsuccessfulResponseErrorMessage("Unsuccessful response"));
	}

}