/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api.online;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;

import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.application.service.EncryptedLongReturnCodeSharesService;
import ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetComputationService;
import ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetDownloadService;

@ExtendWith(MockitoExtension.class)
@ContextConfiguration
class VotingCardSetOnlineControllerTest {

	private final String VALID_ELECTION_EVENT_ID = "17ccbe962cf341bc93208c26e911090c";
	private final String VALID_VOTING_CARD_SET_ID = "17ccbe962cf341bc93208c26e911090c";

	@Mock
	private VotingCardSetDownloadService votingCardSetDownloadService;

	@Mock
	private VotingCardSetComputationService votingCardSetComputationService;

	@Mock
	private EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService;

	private VotingCardSetOnlineController votingCardSetOnlineController;

	@BeforeEach
	void setUp() {
		votingCardSetOnlineController = new VotingCardSetOnlineController(votingCardSetDownloadService, votingCardSetComputationService,
				encryptedLongReturnCodeSharesService, true);
	}

	@Test
	void testCorrectStatusTransition() throws ResourceNotFoundException, IOException {

		final HttpStatus statusCodeComputing = votingCardSetOnlineController.setVotingCardSetStatusComputing(VALID_ELECTION_EVENT_ID,
				VALID_VOTING_CARD_SET_ID).getStatusCode();
		assertEquals(HttpStatus.NO_CONTENT, statusCodeComputing);

		final HttpStatus statusCodeDownloaded = votingCardSetOnlineController.setVotingCardSetStatusDownloaded(VALID_ELECTION_EVENT_ID,
				VALID_VOTING_CARD_SET_ID).getStatusCode();
		assertEquals(HttpStatus.NO_CONTENT, statusCodeDownloaded);
	}
}
