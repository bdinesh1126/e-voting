/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.task;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;

class ExecutionTaskTest {

	private static final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
	private static ExecutionTask executionTask;
	private static ObjectNode rootNode;

	@BeforeAll
	static void setupAll() throws JsonProcessingException {

		// Create executionTask.
		final ExecutionTaskType type = ExecutionTaskType.RETURN_CODES_PAYLOADS;
		final String businessId = "businessId";
		final String description = "description";
		final ExecutionTaskStatus status = ExecutionTaskStatus.STARTED;

		executionTask = new ExecutionTask.Builder()
				.setType(type)
				.setBusinessId(businessId)
				.setDescription(description)
				.setStatus(status)
				.build();

		// Create expected Json.
		rootNode = mapper.createObjectNode();
		rootNode.set("id", mapper.readTree(mapper.writeValueAsString(String.format("%s-%s", type, businessId))));
		rootNode.set("description", mapper.readTree(mapper.writeValueAsString(description)));
		rootNode.set("status", mapper.readTree(mapper.writeValueAsString(status)));
	}

	@Test
	@DisplayName("serialized gives expected json")
	void serializeObject() throws JsonProcessingException {
		final String serializedPayload = mapper.writeValueAsString(executionTask);
		assertEquals(rootNode.toString(), serializedPayload);
	}

	@Test
	@DisplayName("deserialized gives expected object")
	void deserializeObject() throws IOException {
		final ExecutionTask deserializedObject = mapper.readValue(rootNode.toString(), ExecutionTask.class);
		assertEquals(executionTask, deserializedObject);
	}

	@Test
	@DisplayName("serialized then deserialized gives original object")
	void cycle() throws IOException {
		final ExecutionTask deserializedObject = mapper.readValue(mapper.writeValueAsString(executionTask), ExecutionTask.class);

		assertEquals(executionTask, deserializedObject);
	}
}
