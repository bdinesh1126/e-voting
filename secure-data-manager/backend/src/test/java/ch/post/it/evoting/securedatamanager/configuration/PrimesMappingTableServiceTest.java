/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.file.Path;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;

@DisplayName("A PrimesMappingTableService")
class PrimesMappingTableServiceTest {

	private static final Random random = RandomFactory.createRandom();

	private static PrimesMappingTableService primesMappingTableService;
	private static PrimesMappingTableFileRepository primesMappingTableFileRepository;

	@BeforeAll
	static void setUpAll() {
		primesMappingTableFileRepository = mock(PrimesMappingTableFileRepository.class);
		when(primesMappingTableFileRepository.save(any(PrimesMappingTableFileEntity.class))).thenReturn(Path.of(""));

		primesMappingTableService = new PrimesMappingTableService(primesMappingTableFileRepository);
	}

	@Nested
	@DisplayName("saving")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SaveTest {

		private String electionEventId;
		private String verificationCardSetId;
		private PrimesMappingTable primesMappingTable;

		@BeforeEach
		void setUp() {
			reset(primesMappingTableFileRepository);

			final GqGroup encryptionGroup = GroupTestData.getGqGroup();
			electionEventId = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
			verificationCardSetId = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);

			final GroupVector<PrimeGqElement, GqGroup> smallPrimeGroupMembers = PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(
					encryptionGroup, 1);
			primesMappingTable = PrimesMappingTable.from(
					List.of(new PrimesMappingTableEntry("actualVotingOption", smallPrimeGroupMembers.get(0), "semantic")));
		}

		@Test
		@DisplayName("primes mapping table saves with repository")
		void save() {
			assertDoesNotThrow(() -> primesMappingTableService.save(electionEventId, verificationCardSetId, primesMappingTable));

			verify(primesMappingTableFileRepository).save(any(PrimesMappingTableFileEntity.class));
		}

		@Test
		@DisplayName("invalid ids throws FailedValidationException")
		void invalidIds() {
			assertThrows(FailedValidationException.class, () -> primesMappingTableService.save(electionEventId, "1234", primesMappingTable));
			assertThrows(FailedValidationException.class, () -> primesMappingTableService.save("1234", verificationCardSetId, primesMappingTable));

			verify(primesMappingTableFileRepository, times(0)).save(any(PrimesMappingTableFileEntity.class));
		}

		@Test
		@DisplayName("null primes mapping table throws NullPointerException")
		void nullPrimesMappingTable() {
			assertThrows(NullPointerException.class, () -> primesMappingTableService.save(electionEventId, verificationCardSetId, null));

			verify(primesMappingTableFileRepository, times(0)).save(any(PrimesMappingTableFileEntity.class));
		}

		@Nested
		@DisplayName("loading")
		@TestInstance(TestInstance.Lifecycle.PER_CLASS)
		class LoadTest {

			private String electionEventId;
			private String verificationCardSetId;
			private PrimesMappingTableFileEntity primesMappingTableFileEntity;

			@BeforeEach
			void setUp() {
				reset(primesMappingTableFileRepository);

				electionEventId = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);
				verificationCardSetId = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);

				final GqGroup encryptionGroup = GroupTestData.getGqGroup();
				final GroupVector<PrimeGqElement, GqGroup> smallPrimeGroupMembers = PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(
						encryptionGroup, 1);
				final PrimesMappingTable primesMappingTable = PrimesMappingTable.from(
						List.of(new PrimesMappingTableEntry("actualVotingOption", smallPrimeGroupMembers.get(0), "semantic")));

				primesMappingTableFileEntity = new PrimesMappingTableFileEntity(encryptionGroup, electionEventId, verificationCardSetId,
						primesMappingTable);

				when(primesMappingTableFileRepository.findByVerificationCardSetId(electionEventId, verificationCardSetId)).thenReturn(
						Optional.of(primesMappingTableFileEntity));
			}

			@Test
			@DisplayName("existing primes mapping table returns it")
			void load() {
				final PrimesMappingTable primesMappingTable = primesMappingTableService.load(electionEventId, verificationCardSetId);

				assertEquals(primesMappingTable, primesMappingTableFileEntity.primesMappingTable());

				verify(primesMappingTableFileRepository).findByVerificationCardSetId(electionEventId, verificationCardSetId);
			}

			@Test
			@DisplayName("invalid ids throws FailedValidationException")
			void invalidIds() {
				assertThrows(FailedValidationException.class, () -> primesMappingTableService.load(electionEventId, "1234"));
				assertThrows(FailedValidationException.class, () -> primesMappingTableService.load("1234", verificationCardSetId));

				verify(primesMappingTableFileRepository, times(0)).findByVerificationCardSetId(any(), any());
			}

			@Test
			@DisplayName("non existing primes mapping table throws IllegalStateException")
			void nonExistingPrimesMappingTable() {
				final String otherElectionEventId = random.genRandomBase16String(32).toLowerCase(Locale.ENGLISH);

				final IllegalStateException exception = assertThrows(IllegalStateException.class,
						() -> primesMappingTableService.load(otherElectionEventId, verificationCardSetId));

				final String errorMessage = String.format("Primes mapping table not found. [electionEventId: %s, verificationCardSetId: %s]",
						otherElectionEventId, verificationCardSetId);
				assertEquals(errorMessage, Throwables.getRootCause(exception).getMessage());
			}

		}

	}

}