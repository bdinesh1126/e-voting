/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.IntStream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Factory;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Profile;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.common.Alphabet;
import ch.post.it.evoting.evotinglibraries.domain.common.StartVotingKeyAlphabet;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.sdmsetup.configuration.setupvoting.GenRandomSVKAlgorithm;

@DisplayName("GetVoterAuthenticationDataAlgorithm")
class GetVoterAuthenticationDataAlgorithmTest {

	private final Random random = RandomFactory.createRandom();
	private final int numberOfEligibleVoters = random.genRandomInteger(BigInteger.valueOf(2L)).intValue() + 1;
	private final GenRandomSVKAlgorithm genRandomSVKAlgorithm = new GenRandomSVKAlgorithm(random);
	private final List<String> startVotingKeys = IntStream.range(0, numberOfEligibleVoters)
			.mapToObj(ignored -> genRandomSVKAlgorithm.genRandomSVK(Constants.SVK_LENGTH, StartVotingKeyAlphabet.getInstance()))
			.toList();
	private final List<String> extendedAuthenticationFactors = IntStream.range(0, numberOfEligibleVoters)
			.mapToObj(i -> "1944")
			.toList();
	private final GetVoterAuthenticationDataInput input = new GetVoterAuthenticationDataInput(startVotingKeys, extendedAuthenticationFactors);
	private final String electionEventId = random.genRandomBase16String(Constants.BASE16_ID_LENGTH);
	private final Hash hash = HashFactory.createHash();
	private final Argon2 argon2 = Argon2Factory.createArgon2(Argon2Profile.TEST.getContext());
	private final DeriveCredentialIdAlgorithm deriveCredentialIdAlgorithm = new DeriveCredentialIdAlgorithm(hash, BaseEncodingFactory.createBase16(),
			argon2);
	private final DeriveBaseAuthenticationChallengeAlgorithm deriveBaseAuthenticationChallengeAlgorithm = new DeriveBaseAuthenticationChallengeAlgorithm(
			hash, argon2, BaseEncodingFactory.createBase64());
	private final GetVoterAuthenticationDataAlgorithm getVoterAuthenticationDataAlgorithm = new GetVoterAuthenticationDataAlgorithm(
			deriveCredentialIdAlgorithm, deriveBaseAuthenticationChallengeAlgorithm);

	@Test
	@DisplayName("calling getVoterAuthenticationData with null parameters throws a NullPointerException.")
	void getVoterAuthenticationDataWithNullParametersThrows() {
		assertThrows(NullPointerException.class, () -> getVoterAuthenticationDataAlgorithm.getVoterAuthenticationData(null, input));
		assertThrows(NullPointerException.class, () -> getVoterAuthenticationDataAlgorithm.getVoterAuthenticationData(electionEventId, null));
	}

	@Test
	@DisplayName("calling getVoterAuthenticationData with invalid election event id throws a FailedValidationException.")
	void getVoterAuthenticationDataWithInvalidParametersThrows() {
		assertThrows(FailedValidationException.class,
				() -> getVoterAuthenticationDataAlgorithm.getVoterAuthenticationData("not UUID", input));
	}

	@Test
	@DisplayName("calling getVoterAuthenticationData with correct parameters does not throw.")
	void getVoterAuthenticationData() {
		assertDoesNotThrow(() -> getVoterAuthenticationDataAlgorithm.getVoterAuthenticationData(electionEventId, input));
	}

	@Nested
	@DisplayName("a GetVoterAuthenticationDataInput built with")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class GetVoterAuthenticationDataInputTest {

		@Test
		@DisplayName("null parameters throws a NullPointerException.")
		void nullParametersThrowsANullPointer() {
			assertThrows(NullPointerException.class, () -> new GetVoterAuthenticationDataInput(null, extendedAuthenticationFactors));
			assertThrows(NullPointerException.class, () -> new GetVoterAuthenticationDataInput(startVotingKeys, null));
		}

		@Test
		@DisplayName("empty start voting keys throws an IllegalArgumentException.")
		void emptyStartVotingKeysThrowsIllegalArgument() {
			final List<String> emptyStartVotingKeys = List.of();

			assertThrows(IllegalArgumentException.class,
					() -> new GetVoterAuthenticationDataInput(emptyStartVotingKeys, extendedAuthenticationFactors));
		}

		@Test
		@DisplayName("empty extended authentication factors throws an IllegalArgumentException.")
		void emptyExtendedAuthenticationFactorsThrowsIllegalArgument() {
			final List<String> emptyExtendedAuthenticationFactors = List.of();

			assertThrows(IllegalArgumentException.class,
					() -> new GetVoterAuthenticationDataInput(startVotingKeys, emptyExtendedAuthenticationFactors));
		}

		@Test
		@DisplayName("invalid start voting keys throws a FailedValidationException.")
		void invalidStartVotingKeysThrowsFailedValidation() {
			final List<String> invalidStartVotingKeys = startVotingKeys.stream().map(ignored -> "not base 32").toList();

			assertThrows(FailedValidationException.class,
					() -> new GetVoterAuthenticationDataInput(invalidStartVotingKeys, extendedAuthenticationFactors));
		}

		@Test
		@DisplayName("invalid extended authentication factors throws an IllegalArgumentException.")
		void invalidExtendedAuthenticationFactorsThrowsIllegalArgument() {
			final List<String> notDigitExtendedAuthenticationFactors = extendedAuthenticationFactors.stream().map(ignored -> "notdigit").toList();
			final List<String> wrongSizeExtendedAuthenticationFactors = extendedAuthenticationFactors.stream().map(ignored -> "123").toList();

			assertThrows(IllegalArgumentException.class,
					() -> new GetVoterAuthenticationDataInput(startVotingKeys, notDigitExtendedAuthenticationFactors));
			assertThrows(IllegalArgumentException.class,
					() -> new GetVoterAuthenticationDataInput(startVotingKeys, wrongSizeExtendedAuthenticationFactors));
		}

		@Test
		@DisplayName("start voting keys and extended authentication factors of different size throws an IllegalArgumentException.")
		void differentSizeStartVotingKeysThrowsIllegalArgument() {
			final List<String> differentSizeStartVotingKeys = startVotingKeys.subList(1, startVotingKeys.size());

			assertThrows(IllegalArgumentException.class,
					() -> new GetVoterAuthenticationDataInput(differentSizeStartVotingKeys, extendedAuthenticationFactors));
		}
	}

	@Nested
	@DisplayName("a GetVoterAuthenticationDataOutput built with")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class GetVoterAuthenticationDataOutputTest {

		private final GetVoterAuthenticationDataOutput output = getVoterAuthenticationDataAlgorithm.getVoterAuthenticationData(electionEventId,
				input);
		private final List<String> derivedVoterIdentifiers = output.derivedVoterIdentifiers();
		private final List<String> baseAuthenticationChallenges = output.baseAuthenticationChallenges();

		@Test
		@DisplayName("null parameters throws a NullPointerException.")
		void nullParameterThrowsANullPointer() {
			assertAll(
					() -> assertThrows(NullPointerException.class, () -> new GetVoterAuthenticationDataOutput(null, baseAuthenticationChallenges)),
					() -> assertThrows(NullPointerException.class, () -> new GetVoterAuthenticationDataOutput(derivedVoterIdentifiers, null))
			);
		}

		@Test
		@DisplayName("empty derived voter identifiers throws an IllegalArgumentException.")
		void emptyDerivedVoterIdentifiersThrowsIllegalArgument() {
			final List<String> emptyDerivedVoterIdentifiers = List.of();

			assertThrows(IllegalArgumentException.class,
					() -> new GetVoterAuthenticationDataOutput(emptyDerivedVoterIdentifiers, baseAuthenticationChallenges));
		}

		@Test
		@DisplayName("empty base authentication challenges throws an IllegalArgumentException.")
		void emptyBaseAuthenticationChallengesThrowsIllegalArgument() {
			final List<String> emptyBaseAuthenticationChallenges = List.of();

			assertThrows(IllegalArgumentException.class,
					() -> new GetVoterAuthenticationDataOutput(derivedVoterIdentifiers, emptyBaseAuthenticationChallenges));
		}

		@Test
		@DisplayName("invalid derived voter identifiers throws a FailedValidationException.")
		void invalidDerivedVoterIdentifiersThrowsIllegalArgument() {
			final List<String> invalidDerivedVoterIdentifiers = derivedVoterIdentifiers.stream().map(ignored -> "not valid UUID").toList();

			assertThrows(FailedValidationException.class,
					() -> new GetVoterAuthenticationDataOutput(invalidDerivedVoterIdentifiers, baseAuthenticationChallenges));
		}

		@Test
		@DisplayName("invalid base authentication challenges throws an FailedValidationException.")
		void invalidBaseAuthenticationChallengesThrowsIllegalArgument() {
			final List<String> invalidBaseAuthenticationChallenges = baseAuthenticationChallenges.stream().map(ignored -> "not valid Base64")
					.toList();

			assertThrows(FailedValidationException.class,
					() -> new GetVoterAuthenticationDataOutput(derivedVoterIdentifiers, invalidBaseAuthenticationChallenges));
		}

		@Test
		@DisplayName("derived voter identifiers and base authentication challenges of different size throws an IllegalArgumentException.")
		void differentSizeBaseAuthenticationChallengesThrowsIllegalArgument() {
			final List<String> differentSizeBaseAuthenticationChallenges = baseAuthenticationChallenges.subList(1,
					baseAuthenticationChallenges.size());

			assertThrows(IllegalArgumentException.class,
					() -> new GetVoterAuthenticationDataOutput(derivedVoterIdentifiers, differentSizeBaseAuthenticationChallenges));
		}
	}
}
