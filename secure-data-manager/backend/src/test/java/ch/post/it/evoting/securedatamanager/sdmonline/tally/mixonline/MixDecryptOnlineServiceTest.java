/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.sdmonline.tally.mixonline;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.mixnet.VerifiableShuffle;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.DecryptionProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.VerifiableDecryptions;
import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.domain.tally.MixDecryptOnlinePayload;
import ch.post.it.evoting.domain.tally.VerifyMixDecHelper;
import ch.post.it.evoting.evotinglibraries.domain.tally.ControlComponentBallotBoxPayload;
import ch.post.it.evoting.securedatamanager.sdmcommon.tally.ControlComponentBallotBoxPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.sdmcommon.tally.ControlComponentShufflePayloadFileRepository;
import ch.post.it.evoting.securedatamanager.services.application.service.BallotBoxService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.MessageBrokerOrchestratorClient;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

@ExtendWith(MockitoExtension.class)
@DisplayName("Use MixDecryptOnlineService to ")
class MixDecryptOnlineServiceTest extends TestGroupSetup {

	private static final String VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE = "The voting-portal connection is not enabled.";
	private static final int N = 2;
	private static final SecureRandom RANDOM = new SecureRandom();

	private final String electionEventId = "426b2de832ac4cf384af0f68bb2b5d20";
	private final String ballotId = "0fca7e83f8cf41acb2a48b67b2e37a71";
	private final String ballotBoxId = "64ea41b3881e4bef81a2cddab7597ecb";
	private final int l = RANDOM.nextInt(5) + 1;

	private final BallotBoxService ballotBoxService = mock(BallotBoxService.class);
	private final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient = mock(MessageBrokerOrchestratorClient.class);
	private final ControlComponentShufflePayloadFileRepository controlComponentShufflePayloadFileRepository = mock(
			ControlComponentShufflePayloadFileRepository.class);
	private final ControlComponentBallotBoxPayloadFileRepository controlComponentBallotBoxPayloadFileRepository = mock(
			ControlComponentBallotBoxPayloadFileRepository.class);

	private final MixDecryptOnlineService sut = new MixDecryptOnlineService(ballotBoxService, messageBrokerOrchestratorClient,
			controlComponentShufflePayloadFileRepository, controlComponentBallotBoxPayloadFileRepository, true);

	@Nested
	@DisplayName("Test startOnlineMixing calls")
	class StartOnlineMixing {

		@Test
		@DisplayName("startOnlineMixing with null arguments throws a NullPointerException")
		void startOnlineMixingWithNullArgumentsThrows() {
			assertThrows(NullPointerException.class, () -> sut.startOnlineMixing(null, ballotBoxId));
			assertThrows(NullPointerException.class, () -> sut.startOnlineMixing(electionEventId, null));
		}

		@Test
		@DisplayName("startOnlineMixing with voting portal not enabled throws an IllegalStateException")
		void startOnlineMixingWithIsVotingPortalEnabledFalseThrows() {
			final MixDecryptOnlineService mixDecryptOnlineService = new MixDecryptOnlineService(ballotBoxService, messageBrokerOrchestratorClient,
					controlComponentShufflePayloadFileRepository, controlComponentBallotBoxPayloadFileRepository, false);
			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> mixDecryptOnlineService.startOnlineMixing(electionEventId, ballotBoxId));
			assertEquals(VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE, exception.getMessage());
		}

		@Test
		@DisplayName("startOnlineMixing response is unsuccessful")
		void startOnlineMixingTest_response_unsuccessful() throws IOException {
			when(ballotBoxService.getDateTo(ballotBoxId)).thenReturn(LocalDateTime.now().minusDays(1));

			final Call<Void> callMock = (Call<Void>) Mockito.mock(Call.class);
			when(callMock.execute()).thenReturn(Response.error(500, ResponseBody.create("", MediaType.parse("application/json"))));
			when(messageBrokerOrchestratorClient.createMixDecryptOnline(any(), any())).thenReturn(callMock);

			assertThrows(IllegalStateException.class, () -> sut.startOnlineMixing(electionEventId, ballotBoxId));

			verify(messageBrokerOrchestratorClient, times(1)).createMixDecryptOnline(electionEventId, ballotBoxId);
			verify(ballotBoxService, times(0)).updateBallotBoxStatus(ballotBoxId, BallotBoxStatus.MIXING);

		}

		@Test
		@DisplayName("happyPath")
		void startOnlineMixingTest_happyPath() throws IOException {
			when(ballotBoxService.getDateTo(ballotBoxId)).thenReturn(LocalDateTime.now().minusDays(1));

			final Call<Void> callMock = (Call<Void>) Mockito.mock(Call.class);
			when(callMock.execute()).thenReturn(Response.success(null));
			when(messageBrokerOrchestratorClient.createMixDecryptOnline(any(), any())).thenReturn(callMock);
			when(ballotBoxService.updateBallotBoxStatus(any(), any())).thenReturn(BallotBoxStatus.MIXING);

			sut.startOnlineMixing(electionEventId, ballotBoxId);

			verify(messageBrokerOrchestratorClient, times(1)).createMixDecryptOnline(electionEventId, ballotBoxId);
			verify(ballotBoxService, times(1)).updateBallotBoxStatus(ballotBoxId, BallotBoxStatus.MIXING);

		}
	}

	@Nested
	@DisplayName("Test startOnlineMixing calls")
	class GetOnlineStatus {

		@Test
		@DisplayName("getOnlineStatus with null arguments throws a NullPointerException")
		void getOnlineStatusWithNullArgumentsThrows() {
			assertThrows(NullPointerException.class, () -> sut.getOnlineStatus(null, ballotBoxId));
			assertThrows(NullPointerException.class, () -> sut.getOnlineStatus(electionEventId, null));
		}

		@Test
		@DisplayName("getOnlineStatus with voting portal not enabled throws an IllegalStateException")
		void getOnlineStatusWithIsVotingPortalEnabledFalseThrows() {
			final MixDecryptOnlineService mixDecryptOnlineService = new MixDecryptOnlineService(ballotBoxService, messageBrokerOrchestratorClient,
					controlComponentShufflePayloadFileRepository, controlComponentBallotBoxPayloadFileRepository, false);
			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> mixDecryptOnlineService.getOnlineStatus(electionEventId, ballotBoxId));
			assertEquals(VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE, exception.getMessage());
		}

		@Test
		@DisplayName("happy path")
		void getOnlineStatus_happyPath() throws IOException {

			final Call<BallotBoxStatus> callMock = (Call<BallotBoxStatus>) Mockito.mock(Call.class);
			when(callMock.execute()).thenReturn(Response.success(BallotBoxStatus.MIXING));
			when(messageBrokerOrchestratorClient.checkMixDecryptOnlineStatus(any(), any())).thenReturn(callMock);

			sut.getOnlineStatus(electionEventId, ballotBoxId);

			verify(messageBrokerOrchestratorClient, times(1)).checkMixDecryptOnlineStatus(electionEventId, ballotBoxId);
		}
	}

	@Nested
	@DisplayName("Test downloadOnlineMixnetPayloads calls")
	class DownloadOnlineMixnetPayloads {

		private final int N_NODE_IDS = 4;

		private List<ControlComponentBallotBoxPayload> controlComponentBallotBoxPayloads;
		private List<ControlComponentShufflePayload> controlComponentShufflePayloads;
		private MixDecryptOnlinePayload mixDecryptOnlinePayload;

		@BeforeEach
		void createData() {
			final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
			final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> ciphertexts = elGamalGenerator.genRandomCiphertextVector(N, l);
			final DecryptionProof decryptionProof = new DecryptionProof(zqGroupGenerator.genRandomZqElementMember(),
					zqGroupGenerator.genRandomZqElementVector(l));
			final VerifiableDecryptions verifiableDecryptions = new VerifiableDecryptions(ciphertexts,
					GroupVector.of(decryptionProof, decryptionProof));
			final VerifyMixDecHelper verifyMixDecOnlineHelper = new VerifyMixDecHelper(gqGroup);
			final VerifiableShuffle verifiableShuffle = new VerifiableShuffle(ciphertexts, verifyMixDecOnlineHelper.createShuffleArgument(N, l));

			controlComponentBallotBoxPayloads = new ArrayList<>();
			for (int nodeId = 1; nodeId <= N_NODE_IDS; nodeId++) {
				final ControlComponentBallotBoxPayload controlComponentBallotBoxPayload = new ControlComponentBallotBoxPayload(gqGroup,
						electionEventId,
						ballotBoxId, nodeId, List.of());
				controlComponentBallotBoxPayload.setSignature(
						new CryptoPrimitivesSignature("controlComponentBallotBoxPayloads".getBytes(StandardCharsets.UTF_8)));
				controlComponentBallotBoxPayloads.add(controlComponentBallotBoxPayload);
			}

			controlComponentShufflePayloads = new ArrayList<>();
			for (int nodeId = 1; nodeId <= N_NODE_IDS; nodeId++) {
				final ControlComponentShufflePayload mixnetShufflePayload = new ControlComponentShufflePayload(gqGroup, electionEventId, ballotBoxId,
						nodeId, verifiableDecryptions, verifiableShuffle);
				mixnetShufflePayload.setSignature(
						new CryptoPrimitivesSignature("controlComponentShufflePayload".getBytes(StandardCharsets.UTF_8)));
				controlComponentShufflePayloads.add(mixnetShufflePayload);
			}

			mixDecryptOnlinePayload = new MixDecryptOnlinePayload(electionEventId, ballotBoxId, controlComponentBallotBoxPayloads,
					controlComponentShufflePayloads);
		}

		@Test
		@DisplayName("downloadOnlineMixnetPayloads with null arguments throws a NullPointerException")
		void downloadOnlineMixnetPayloadsWithNullArgumentsThrows() {
			assertThrows(NullPointerException.class, () -> sut.downloadOnlineMixnetPayloads(null, ballotBoxId));
			assertThrows(NullPointerException.class, () -> sut.downloadOnlineMixnetPayloads(electionEventId, null));
		}

		@Test
		@DisplayName("downloadOnlineMixnetPayloads with voting portal not enabled throws an IllegalStateException")
		void downloadOnlineMixnetPayloadsWithIsVotingPortalEnabledFalseThrows() {
			final MixDecryptOnlineService mixDecryptOnlineService = new MixDecryptOnlineService(ballotBoxService, messageBrokerOrchestratorClient,
					controlComponentShufflePayloadFileRepository, controlComponentBallotBoxPayloadFileRepository, false);
			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> mixDecryptOnlineService.downloadOnlineMixnetPayloads(electionEventId, ballotBoxId));
			assertEquals(VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE, exception.getMessage());
		}

		@Test
		@DisplayName("ballotBoxService.hasStatus(ballotBoxId, BallotBoxStatus.MIXED) return false")
		void ballotBoxService_hasStatus_return_false() {
			when(ballotBoxService.hasStatus(any(), any())).thenReturn(false);

			final IllegalArgumentException uncheckedIOException =
					assertThrows(IllegalArgumentException.class, () -> sut.downloadOnlineMixnetPayloads(electionEventId, ballotBoxId));

			assertEquals("Ballot box is not mixed [ballotBoxId : 64ea41b3881e4bef81a2cddab7597ecb]", uncheckedIOException.getMessage());
		}

		@Test
		@DisplayName("messageBrokerOrchestratorClient.downloadMixDecryptOnline raises IOException")
		void messageBrokerOrchestratorClient_downloadMixDecryptOnline_raises_IOException() {
			doAnswer((invocation) -> {
				throw new IOException("invalid");
			}).when(messageBrokerOrchestratorClient).downloadMixDecryptOnline(any(), any());

			final IllegalArgumentException uncheckedIOException =
					assertThrows(IllegalArgumentException.class, () -> sut.downloadOnlineMixnetPayloads(electionEventId, ballotBoxId));

			assertEquals("Ballot box is not mixed [ballotBoxId : 64ea41b3881e4bef81a2cddab7597ecb]", uncheckedIOException.getMessage());
		}

		@Test
		@DisplayName("happy path")
		void downloadOnlineMixnetPayloadsTest() throws IOException {

			when(ballotBoxService.hasStatus(any(), any())).thenReturn(true);
			when(ballotBoxService.getBallotId(any())).thenReturn(ballotId);
			when(ballotBoxService.updateBallotBoxStatus(any(), any())).thenReturn(BallotBoxStatus.DOWNLOADED);

			final Call<MixDecryptOnlinePayload> callMock = (Call<MixDecryptOnlinePayload>) Mockito.mock(Call.class);
			when(callMock.execute()).thenReturn(Response.success(mixDecryptOnlinePayload));
			when(messageBrokerOrchestratorClient.downloadMixDecryptOnline(any(), any())).thenReturn(callMock);

			when(controlComponentShufflePayloadFileRepository.savePayload(any(), any())).thenReturn(
					Path.of("/controlComponentShufflePayloadFile"));

			assertNotNull(sut);

			sut.downloadOnlineMixnetPayloads(electionEventId, ballotBoxId);

			verify(ballotBoxService).hasStatus(any(), any());
			verify(messageBrokerOrchestratorClient).downloadMixDecryptOnline(any(), any());
			verify(ballotBoxService).getBallotId(any());
			verify(controlComponentShufflePayloadFileRepository, times(N_NODE_IDS)).savePayload(any(), any());
			verify(ballotBoxService).updateBallotBoxStatus(any(), any());
		}

	}

}
