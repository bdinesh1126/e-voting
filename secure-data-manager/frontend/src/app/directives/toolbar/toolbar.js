/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */

import toolbarTemplate from './toolbar.html';

angular
    .module('ovToolbar', [])

    .directive('ovToolbar', function (
        $mdSidenav,
        $state,
        ToolbarTitle,
        sessionService,
    ) {
        'use strict';
        const electionEventListName = 'election-event-list';
        const administrationBoardListName = 'administration-board-list';
        return {
            restrict: 'E',
            transclude: true,
            template: toolbarTemplate,

            link: function (scope) {
                scope.getMainTitle = function () {
                    switch ($state.current.name) {
                        case electionEventListName:
                            return 'Election Events';
                        case administrationBoardListName:
                            return 'Administration Boards';
                        default:
                            return sessionService.getSelectedElectionEvent().defaultTitle;
                    }
                };

                scope.getElectionEventSection = function () {
                    switch ($state.current.name) {
                        case 'ballots':
                            return 'Ballots';
                        case 'voting-cards':
                            return'Voting Card Sets';
                        case 'electoral-board-list':
                            return 'Electoral Board';
                        case 'ballot-boxes':
                            return 'Ballot Boxes';
                        default:
                            return 'Ballots';
                    }
                };

                scope.enableElectionEventSection = function () {
                    return (
                        sessionService.getSelectedElectionEvent() &&
                        $state.current.name !== administrationBoardListName &&
                        $state.current.name !== electionEventListName
                    );
                };

                scope.showImport = function () {
                    return $state.current.name === electionEventListName;
                };

                scope.showPreconfigure = function () {
                    return $state.current.name === electionEventListName;
                };

                scope.toggleLeftMenu = function () {
					const target = angular.element(document.getElementById('main-wrapper'));
					target.toggleClass('sidebar-is-closed');
                };
            },
        };
    })
    .factory('ToolbarTitle', function () {
        'use strict';

        return function (sessionService) {
            this.title = function () {
                return sessionService.getSelectedElectionEvent().alias;
            };

            function _has(obj) {
                return angular.isDefined(obj) && obj !== null;
            }

            this.has = function () {
                return _has(sessionService.getSelectedElectionEvent());
            };
        };
    });
