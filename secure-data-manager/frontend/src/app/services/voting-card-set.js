/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
angular
	.module('votingCardSet', [])
	.service('votingCardSetService', function ($http, endpoints, $q) {
		'use strict';

		return {
			/**
			 * Change the status of a voting card set.
			 *
			 * @param {string} endpoint the voting card set endpoint URL (computing, downloaded).
			 * @param {object} votingCardSet the voting card set to modify.
			 * @param {object} options other properties needed for the status change, such as the PEM of the private key used for signing.
			 * @returns a promise, successful if the status was changed.
			 */
			changeStatus: function (endpoint, votingCardSet, options) {
				// Build the voting card set endpoint URL.
				const url =
					endpoints.host() +
					endpoint
						.replace('{electionEventId}', votingCardSet.electionEvent.id)
						.replace('{votingCardSetId}', votingCardSet.id);
				// Return a promise that will call the voting card set endpoint to change the status.
				if (options) {
					// Add the optional parameters to the request body.
					return $http.put(url, options).then(function (response) {
						return response.data;
					});
				} else {
					return $http.put(url).then(function (response) {
						return response.data;
					});
				}
			},

			/**
			 * Precompute the voting card sets.
			 *
			 * @param {string} electionEventId the election event id.
			 * @param {object} data request body.
			 * @returns a promise.
			 */
			precompute: function (electionEventId, data) {
				const url =
					endpoints.host() +
					endpoints.votingCardSetsPrecompute.replace('{electionEventId}', electionEventId);

				return $http.put(url, data);
			},

			/**
			 * Sign the voting card sets.
			 *
			 * @param {string} electionEventId the election event id.
			 * @param {object} data request body.
			 * @returns a promise.
			 */
			sign: function (electionEventId) {
				const url =
					endpoints.host() +
					endpoints.votingCardSetsSign.replace('{electionEventId}', electionEventId);

				return $http.put(url);
			}
		};
	});
