/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
/*jshint maxparams: 13 */

import dialogCustomProgressTemplateTemplate from '../dialogs/dialog-custom-progress-template.html';

export default angular
	.module('home-manage', [])
	.controller('home-manage', function (
		$rootScope,
		$scope,
		$state,
		$mdDialog,
		$mdToast,
		toastCustom,
		sessionService,
		$q,
		endpoints,
		$http,
		settler,
		gettextCatalog,
		ErrorsDict,
		entitiesCounterService,
		uploadZip
	) {
		'use strict';

		const refreshElectionEventsState = 'refresh-election-events';
		$scope.data = {};
		$scope.preconfigureInProgress = false;
		$scope.administrationBoardsList = {};

		// set state to be used in the HTML template
		$scope.state = $state.current.name;
		$rootScope.$on('$stateChangeSuccess', function (event, toState) {
			$scope.state = toState.name;
		});

		/*
		Launch the endpoint responsible for preconfiguring the election
		*/
		$scope.preconfigure = function () {

			$scope.preconfigureInProgress = true;

			$mdToast.show(
				toastCustom.topCenter(
					gettextCatalog.getString('Election preconfiguration started'),
					'success',
				),
			);

			$http.post(endpoints.host() + endpoints.preconfiguration)
				.then(function () {
					$rootScope.$broadcast(refreshElectionEventsState);
					$scope.listAdminBoards();
					entitiesCounterService.getTheMainItemsCount();
					$mdToast.show(
						toastCustom.topCenter(
							gettextCatalog.getString('Election preconfigured!'),
							'success',
						),
					);
					$scope.preconfigureInProgress = false;
				})
				.catch(function () {
					$rootScope.$broadcast(refreshElectionEventsState);
					$mdToast.show(
						toastCustom.topCenter(
							gettextCatalog.getString('Failed to preconfigure election. Please check input files.'),
							'error',
						),
					);
					$scope.preconfigureInProgress = false;
				});
		};

		$scope.import = function (input) {
			const url = endpoints.host() + endpoints.import;

			$scope.title = gettextCatalog.getString('Importing...');

			$mdDialog.show({
				scope: $scope,
				preserveScope: true,
				template: dialogCustomProgressTemplateTemplate,
				escapeToClose: false,
				parent: angular.element(document.body),
			});

			uploadZip.upload(url, input.files[0])
				.then(function (_res) {
					$mdDialog.hide();
					$mdToast.show(
						toastCustom.topCenter(
							gettextCatalog.getString('Data imported successfully'),
							'success',
						),
					);
				})
				.catch(function (e) {
					$mdDialog.hide();
					if (e.data.error === '4005') {
						$mdDialog.show(
							$mdDialog.customAlert({
								locals: {
									title: gettextCatalog.getString('Data has not been imported'),
									content: gettextCatalog.getString(ErrorsDict(e.data.error)),
								},
							}),
						);
					} else {
						$mdToast.show(
							toastCustom.topCenter(
								`${gettextCatalog.getString('Import')}: ${gettextCatalog.getString('Something went wrong. Contact with Support')}. ${gettextCatalog.getString('Error code')}: ${e.data.error}, ${gettextCatalog.getString(ErrorsDict(e.data.error))}`,
								'error'
							),
						);
					}
				})
				.finally(function () {
					$rootScope.$broadcast(refreshElectionEventsState);
					$scope.listAdminBoards();
					entitiesCounterService.getTheMainItemsCount();
				});
		};

		$scope.listAdminBoards = function () {
			$scope.errors = {};

			$http.get(endpoints.host() + endpoints.adminBoardList).then(
				function (response) {
					const data = response.data;
					try {
						sessionService.setAdminBoards(data.result);
						$scope.administrationBoardsList = data.result;
					} catch (e) {
						console.log(e);
						$scope.data.message = e.message;
						$scope.errors.administrationBoardsFailed = true;
					}
				},
				function () {
					$scope.errors.administrationBoardsFailed = true;
				},
			);
		};

		$scope.isPreconfigureEnabled = function () {
			return sessionService.isSetupMode();
		};

	});
