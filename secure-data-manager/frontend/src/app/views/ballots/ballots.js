/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */

/*jshint maxparams: 12 */

export default angular
  .module('ballots', [])
  .controller('ballots', function (
    $scope,
    $rootScope,
    $mdDialog,
    $mdToast,
    $sce,
    sessionService,
    endpoints,
    $http,
    $q,
    _,
    settler,
    CustomDialog,
    gettextCatalog,
    toastCustom,
    adminBoardActivationService
  ) {
    'use strict';

    const electionEventIdPattern = '{electionEventId}';
    const ballotIdPattern = '{ballotId}';

    const BALLOT_MSG_ID = 'Ballot';
    const BALLOT_SELECT_FIRST_MSG_ID = 'Please select first a Ballot';
    const BALLOT_UPDATING_MSG_ID = 'Ballot updating';
    const BALLOT_UPDATING_ERROR_MSG_ID = 'Some ballot(s) could not be updated. Please review the list';
    const BALLOT_UPDATED_MSG_ID = 'Ballot(s) updated!';
    const ADMIN_BOARD_ACTIVATE_MSG_ID = 'Please, activate the administration board';
    const ADMIN_BOARD_ACTIVATE_OK_MSG_ID = 'Activate now';
    const ADMIN_BOARD_WRONG_ACTIVATED_MSG_ID = 'Wrong Administration Board activated';
    const ADMIN_BOARD_WRONG_ACTIVATED_EXPLANATION_MSG_ID = 'The active Administration Board does not belong to the Election Event that you are' +
      ' trying to operate. Please deactivate it and activate the corresponding Administration Board for the Election Event';

    $scope.updateInProgress = false;

    $scope.selectBallotText = function () {
      return gettextCatalog.getString(BALLOT_SELECT_FIRST_MSG_ID);
    };
    $scope.listBallots = function () {
      $scope.errors.getBallotsFailed = false;

      const url =
        endpoints.host() +
        endpoints.ballots.replace(
          electionEventIdPattern,
          $scope.selectedElectionEventId,
        );
      $http.get(url).then(
        function (response) {
          const data = response.data;
          $rootScope.safeApply(function () {
            $scope.ballots = data;
          });
        },
        function () {
          $scope.errors.getBallotsFailed = true;
        },
      );
    };

    $scope.getBallotTexts = function (ballotid) {
      return _.find($scope.ballotTextsToView, function (text) {
        return ballotid === text[0]['ballot']['id'];
      });
    };

    const showBallotApprovalError = function () {
      $mdDialog.show(
        $mdDialog.customAlert({
          locals: {
            title: gettextCatalog.getString(BALLOT_UPDATING_MSG_ID),
            content: gettextCatalog.getString(BALLOT_UPDATING_ERROR_MSG_ID),
          },
        }),
      );
    };

    $scope.update = function (ev) {
      $scope.errors.ballotApprovalError = false;

      const ballotsToUpdate = [];
      $scope.ballots.result.forEach(function (ballot) {
        if (ballot.status === 'LOCKED') {
          ballotsToUpdate.push(ballot);
        }
      });

      if (ballotsToUpdate.length <= 0) {
        new CustomDialog()
          .title(gettextCatalog.getString(BALLOT_UPDATING_MSG_ID))
          .cannotPerform(gettextCatalog.getString(BALLOT_MSG_ID))
          .show();
        return;
      }

      if (!adminBoardActivationService.isActivated()) {
        const p = $mdDialog.show(
          $mdDialog.customConfirm({
            locals: {
              title: gettextCatalog.getString(BALLOT_UPDATING_MSG_ID),
              content: gettextCatalog.getString(ADMIN_BOARD_ACTIVATE_MSG_ID),
              ok: gettextCatalog.getString(ADMIN_BOARD_ACTIVATE_OK_MSG_ID),
            },
          }),
        );
        p.then(
          () => adminBoardActivationService.activate($scope, 'setup').then(() => $scope.update()),
          function (error) {
            //Not possible to open the $mdDialog
          },
        );
        return;
      } else {
        const electionEvent = sessionService.getSelectedElectionEvent();
        if (!sessionService.doesActivatedABBelongToSelectedEE()) {
          $mdDialog.show(
            $mdDialog.customAlert({
              locals: {
                title: gettextCatalog.getString(ADMIN_BOARD_WRONG_ACTIVATED_MSG_ID),
                content: `${gettextCatalog.getString(ADMIN_BOARD_WRONG_ACTIVATED_EXPLANATION_MSG_ID)} ${electionEvent.defaultTitle}.`
              },
            }),
          );
          return;
        }
      }

      $scope.ballotApprovalResults = [];
      $scope.ballotApprovalError = false;
      $scope.updateInProgress = true;

      $q.allSettled(
        ballotsToUpdate.map(function (ballot) {
          const url = (endpoints.host() + endpoints.ballotUpdate)
            .replace(electionEventIdPattern, $scope.selectedElectionEventId)
            .replace(ballotIdPattern, ballot.id);

          return $http.put(url);
        }),
      ).then(function (responses) {
        const settled = settler.settle(responses);
        $scope.ballotApprovalResults = settled.fulfilled;

        if (settled.ok) {
          $mdToast.show(
            toastCustom.topCenter(
              gettextCatalog.getString(BALLOT_UPDATED_MSG_ID),
              'success',
            ),
          );
          $scope.listBallots();
        }
        if (settled.error) {
          showBallotApprovalError();
        }

        $scope.updateInProgress = false;
      });

      $scope.unselectAll();
    };

    $scope.unselectAll = function () {
      $scope.selectAll = false;
      $scope.ballots.result.forEach(function (ballot) {
        ballot.selected = false;
      });
    };

    $scope.onSelectAll = function (value) {
      $scope.ballots.result.forEach(function (ballot) {
        ballot.selected = value;
      });
    };

    $scope.updateSelectAll = function (value) {
      if (!value) {
        $scope.selectAll = false;
      }
    };

    $scope.$on('refresh-ballots', function () {
      $scope.listBallots();
      $scope.selectAll = false;
    });

    $scope.capitalizeFirstLetter = function (string) {
      const lowerCaseString = string.toLowerCase();
      return lowerCaseString.charAt(0).toUpperCase() + lowerCaseString.slice(1);
    };

    $scope.linkedContests = function (arr) {
      return $sce.trustAsHtml(arr.map(e => e.defaultTitle).join('<br>'));
    }

    $scope.isSetupMode = function () {
      return sessionService.isSetupMode();
    };

    $scope.isThereLockedBallot = function () {
      let lockedBallot = false;

      if ($scope.ballots) {
        $scope.ballots.result.forEach(function (ballot) {
          if (ballot.status === 'LOCKED') {
            lockedBallot = true;
          }
        });
      }

      return lockedBallot;
    };

    //initialize && populate view
    // -------------------------------------------------------------
    $scope.alert = '';
    $scope.errors = {};
    $scope.selectedElectionEventId = sessionService.getSelectedElectionEvent().id;
    $scope.listBallots();
  });
