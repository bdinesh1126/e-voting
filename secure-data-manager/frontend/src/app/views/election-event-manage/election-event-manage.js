/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
/*jshint maxparams: 13 */

export default angular
	.module('election-event-manage', [])
	.controller('election-event-manage', function (
		$scope,
		$rootScope,
		sessionService,
		endpoints,
		$http,
		$state,
		$mdDialog,
		$q,
		gettextCatalog,
		settler,
		$mdToast,
		toastCustom,
		entitiesCounterService,
		adminBoardActivationService
	) {
		'use strict';

		// set state to be used in the HTML template
		$scope.state = $state.current.name;
		$rootScope.$on('$stateChangeSuccess', function (event, toState) {
			$scope.state = toState.name;
		});

		$scope.activateAdminBoard = function () {
			adminBoardActivationService.activate($scope, sessionService.isSetupMode() ? 'setup' : 'tally').then(() => null);
		};

		$scope.deactivateAdminBoard = function () {
			adminBoardActivationService.deactivate();
		};

		$scope.isAdminBoardActivated = function () {
			return adminBoardActivationService.isActivated();
		};

		$scope.isOnline = function () {
			return sessionService.isOnlineMode();
		};

		$scope.isSetupMode = function () {
			return sessionService.isSetupMode();
		};

		$scope.isTallyMode = function () {
			return sessionService.isTallyMode();
		};

		$scope.isPreconfigureEnabled = function () {
			return sessionService.isSetupMode();
		};
	});
