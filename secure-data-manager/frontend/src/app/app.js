/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
/* global require */

angular
	.module('app', [
		'lodash',
		'routes',
		'ngMaterial',
		'ngPromiseExtras',
		'settler',
		'gettext',
		'ngFileUpload',
		'oc.lazyLoad',

		// filters
		'filters',

		// directives
		'svSidebar',
		'ovToolbar',
		'ovFolderInputDirective',
		'app.errors.dict',
		'datatableData',
		'ui.bootstrap.collapse',
		'tabBar',

		// services
		'app.sessionService',
		'endpoints',
		'databaseService',
		'app.dialogs',
		'i18n',
		'uploadZip',
		'statusBox',
		'toastCustom',
		'dialogsCustom',
		'votingCardSet',
		'adminBoardActivation',
		'entitiesCounterService',
		'configElectionConstants',
		'activeFilters',

		// controllers (not lazy loaded)
			'splash',
			'boardMembersPasswords'
	])

	.config(function ($compileProvider, $provide) {
		'use strict';
		$provide.constant('$MD_THEME_CSS', '');
	})

	.run(function (
		$rootScope,
		$state,
		databaseService,
		sessionService,
		i18n,
		gettextCatalog,
		$mdDialog,
		$mdToast,
		toastCustom,
		$http,
		endpoints,
	) {
		'use strict';

		// safe apply

		$rootScope.safeApply = function (fn) {
			const phase = this.$root.$$phase;
			if (phase === '$apply' || phase === '$digest') {
				if (fn && typeof fn === 'function') {
					fn();
				}
			} else {
				this.$apply(fn);
			}
		};

		// ----------------------------------------------------------------------------------
		// language management

		$rootScope.getLanguageUrl = function (code) {
			return endpoints.host() + endpoints.languages + code;
		}

		$rootScope.switchLanguage = function (lang) {
			$rootScope.lang = lang.code;

			$rootScope.langAttr = $rootScope.transformLanguageCode(lang.code, true);

			gettextCatalog.setCurrentLanguage(lang.code);
			if (lang.code !== 'xx') {
				// 'xx' is the default (pseudo english) base lang
				//gettextCatalog.loadRemote('assets/lang/' + lang.code + '.json');
				const langUrl = $rootScope.getLanguageUrl(lang.code);
				gettextCatalog.loadRemote(langUrl);
			}
		};

		const setLanguages = function (defaultLanguage, languages) {
			$rootScope.i18n = {
				languages: languages,
				lang: {
					code: defaultLanguage,
				},
			};
		};
		$rootScope.transformLanguageCode = function (code, countryFormat) {
			// specify countryFormat to true if you want to add the country code. e.g. 'en-US'
			// set to false if you just need the lang code. e.g. 'en'
			try {
				let langCode;
				if (countryFormat) {
					langCode = code.replace('_', '-');
				} else {
					langCode = code.split('_')[0];
				}
				return langCode;
			} catch (e) {
				return '';
			}
		};

		//-----------------------------------------------------------------------------------
		// user config

		$rootScope.getConfig = function () {
			$http.get(endpoints.host() + endpoints.sdmConfig).then(function (response) {
				const data = response.data;
				setLanguages(data.config.i18n.default, data.config.i18n.languages);
				$rootScope.switchLanguage($rootScope.i18n.lang);

				// we go to the default page once the promise is a success
				// this is to prevent view blinking
				$state.go('election-event-list');
			});
		};

		// ----------------------------------------------------------------------------------
		// startup

		function formatDate(date) {
			function pad(num, size) {
				const s = '000000000' + num;
				return s.substr(s.length - size);
			}

			return `${pad(date.getUTCHours(), 2)}:${pad(date.getUTCMinutes(), 2)}:${pad(date.getUTCSeconds(), 2)}`;
		}

		$rootScope.log = function (data) {
			const ts = formatDate(new Date());
			console.log(`${ts};${data}`);
		};

		$rootScope.isSync = function () {
			return sessionService.isSync();
		};
	});
